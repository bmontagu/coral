.PHONY:	build check clean test test-short test-cfa-nabla-rec-sequential-time test-cfa-nabla-sequential-time fmt bench pdf

build:
	dune build @install
	dune build --profile=release src/cfa/demo/cfa.bc.js
	dune build --profile=release src/simpl/demo/stableRels.bc.js

check:
	dune build @check

clean:
	dune clean

test:
	dune runtest --profile=release --auto-promote --display=short

test-short:
	dune runtest --profile=release --auto-promote src/cfa/analyzers/ src/cfa/functionGraph

test-cfa-nabla-sequential-time:
	dune runtest --profile=release
	rm -f _build/default/src/cfa/examples/*.out-nabla
	time dune runtest --profile=release -j 1 --force src/cfa/examples/

test-cfa-nabla-rec-sequential-time:
	dune runtest --profile=release
	rm -f _build/default/src/cfa/examples/*.out-nabla-rec*
	time dune runtest --profile=release -j 1 --force src/cfa/examples/

fmt:
	dune build @fmt --auto-promote

bench:
	dune exec cfa-generate-bench src/cfa/examples/ > src/cfa/examples/cfa_bench_table.tex

pdf:
	( cd src/cfa/examples; pdflatex bench.tex; cd ../../.. )
