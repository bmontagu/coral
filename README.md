Description:
------------

This project contains several implementations of static analyzers for
small higher-order languages.

- The first analyzer is a compositional relational analyzer for the
  simply-typed λ-calculus with sums and products with non-recursive
  types. It is based on the ICFP'20 paper entitled *"Stable Relations
  and Abstract Interpretation of Higher-Order Programs"*.

  The source code is located in ``src/simpl/``, and uses the abstract
  domain of correlations whose source code is located in
  ``src/coral/``. To run this analyzer, type the following command:

        dune exec -- coral-simpl [OPTIONS] FILE

  The command provides a ``--help`` flag that describes the possible options.
  The directory ``src/simpl/examples`` contains example files.

  A self-contained web demo is also available by opening the
  ``src/simpl/demo/stable-rels.html`` file with your web browser.

- The second analyzer implements a family of control-flow analyzers
  (CFA) for the untyped λ-calculus with sums and products. It is based
  on the PLDI'21 paper entitled *"Trace-Based Control-Flow Analysis"*.
  Some analyzers are experimental work in progress.

  The source code is located in ``src/cfa/``. To run this analyzer,
  type the following command:

        dune exec -- cfa-simpl [OPTIONS] FILE

  The command provides a ``--help`` flag that describes the possible options.
  The directory ``src/cfa/examples`` contains example files.

  A self-contained web demo is also available by opening the
  ``src/cfa/demo/cfa.html`` file with your web browser.

Build requirements:
-------------------

- OCaml
- dune
- menhir
- cmdliner
- inferno
- ppx_deriving
- dmap
- function_graphs
  (using ``opam pin add git+https://gitlab.inria.fr/bmontagu/function_graphs.git``)

Build instructions:
-------------------

Type the command ``dune build`` or simply type ``make``

To run tests, type ``make test``
