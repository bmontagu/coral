type 'a hash_consed = 'a Hashcons.hash_consed

let[@inline always] node x = x.Hashcons.node
let[@inline always] tag x = x.Hashcons.tag

(* the functor is made generative on purpose *)
module Make (H : Hashtbl.HashedType) () = Hashcons.Make (H)
