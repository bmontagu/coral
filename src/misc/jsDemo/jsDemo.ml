open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
open Ezjs_ace

module Utils = struct
  let set_color elt color =
    elt##.style##.color := (CSS.Color.js color :> Js.js_string Js.t)

  let array_find_first p a =
    let n = Array.length a in
    let rec search i =
      if i >= n then None else if p i a.(i) then Some i else search (i + 1)
    in
    search 0

  let rec make_list_aux min max f acc =
    if max < min
    then acc
    else
      let acc' =
        match f max with
        | Some v -> v :: acc
        | None -> acc
      in
      make_list_aux min (max - 1) f acc'

  let make_list min max f = make_list_aux min max f []

  let make_select ~name ~min ~max ~of_enum ~to_string ~default =
    assert (min <= default);
    assert (default <= max);
    let open Html in
    let options =
      let make_option i =
        Option.map (fun k -> option (txt @@ to_string k)) (of_enum i)
      in
      make_list min max make_option
    in
    let item = Html.select options in
    (To_dom.of_select item)##.selectedIndex := default;
    let get () =
      let idx = (To_dom.of_select item)##.selectedIndex in
      Option.get @@ of_enum idx
    and modify_onchange handler =
      (To_dom.of_select item)##.onchange
      := Dom.handler (fun x ->
             handler x;
             Js._false)
    in
    (div [label [txt name]; space (); item], get, modify_onchange)

  let make_checkbox ~name ~checked =
    let open Html in
    let item =
      let a = [a_input_type `Checkbox] in
      let a = if checked then a_checked () :: a else a in
      input ~a ()
    in
    let get () = Js.to_bool @@ (To_dom.of_input item)##.checked in
    let modify_onchange handler =
      (To_dom.of_input item)##.onchange
      := Dom.handler (fun x ->
             handler x;
             Js._false)
    in
    (div [item; space (); label [txt name]], get, modify_onchange)
end

module type OPTIONS = sig
  module Examples : sig
    val collection : (string * string) array
    val default : string
    val label : string
  end

  module Color : sig
    val normal_text : Js_of_ocaml.CSS.Color.t
    val error_text : Js_of_ocaml.CSS.Color.t
    val disabled_text : Js_of_ocaml.CSS.Color.t
  end

  module Button : sig
    val label : string
    val dom_id : string
    val dom_class : string list
  end

  module Output : sig
    val dom_id : string
    val dom_class : string list
    val label : string
    val font_size : int
  end

  module Editor : sig
    val dom_id : string
    val dom_class : string list
    val tab_size : int
    val theme : string
    val mode : string
    val font_size : int
  end

  module OptionsDialog : sig
    type value

    val html_elt : [> Html_types.fieldset ] Js_of_ocaml_tyxml.Tyxml_js.Html.elt
    val get_value : unit -> value

    val set_onchange :
      (Js_of_ocaml.Dom_html.event Js_of_ocaml__.Js.t -> unit) -> unit
  end

  val analyzer :
    OptionsDialog.value ->
    string ->
    ( string * (Ezjs_ace.Ace.loc * string) list,
      Ezjs_ace.Ace.loc * string )
    result
end

module Make (Options : OPTIONS) = struct
  let default_example =
    Option.get
    @@ Utils.array_find_first
         (fun _i (file, _contents) ->
           String.equal file Options.Examples.default)
         Options.Examples.collection

  let example_selector =
    let name = Options.Examples.label
    and min = 0
    and max = Array.length Options.Examples.collection - 1
    and of_enum i =
      if 0 <= i && i < Array.length Options.Examples.collection
      then Some Options.Examples.collection.(i)
      else None
    and to_string (file, _contents) = file in
    Utils.make_select
      ~name
      ~min
      ~max
      ~of_enum
      ~to_string
      ~default:default_example

  let make_analyze_button editor output_area f =
    let open Html in
    let js_output_area = To_dom.of_textarea output_area in
    let callback self _attr =
      self##.disabled := Js._true;
      Ace.clear_marks editor;
      begin
        match f (Ace.get_contents editor) with
        | Ok (res, warnings) ->
          Utils.set_color js_output_area Options.Color.normal_text;
          js_output_area##.value := Js.string res;
          List.iter
            (fun (loc, msg) -> Ace.set_mark editor ~loc ~type_:Ace.Warning msg)
            warnings
        | Error (loc, msg) ->
          Utils.set_color js_output_area Options.Color.error_text;
          Ace.set_mark editor ~loc ~type_:Ace.Error msg;
          js_output_area##.value := Js.string msg
      end;
      self##.disabled := Js._false;
      Js._false
    in
    let b =
      button
        ~a:
          [
            a_button_type `Submit;
            a_class Options.Button.dom_class;
            a_id Options.Button.dom_id;
          ]
        [txt Options.Button.label]
    in
    let () =
      let self = To_dom.of_button b in
      self##.onclick := Dom.handler (callback self)
    in
    b

  let dummy_loc =
    let p = (-1, -1) in
    Ezjs_ace.Ace.{ loc_start = p; loc_end = p }

  let analyzer_handler get_options input =
    match Options.analyzer (get_options ()) input with
    | res -> res
    | exception exn ->
      let exc = Printexc.to_string exn
      and trace = Printexc.get_backtrace () in
      let msg = String.concat "\n" ["Anomaly: please report."; exc; trace] in
      Error (dummy_loc, msg)

  let contents, editor =
    let open Html in
    let div_input, (editor : unit Ace.editor) =
      let elt =
        div
          ~a:[a_id Options.Editor.dom_id; a_class Options.Editor.dom_class]
          [txt @@ snd @@ Options.Examples.collection.(default_example)]
      in
      (elt, Ace.create_editor (To_dom.of_div elt))
    in
    let output_area =
      textarea
        ~a:
          [
            a_id Options.Output.dom_id;
            a_class Options.Output.dom_class;
            a_cols 80;
            a_rows 10;
            a_spellcheck false;
            a_readonly ();
            a_style
              ("font-size:" ^ string_of_int Options.Output.font_size ^ "px");
          ]
        (txt "")
    in
    let set_output_to_gray () =
      Utils.set_color
        (To_dom.of_textarea output_area)
        Options.Color.disabled_text
    in
    Options.OptionsDialog.set_onchange (fun _ -> set_output_to_gray ());
    let examples, get_examples, modify_onchange_examples = example_selector in
    modify_onchange_examples (fun _ ->
        Ace.set_contents editor (snd @@ get_examples ());
        set_output_to_gray ());
    Ace.record_event_handler editor "change" set_output_to_gray;
    ( div
      @@ [
           Options.OptionsDialog.html_elt;
           br ();
           div
             [
               examples;
               div_input;
               br ();
               make_analyze_button
                 editor
                 output_area
                 (analyzer_handler Options.OptionsDialog.get_value);
             ];
           br ();
           div [txt Options.Output.label; br (); output_area];
         ],
      editor )

  let setup () =
    Ace.set_font_size editor Options.Editor.font_size;
    Ace.set_tab_size editor Options.Editor.tab_size;
    Ace.set_theme editor Options.Editor.theme;
    Ace.set_mode editor Options.Editor.mode;
    Register.body [contents]
end
