module Utils : sig
  val make_select :
    name:string ->
    min:int ->
    max:int ->
    of_enum:(int -> 'a option) ->
    to_string:('a -> string) ->
    default:int ->
    [> Html_types.div ] Js_of_ocaml_tyxml.Tyxml_js.Html.elt
    * (unit -> 'a)
    * ((Js_of_ocaml.Dom_html.event Js_of_ocaml.Js.t -> unit) -> unit)
  (** [make_select ~name ~min ~max ~of_enum ~to_string ~default]
     creates an HTML element of type 'select', with [max - min + 1]
     elements and default value [default]. [default] must be a valid
     value, i.e. must satisfy [min <= default <= max]. [of_enum i]
     must not return [None] for any value of [i] such that [min <= i
     <= max]. [to_string] is used to associate a string for each
     value, that is printed in the selector. [name] is used as a
     description label for the selector. [make_select] also returns
     two functions: a getter, that returns the currently selected
     value, and a function that sets the "onchange" method of the
     selector. *)

  val make_checkbox :
    name:string ->
    checked:bool ->
    [> Html_types.div ] Js_of_ocaml_tyxml.Tyxml_js.Html.elt
    * (unit -> bool)
    * ((Js_of_ocaml.Dom_html.event Js_of_ocaml.Js.t -> unit) -> unit)
  (** [make_checkbox ~name ~checked] creates an HTML element of type
     'checkbox', that is initially checked iff [checked = true].
     [name] is used as a description label for the selector.
     [make_checkbox] also returns two functions: a getter, that
     returns the current check status, and a function that sets the
     "onchange" method of the checkbox. *)
end

module type OPTIONS = sig
  module Examples : sig
    val collection : (string * string) array
    val default : string
    val label : string
  end

  module Color : sig
    val normal_text : Js_of_ocaml.CSS.Color.t
    val error_text : Js_of_ocaml.CSS.Color.t
    val disabled_text : Js_of_ocaml.CSS.Color.t
  end

  module Button : sig
    val label : string
    val dom_id : string
    val dom_class : string list
  end

  module Output : sig
    val dom_id : string
    val dom_class : string list
    val label : string
    val font_size : int
  end

  module Editor : sig
    val dom_id : string
    val dom_class : string list
    val tab_size : int
    val theme : string
    val mode : string
    val font_size : int
  end

  module OptionsDialog : sig
    type value

    val html_elt : [> Html_types.fieldset ] Js_of_ocaml_tyxml.Tyxml_js.Html.elt
    val get_value : unit -> value

    val set_onchange :
      (Js_of_ocaml.Dom_html.event Js_of_ocaml.Js.t -> unit) -> unit
  end

  val analyzer :
    OptionsDialog.value ->
    string ->
    ( string * (Ezjs_ace.Ace.loc * string) list,
      Ezjs_ace.Ace.loc * string )
    result
  (** [analyzer options s] runs the analyzer on the string s
       (presumably representing the source of a program), and returns
       a string output along with a list of localized warning
       messages, or a localized error message. *)
end

module Make (_ : OPTIONS) : sig
  val setup : unit -> unit
  (** [setup ()] initializes the editor environment, and fills in the HTML page *)
end
