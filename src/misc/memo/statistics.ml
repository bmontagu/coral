(** Statistics printed by the fixpoint solver modules *)

type 'a t = {
  entries: 'a Seq.t;
      (** the sequence of entries that were recorded in the memoization
     table *)
  states: int;  (** number of explored states (nodes in the call graph) *)
  edges: int;  (** number of explored edges (edges in the call graph) *)
  max_fanout: int;
      (** maximum fanout degree of states (indicates large disjunctions) *)
  med_fanout: int;  (** median fanout degree *)
  iterations: int;  (** number of iterations performed to reach a fixpoint *)
  calls: int;  (** how many times the functional has been called *)
}

(** Zero-filled statistics *)
let zero =
  {
    entries = Seq.empty;
    states = 0;
    edges = 0;
    max_fanout = 0;
    med_fanout = 0;
    iterations = 0;
    calls = 0;
  }

(** Pretty-printer *)
let pp ~print_memo_table pp_entry fmt
    { entries; states; edges; max_fanout; med_fanout; iterations; calls } =
  if print_memo_table
  then
    Format.fprintf
      fmt
      "entries:@.@[%a@]@."
      (Format.pp_print_seq
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@.")
         pp_entry)
      entries;
  Format.fprintf
    fmt
    "states: %i; edges: %i; max fanout (median): %i (%i); iterations: %i; \
     calls: %i"
    states
    edges
    max_fanout
    med_fanout
    iterations
    calls
