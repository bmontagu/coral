(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

(** Priorities *)
module Priority : sig
  type t
  (** The type of priorities *)

  val leq : t -> t -> bool
  (** Total order on priorities *)

  val create_gensym : unit -> unit -> t
  (** [create_gensym ()] returns a function that produces fresh,
        decreasing priorities *)
end = struct
  type t = int

  let leq (x : int) (y : int) = x >= y

  let create_gensym () =
    let r = ref 0 in
    let next () =
      let n = !r in
      incr r;
      n
    in
    next
end

(** The [NoDup] functor modifies a heap data-structure, so that you
    cannot extract more than one value of a given priority from that
    heap. *)
module NoDup (H : Heaps.HEAP_WITH_INFO) :
  Heaps.HEAP_WITH_INFO with type Priority.t = H.Priority.t = struct
  module Priority = H.Priority

  type 'a t = 'a H.t

  let empty = H.empty
  let is_empty = H.is_empty
  let insert = H.insert
  let find_min = H.find_min

  let rec delete_min h =
    let p, _, h' = H.pop_min h in
    if (not @@ H.is_empty h') && Priority.leq (fst @@ H.find_min h') p
    then delete_min h'
    else h'

  let rec pop_min h =
    let ((p, _m, h') as res) = H.pop_min h in
    if (not @@ H.is_empty h') && Priority.leq (fst @@ H.find_min h') p
    then pop_min h'
    else res

  let merge = H.merge
end

(** Imperative priority queues *)
module Q : sig
  type 'a t
  (** The type of priority queues. They have the property that at most
      one element of a given priority can be extracted from the
      queue. *)

  exception Empty
  (** Exception that is raised when the queue is empty *)

  val create : unit -> 'a t
  (** Creates a fresh, empty queue *)

  val insert : 'a t -> Priority.t -> 'a -> unit
  (** [insert q p v] Inserts in the queue [q] the value [v] with the
      priority [p]. If an element with priority [p] was already
      present, the queue remains the same. *)

  val find_min : 'a t -> Priority.t * 'a
  (** [find_min q] returns the a value and its priority that is
      present in [q], such that this priority is minimal. If [q] was
      empty, then the [Empty] exception is raised. *)

  val delete_min : 'a t -> unit
  (** [delete_min q] removes the value with minimal priority from [q].
      If [q] was empty, nothing happens. *)
end = struct
  open Heaps.WithInfo
  module H = NoDup (Leftist (Priority))

  exception Empty = Heaps.Empty

  type 'a t = 'a H.t ref

  let create () = ref H.empty
  let insert r p x = r := H.insert p x !r
  let find_min r = H.find_min !r
  let delete_min r = try r := H.delete_min !r with Empty -> ()
end

(** Functor to compute post-fixpoints of recursively defined functions
    with values in a semi-lattice equipped with widening. The goal is
    to compute a function [f] of type [X.t -> D.t], that is a
    post-fixpoint of a functional of type [(X.t -> D.t) -> (X.t ->
    D.t)]. [D] must be equipped with a widening. It is necessary that
    the functional is called on a finite number of values of [X], so
    that the fixpoint solver terminates.

   This implementation is inspired by the "terminating structured"
   local solvers TSTP and TSMP, and avoids unnecessary recomputations
   by maintaining a dependency graph. The choice of which point to
   (re-)compute is based on priorities.

   Schulze Frielinghaus, S.; Seidl, H. & Vogler, R.
   Enforcing termination of interprocedural analysis
   Formal Methods in System Design, Springer Science and Business Media LLC, 2017, 53, 313-338
 *)
module Make
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : MAP with type key = X.t)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false) f =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and priority =
      (* priorities, i.e. creation time for nodes *)
      ref M.empty
    and time =
      (* timestamp, i.e. how many calls to [f] have been performed *) ref 0
    in
    let get x =
      match M.find_opt x !sigma with
      | Some (y, _) -> y
      | None -> assert false
    and set x y =
      sigma :=
        M.update
          x
          (function
            | None -> Some (y, 1)
            | Some (_, n) -> Some (y, n + 1))
          !sigma
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and mark_unwiden y = wpoint := Edges.Nodes.remove y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y
    and get_infl x = Edges.get infl x
    and get_prio x = Option.get @@ M.find_opt x !priority
    and set_prio x p = priority := M.update x (fun _ -> Some p) !priority in

    let next_prio = Priority.create_gensym () in
    let q = Q.create () in
    let rec iterate n =
      match Q.find_min q with
      | exception Q.Empty -> ()
      | p, y ->
        if Priority.leq p n
        then begin
          Q.delete_min q;
          do_var y;
          iterate n
        end
    and solve y =
      match M.find_opt y !sigma with
      | Some _ -> ()
      | None ->
        set y D.bot;
        let p = next_prio () in
        set_prio y p;
        init_infl y;
        do_var y;
        iterate p
    and eval y z =
      solve z;
      if Priority.leq (get_prio y) (get_prio z) then mark_widen z;
      add_infl z y;
      Edges.record edges y z;
      get z
    and do_var y =
      let is_point = is_widen y in
      mark_unwiden y;
      incr time;
      let tmp = f (eval y) y in
      let v = get y in
      let tmp = if is_point then D.widen v tmp else tmp in
      if not @@ D.leq tmp v
      then begin
        set y tmp;
        Edges.Nodes.iter (fun z -> Q.insert q (get_prio z) z) (get_infl y);
        init_infl y
      end
    in
    fun x ->
      solve x;
      let result = get x in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            Seq.map (fun (x, (y, _)) -> (x, y)) @@ M.to_seq !sigma;
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions
   with values in a semi-lattice equipped with widening. The goal is
   to compute a function [f] of type [X.t -> D.t], that is a
   post-fixpoint of a functional of type [(bool -> X.t -> D.t) ->
   (bool -> X.t -> D.t)]. Both [X] and [D] must be equipped with a
   widening. The boolean argument indicates whether the fixpoint
   solver should use widening on [X].

   This implementation is inspired by the "terminating structured"
   local solvers TSTP and TSMP, and avoids unnecessary recomputations
   by maintaining a dependency graph. The choice of which point to
   (re-)compute is based on priorities.

   Schulze Frielinghaus, S.; Seidl, H. & Vogler, R.
   Enforcing termination of interprocedural analysis
   Formal Methods in System Design, Springer Science and Business Media LLC, 2017, 53, 313-338
 *)
module MakeWithInputWidening
    (Log : LOG)
    (X : sig
      include WITH_COMPARE

      val widen : t -> t -> t
    end)
    (M : MAP2 with type key = X.t)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((bool -> X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false) f =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and priority =
      (* priorities, i.e. creation time for nodes *)
      ref M.empty
    and time =
      (* timestamp, i.e. how many calls to [f] have been performed *) ref 0
    in
    let get x =
      match M.find_opt x !sigma with
      | Some (y, _, _) -> y
      | None -> assert false
    and set x y time =
      sigma :=
        M.update
          x
          (function
            | None -> Some (y, time, 1)
            | Some (_, t, n) -> Some (y, t, n + 1))
          !sigma
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and mark_unwiden y = wpoint := Edges.Nodes.remove y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y
    and get_infl x = Edges.get infl x
    and get_prio x = Option.get @@ M.find_opt x !priority
    and set_prio x p = priority := M.update x (fun _ -> Some p) !priority in

    let next_prio = Priority.create_gensym () in
    let q = Q.create () in
    let rec iterate n =
      match Q.find_min q with
      | exception Q.Empty -> ()
      | p, y ->
        if Priority.leq p n
        then begin
          Q.delete_min q;
          do_var y;
          iterate n
        end
    and solve y =
      match M.find_opt y !sigma with
      | Some _ -> ()
      | None ->
        set y D.bot !time;
        let p = next_prio () in
        set_prio y p;
        init_infl y;
        do_var y;
        iterate p
    and eval y widen z =
      let z =
        if widen
        then
          match
            M.fold_similar
              (fun z' (_, time, _) acc ->
                match acc with
                | None -> Some (z', time)
                | Some (_z_acc, youngest) as acc ->
                  if time > youngest then Some (z', time) else acc)
              z
              !sigma
              None
          with
          | Some (z', _) ->
            let new_ = X.widen z' z in
            if X.compare new_ z <> 0
            then begin
              add_infl new_ y;
              Edges.record edges y new_
            end;
            new_
          | None -> z
        else z
      in
      solve z;
      if Priority.leq (get_prio y) (get_prio z) then mark_widen z;
      add_infl z y;
      Edges.record edges y z;
      get z
    and do_var y =
      let is_point = is_widen y in
      mark_unwiden y;
      incr time;
      let tmp = f (eval y) y in
      let v = get y in
      let tmp = if is_point then D.widen v tmp else tmp in
      if not @@ D.leq tmp v
      then begin
        set y tmp !time;
        Edges.Nodes.iter (fun z -> Q.insert q (get_prio z) z) (get_infl y);
        init_infl y
      end
    in
    fun x ->
      solve x;
      let result = get x in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, _, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            !sigma |> M.to_seq |> Seq.map (fun (x, (v, _, _)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions
    with values in a semi-lattice for function graphs equipped with
    widening. The goal is to compute a function [f] of type

    [X.t -> MFG.dom -> MFG.codom],

    that is a post-fixpoint of a functional of type

    [(X.t -> MFG.dom -> MFG.codom) -> (X.t -> MFG.dom -> MFG.codom)].

    [MFG] must be equipped with a widening. The functional has to be
    called on a finite number of elements of type [X.t].

   This implementation is inspired by the "terminating structured"
   local solvers TSTP and TSMP, and avoids unnecessary recomputations
   by maintaining a dependency graph. The choice of which point to
   (re-)compute is based on priorities.

   Schulze Frielinghaus, S.; Seidl, H. & Vogler, R.
   Enforcing termination of interprocedural analysis
   Formal Methods in System Design, Springer Science and Business Media LLC, 2017, 53, 313-338
 *)
module MakeMFG
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : MAP with type key = X.t)
    (MFG : sig
      type t
      type dom
      type codom

      val leq : t -> t -> bool
      val init : dom -> t
      val apply : t -> dom -> codom
      val update : (dom -> codom) -> t -> t
      val widen : int -> t -> t -> t
    end) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) ->
    X.t ->
    MFG.dom ->
    MFG.codom * (X.t * MFG.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false)
      (f : (X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) :
      X.t -> MFG.dom -> MFG.codom * (X.t * MFG.t) Statistics.t =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and priority =
      (* priorities, i.e. creation time for nodes *)
      ref M.empty
    and time =
      (* timestamp, i.e. how many calls to [f] have been performed *) ref 0
    in
    let get x =
      match M.find_opt x !sigma with
      | Some ((_y, _n) as p) -> p
      | None -> assert false
    and set n x y = sigma := M.update x (fun _ -> Some (y, n)) !sigma
    and set_iteration n x =
      sigma :=
        M.update
          x
          (function
            | None -> assert false
            | Some (y, _) -> Some (y, n))
          !sigma
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and mark_unwiden y = wpoint := Edges.Nodes.remove y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y
    and get_infl x = Edges.get infl x
    and get_prio x = Option.get @@ M.find_opt x !priority
    and set_prio x p = priority := M.update x (fun _ -> Some p) !priority in

    let next_prio = Priority.create_gensym () in
    let q = Q.create () in
    let rec iterate n =
      match Q.find_min q with
      | exception Q.Empty -> ()
      | p, y ->
        if Priority.leq p n
        then begin
          Q.delete_min q;
          do_var y;
          iterate n
        end
    and solve y y1 =
      match M.find_opt y !sigma with
      | Some (d, n) ->
        let d' = MFG.widen n d (MFG.init y1) in
        if not @@ MFG.leq d' d
        then begin
          set n y d';
          let p = get_prio y in
          Edges.Nodes.iter (fun z -> Q.insert q (get_prio z) z) (get_infl y);
          init_infl y;
          do_var y;
          iterate p
        end
      | None ->
        set 1 y (MFG.init y1);
        let p = next_prio () in
        set_prio y p;
        init_infl y;
        do_var y;
        iterate p
    and eval y z z1 =
      solve z z1;
      if Priority.leq (get_prio y) (get_prio z) then mark_widen z;
      add_infl z y;
      Edges.record edges y z;
      MFG.apply (fst @@ get z) z1
    and do_var y =
      let is_point = is_widen y in
      mark_unwiden y;
      let v, n = get y in
      let tmp =
        MFG.update
          (fun z ->
            incr time;
            set_iteration (n + 1) y;
            f (eval y) y z)
          v
      in
      let v, _n = get y in
      let tmp = if is_point then MFG.widen n v tmp else tmp in
      if not @@ MFG.leq tmp v
      then begin
        set (n + 1) y tmp;
        Edges.Nodes.iter (fun z -> Q.insert q (get_prio z) z) (get_infl y);
        init_infl y
      end
    in
    fun x z1 ->
      solve x z1;
      let result = MFG.apply (fst @@ get x) z1 in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            Seq.map (fun (x, (d, _n)) -> (x, d)) @@ M.to_seq !sigma;
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end
