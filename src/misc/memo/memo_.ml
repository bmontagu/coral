(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

open Sigs

(** Functor to create memoizing functions *)
module Make
    (Log : LOG)
    (X : WITH_PP)
    (M : sig
      include MAP with type key = X.t
      include WITH_PP1 with type 'a t := 'a t
    end)
    (V : WITH_PP) : sig
  val memo : (X.t -> V.t) -> X.t -> V.t

  val fix :
    ?transient:bool -> ?debug:bool -> ((X.t -> V.t) -> X.t -> V.t) -> X.t -> V.t
end = struct
  (** Inititializes the memoization table *)
  let init () = ref M.empty

  (** Empties the memoization table *)
  let reset r = r := M.empty

  (** Gets a value from the cache *)
  let get cache t = M.find_opt t !cache

  (** Updates a value in the cache: requires that a value should be
     already present *)
  let update cache t c =
    cache :=
      M.update
        t
        (function
          | Some _ -> assert false
          | None -> Some c)
        !cache

  (** [memo f] memoizes the [(v, f v)] pairs in a table, so that
     subsequent calls to the memoized [f] are faster *)
  let memo f =
    let cache = init () in
    let g x =
      match get cache x with
      | Some y -> y
      | None ->
        let y = f x in
        update cache x y;
        y
    in
    g

  (** Pretty-printer for the cache *)
  let pp_cache fmt m = M.pp V.pp fmt m

  (** [fix ~transient ~debug f] memoizes the functional [f] so that
     intermediate results in the computation of the fixpoint of [f]
     are recorded in a table for faster computation. This function is
     meant to exploit memoization when defining a recursive function.
     If [transient = true], then the table persists, and is also used
     to memoize subsequent calls to the fixpoint. This means that if
     [transient = false], then the cache is not shared between
     different calls to the fixpoint (the cache is emptied at the end
     of each call). If [debug = true], then the contents of the cache
     is printed at the end of the fixpoint computation. *)
  let fix ?(transient = true) ?(debug = false) f =
    let cache = init () in
    let rec self x =
      match get cache x with
      | Some y -> y
      | None ->
        let y = f self x in
        update cache x y;
        y
    in
    fun x ->
      let res = self x in
      if debug
      then
        Log.eprintf
          "Elements in cache:@.@[%a@]@.Total elements in cache: %i@."
          pp_cache
          !cache
          (M.cardinal !cache);
      if not transient then reset cache;
      res
end

(** Functor to create memoizing functions for dependently-typed functions *)
module MakeDep
    (Log : LOG)
    (X : sig
      type 'a t
    end)
    (M : DMAP with type 'a key = 'a X.t) : sig
  val memo : ('a X.t -> 'a) -> 'a X.t -> 'a

  type poly = { fun_: 'a. 'a X.t -> 'a } [@@unboxed]

  val fix : ?transient:bool -> ?debug:bool -> (poly -> poly) -> poly
end = struct
  (** Inititializes the memoization table *)
  let init () = ref M.empty

  (** Empties the memoization table *)
  let reset r = r := M.empty

  (** Gets a value from the cache *)
  let get cache t = M.find_opt t !cache

  (** Updates a value in the cache: requires that a value should be
     already present *)
  let update cache t c =
    cache :=
      M.update
        t
        (function
          | Some _ -> assert false
          | None -> Some c)
        !cache

  (** [memo f] memoizes the [(v, f v)] pairs in a table, so that
     subsequent calls to the memoized [f] are faster *)
  let memo f =
    let cache = init () in
    let g x =
      match get cache x with
      | Some y -> y
      | None ->
        let y = f x in
        update cache x y;
        y
    in
    g

  (** Pretty-printer for the cache *)
  let pp_cache fmt m = M.pp fmt m

  type poly = { fun_: 'a. 'a X.t -> 'a } [@@unboxed]

  (** [fix ~transient ~debug f] memoizes the functional [f] so that
     intermediate results in the computation of the fixpoint of [f]
     are recorded in a table for faster computation. This function is
     meant to exploit memoization when defining a recursive function.
     If [transient = true], then the table persists, and is also used
     to memoize subsequent calls to the fixpoint. This means that if
     [transient = false], then the cache is not shared between
     different calls to the fixpoint (the cache is emptied at the end
     of each call). If [debug = true], then the contents of the cache
     is printed at the end of the fixpoint computation. *)
  let fix ?(transient = true) ?(debug = false) f =
    let cache = init () in
    let rec self =
      {
        fun_ =
          (fun x ->
            match get cache x with
            | Some y -> y
            | None ->
              let y = (f self).fun_ x in
              update cache x y;
              y);
      }
    in
    {
      fun_ =
        (fun x ->
          let res = self.fun_ x in
          if debug
          then
            Log.eprintf
              "Elements in cache:@.@[%a@]@.Total elements in cache: %i@."
              pp_cache
              !cache
              (M.cardinal !cache);
          if not transient then reset cache;
          res);
    }
end
