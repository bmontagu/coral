(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

module Naive : sig
  module Make : NAIVE_SOLVER
  module MakeWithInputWidening : NAIVE_SOLVER_WITH_INPUT_WIDENING
  module MakeMFG : NAIVE_SOLVER_MFG
end

module TopDownLegacy : sig
  module Make : SOLVER
  module MakeWithInputWidening : SOLVER_WITH_INPUT_WIDENING
  module MakeMFG : SOLVER_MFG
end

module TopDown : sig
  module Make : SOLVER
  module MakeWithInputWidening : SOLVER_WITH_INPUT_WIDENING
  module MakeMFG : SOLVER_MFG
end

module Priority : sig
  module Make : SOLVER
  module MakeWithInputWidening : SOLVER_WITH_INPUT_WIDENING
  module MakeMFG : SOLVER_MFG
end
