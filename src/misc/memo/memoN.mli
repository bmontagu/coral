(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

(** Functor to create memoizing functions for two mutually-recursive
   functions *)
module Make2
    (_ : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP) : sig
  type 'a fun_ = (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> 'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t)
end

(** Functor to create memoizing functions for three mutually-recursive
   functions *)
module Make3
    (_ : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) -> (XIn2.t -> XOut2.t) -> (XIn3.t -> XOut3.t) -> 'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn1.t -> XOut1.t) * (XIn2.t -> XOut2.t) * (XIn3.t -> XOut3.t)
end

(** Functor to create memoizing functions for four mutually-recursive
   functions *)
module Make4
    (_ : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP)
    (XIn4 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut4 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn4.t -> XOut4.t) fun_ ->
    (XIn1.t -> XOut1.t)
    * (XIn2.t -> XOut2.t)
    * (XIn3.t -> XOut3.t)
    * (XIn4.t -> XOut4.t)
end

(** Functor to create memoizing functions for five mutually-recursive
   functions *)
module Make5
    (_ : LOG)
    (XIn1 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut1 : WITH_PP)
    (XIn2 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut2 : WITH_PP)
    (XIn3 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut3 : WITH_PP)
    (XIn4 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut4 : WITH_PP)
    (XIn5 : sig
      include WITH_COMPARE
      include WITH_PP with type t := t
    end)
    (XOut5 : WITH_PP) : sig
  type 'a fun_ =
    (XIn1.t -> XOut1.t) ->
    (XIn2.t -> XOut2.t) ->
    (XIn3.t -> XOut3.t) ->
    (XIn4.t -> XOut4.t) ->
    (XIn5.t -> XOut5.t) ->
    'a

  val fix :
    ?transient:bool ->
    ?debug:bool ->
    (XIn1.t -> XOut1.t) fun_ ->
    (XIn2.t -> XOut2.t) fun_ ->
    (XIn3.t -> XOut3.t) fun_ ->
    (XIn4.t -> XOut4.t) fun_ ->
    (XIn5.t -> XOut5.t) fun_ ->
    (XIn1.t -> XOut1.t)
    * (XIn2.t -> XOut2.t)
    * (XIn3.t -> XOut3.t)
    * (XIn4.t -> XOut4.t)
    * (XIn5.t -> XOut5.t)
end
