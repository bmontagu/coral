(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2022
*)

open Sigs

(** Functor to compute post-fixpoints of recursively defined functions
    with values in a semi-lattice equipped with widening. The goal is
    to compute a function [f] of type [X.t -> D.t], that is a
    post-fixpoint of a functional of type [(X.t -> D.t) -> (X.t ->
    D.t)]. [D] must be equipped with a widening. It is necessary that
    the functional is called on a finite number of values of [X], so
    that the fixpoint solver terminates.

   This implementation is inspired by the Top-Down fixpoint algorithm,
   and avoids unnecessary recomputations by maintaining a dependency graph.


   Seidl, H. & Vogler, R.
   Sabel, D. & Thiemann, P. (Eds.)
   Three Improvements to the Top-Down Solver
   Proceedings of the 20th International Symposium on Principles and Practice of Declarative Programming, PPDP 2018, Frankfurt am Main, Germany, September 03-05, 2018, ACM, 2018, 21:1-21:14

   Seidl, H.; Erhard, J. & Vogler, R.
   Incremental Abstract Interpretation
   From Lambda Calculus to Cybersecurity Through Program Analysis, Springer International Publishing, 2020, 132-148
 *)
module Make
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : MAP with type key = X.t)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false) f =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and stable =
      (* set of nodes whose values are stable *) ref Edges.Nodes.empty
    and called =
      (* set of nodes whose computation is ongoing, i.e. not yet finalized *)
      ref Edges.Nodes.empty
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and time =
      (* timestamp, i.e. how many calls to [f] have been performed *) ref 0
    in
    let get x =
      match M.find_opt x !sigma with
      | Some (y, _) -> y
      | None -> assert false
    and mark_stable y = stable := Edges.Nodes.add y !stable
    and mark_unstable y = stable := Edges.Nodes.remove y !stable
    and is_stable x = Edges.Nodes.mem x !stable
    and mark_called y = called := Edges.Nodes.add y !called
    and mark_uncalled y = called := Edges.Nodes.remove y !called
    and is_called x = Edges.Nodes.mem x !called
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y in
    let rec destabilize x =
      let infl_x = Edges.get infl x in
      init_infl x;
      Edges.Nodes.iter
        (fun y ->
          mark_unstable y;
          if not @@ is_called y then destabilize y)
        infl_x
    in
    let rec solve x =
      if not (is_stable x || is_called x)
      then begin
        sigma :=
          M.update
            x
            (function
              | None -> Some (D.bot, 1)
              | Some _ as o -> o)
            !sigma;
        mark_stable x;
        mark_called x;
        incr time;
        let f_x = f (eval x) x in
        mark_uncalled x;
        let old = get x in
        if not @@ D.leq f_x old
        then begin
          let f_x = if is_widen x then D.widen old f_x else f_x in
          sigma :=
            M.update
              x
              (function
                | None -> Some (f_x, 1)
                | Some (_, n) -> Some (f_x, n + 1))
              !sigma;
          destabilize x;
          solve x
        end
      end
    and eval x y =
      if is_called y then mark_widen y else solve y;
      add_infl y x;
      Edges.record edges x y;
      get y
    in
    fun x ->
      solve x;
      let result = get x in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            Seq.map (fun (x, (y, _)) -> (x, y)) @@ M.to_seq !sigma;
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        stable := Edges.Nodes.empty;
        called := Edges.Nodes.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions
   with values in a semi-lattice equipped with widening. The goal is
   to compute a function [f] of type [X.t -> D.t], that is a
   post-fixpoint of a functional of type [(bool -> X.t -> D.t) ->
   (bool -> X.t -> D.t)]. Both [X] and [D] must be equipped with a
   widening. The boolean argument indicates whether the fixpoint
   solver should use widening on [X].

   This implementation is inspired by the Top-Down fixpoint algorithm,
   and avoids unnecessary recomputations by maintaining a dependency graph.


   Seidl, H. & Vogler, R.
   Sabel, D. & Thiemann, P. (Eds.)
   Three Improvements to the Top-Down Solver
   Proceedings of the 20th International Symposium on Principles and Practice of Declarative Programming, PPDP 2018, Frankfurt am Main, Germany, September 03-05, 2018, ACM, 2018, 21:1-21:14

   Seidl, H.; Erhard, J. & Vogler, R.
   Incremental Abstract Interpretation
   From Lambda Calculus to Cybersecurity Through Program Analysis, Springer International Publishing, 2020, 132-148
 *)
module MakeWithInputWidening
    (Log : LOG)
    (X : sig
      include WITH_COMPARE

      val widen : t -> t -> t
    end)
    (M : MAP2 with type key = X.t)
    (D : DOMAIN) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((bool -> X.t -> D.t) -> X.t -> D.t) ->
    X.t ->
    D.t * (X.t * D.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false) f =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and stable =
      (* set of nodes whose values are stable *) ref Edges.Nodes.empty
    and called =
      (* set of nodes whose computation is ongoing, i.e. not yet finalized *)
      ref Edges.Nodes.empty
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and time =
      (* timestamp, i.e. how many calls to [f] have been performed *) ref 0
    in
    let get x =
      match M.find_opt x !sigma with
      | Some (y, _, _) -> y
      | None -> assert false
    and mark_stable y = stable := Edges.Nodes.add y !stable
    and mark_unstable y = stable := Edges.Nodes.remove y !stable
    and is_stable x = Edges.Nodes.mem x !stable
    and mark_called y = called := Edges.Nodes.add y !called
    and mark_uncalled y = called := Edges.Nodes.remove y !called
    and is_called x = Edges.Nodes.mem x !called
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y in
    let rec destabilize x =
      let infl_x = Edges.get infl x in
      init_infl x;
      Edges.Nodes.iter
        (fun y ->
          mark_unstable y;
          if not @@ is_called y then destabilize y)
        infl_x
    in
    let rec solve (widen : bool) x =
      if not (is_stable x || is_called x)
      then begin
        sigma :=
          M.update
            x
            (function
              | None -> Some (D.bot, !time, 1)
              | Some _ as o -> o)
            !sigma;
        mark_stable x;
        mark_called x;
        incr time;
        let f_x = f (eval x) x in
        mark_uncalled x;
        let old = get x in
        if not @@ D.leq f_x old
        then begin
          let f_x = if is_widen x then D.widen old f_x else f_x in
          sigma :=
            M.update
              x
              (function
                | None -> Some (f_x, !time, 1)
                | Some (_, t, n) -> Some (f_x, t, n + 1))
              !sigma;
          destabilize x;
          solve widen x
        end
      end
    and eval x (widen : bool) y =
      let y =
        if widen
        then
          match
            M.fold_similar
              (fun z (_, time, _) acc ->
                match acc with
                | None -> Some (z, time)
                | Some (_z_acc, youngest) as acc ->
                  if time > youngest then Some (z, time) else acc)
              y
              !sigma
              None
          with
          | Some (y', _) ->
            let new_ = X.widen y' y in
            if X.compare new_ y <> 0
            then begin
              add_infl new_ y;
              Edges.record edges y new_
            end;
            new_
          | None -> y
        else y
      in
      if is_called y then mark_widen y else solve widen y;
      add_infl y x;
      Edges.record edges x y;
      get y
    in
    fun x ->
      solve false x;
      let result = get x in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, _, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            !sigma |> M.to_seq |> Seq.map (fun (x, (v, _, _)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        stable := Edges.Nodes.empty;
        called := Edges.Nodes.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end

(** Functor to compute post-fixpoints of recursively defined functions
    with values in a semi-lattice for function graphs equipped with
    widening. The goal is to compute a function [f] of type

    [X.t -> MFG.dom -> MFG.codom],

    that is a post-fixpoint of a functional of type

    [(X.t -> MFG.dom -> MFG.codom) -> (X.t -> MFG.dom -> MFG.codom)].

    [MFG] must be equipped with a widening. The functional has to be
    called on a finite number of elements of type [X.t].

   This implementation is inspired by the Top-Down fixpoint algorithm,
   and avoids unnecessary recomputations by maintaining a dependency graph.


   Seidl, H. & Vogler, R.
   Sabel, D. & Thiemann, P. (Eds.)
   Three Improvements to the Top-Down Solver
   Proceedings of the 20th International Symposium on Principles and Practice of Declarative Programming, PPDP 2018, Frankfurt am Main, Germany, September 03-05, 2018, ACM, 2018, 21:1-21:14

   Seidl, H.; Erhard, J. & Vogler, R.
   Incremental Abstract Interpretation
   From Lambda Calculus to Cybersecurity Through Program Analysis, Springer International Publishing, 2020, 132-148
 *)
module MakeMFG
    (Log : LOG)
    (X : WITH_COMPARE)
    (M : MAP with type key = X.t)
    (MFG : sig
      type t
      type dom
      type codom

      val leq : t -> t -> bool
      val init : dom -> t
      val apply : t -> dom -> codom
      val update : (dom -> codom) -> t -> t
      val widen : int -> t -> t -> t
    end) : sig
  val solve :
    ?debug:bool ->
    ?transient:bool ->
    ((X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) ->
    X.t ->
    MFG.dom ->
    MFG.codom * (X.t * MFG.t) Statistics.t
end = struct
  (** Edges are recorded for statistics and to avoid useless recomputations. *)
  module Edges = struct
    module Nodes = Set.Make (X)
    module E = Map.Make (X)

    type edges = Nodes.t E.t

    (** Computes how many edges were recorded *)
    let cardinal (edges : edges) =
      E.fold (fun _ ns acc -> Nodes.cardinal ns + acc) edges 0

    (** Computes the maximal fanout degree for the recorded edges *)
    let max_fanout (edges : edges) =
      E.fold (fun _ ns acc -> max (Nodes.cardinal ns) acc) edges 0

    (** Computes the median fanout degree for the recorded edges *)
    let med_fanout (nodes : _ M.t) (edges : edges) =
      let l =
        M.fold
          (fun n _ acc ->
            match E.find_opt n edges with
            | Some ns -> Nodes.cardinal ns :: acc
            | None -> 0 :: acc)
          nodes
          []
      in
      let a = Array.of_list l in
      Array.sort compare a;
      let n = Array.length a in
      if n = 0 then 0 else a.(n / 2)

    (** Initializes the edges *)
    let init () = ref E.empty

    (** Initializes the edges *)
    let record (edges : edges ref) x1 x2 =
      edges :=
        E.update
          x1
          (function
            | None -> Some (Nodes.singleton x2)
            | Some xs -> Some (Nodes.add x2 xs))
          !edges

    let get (edges : edges ref) x =
      match E.find x !edges with
      | v -> v
      | exception Not_found ->
        edges := E.add x Nodes.empty !edges;
        Nodes.empty

    let forget (edges : edges ref) x = edges := E.add x Nodes.empty !edges
  end

  let solve ?(debug = false) ?(transient = false)
      (f : (X.t -> MFG.dom -> MFG.codom) -> X.t -> MFG.dom -> MFG.codom) =
    let infl =
      (* infl(x) is the set of nodes that x may (directly) influence
         (recorded to avoid useless recomputations) *)
      Edges.init ()
    and edges =
      (* edges(x) is the set of nodes that may (directly) influence x
         (recorded for statistics only) *)
      Edges.init ()
    and stable =
      (* set of nodes whose values are stable *) ref Edges.Nodes.empty
    and called =
      (* set of nodes whose computation is ongoing, i.e. not yet finalized *)
      ref Edges.Nodes.empty
    and wpoint =
      (* set of nodes whose computation demands widening *)
      ref Edges.Nodes.empty
    and sigma =
      (* map from nodes to values (this is the solution we are looking for) *)
      ref M.empty
    and time = ref 0 (* how many times the function was called *) in
    let get x =
      match M.find_opt x !sigma with
      | Some p -> p
      | None -> assert false
    and set n x y = sigma := M.update x (fun _ -> Some (y, n)) !sigma
    and mark_stable y = stable := Edges.Nodes.add y !stable
    and mark_unstable y = stable := Edges.Nodes.remove y !stable
    and is_stable x = Edges.Nodes.mem x !stable
    and mark_called y = called := Edges.Nodes.add y !called
    and mark_uncalled y = called := Edges.Nodes.remove y !called
    and is_called x = Edges.Nodes.mem x !called
    and mark_widen y = wpoint := Edges.Nodes.add y !wpoint
    and is_widen x = Edges.Nodes.mem x !wpoint
    and init_infl x = Edges.forget infl x
    and add_infl x y = Edges.record infl x y in
    let rec destabilize x =
      let infl_x = Edges.get infl x in
      init_infl x;
      Edges.Nodes.iter
        (fun y ->
          mark_unstable y;
          if not @@ is_called y then destabilize y)
        infl_x
    in
    let rec solve x y =
      let d, continue =
        match M.find_opt x !sigma with
        | None ->
          let d = MFG.init y in
          set 1 x d;
          (d, true)
        | Some (d, n) ->
          let d' = MFG.widen n d (MFG.init y) in
          if MFG.leq d' d
          then (d, not (is_stable x || is_called x))
          else begin
            set (n + 1) x d';
            (d', true)
          end
      in
      if continue
      then begin
        mark_stable x;
        mark_called x;
        let f_x =
          MFG.update
            (fun y ->
              incr time;
              f (eval x) x y)
            d
        in
        mark_uncalled x;
        let old, n = get x in
        if not @@ MFG.leq f_x old
        then begin
          let f_x, n =
            if is_widen x then (MFG.widen n old f_x, n + 1) else (f_x, n)
          in
          set n x f_x;
          destabilize x;
          solve x y
        end
      end
    and eval x y z =
      if is_called y then mark_widen y;
      solve y z;
      add_infl y x;
      Edges.record edges x y;
      MFG.apply (fst @@ get y) z
    in
    fun x y ->
      solve x y;
      let result = MFG.apply (fst @@ get x) y in
      let nb_nodes = M.cardinal !sigma
      and nb_edges = Edges.cardinal !edges
      and max_fanout = Edges.max_fanout !edges
      and med_fanout = Edges.med_fanout !sigma !edges
      and nb_iterations = M.fold (fun _ (_, n) acc -> max n acc) !sigma 0 in
      if debug
      then
        Log.eprintf
          "Total number of explored states: %i@.Total number of edges: %i@.Max \
           fanout (median): %i (%i)@.Total number of iterations: %i@.Total \
           number of calls: %i@."
          nb_nodes
          nb_edges
          max_fanout
          med_fanout
          nb_iterations
          !time;
      let stats =
        {
          Statistics.entries =
            !sigma |> M.to_seq |> Seq.map (fun (x, (v, _)) -> (x, v));
          states = nb_nodes;
          edges = nb_edges;
          max_fanout;
          med_fanout;
          iterations = nb_iterations;
          calls = !time;
        }
      in
      if not transient
      then begin
        infl := Edges.E.empty;
        edges := Edges.E.empty;
        stable := Edges.Nodes.empty;
        called := Edges.Nodes.empty;
        sigma := M.empty;
        time := 0
      end;
      (result, stats)
end
