(** Statistics printed by the fixpoint solvers modules *)

type 'a t = {
  entries: 'a Seq.t;
      (** the sequence of entries that were recorded in the memoization
     table *)
  states: int;  (** number of explored states (nodes in the call graph) *)
  edges: int;  (** number of explored edges (edges in the call graph) *)
  max_fanout: int;
      (** maximum fanout degree of states (indicates large disjunctions) *)
  med_fanout: int;  (** median fanout degree *)
  iterations: int;  (** number of iterations performed to reach a fixpoint *)
  calls: int;  (** how many times the functional has been called *)
}

val zero : 'a t
(** Zero-filled statistics *)

val pp :
  print_memo_table:bool ->
  (Format.formatter -> 'a -> unit) ->
  Format.formatter ->
  'a t ->
  unit
(** Pretty-printer *)
