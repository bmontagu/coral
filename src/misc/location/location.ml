(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

type t = Lexing.position * Lexing.position

let get_start = fst
let get_end = snd
let from_pos pos2 = pos2

let from_lexbuf lexbuf =
  from_pos (Lexing.lexeme_start_p lexbuf, Lexing.lexeme_end_p lexbuf)

let dummy = from_pos Lexing.(dummy_pos, dummy_pos)

let pp fmt (start_pos, end_pos) =
  let open Lexing in
  if String.equal "" start_pos.pos_fname
  then
    Format.fprintf
      fmt
      "Line %i, characters %i-%i:"
      start_pos.pos_lnum
      (start_pos.pos_cnum - start_pos.pos_bol)
      (end_pos.pos_cnum - start_pos.pos_bol)
  else
    Format.fprintf
      fmt
      "File \"%s\", line %i, characters %i-%i:"
      start_pos.pos_fname
      start_pos.pos_lnum
      (start_pos.pos_cnum - start_pos.pos_bol)
      (end_pos.pos_cnum - start_pos.pos_bol)

type 'a located = {
  contents: 'a;
  loc: t;
}

let locate loc contents = { contents; loc }
let locate_with { contents = _; loc } x = locate loc x

let to_string_compact prepend (start_pos, _end_pos) =
  let open Lexing in
  if String.equal "" prepend && String.equal "" start_pos.pos_fname
  then
    Printf.sprintf
      "%i:%i"
      start_pos.pos_lnum
      (start_pos.pos_cnum - start_pos.pos_bol)
  else
    Printf.sprintf
      "%s%s:%i:%i"
      prepend
      start_pos.pos_fname
      start_pos.pos_lnum
      (start_pos.pos_cnum - start_pos.pos_bol)
