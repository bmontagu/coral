(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

type t

val get_start : t -> Lexing.position
val get_end : t -> Lexing.position
val from_pos : Lexing.position * Lexing.position -> t
val from_lexbuf : Lexing.lexbuf -> t
val dummy : t
val pp : Format.formatter -> t -> unit

type 'a located = {
  contents: 'a;
  loc: t;
}

val locate : t -> 'a -> 'a located
val locate_with : 'a located -> 'b -> 'b located
val to_string_compact : string -> t -> string
