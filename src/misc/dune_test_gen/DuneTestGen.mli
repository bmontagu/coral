(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

module type CONFIG = sig
  val input_ext : string
  (** Extension of the input files. Must start with a period. *)

  val output_ext : string
  (** Extension for the output files. Must start with a period. *)

  val expected_ext : string
  (** Extention for the expected output files. Must start with a period. *)

  val exe : string
  (** Name of the executable to run for each test. *)

  val default_flags : string list
  (** Flags that should be given by default to the executable. *)

  val file_specific_flags : (string list * string list) list
  (** Flags that should be given to specific files. Files must be
     listed without their extensions. *)

  val excludes : string list
  (** Files in the following list are excluded. *)
end

module Make (_ : CONFIG) : sig
  val print_rule : out_channel -> string -> unit
  (** [print_rule chn file] prints the test rule for the file [file]
       to the output channel [chn] *)

  val generate_rules : out_channel -> string -> unit
  (** [generate_rules chn dir] generates the rules for all the files
       in the directory [dir], and prints them to the output channel
       [chn] *)
end
