(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

(** Tool to create an OCaml file whose contents is the data from a
   list of files from some directory. *)

module type CONFIG = sig
  val project : string
  (** Project name (used in logs only) *)

  val dir : string
  (** Directory that contains the files to be scraped *)

  val extension : string
  (** Extension of the files to be scraped *)

  val exclude_list : string list
  (** Files that must not be scraped *)

  val value_name : string
  (** OCaml name of the created value *)

  val value_type : [ `Array | `List ]
  (** Type of the value: array, or list *)

  val out_channel : out_channel
  (** Output where the OCaml file should be dumped *)
end

module Make (Config : CONFIG) = struct
  let read_file dir file =
    (* Printf.eprintf "Scraping %s...%!" file; *)
    let chn = open_in (Filename.concat dir file) in
    let buf = Buffer.create 1024 in
    begin
      try
        while true do
          let line = input_line chn in
          Buffer.add_string buf line;
          Buffer.add_char buf '\n'
        done
      with End_of_file -> ()
    end;
    close_in chn;
    (* Printf.eprintf " OK\n%!"; *)
    Buffer.contents buf

  let is_excluded =
    let module S = Set.Make (String) in
    let excluded = S.of_list Config.exclude_list in
    fun file -> S.mem file excluded

  let read_files_in_dir dir =
    let all_files = Sys.readdir dir in
    let () = Array.sort String.compare all_files in
    let seq = Array.to_seq all_files in
    Seq.filter_map
      (fun file ->
        if Filename.check_suffix file Config.extension
           && (not @@ is_excluded file)
        then
          Some
            ( Filename.chop_suffix file Config.extension,
              read_file Config.dir file )
        else None)
      seq

  let print files =
    Printf.eprintf
      "Scraping files with extension %s in directory %s.\n%!"
      Config.extension
      Config.dir;
    let n = ref 0 in
    let open_, close_ =
      match Config.value_type with
      | `Array -> ("[|", "|]")
      | `List -> ("[", "]")
    in
    Printf.fprintf Config.out_channel "let %s = %s\n" Config.value_name open_;
    Seq.iter
      (fun (file, contents) ->
        Printf.fprintf Config.out_channel "  (%S,\n   %S\n  );\n" file contents;
        incr n)
      files;
    Printf.fprintf Config.out_channel "%s\n%!" close_;
    Printf.eprintf "Scraped %i files for %s.\n%!" !n Config.project

  let run () =
    let files = read_files_in_dir Config.dir in
    print files
end
