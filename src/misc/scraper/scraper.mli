(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

(** Tool to create an OCaml file whose contents is the data from a
   list of files from some directory. *)

module type CONFIG = sig
  val project : string
  (** Project name (used in logs only) *)

  val dir : string
  (** Directory that contains the files to be scraped *)

  val extension : string
  (** Extension of the files to be scraped *)

  val exclude_list : string list
  (** Files that must not be scraped *)

  val value_name : string
  (** OCaml name of the created value *)

  val value_type : [ `Array | `List ]
  (** Type of the value: array, or list *)

  val out_channel : out_channel
  (** Output where the OCaml file should be dumped *)
end

module Make (_ : CONFIG) : sig
  val run : unit -> unit
  (** Run the scraper *)
end
