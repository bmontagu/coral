(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t = {
  d: int;
  h: int;
  m: int;
  s: int;
  ms: int;
}
(** The type of durations, in a human-friendly form *)

(** Pretty-printer *)
let pp fmt { d; h; m; s; ms } =
  let l = [(d, "d"); (h, "h"); (m, "m"); (s, "s"); (ms, "ms")] in
  let all_zero = ref true in
  List.iter
    (fun (i, s) ->
      if !all_zero && i = 0
      then ()
      else begin
        all_zero := false;
        if s = "ms"
        then Format.fprintf fmt "%i%s" i s
        else Format.fprintf fmt "%i%s " i s
      end)
    l;
  if !all_zero then Format.fprintf fmt "0ms"

(** Conversion from the format returned by [Unix.time] *)
let from_unix_time tm =
  let ms = truncate (1000. *. (tm -. floor tm))
  and s = truncate tm in
  let m, s = (s / 60, s mod 60) in
  let h, m = (m / 60, m mod 60) in
  let d, h = (h / 24, h mod 24) in
  { d; h; m; s; ms }

(** [call f] returns the result of [f ()] along with the time it took
   to compute that value *)
let call f =
  let t1 = Unix.gettimeofday () in
  let res = f () in
  let t2 = Unix.gettimeofday () in
  let delta = t2 -. t1 in
  (res, from_unix_time delta)
