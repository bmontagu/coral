(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t = {
  d: int;  (** days *)
  h: int;  (** hours *)
  m: int;  (** minutes *)
  s: int;  (** seconds *)
  ms: int;  (** milli-seconds *)
}
(** The type of durations, in a human-friendly form *)

val pp : Format.formatter -> t -> unit
(** Pretty-printer *)

val from_unix_time : float -> t
(** Conversion from the format returned by [Unix.time] *)

val call : (unit -> 'a) -> 'a * t
(** [call f] returns the result of [f ()] along with the time it took
   to compute that value *)
