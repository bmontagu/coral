(** A fast hash function, that is suitable for hashing
    data-structures. This is not suitable for cryptographic
    applications. The current implementation uses the "djb2" hash
    function. *)

type t = int
type state

val seed : state
val fold : t -> state -> state [@@inline always]
val finalize : state -> t [@@inline always]
