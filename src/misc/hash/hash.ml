(** Hashing based on a hash function proposed by Daniel J. Bernstein,
    also known as "djb2" *)

type t = int
type state = int

let seed : state = 5381

let[@inline always] fold (x : t) (acc : state) =
  (* acc * 33 + x *)
  x + ((acc lsl 5) + acc)

let[@inline always] finalize (x : state) : t = x
