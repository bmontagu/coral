exception Empty
(** Exception that is raised when querying an empty heap *)

(** Signature for ordered types *)
module type ORDERED = sig
  type t

  val leq : t -> t -> bool
  (** Total pre-order *)
end

(** Signature for heaps *)
module type HEAP = sig
  module Elt : ORDERED
  (** Type of elements *)

  type t
  (** Type of the min-heaps of elements of type [elt] *)

  val empty : t
  (** The empty heap *)

  val is_empty : t -> bool
  (** [is_empty h] tells whether [h] is the empty heap. *)

  val insert : Elt.t -> t -> t
  (** [insert x h] inserts [x] in the heap [h]. *)

  val find_min : t -> Elt.t
  (** [find_min h] finds the minimum element in the heap [h], or raises
   [Empty] if [h] is empty. *)

  val delete_min : t -> t
  (** [delete_min h] removes the minimum value of the heap [h], or
   raises [Empty] if [h] is empty. *)

  val pop_min : t -> Elt.t * t
  (** [delete_min h] returns the pair of the minimum value of [h] and of
   the heap [h] without its minimum value, or raises [Empty] if
   [h] is empty. *)

  val merge : t -> t -> t
  (** [merge h1 h2] merges the two heaps [h1] and [h2]. *)
end

(** Signature for heaps that carry some information. These heaps are
   generic data-structures. *)
module type HEAP_WITH_INFO = sig
  module Priority : ORDERED

  type 'a t
  (** Polymorphic min-heaps, with priorities of type [Priority.t] *)

  val empty : 'a t
  (** The empty heap *)

  val is_empty : 'a t -> bool
  (** [is_empty h] tells whether [h] is the empty heap. *)

  val insert : Priority.t -> 'a -> 'a t -> 'a t
  (** [insert p x h] inserts [x] in the heap [h] with priority [p]. *)

  val find_min : 'a t -> Priority.t * 'a
  (** [find_min h] finds the minimum element in the heap [h] and its
   priority, or raises [Empty] if [h] is empty. *)

  val delete_min : 'a t -> 'a t
  (** [delete_min h] removes the value with minimum priority of the heap
   [h], or raises [Empty] if [h] is empty. *)

  val pop_min : 'a t -> Priority.t * 'a * 'a t
  (** [delete_min h] returns the triple of the minimum priority of [h],
   the associated value of [h] and of the heap [h] without its minimum
   value, or raises [Empty] if [h] is empty. *)

  val merge : 'a t -> 'a t -> 'a t
  (** [merge h1 h2] merges the two heaps [h1] and [h2]. *)
end
