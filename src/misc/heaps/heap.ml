open Common

(** Functor that improves the time complexity of [find_min] so that it
   has a constant worst-case time.

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 2
*)
module ExplicitMin (H : HEAP) : HEAP with type Elt.t = H.Elt.t = struct
  module Elt = H.Elt

  type t =
    | E
    | NE of Elt.t * H.t

  let empty = E

  let is_empty = function
    | E -> true
    | NE _ -> false

  let insert x = function
    | E -> NE (x, H.insert x H.empty)
    | NE (m, h) ->
      let min = if Elt.leq m x then m else x in
      NE (min, H.insert x h)

  let find_min = function
    | E -> raise Empty
    | NE (m, _h) -> m

  let delete_min = function
    | E -> raise Empty
    | NE (_, h) ->
      let h' = H.delete_min h in
      if H.is_empty h'
      then empty
      else
        let m = H.find_min h' in
        NE (m, h')

  let pop_min = function
    | E -> raise Empty
    | NE (m, h) ->
      let h' = H.delete_min h in
      if H.is_empty h'
      then (m, empty)
      else
        let m', h' = H.pop_min h' in
        (m, NE (m', h'))

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | NE (m1, h1'), NE (m2, h2') ->
      let min = if Elt.leq m1 m2 then m1 else m2
      and h12 = H.merge h1' h2' in
      NE (min, h12)
end

(** Leftist Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 1
*)
module Leftist (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = X.t

  type t =
    | E
    | T of int * elt * t * t
  (* The integer is the rank of the tree, i.e. the length of its right spine. *)

  let rank = function
    | E -> 0
    | T (r, _, _, _) -> r

  let make x a b =
    let ra = rank a
    and rb = rank b in
    if ra >= rb then T (rb + 1, x, a, b) else T (ra + 1, x, b, a)

  let empty = E

  let is_empty = function
    | E -> true
    | T _ -> false

  let rec merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | T (_, x, a1, b1), T (_, y, a2, b2) ->
      if X.leq x y then make x a1 (merge b1 h2) else make y a2 (merge h1 b2)

  let rec insert x = function
    | E -> T (1, x, E, E)
    | T (_, y, h1, h2) ->
      if X.leq x y then make x h1 (insert y h2) else make y h2 (insert x h1)

  let find_min = function
    | E -> raise Empty
    | T (_, x, _, _) -> x

  let delete_min = function
    | E -> raise Empty
    | T (_, _, a, b) -> merge a b

  let pop_min = function
    | E -> raise Empty
    | T (_, x, a, b) -> (x, merge a b)
end

(** Binomial Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 2
*)
module Binomial (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = X.t

  type tree = Node of int * elt * t [@@unbox]
  (* The integer is the rank of the tree. The list is sorted in
     decreasing order of rank. The elements of the tree are stored in
     heap order. *)

  and t = tree list
  (* A binomial tree is a list of trees with distinct ranks. *)

  let empty = []

  let is_empty = function
    | [] -> true
    | _ -> false

  (* Precondition: [link] is called on trees with equal ranks *)
  let link (Node (r, x1, c1) as t1) (Node (_, x2, c2) as t2) =
    if X.leq x1 x2
    then Node (r + 1, x1, t2 :: c1)
    else Node (r + 1, x2, t1 :: c2)

  let rank (Node (r, _, _)) = r

  let rec insert_tree t = function
    | [] -> [t]
    | t' :: ts' as ts ->
      if rank t < rank t' then t :: ts else insert_tree (link t t') ts'

  let insert x ts = insert_tree (Node (0, x, [])) ts

  let rec merge ts1 ts2 =
    match (ts1, ts2) with
    | [], ts | ts, [] -> ts
    | t1 :: ts1', t2 :: ts2' ->
      if rank t1 < rank t2
      then t1 :: merge ts1' ts2
      else if rank t2 < rank t1
      then t2 :: merge ts1 ts2'
      else insert_tree (link t1 t2) (merge ts1' ts2')

  let root (Node (_, x, _)) = x

  let rec remove_min_tree = function
    | [] -> raise Empty
    | [t] -> (t, [])
    | t :: ts ->
      let t', ts' = remove_min_tree ts in
      if X.leq (root t) (root t') then (t, ts) else (t', t :: ts')

  let rec find_min = function
    | [] -> raise Empty
    | [t] -> root t
    | t :: ts ->
      let m = root t
      and m' = find_min ts in
      if X.leq m m' then m else m'

  let delete_min ts =
    let Node (_, _, ts1), ts2 = remove_min_tree ts in
    merge (List.rev ts1) ts2

  let pop_min ts =
    let Node (_, x, ts1), ts2 = remove_min_tree ts in
    (x, merge (List.rev ts1) ts2)
end

(** Splay Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 5, Section 4
*)
module Splay (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = Elt.t

  (** A binary search tree *)
  type t =
    | E
    | T of t * elt * t

  let empty = E

  (** [is_empty t] tells whether [t] is the empty tree *)
  let is_empty = function
    | E -> true
    | T _ -> false

  (** [partition pivot t] returns a pair of trees [(small, big)] such
     that [small] contains the elements of [t] that are smaller or
     equal to [pivot], and [big] contains the elements of [t] that are
     strictly larger than [pivot]. *)
  let rec partition pivot = function
    | E -> (E, E)
    | T (a, x, b) as t -> (
      if X.leq x pivot
      then
        match b with
        | E -> (t, E)
        | T (b1, y, b2) ->
          if X.leq y pivot
          then
            let small, big = partition pivot b2 in
            (T (T (a, x, b1), y, small), big)
          else
            let small, big = partition pivot b1 in
            (T (a, x, small), T (big, y, b2))
      else
        match a with
        | E -> (E, t)
        | T (a1, y, a2) ->
          if X.leq y pivot
          then
            let small, big = partition pivot a2 in
            (T (a1, y, small), T (big, x, b))
          else
            let small, big = partition pivot a1 in
            (small, T (big, y, T (a2, x, b))))

  (** [insert x t] inserts [x] in the tree [t] *)
  let insert x t =
    let small, big = partition x t in
    T (small, x, big)

  (** [find_min t] finds the minimum element in the tree [t], or
     raises [Empty] if [t] is empty. *)
  let rec find_min = function
    | E -> raise Empty
    | T (E, x, _) -> x
    | T (a, _, _) -> find_min a

  (** [delete_min t] removes the minimum value of the tree [t], or
     raises [Empty] if [t] is empty. *)
  let rec delete_min = function
    | E -> raise Empty
    | T (E, _, b) -> b
    | T (T (E, _, b), y, c) -> T (b, y, c)
    | T (T (a, x, b), y, c) -> T (delete_min a, x, T (b, y, c))

  (** [pop_min t] returns the pair of the minimum value of [t] and of
     the tree [t] without its minimum value, or raises [Empty] if
     [t] is empty. *)
  let rec pop_min = function
    | E -> raise Empty
    | T (E, x, b) -> (x, b)
    | T (T (E, x, b), y, c) -> (x, T (b, y, c))
    | T (T (a, x, b), y, c) ->
      let min, a_min = pop_min a in
      (min, T (a_min, x, T (b, y, c)))

  (** [merge t1 t2] merges the two trees [t1] and [t2]. *)
  let rec merge t1 t2 =
    match t1 with
    | E -> t2
    | T (a, x, b) ->
      let t2a, t2b = partition x t2 in
      T (merge t2a a, x, merge t2b b)
end

(** Pairing Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 5, Section 5
*)
module Pairing (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = X.t

  type t =
    | E
    | T of elt * t list

  let empty = E

  let is_empty = function
    | E -> true
    | _ -> false

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | T (x, hs1), T (y, hs2) ->
      if X.leq x y then T (x, h2 :: hs1) else T (y, h1 :: hs2)

  let insert x h = merge (T (x, [])) h

  let find_min = function
    | E -> raise Empty
    | T (x, _) -> x

  let rec merge_pairs = function
    | [] -> E
    | [h] -> h
    | h1 :: h2 :: hs -> merge (merge h1 h2) (merge_pairs hs)

  let delete_min = function
    | E -> raise Empty
    | T (_, hs) -> merge_pairs hs

  let pop_min = function
    | E -> raise Empty
    | T (x, hs) -> (x, merge_pairs hs)
end

(** Skew Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 9, Section 3.2
*)
module Skew (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = X.t

  type tree = Node of int * elt * elt list * t [@@unbox]
  and t = tree list
  (* list ordered in increasing rank, with the exception of the first
     two trees, that might have the same rank *)

  let rank (Node (r, _, _, _)) = r
  let empty = []

  let is_empty = function
    | [] -> true
    | _ -> false

  (** Precondition: calls to [link t1 t2] are such that [t1] and [t2]
   have the same rank *)
  let link (Node (r, x1, xs1, c1) as t1) (Node (_, x2, xs2, c2) as t2) =
    if X.leq x1 x2
    then Node (r + 1, x1, xs1, t2 :: c1)
    else Node (r + 1, x2, xs2, t1 :: c2)

  (** Precondition: calls to [skew_link x t1 t2] are such that [t1]
     and [t2] have the same rank *)
  let skew_link x t1 t2 =
    let (Node (r, y, ys, c)) = link t1 t2 in
    if X.leq x y then Node (r, x, y :: ys, c) else Node (r, y, x :: ys, c)

  let insert x = function
    | t1 :: t2 :: rest as ts ->
      if rank t1 = rank t2
      then skew_link x t1 t2 :: rest
      else Node (0, x, [], []) :: ts
    | ts -> Node (0, x, [], []) :: ts

  let rec insert_tree t = function
    | [] -> [t]
    | t' :: ts' as ts ->
      if rank t < rank t' then t :: ts else insert_tree (link t t') ts'

  let rec merge_trees ts1 ts2 =
    match (ts1, ts2) with
    | [], ts | ts, [] -> ts
    | t1 :: ts1', t2 :: ts2' ->
      if rank t1 < rank t2
      then t1 :: merge_trees ts1' ts2
      else if rank t2 < rank t1
      then t2 :: merge_trees ts1 ts2'
      else insert_tree (link t1 t2) (merge_trees ts1' ts2')

  let normalize = function
    | [] -> []
    | t :: ts -> insert_tree t ts

  let merge ts1 ts2 = merge_trees (normalize ts1) (normalize ts2)
  let root (Node (_, x, _, _)) = x

  let rec remove_min_tree = function
    | [] -> raise Empty
    | [t] -> (t, [])
    | t :: ts ->
      let t', ts' = remove_min_tree ts in
      if X.leq (root t) (root t') then (t, ts) else (t', t :: ts')

  let rec find_min = function
    | [] -> raise Empty
    | [t] -> root t
    | t :: ts ->
      let m = root t
      and m' = find_min ts in
      if X.leq m m' then m else m'

  let rec insert_all l ts =
    match l with
    | [] -> ts
    | x :: xs -> insert_all xs (insert x ts)

  let delete_min ts =
    let Node (_, _, xs, ts1), ts2 = remove_min_tree ts in
    insert_all xs (merge (List.rev ts1) ts2)

  let pop_min ts =
    let Node (_, x, xs, ts1), ts2 = remove_min_tree ts in
    (x, insert_all xs (merge (List.rev ts1) ts2))
end

(** Bootstrap functor

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 10, Section 2.2
*)
module Bootstrap
    (Make : functor (X : ORDERED) -> HEAP with type Elt.t = X.t)
    (X : ORDERED) : HEAP with type Elt.t = X.t = struct
  module Elt = X

  type elt = X.t

  type 'h u =
    | E
    | H of elt * 'h

  module rec BootstrappedElt : sig
    include ORDERED with type t = H.t u
  end = struct
    type t = H.t u

    let leq h1 h2 =
      match (h1, h2) with
      | E, _ -> true
      | _, E -> false
      | H (x, _), H (y, _) -> X.leq x y
  end

  and H : (HEAP with type Elt.t = BootstrappedElt.t) = Make (BootstrappedElt)

  type t = BootstrappedElt.t

  let empty = E

  let is_empty = function
    | E -> true
    | _ -> false

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | H (x, p1), H (y, p2) ->
      if X.leq x y then H (x, H.insert h2 p1) else H (y, H.insert h1 p2)

  let insert x h = merge (H (x, H.empty)) h

  let find_min = function
    | E -> raise Empty
    | H (x, _) -> x

  let delete_min = function
    | E -> raise Empty
    | H (_x, p) -> (
      if H.is_empty p
      then E
      else
        let m, p2 = H.pop_min p in
        match m with
        | E -> assert false
        | H (y, p1) -> H (y, H.merge p1 p2))

  let pop_min = function
    | E -> raise Empty
    | H (x, p) -> (
      if H.is_empty p
      then (x, E)
      else
        let m, p2 = H.pop_min p in
        match m with
        | E -> assert false
        | H (y, p1) -> (x, H (y, H.merge p1 p2)))
end
