open Common

(** Functor that improves the time complexity of [find_min] so that it
   has a constant worst-case time.

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 2
*)
module ExplicitMin (H : HEAP_WITH_INFO) :
  HEAP_WITH_INFO with type Priority.t = H.Priority.t = struct
  module Priority = H.Priority

  type 'a t =
    | E
    | NE of Priority.t * 'a * 'a H.t

  let empty = E

  let is_empty = function
    | E -> true
    | NE _ -> false

  let insert x_prio x = function
    | E -> NE (x_prio, x, H.insert x_prio x H.empty)
    | NE (m_prio, m, h) ->
      let prio, y =
        if Priority.leq m_prio x_prio then (m_prio, m) else (x_prio, x)
      in
      NE (prio, y, H.insert x_prio x h)

  let find_min = function
    | E -> raise Empty
    | NE (m_prio, m, _h) -> (m_prio, m)

  let delete_min = function
    | E -> raise Empty
    | NE (_, _, h) ->
      let h' = H.delete_min h in
      if H.is_empty h'
      then empty
      else
        let prio, m = H.find_min h' in
        NE (prio, m, h')

  let pop_min = function
    | E -> raise Empty
    | NE (m_prio, m, h) ->
      let h' = H.delete_min h in
      if H.is_empty h'
      then (m_prio, m, empty)
      else
        let prio', m', h' = H.pop_min h' in
        (m_prio, m, NE (prio', m', h'))

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | NE (prio1, m1, h1'), NE (prio2, m2, h2') ->
      let min, y = if Priority.leq prio1 prio2 then (prio1, m1) else (prio2, m2)
      and h12 = H.merge h1' h2' in
      NE (min, y, h12)
end

(** Leftist Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 1
*)
module Leftist (X : ORDERED) : HEAP_WITH_INFO with type Priority.t = X.t =
struct
  module Priority = X

  type 'a t =
    | E
    | T of int * X.t * 'a * 'a t * 'a t
  (* The integer is the rank of the tree, i.e. the length of its right spine. *)

  let rank = function
    | E -> 0
    | T (r, _, _, _, _) -> r

  let make prio x a b =
    let ra = rank a
    and rb = rank b in
    if ra >= rb then T (rb + 1, prio, x, a, b) else T (ra + 1, prio, x, b, a)

  let empty = E

  let is_empty = function
    | E -> true
    | T _ -> false

  let rec merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | T (_, x_prio, x, a1, b1), T (_, y_prio, y, a2, b2) ->
      if X.leq x_prio y_prio
      then make x_prio x a1 (merge b1 h2)
      else make y_prio y a2 (merge h1 b2)

  let rec insert x_prio x = function
    | E -> T (1, x_prio, x, E, E)
    | T (_, y_prio, y, h1, h2) ->
      if X.leq x_prio y_prio
      then make x_prio x h1 (insert y_prio y h2)
      else make y_prio y h2 (insert x_prio x h1)

  let find_min = function
    | E -> raise Empty
    | T (_, prio, x, _, _) -> (prio, x)

  let delete_min = function
    | E -> raise Empty
    | T (_, _, _, a, b) -> merge a b

  let pop_min = function
    | E -> raise Empty
    | T (_, prio, x, a, b) -> (prio, x, merge a b)
end

(** Binomial Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 3, Section 2
*)
module Binomial (X : ORDERED) : HEAP_WITH_INFO with type Priority.t = X.t =
struct
  module Priority = X

  type 'a tree = Node of int * X.t * 'a * 'a t [@@unbox]
  (* The integer is the rank of the tree. The list is sorted in
     decreasing order of rank. The elements of the tree are stored in
     heap order. *)

  and 'a t = 'a tree list
  (* A binomial tree is a list of trees with distinct ranks. *)

  let empty = []

  let is_empty = function
    | [] -> true
    | _ -> false

  (* Precondition: [link] is called on trees with equal ranks *)
  let link (Node (r, prio1, x1, c1) as t1) (Node (_, prio2, x2, c2) as t2) =
    if X.leq prio1 prio2
    then Node (r + 1, prio1, x1, t2 :: c1)
    else Node (r + 1, prio2, x2, t1 :: c2)

  let rank (Node (r, _, _, _)) = r

  let rec insert_tree t = function
    | [] -> [t]
    | t' :: ts' as ts ->
      if rank t < rank t' then t :: ts else insert_tree (link t t') ts'

  let insert prio x ts = insert_tree (Node (0, prio, x, [])) ts

  let rec merge ts1 ts2 =
    match (ts1, ts2) with
    | [], ts | ts, [] -> ts
    | t1 :: ts1', t2 :: ts2' ->
      if rank t1 < rank t2
      then t1 :: merge ts1' ts2
      else if rank t2 < rank t1
      then t2 :: merge ts1 ts2'
      else insert_tree (link t1 t2) (merge ts1' ts2')

  let priority (Node (_, prio, _, _)) = prio

  let rec remove_min_tree = function
    | [] -> raise Empty
    | [t] -> (t, [])
    | t :: ts ->
      let t', ts' = remove_min_tree ts in
      if X.leq (priority t) (priority t') then (t, ts) else (t', t :: ts')

  let root (Node (_, prio, x, _)) = (prio, x)

  let rec find_min = function
    | [] -> raise Empty
    | [t] -> root t
    | t :: ts ->
      let ((prio, _) as m) = root t
      and ((prio', _) as m') = find_min ts in
      if X.leq prio prio' then m else m'

  let delete_min ts =
    let Node (_, _, _, ts1), ts2 = remove_min_tree ts in
    merge (List.rev ts1) ts2

  let pop_min ts =
    let Node (_, prio, x, ts1), ts2 = remove_min_tree ts in
    (prio, x, merge (List.rev ts1) ts2)
end

(** Splay Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 5, Section 4
*)
module Splay (X : ORDERED) : HEAP_WITH_INFO with type Priority.t = X.t = struct
  module Priority = X

  (** A binary search tree *)
  type 'a t =
    | E
    | T of 'a t * X.t * 'a * 'a t

  let empty = E

  (** [is_empty t] tells whether [t] is the empty tree *)
  let is_empty = function
    | E -> true
    | T _ -> false

  (** [partition pivot t] returns a pair of trees [(small, big)] such
     that [small] contains the elements of [t] that are smaller or
     equal to [pivot], and [big] contains the elements of [t] that are
     strictly larger than [pivot]. *)
  let rec partition pivot = function
    | E -> (E, E)
    | T (a, x_prio, x, b) as t -> (
      if X.leq x_prio pivot
      then
        match b with
        | E -> (t, E)
        | T (b1, y_prio, y, b2) ->
          if X.leq y_prio pivot
          then
            let small, big = partition pivot b2 in
            (T (T (a, x_prio, x, b1), y_prio, y, small), big)
          else
            let small, big = partition pivot b1 in
            (T (a, x_prio, x, small), T (big, y_prio, y, b2))
      else
        match a with
        | E -> (E, t)
        | T (a1, y_prio, y, a2) ->
          if X.leq y_prio pivot
          then
            let small, big = partition pivot a2 in
            (T (a1, y_prio, y, small), T (big, x_prio, x, b))
          else
            let small, big = partition pivot a1 in
            (small, T (big, y_prio, y, T (a2, x_prio, x, b))))

  (** [insert prio x t] inserts [x] in the tree [t] with priority [p] *)
  let insert prio x t =
    let small, big = partition prio t in
    T (small, prio, x, big)

  (** [find_min t] finds the minimum element in the tree [t], or
     raises [Empty] if [t] is empty. *)
  let rec find_min = function
    | E -> raise Empty
    | T (E, prio, x, _) -> (prio, x)
    | T (a, _, _, _) -> find_min a

  (** [delete_min t] removes the minimum value of the tree [t], or
     raises [Empty] if [t] is empty. *)
  let rec delete_min = function
    | E -> raise Empty
    | T (E, _, _, b) -> b
    | T (T (E, _, _, b), y_prio, y, c) -> T (b, y_prio, y, c)
    | T (T (a, x_prio, x, b), y_prio, y, c) ->
      T (delete_min a, x_prio, x, T (b, y_prio, y, c))

  (** [pop_min t] returns the pair of the minimum value of [t] and of
     the tree [t] without its minimum value, or raises [Empty] if
     [t] is empty. *)
  let rec pop_min = function
    | E -> raise Empty
    | T (E, x_prio, x, b) -> (x_prio, x, b)
    | T (T (E, x_prio, x, b), y_prio, y, c) -> (x_prio, x, T (b, y_prio, y, c))
    | T (T (a, x_prio, x, b), y_prio, y, c) ->
      let min_prio, min, a_min = pop_min a in
      (min_prio, min, T (a_min, x_prio, x, T (b, y_prio, y, c)))

  (** [merge t1 t2] merges the two trees [t1] and [t2]. *)
  let rec merge t1 t2 =
    match t1 with
    | E -> t2
    | T (a, x_prio, x, b) ->
      let t2a, t2b = partition x_prio t2 in
      T (merge t2a a, x_prio, x, merge t2b b)
end

(** Pairing Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 5, Section 5
*)
module Pairing (X : ORDERED) : HEAP_WITH_INFO with type Priority.t = X.t =
struct
  module Priority = X

  type 'a t =
    | E
    | T of X.t * 'a * 'a t list

  let empty = E

  let is_empty = function
    | E -> true
    | _ -> false

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | T (x_prio, x, hs1), T (y_prio, y, hs2) ->
      if X.leq x_prio y_prio
      then T (x_prio, x, h2 :: hs1)
      else T (y_prio, y, h1 :: hs2)

  let insert prio x h = merge (T (prio, x, [])) h

  let find_min = function
    | E -> raise Empty
    | T (prio, x, _) -> (prio, x)

  let rec merge_pairs = function
    | [] -> E
    | [h] -> h
    | h1 :: h2 :: hs -> merge (merge h1 h2) (merge_pairs hs)

  let delete_min = function
    | E -> raise Empty
    | T (_, _, hs) -> merge_pairs hs

  let pop_min = function
    | E -> raise Empty
    | T (prio, x, hs) -> (prio, x, merge_pairs hs)
end

(** Skew Heaps

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 9, Section 3.2
*)
module Skew (X : ORDERED) : HEAP_WITH_INFO with type Priority.t = X.t = struct
  module Priority = X

  type 'a tree = Node of int * X.t * 'a * (X.t * 'a) list * 'a t [@@unbox]
  and 'a t = 'a tree list
  (* list ordered in increasing rank, with the exception of the first
     two trees, that might have the same rank *)

  let rank (Node (r, _, _, _, _)) = r
  let empty = []

  let is_empty = function
    | [] -> true
    | _ -> false

  (** Precondition: calls to [link t1 t2] are such that [t1] and [t2]
   have the same rank *)
  let link (Node (r, prio1, x1, xs1, c1) as t1)
      (Node (_, prio2, x2, xs2, c2) as t2) =
    if X.leq prio1 prio2
    then Node (r + 1, prio1, x1, xs1, t2 :: c1)
    else Node (r + 1, prio2, x2, xs2, t1 :: c2)

  (** Precondition: calls to [skew_link x_prio x t1 t2] are such that [t1]
     and [t2] have the same rank *)
  let skew_link x_prio x t1 t2 =
    let (Node (r, y_prio, y, ys, c)) = link t1 t2 in
    if X.leq x_prio y_prio
    then Node (r, x_prio, x, (y_prio, y) :: ys, c)
    else Node (r, y_prio, y, (x_prio, x) :: ys, c)

  let insert prio x = function
    | t1 :: t2 :: rest as ts ->
      if rank t1 = rank t2
      then skew_link prio x t1 t2 :: rest
      else Node (0, prio, x, [], []) :: ts
    | ts -> Node (0, prio, x, [], []) :: ts

  let rec insert_tree t = function
    | [] -> [t]
    | t' :: ts' as ts ->
      if rank t < rank t' then t :: ts else insert_tree (link t t') ts'

  let rec merge_trees ts1 ts2 =
    match (ts1, ts2) with
    | [], ts | ts, [] -> ts
    | t1 :: ts1', t2 :: ts2' ->
      if rank t1 < rank t2
      then t1 :: merge_trees ts1' ts2
      else if rank t2 < rank t1
      then t2 :: merge_trees ts1 ts2'
      else insert_tree (link t1 t2) (merge_trees ts1' ts2')

  let normalize = function
    | [] -> []
    | t :: ts -> insert_tree t ts

  let merge ts1 ts2 = merge_trees (normalize ts1) (normalize ts2)
  let priority (Node (_, prio, _, _, _)) = prio
  let root (Node (_, prio, x, _, _)) = (prio, x)

  let rec remove_min_tree = function
    | [] -> raise Empty
    | [t] -> (t, [])
    | t :: ts ->
      let t', ts' = remove_min_tree ts in
      if X.leq (priority t) (priority t') then (t, ts) else (t', t :: ts')

  let rec find_min = function
    | [] -> raise Empty
    | [t] -> root t
    | t :: ts ->
      let ((prio, _) as m) = root t
      and ((prio', _) as m') = find_min ts in
      if X.leq prio prio' then m else m'

  let rec insert_all l ts =
    match l with
    | [] -> ts
    | (prio, x) :: xs -> insert_all xs (insert prio x ts)

  let delete_min ts =
    let Node (_, _, _, xs, ts1), ts2 = remove_min_tree ts in
    insert_all xs (merge (List.rev ts1) ts2)

  let pop_min ts =
    let Node (_, prio, x, xs, ts1), ts2 = remove_min_tree ts in
    (prio, x, insert_all xs (merge (List.rev ts1) ts2))
end

(** Bootstrap functor

    Okasaki, C.
    Purely functional data structures
    Cambridge University Press, 1999
    Chapter 10, Section 2.2
*)
module Bootstrap (H : HEAP_WITH_INFO) :
  HEAP_WITH_INFO with type Priority.t = H.Priority.t = struct
  module Priority = H.Priority

  type 'a t =
    | E
    | H of Priority.t * 'a * 'a t H.t

  let empty = E

  let is_empty = function
    | E -> true
    | _ -> false

  let merge h1 h2 =
    match (h1, h2) with
    | E, h | h, E -> h
    | H (x_prio, x, p1), H (y_prio, y, p2) ->
      if Priority.leq x_prio y_prio
      then H (x_prio, x, H.insert y_prio h2 p1)
      else H (y_prio, y, H.insert x_prio h1 p2)

  let insert prio x h = merge (H (prio, x, H.empty)) h

  let find_min = function
    | E -> raise Empty
    | H (prio, x, _) -> (prio, x)

  let delete_min = function
    | E -> raise Empty
    | H (_prio, _x, p) -> (
      if H.is_empty p
      then E
      else
        let _, m, p2 = H.pop_min p in
        match m with
        | E -> assert false
        | H (y_prio, y, p1) -> H (y_prio, y, H.merge p1 p2))

  let pop_min = function
    | E -> raise Empty
    | H (x_prio, x, p) -> (
      if H.is_empty p
      then (x_prio, x, E)
      else
        let _, m, p2 = H.pop_min p in
        match m with
        | E -> assert false
        | H (y_prio, y, p1) -> (x_prio, x, H (y_prio, y, H.merge p1 p2)))
end
