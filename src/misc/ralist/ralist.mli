(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** Random Access Lists, inspired by Chris Okasaki's "Purely
   Functional Data Structures"
   https://www.cambridge.org/fr/academic/subjects/computer-science/programming-languages-and-applied-logic/purely-functional-data-structures?format=PB&isbn=9780521663502

   [cons], [uncons], [nth] and [nth_opt] have a logarithmic time
   complexity. *)

type 'a t

val nil : 'a t
val empty : 'a t
val is_empty : 'a t -> bool
val cons : 'a -> 'a t -> 'a t
val uncons : 'a t -> ('a * 'a t) option
val match_ : 'a t -> (unit -> 'b) -> ('a -> 'a t -> 'b) -> 'b
val hd : 'a t -> 'a
val tl : 'a t -> 'a t
val length : 'a t -> int
val nth : 'a t -> int -> 'a
val nth_opt : 'a t -> int -> 'a option
val rev : 'a t -> 'a t
val init : int -> (int -> 'a) -> 'a t
val append : 'a t -> 'a t -> 'a t
val rev_append : 'a t -> 'a t -> 'a t
val concat : 'a t t -> 'a t
val flatten : 'a t t -> 'a t
val map : ('a -> 'b) -> 'a t -> 'b t
val mapi : (int -> 'a -> 'b) -> 'a t -> 'b t
val fold_right : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
val fold_left : ('b -> 'a -> 'b) -> 'b -> 'a t -> 'b
val iter : ('a -> unit) -> 'a t -> unit
val iteri : (int -> 'a -> unit) -> 'a t -> unit
val to_list : 'a t -> 'a list
val of_list : 'a list -> 'a t
val split : ('a * 'b) t -> 'a t * 'b t
val combine : 'a t -> 'b t -> ('a * 'b) t
val exists : ('a -> bool) -> 'a t -> bool
val for_all : ('a -> bool) -> 'a t -> bool
val to_seq : 'a t -> 'a Seq.t
val of_seq : 'a Seq.t -> 'a t

val pp :
  ?pp_sep:(Format.formatter -> unit -> unit) ->
  (Format.formatter -> 'a -> unit) ->
  Format.formatter ->
  'a t ->
  unit
