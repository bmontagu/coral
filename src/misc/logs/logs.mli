(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type S = sig
  val printf : ('a, Format.formatter, unit) format -> 'a

  val kprintf :
    (Format.formatter -> 'a) -> ('b, Format.formatter, unit, 'a) format4 -> 'b

  val eprintf : ('a, Format.formatter, unit) format -> 'a

  val keprintf :
    (Format.formatter -> 'a) -> ('b, Format.formatter, unit, 'a) format4 -> 'b
end

module Std : S
(** Usual, printf-like logging functions: to [stdout] and [stderr] *)

module StdNoErr : S
(** Printf-like logging functions: only to [stdout]; [stderr] is ignored *)

module Silent : S
(** Printf-like logging functions: everything is ignored *)

(** Printf-like logging functions: functor parametrized by std and err formatters *)
module Make (_ : sig
  val std_formatter : Format.formatter
  val err_formatter : Format.formatter
end) : S
