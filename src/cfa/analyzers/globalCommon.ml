(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Lang.Ast

module Make
    (Log : Logs.S)
    (Options : sig
      val no_alarms : bool
      val debug : bool
    end)
    (Solver : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (Calls : Calls.S)
    (CtxtEnv : sig
      type t

      val empty : t
      val compare : t -> t -> int
      val pp : Format.formatter -> t -> unit
    end)
    (Abs : sig
      type t

      val bot : t
      val is_bot : t -> bool
      val leq : t -> t -> bool
      val join : t -> t -> t
      val widen : t -> t -> t
      val meet : t -> t -> t
      val pp : Format.formatter -> t -> unit

      val fold_vars :
        (PP.t * Calls.t -> 'a -> 'a) ->
        (PP.t * Calls.t -> 'a -> 'a) ->
        (PP.t * Calls.t -> 'a -> 'a) ->
        t ->
        'a ->
        'a
      (** [fold fenv fcache] folds over the variables of an abstract
         value, using [fenv] to fold over the variables that are
         recorded in the environment (e.g. the variables that are in
         the environments of abstract closures), and using [fcache] to
         fold over the variables that are recorded in the cache (e.g.
         the variables that are recorded in abstract tuples). *)
    end) =
struct
  (** Global environments are maps whose keys are pairs made of a
     variable and a calling context (i.e. addresses, in the AAM
     terminology). The values that global environments contain are the
     abstract values of the module [Abs]. *)
  module GEnv = struct
    (** Variables with their calling context *)
    module V = struct
      type t = PP.t * Calls.t [@@deriving ord]

      let pp fmt (x, ctxt) =
        Format.fprintf fmt "@[%a @@ [%a]@]" PP.pp x Calls.pp ctxt
    end

    module M = MapMake (V)

    type key = V.t
    type value = Abs.t
    type t = value M.t

    let empty = M.empty
    let is_empty = M.is_empty
    let mem = M.mem
    let add = M.add
    let find = M.find
    let fold = M.fold
    let remove = M.remove
    let singleton = M.singleton

    let get k m =
      match M.find_opt k m with
      | None -> Abs.bot
      | Some a -> a

    let bot = M.empty

    let leq c1 c2 =
      M.for_all
        (fun k v1 ->
          match M.find_opt k c2 with
          | None -> false
          | Some v2 -> Abs.leq v1 v2)
        c1

    let join c1 c2 = M.union (fun _k v1 v2 -> Some (Abs.join v1 v2)) c1 c2
    let widen c1 c2 = M.union (fun _k v1 v2 -> Some (Abs.widen v1 v2)) c1 c2

    let pp fmt c =
      Format.fprintf
        fmt
        "@[<hv 0>{@[<hv 1> %a@]@ }@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
           (fun fmt (k, v) ->
             Format.fprintf fmt "@[%a ->@ %a@]" V.pp k Abs.pp v))
        (M.bindings c)

    let find_opt = M.find_opt
    let union = M.union
    let merge = M.merge
    let iter = M.iter
    let cardinal = M.cardinal
  end

  module BoolGEnv = struct
    type key = GEnv.key
    type value = bool
    type t = value GEnv.M.t

    let empty = GEnv.M.empty
    let find_opt = GEnv.M.find_opt
    let add = GEnv.M.add
  end

  (** Tuples of a global environment, an abstract value and a log of
     alarms: such tuples are the results of the analyzer. *)
  module AlarmGEnvAbs = struct
    module M = Alarming.Make (Options) (Calls) (Abs)

    type 'a m = {
      genv: GEnv.t;
      cache: GEnv.t;
      value: 'a M.m;
    }

    type t = Abs.t m

    let bot = { genv = GEnv.bot; cache = GEnv.bot; value = M.bot }

    let leq { genv = g1; cache = c1; value = m1 }
        { genv = g2; cache = c2; value = m2 } =
      GEnv.leq g1 g2 && GEnv.leq c1 c2 && M.leq m1 m2

    let join { genv = g1; cache = c1; value = m1 }
        { genv = g2; cache = c2; value = m2 } =
      { genv = GEnv.join g1 g2; cache = GEnv.join c1 c2; value = M.join m1 m2 }

    let widen { genv = g1; cache = c1; value = m1 }
        { genv = g2; cache = c2; value = m2 } =
      {
        genv = GEnv.widen g1 g2;
        cache = GEnv.widen c1 c2;
        value = M.widen m1 m2;
      }

    let pp fmt { genv = g; cache = c; value = m } =
      let a, v = M.run m in
      if not @@ M.Alarms.is_bot a
      then Format.fprintf fmt "@[%a@]@." M.Alarms.pp a;
      if GEnv.is_empty g
      then
        if GEnv.is_empty c
        then Abs.pp fmt v
        else
          Format.fprintf fmt "@[<hv 0>cache =@ %a@ ⊢@ %a@]" GEnv.pp c Abs.pp v
      else if GEnv.is_empty c
      then Format.fprintf fmt "@[<hv 0>%a@ ⊢@ %a@]" GEnv.pp g Abs.pp v
      else
        Format.fprintf
          fmt
          "@[<hv 0>genv =@ %a@ cache =@ %a@ ⊢@ %a@]"
          GEnv.pp
          g
          GEnv.pp
          c
          Abs.pp
          v

    let run { genv = g; cache = c; value = m } = (g, c, M.run m)
    let make g c (a, v) = { genv = g; cache = c; value = M.make a v }
    let return v = { genv = GEnv.empty; cache = GEnv.empty; value = M.return v }

    let bind m f =
      let { genv = g1; cache = c1; value = av1 } = m in
      let a1, v1 = M.run av1 in
      let { genv = g2; cache = c2; value = av2 } = f v1 in
      let a2, v2 = M.run av2 in
      {
        genv = GEnv.join g1 g2;
        cache = GEnv.join c1 c2;
        value = M.make (M.Alarms.join a1 a2) v2;
      }

    let ( let* ) = bind

    let map m f =
      let { genv = g; cache = c; value = av } = m in
      { genv = g; cache = c; value = M.map av f }

    let ( let+ ) = map

    let plus m1 m2 =
      let { genv = g1; cache = c1; value = av1 } = m1 in
      let { genv = g2; cache = c2; value = av2 } = m2 in
      {
        genv = GEnv.join g1 g2;
        cache = GEnv.join c1 c2;
        value = M.plus av1 av2;
      }

    let ( and+ ) = plus
    let record_genv g = { genv = g; cache = GEnv.empty; value = M.return () }
    let record_cache c = { genv = GEnv.empty; cache = c; value = M.return () }

    let with_recorded m =
      let { genv = g; cache = c; value = av } = m in
      { genv = g; cache = c; value = M.map av (fun v -> (g, v)) }

    let record_parameter_instance loc call v =
      record_genv (GEnv.singleton (loc, call) v)

    let record_produced_value loc call v =
      record_cache (GEnv.singleton (loc, call) v)

    let alarm x c v =
      { genv = GEnv.empty; cache = GEnv.empty; value = M.alarm x c v }

    let get_alarms { genv = _; cache = _; value = a } = M.get_alarms a

    module Alarm = M.Alarm
  end

  (** Memoization module for the analyzer *)
  module MemoFix = struct
    module CallsTermEnv = struct
      type t = Calls.t * Term.t * CtxtEnv.t [@@deriving ord]

      let pp fmt (calls, t, env) =
        Format.fprintf
          fmt
          "@[@[[%a]@]@ %a@ %a@]@."
          Calls.pp
          calls
          Term.pp
          t
          CtxtEnv.pp
          env
    end

    module Unit = struct
      type t = unit

      let compare () () = 0
      let pp _fmt () = ()
    end

    module AbsClosure = struct
      type t = (Calls.t * Term.t * CtxtEnv.t) * unit [@@deriving ord]

      let widen _ _ = assert false
      (* there should be no widening at all *)
    end

    module M = struct
      include Map2.Make (CallsTermEnv) (Unit)

      (* all values are similar: this is ok, since there is no
         widening at all *)
      let sim _ _ = true
      let fold_similar f k m = fold_similar f sim k m
    end

    include Solver (Log) (AbsClosure) (M) (AlarmGEnvAbs)
  end

  (** Helper function to define the abstract evaluator *)
  let solve =
    let curry f calls env t = f ((calls, t, env), ())
    and curry1 f b calls env t = f b ((calls, t, env), ()) in
    let uncurry2 f g ((calls, t, env), ()) = f (curry1 g) calls env t in
    fun f ->
      curry @@ MemoFix.solve ~debug:Options.debug ~transient:false @@ uncurry2 f

  (** Outer fixpoint computation *)
  let fix leq join bot f =
    let rec loop ((n, x) as acc) =
      if Options.debug then Log.eprintf "Outer iteration %i:@." n;
      let y = f x in
      if leq y x then acc else loop (n + 1, join x y)
    in
    let n, y = loop (1, bot) in
    if Options.debug then Log.eprintf "Total outer iterations: %i@." n;
    (n, y)

  module GEnv2 = struct
    let empty = (GEnv.empty, GEnv.empty)
    let is_empty (g1, g2) = GEnv.is_empty g1 && GEnv.is_empty g2
    let mem_left x (g1, _g2) = GEnv.mem x g1
    let mem_right x (_g1, g2) = GEnv.mem x g2
    let add_left x v (g1, g2) = (GEnv.add x v g1, g2)
    let add_right x v (g1, g2) = (g1, GEnv.add x v g2)
    let remove_left x (g1, g2) = (GEnv.remove x g1, g2)
    let remove_right x (g1, g2) = (g1, GEnv.remove x g2)
    let fold f1 f2 (g1, g2) acc = acc |> GEnv.fold f1 g1 |> GEnv.fold f2 g2
  end

  (** Extract the minimal part of the global environment [genv] and of
     the global cache [c] that [v] depends on *)
  let extract_min_genv_cache genv cache v =
    let mem_genv = GEnv2.mem_left in
    let mem_cache = GEnv2.mem_right in
    let add_var_genv var g = GEnv2.add_left var (GEnv.find var genv) g in
    let add_var_cache var c = GEnv2.add_right var (GEnv.find var cache) c in
    let remove_genv = GEnv2.remove_left in
    let remove_cache = GEnv2.remove_right in
    let rec loop ((new_, old) as acc) =
      if GEnv2.is_empty new_
      then acc
      else
        let handle_val =
          let fold_in_genv var (new_, old) =
            if mem_genv var old
            then (new_, old)
            else (add_var_genv var new_, old)
          and fold_in_cache var (new_, old) =
            if mem_cache var old
            then (new_, old)
            else (add_var_cache var new_, old)
          in
          Abs.fold_vars fold_in_genv fold_in_cache fold_in_cache
        in
        let acc =
          GEnv2.fold
            (fun x vx (new_, old) ->
              handle_val vx (remove_genv x new_, add_var_genv x old))
            (fun x vx (new_, old) ->
              handle_val vx (remove_cache x new_, add_var_cache x old))
            new_
            (GEnv2.empty, old)
        in
        loop acc
    in
    let init_vars =
      Abs.fold_vars add_var_genv add_var_cache add_var_cache v GEnv2.empty
    in
    snd @@ loop (init_vars, GEnv2.empty)

  (** The final evaluator: computes a fixpoint of the inner evaluator *)
  let make_eval ~print_stats ~check_genv ~whole_genv_cache eval t =
    let stats = ref Memo.Statistics.zero in
    let nb_iter, res =
      fix AlarmGEnvAbs.leq AlarmGEnvAbs.widen AlarmGEnvAbs.bot (fun acc ->
          let genv, cache, (_alarms, _v) =
            AlarmGEnvAbs.run (acc : AlarmGEnvAbs.t)
          in
          let res, stats' = eval genv cache Calls.empty CtxtEnv.empty t in
          stats := stats';
          res)
    in
    let genv, cache, (alarms, v) = AlarmGEnvAbs.run res in
    stats := { !stats with iterations = nb_iter - 1 };
    print_stats genv cache;
    check_genv genv;
    let genv, cache =
      if whole_genv_cache
      then (genv, cache)
      else extract_min_genv_cache genv cache v
    in
    (AlarmGEnvAbs.make genv cache (alarms, v), !stats)
end
