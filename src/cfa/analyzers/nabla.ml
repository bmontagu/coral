(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Lang.Ast

module type COMPARABLE = sig
  type t

  val compare : t -> t -> int
end

(** Options of program points *)
module OPP = struct
  type t = PP.t option [@@deriving ord]
end

module MOPP = Map.Make (OPP)
(** Maps indexed by options of program points *)

module AbsDomain
    (Options : sig
      val sim_threshold : int
    end)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (Annot : COMPARABLE) :
  NablaCommon.ABS_DOMAIN
    with type bools := Bools.t
     and type ints := Ints.t
     and type annot := Annot.t = struct
  module FunAnnot = struct
    type t = NablaFun.t * Annot.t option [@@deriving ord]
  end

  module M = Map.Make (FunAnnot)
  module MInt = Ptmap
  module MCons = Map.Make (Cons)

  type t = {
    height: int;
    bools: Bools.t;
    ints: Ints.t;
    closures: closures;
    tuples: tuples;
    variants: variants;
  }

  and closures =
    | Clos of int * (* height *) t Env.t M.t
    | ClosTop
  [@@deriving ord]

  and tuples =
    | Tuples of int * (* height *) t list MOPP.t MInt.t
    | TuplesTop
  [@@deriving ord]

  and variants =
    | Variants of int * (* height *) t variant_args MOPP.t MCons.t
    | VariantsTop
  [@@deriving ord]

  and 'a variant_args =
    | NakedOnly
    | WithArgOnly of 'a
    | NakedOrWithArg of 'a
  [@@deriving ord]

  let height_clos = function
    | Clos (h, _) -> h
    | ClosTop -> 0

  let height_tuples = function
    | Tuples (h, _) -> h
    | TuplesTop -> 0

  let height_variants = function
    | Variants (h, _) -> h
    | VariantsTop -> 0

  let height t = t.height

  let clos_is_bot = function
    | Clos (_, m) -> M.is_empty m
    | ClosTop -> false

  let tuples_is_bot = function
    | Tuples (_h, m) -> MInt.is_empty m
    | TuplesTop -> false

  let variants_is_bot = function
    | Variants (_, m) -> MCons.is_empty m
    | VariantsTop -> false

  let is_bot { height = _; bools; ints; closures; tuples; variants } =
    Bools.is_bot bools
    && Ints.is_bot ints
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let clos_is_top = function
    | ClosTop -> true
    | Clos _ -> false

  let tuples_is_top = function
    | Tuples _ -> false
    | TuplesTop -> true

  let variants_is_top = function
    | Variants _ -> false
    | VariantsTop -> true

  let is_top { height = _; bools; ints; closures; tuples; variants } =
    Bools.is_top bools
    && Ints.is_top ints
    && clos_is_top closures
    && tuples_is_top tuples
    && variants_is_top variants

  let rec pp fmt ({ height = _; bools; ints; closures; tuples; variants } as v)
      =
    if is_bot v
    then Format.fprintf fmt "⊥"
    else if is_top v
    then Format.fprintf fmt "⊤"
    else
      let open Format in
      fprintf fmt "@[<hv 0>{@[<hv 1>";
      if not @@ Bools.is_bot bools
      then fprintf fmt "@ @[bools =@ %a@]" Bools.pp bools;
      if not @@ Ints.is_bot ints
      then fprintf fmt "@ @[ints =@ %a@]" Ints.pp ints;
      if not @@ clos_is_bot closures
      then fprintf fmt "@ @[closures =@ %a@]" pp_closures closures;
      if not @@ tuples_is_bot tuples
      then fprintf fmt "@ @[tuples =@ %a@]" pp_tuples tuples;
      if not @@ variants_is_bot variants
      then fprintf fmt "@ @[variants =@ %a@]" pp_variants variants;
      fprintf fmt "@]@ }@]"

  and pp_closures fmt = function
    | ClosTop -> Format.pp_print_string fmt "⊤"
    | Clos (_h, m) ->
      if M.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt ((x, _annot), mx) ->
               Format.fprintf
                 fmt
                 "@[%a@ %a@]"
                 (NablaFun.pp_rec true)
                 x
                 (Env.pp pp)
                 mx))
          (M.bindings m)

  and pp_tuples fmt = function
    | TuplesTop -> Format.pp_print_string fmt "⊤"
    | Tuples (_h, m) ->
      if MInt.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (i, mopp) ->
               Format.pp_print_list
                 ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
                 (fun fmt (_opp, li) ->
                   match li with
                   | [] -> Format.fprintf fmt "%i: ()" i
                   | _ ->
                     Format.fprintf
                       fmt
                       "@[%i:@ %a@]"
                       i
                       (Format.pp_print_list
                          ~pp_sep:(fun fmt () -> Format.fprintf fmt "@ × ")
                          pp)
                       li)
                 fmt
                 (MOPP.bindings mopp)))
          (MInt.bindings m)

  and pp_variants fmt = function
    | VariantsTop -> Format.pp_print_string fmt "⊤"
    | Variants (_h, m) ->
      if MCons.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (c, mopp) ->
               Format.pp_print_list
                 ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
                 (fun fmt (_opp, varg) ->
                   Format.fprintf fmt "@[";
                   begin
                     match varg with
                     | NakedOnly -> Format.fprintf fmt "@[%a:@ .@]" Cons.pp c
                     | WithArgOnly arg ->
                       if not @@ is_bot arg
                       then Format.fprintf fmt "@[%a:@ %a@]" Cons.pp c pp arg
                     | NakedOrWithArg arg ->
                       Format.fprintf fmt "@[%a:@ .@]" Cons.pp c;
                       if not @@ is_bot arg
                       then Format.fprintf fmt "@[%a:@ %a@]" Cons.pp c pp arg
                   end;
                   Format.fprintf fmt "@]")
                 fmt
                 (MOPP.bindings mopp)))
          (MCons.bindings m)

  let normalize_clos_map m =
    M.filter (fun _f env -> Env.for_all (fun _x v -> not @@ is_bot v) env) m

  let compute_clos_height m =
    if M.is_empty m
    then 0
    else
      1
      + M.fold
          (fun _ env acc ->
            Env.fold (fun _x tx acc -> max (height tx) acc) env acc)
          m
          0

  let clos m =
    let m = normalize_clos_map m in
    let h = compute_clos_height m in
    Clos (h, m)

  let normalize_tuples_map m =
    MInt.filter_map
      (fun _i mopp ->
        let mopp =
          MOPP.filter
            (fun _opp l -> List.for_all (fun v -> not @@ is_bot v) l)
            mopp
        in
        if MOPP.is_empty mopp then None else Some mopp)
      m

  let compute_tuples_height m =
    if MInt.is_empty m
    then 0
    else
      1
      + MInt.fold
          (fun _i mopp acc ->
            MOPP.fold
              (fun _opp l acc ->
                List.fold_left (fun acc v -> max (height v) acc) acc l)
              mopp
              acc)
          m
          0

  let tuples m =
    let m = normalize_tuples_map m in
    let h = compute_tuples_height m in
    Tuples (h, m)

  let normalize_variants_map m =
    MCons.filter_map
      (fun _c mopp ->
        let mopp =
          MOPP.filter
            (fun _opp varg ->
              match varg with
              | NakedOnly -> true
              | WithArgOnly arg -> not @@ is_bot arg
              | NakedOrWithArg _ -> true)
            mopp
        in
        if MOPP.is_empty mopp then None else Some mopp)
      m

  let compute_variants_height m =
    if MCons.is_empty m
    then 0
    else
      1
      + MCons.fold
          (fun _c mopp acc ->
            MOPP.fold
              (fun _opp varg acc ->
                match varg with
                | NakedOnly -> acc
                | WithArgOnly arg | NakedOrWithArg arg -> max (height arg) acc)
              mopp
              acc)
          m
          0

  let variants m =
    let m = normalize_variants_map m in
    let h = compute_variants_height m in
    Variants (h, m)

  let make ~bools ~ints ~closures ~tuples ~variants =
    {
      height =
        max (height_clos closures)
        @@ max (height_tuples tuples) (height_variants variants);
      bools;
      ints;
      closures;
      tuples;
      variants;
    }

  let clos_bot = Clos (0, M.empty)
  let clos_top = ClosTop
  let tuples_bot = Tuples (0, MInt.empty)
  let tuples_top = TuplesTop
  let variants_bot = Variants (0, MCons.empty)
  let variants_top = VariantsTop

  let bot =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let top =
    make
      ~bools:Bools.top
      ~ints:Ints.top
      ~closures:clos_top
      ~tuples:tuples_top
      ~variants:variants_top

  let is_bool { height = _; bools = _; ints; closures; tuples; variants } =
    Ints.is_bot ints
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let is_int { height = _; bools; ints = _; closures; tuples; variants } =
    Bools.is_bot bools
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let is_fun { height = _; bools; ints; closures = _; tuples; variants } =
    Bools.is_bot bools
    && Ints.is_bot ints
    && tuples_is_bot tuples
    && variants_is_bot variants

  let singleton_closure annot t env =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:(clos (M.singleton (t, annot) env))
      ~tuples:tuples_bot
      ~variants:variants_bot

  let ints i =
    make
      ~bools:Bools.bot
      ~ints:i
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let get_ints { height = _; bools; ints; closures; tuples; variants } =
    (ints, make ~bools ~ints:Ints.bot ~closures ~tuples ~variants)

  let bools b =
    make
      ~bools:b
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let get_bools { height = _; bools; ints; closures; tuples; variants } =
    (bools, make ~bools:Bools.bot ~ints ~closures ~tuples ~variants)

  let singleton_tuple opp i l =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:(tuples @@ MInt.singleton i (MOPP.singleton opp l))
      ~variants:variants_bot

  let make_constant_list =
    let rec loop n v acc = if n <= 0 then acc else loop (n - 1) v (v :: acc) in
    fun n v -> loop n v []

  let get_tuple n
      ({ height = _; bools; ints; closures; tuples = t; variants } as v) =
    match t with
    | TuplesTop ->
      let rem = v in
      Some ([(None, make_constant_list n top)], rem)
    | Tuples (_h, m) -> (
      match MInt.find_opt n m with
      | None -> None
      | Some mopp ->
        let vs = MOPP.bindings mopp in
        let rem =
          make
            ~bools
            ~ints
            ~closures
            ~tuples:(tuples @@ MInt.remove n m)
            ~variants
        in
        Some (vs, rem))

  let singleton_variant mopp c oarg =
    let args =
      match oarg with
      | None -> NakedOnly
      | Some arg -> WithArgOnly arg
    in
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:(variants @@ MCons.singleton c (MOPP.singleton mopp args))

  let list_is_empty = function
    | [] -> true
    | _ -> false

  let get_variant_naked c
      ({ height = _; bools; ints; closures; tuples; variants = t } as v) =
    match t with
    | VariantsTop ->
      let rem = v in
      Some ([None], rem)
    | Variants (_h, m) -> (
      match MCons.find_opt c m with
      | None -> None
      | Some mopp ->
        if MOPP.for_all
             (fun _opp varg ->
               match varg with
               | NakedOnly | NakedOrWithArg _ -> false
               | WithArgOnly _ -> true)
             mopp
        then None
        else
          let pps =
            MOPP.fold
              (fun opp varg acc ->
                match varg with
                | NakedOnly | NakedOrWithArg _ -> opp :: acc
                | WithArgOnly _ -> acc)
              mopp
              []
          in
          if list_is_empty pps
          then None
          else
            let rem =
              make
                ~bools
                ~ints
                ~closures
                ~tuples
                ~variants:
                  (variants
                  @@ MCons.add
                       c
                       (MOPP.filter_map
                          (fun _opp varg ->
                            match varg with
                            | NakedOnly -> None
                            | NakedOrWithArg arg | WithArgOnly arg ->
                              Some (WithArgOnly arg))
                          mopp)
                       m)
            in
            Some (pps, rem))

  let get_variant_with_arg c
      ({ height = _; bools; ints; closures; tuples; variants = t } as v) =
    match t with
    | VariantsTop ->
      let rem = v in
      Some ([(None, top)], rem)
    | Variants (_h, m) -> (
      match MCons.find_opt c m with
      | None -> None
      | Some mopp ->
        let arg =
          MOPP.fold
            (fun opp varg acc ->
              match varg with
              | NakedOnly -> acc
              | WithArgOnly arg | NakedOrWithArg arg ->
                if is_bot arg then acc else (opp, arg) :: acc)
            mopp
            []
        in
        if list_is_empty arg
        then None
        else
          let rem =
            make
              ~bools
              ~ints
              ~closures
              ~tuples
              ~variants:
                (variants
                @@ MCons.add
                     c
                     (MOPP.filter_map
                        (fun _opp varg ->
                          match varg with
                          | NakedOnly | NakedOrWithArg _ -> Some NakedOnly
                          | WithArgOnly _ -> None)
                        mopp)
                     m)
          in
          Some (arg, rem))

  let rec join
      {
        height = _;
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        height = _;
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    make
      ~bools:(Bools.join bools1 bools2)
      ~ints:(Ints.join ints1 ints2)
      ~closures:(clos_join clos1 clos2)
      ~tuples:(tuples_join tuples1 tuples2)
      ~variants:(variants_join variants1 variants2)

  and clos_join c1 c2 =
    match (c1, c2) with
    | ClosTop, _ | _, ClosTop -> clos_top
    | Clos (_h1, m1), Clos (_h2, m2) ->
      let m =
        M.union
          (fun _t env1 env2 ->
            Some (Env.union (fun _x cs1 cs2 -> Some (join cs1 cs2)) env1 env2))
          m1
          m2
      in
      clos m

  and tuples_join om1 om2 =
    match (om1, om2) with
    | TuplesTop, _ | _, TuplesTop -> tuples_top
    | Tuples (_h1, m1), Tuples (_h2, m2) ->
      tuples
      @@ MInt.union
           (fun _i mopp1 mopp2 ->
             Some
               (MOPP.union
                  (fun _opp l1 l2 -> Some (List.map2 join l1 l2))
                  mopp1
                  mopp2))
           m1
           m2

  and variants_join om1 om2 =
    match (om1, om2) with
    | VariantsTop, _ | _, VariantsTop -> variants_top
    | Variants (_h1, m1), Variants (_h2, m2) ->
      variants
      @@ MCons.union
           (fun _c mopp1 mopp2 ->
             Some
               (MOPP.union
                  (fun _opp args1 args2 ->
                    Some (variants_args_join args1 args2))
                  mopp1
                  mopp2))
           m1
           m2

  and variants_args_join varg1 varg2 =
    match (varg1, varg2) with
    | NakedOnly, NakedOnly -> NakedOnly
    | NakedOnly, (NakedOrWithArg v | WithArgOnly v)
    | (NakedOrWithArg v | WithArgOnly v), NakedOnly -> NakedOrWithArg v
    | WithArgOnly v1, WithArgOnly v2 -> WithArgOnly (join v1 v2)
    | NakedOrWithArg v1, WithArgOnly v2
    | WithArgOnly v1, NakedOrWithArg v2
    | NakedOrWithArg v1, NakedOrWithArg v2 -> NakedOrWithArg (join v1 v2)

  let rec meet
      {
        height = _;
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        height = _;
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    make
      ~bools:(Bools.meet bools1 bools2)
      ~ints:(Ints.meet ints1 ints2)
      ~closures:(clos_meet clos1 clos2)
      ~tuples:(tuples_meet tuples1 tuples2)
      ~variants:(variants_meet variants1 variants2)

  and clos_meet c1 c2 =
    match (c1, c2) with
    | ClosTop, c | c, ClosTop -> c
    | Clos (_h1, m1), Clos (_h2, m2) ->
      let m =
        M.merge
          (fun _t oenv1 oenv2 ->
            match (oenv1, oenv2) with
            | Some env1, Some env2 ->
              Some (Env.union (fun _x cs1 cs2 -> Some (meet cs1 cs2)) env1 env2)
            | (None as none), _ | _, (None as none) -> none)
          m1
          m2
      in
      clos m

  and tuples_meet om1 om2 =
    match (om1, om2) with
    | Tuples (_h1, m1), Tuples (_h2, m2) ->
      tuples
      @@ MInt.merge
           (fun _i omopp1 omopp2 ->
             match (omopp1, omopp2) with
             | Some mopp1, Some mopp2 ->
               Some
                 (MOPP.merge
                    (fun _opp o1 o2 ->
                      match (o1, o2) with
                      | Some l1, Some l2 -> Some (List.map2 meet l1 l2)
                      | (None as none), _ | _, (None as none) -> none)
                    mopp1
                    mopp2)
             | (None as none), _ | _, (None as none) -> none)
           m1
           m2
    | TuplesTop, o | o, TuplesTop -> o

  and variants_meet om1 om2 =
    match (om1, om2) with
    | Variants (_h1, m1), Variants (_h2, m2) ->
      variants
      @@ MCons.merge
           (fun _c omopp1 omopp2 ->
             match (omopp1, omopp2) with
             | Some mopp1, Some mopp2 ->
               Some
                 (MOPP.merge
                    (fun _opp o1 o2 ->
                      match (o1, o2) with
                      | Some args1, Some args2 -> variants_args_meet args1 args2
                      | (None as none), _ | _, (None as none) -> none)
                    mopp1
                    mopp2)
             | (None as none), _ | _, (None as none) -> none)
           m1
           m2
    | VariantsTop, o | o, VariantsTop -> o

  and variants_args_meet varg1 varg2 =
    match (varg1, varg2) with
    | NakedOnly, WithArgOnly _ | WithArgOnly _, NakedOnly -> None
    | NakedOnly, NakedOnly
    | NakedOrWithArg _, NakedOnly
    | NakedOnly, NakedOrWithArg _ -> Some NakedOnly
    | WithArgOnly v1, NakedOrWithArg v2
    | NakedOrWithArg v1, WithArgOnly v2
    | WithArgOnly v1, WithArgOnly v2 ->
      let v12 = meet v1 v2 in
      if is_bot v12 then None else Some (WithArgOnly v12)
    | NakedOrWithArg v1, NakedOrWithArg v2 -> Some (NakedOrWithArg (meet v1 v2))

  (* [leq_gen ~weak a1 a2] is an inclusion test between [a1] and [a2].
     If [weak = true], then the integer parts of [a1] and [a2] are
     ignored. *)
  let leq_gen ~weak a1 a2 =
    let rec leq
        {
          height = _;
          bools = bools1;
          ints = ints1;
          closures = clos1;
          tuples = tuples1;
          variants = variants1;
        }
        {
          height = _;
          bools = bools2;
          ints = ints2;
          closures = clos2;
          tuples = tuples2;
          variants = variants2;
        } =
      Bools.leq bools1 bools2
      && (weak || Ints.leq ints1 ints2)
      && clos_leq clos1 clos2
      && tuples_leq tuples1 tuples2
      && variants_leq variants1 variants2
    and clos_leq c1 c2 =
      match (c1, c2) with
      | _, ClosTop -> true
      | ClosTop, _ -> false
      | Clos (_h1, m1), Clos (_h2, m2) ->
        M.for_all
          (fun t env1 ->
            match M.find_opt t m2 with
            | None -> false
            | Some env2 -> Env.equal leq env1 env2)
          m1
    and tuples_leq om1 om2 =
      match (om1, om2) with
      | _, TuplesTop -> true
      | TuplesTop, Tuples _ -> false
      | Tuples (_h1, m1), Tuples (_h2, m2) ->
        MInt.for_all
          (fun i mopp1 ->
            match MInt.find_opt i m2 with
            | None -> false
            | Some mopp2 ->
              MOPP.for_all
                (fun opp l1 ->
                  match MOPP.find_opt opp mopp2 with
                  | None -> false
                  | Some l2 -> List.for_all2 leq l1 l2)
                mopp1)
          m1
    and variants_leq om1 om2 =
      match (om1, om2) with
      | _, VariantsTop -> true
      | VariantsTop, Variants _ -> false
      | Variants (_h1, m1), Variants (_h2, m2) ->
        MCons.for_all
          (fun c mopp1 ->
            match MCons.find_opt c m2 with
            | None -> false
            | Some mopp2 ->
              MOPP.for_all
                (fun opp args1 ->
                  match MOPP.find_opt opp mopp2 with
                  | None -> false
                  | Some args2 -> variants_args_leq args1 args2)
                mopp1)
          m1
    and variants_args_leq varg1 varg2 =
      match (varg1, varg2) with
      | NakedOnly, (NakedOnly | NakedOrWithArg _) -> true
      | (NakedOrWithArg _ | WithArgOnly _), NakedOnly
      | (NakedOrWithArg _ | NakedOnly), WithArgOnly _ -> false
      | WithArgOnly arg1, (NakedOrWithArg arg2 | WithArgOnly arg2)
      | NakedOrWithArg arg1, NakedOrWithArg arg2 -> leq arg1 arg2
    in
    leq a1 a2

  let leq a1 a2 = leq_gen ~weak:false a1 a2

  let mem_bool b
      { height = _; bools; ints = _; closures = _; tuples = _; variants = _ } =
    Bools.mem b bools

  let get_closures { height = _; bools; ints; closures; tuples; variants } =
    let rem = make ~bools ~ints ~closures:clos_bot ~tuples ~variants in
    match closures with
    | ClosTop -> Some (None, rem)
    | Clos (_h, m) ->
      let l = M.fold (fun (f, _annot) env acc -> (f, env) :: acc) m [] in
      Some (Some l, rem)

  let closures_join_map bot join fclos_top fclos r =
    match r.closures with
    | ClosTop -> fclos_top
    | Clos (_h, m) ->
      M.fold (fun (t, _annot) c acc -> join (fclos t c) acc) m bot

  let rec truncate h ({ height; bools; ints; closures; tuples; variants } as t)
      =
    if height <= h
    then t
    else
      make
        ~bools
        ~ints
        ~closures:(clos_truncate h closures)
        ~tuples:(tuples_truncate h tuples)
        ~variants:(variants_truncate h variants)

  and clos_truncate h c =
    match c with
    | ClosTop -> clos_top
    | Clos (h', m) ->
      if h' <= h
      then c
      else if h <= 0
      then
        if clos_is_bot c
        then c
        else (* Format.eprintf "TRUNCATE ClosTop@."; *)
          clos_top
      else
        let h = h - 1 in
        clos (M.map (Env.map (truncate h)) m)

  and tuples_truncate h om =
    match om with
    | TuplesTop -> tuples_top
    | Tuples (h', m) ->
      if h' <= h
      then om
      else if h <= 0
      then
        if tuples_is_bot om
        then om
        else (* Format.eprintf "TRUNCATE TuplesTop@."; *)
          tuples_top
      else
        let h = h - 1 in
        tuples @@ MInt.map (MOPP.map (List.map (truncate h))) m

  and variants_truncate h om =
    match om with
    | VariantsTop -> variants_top
    | Variants (h', m) ->
      if h' <= h
      then om
      else if h <= 0
      then
        if variants_is_bot om
        then om
        else (* Format.eprintf "TRUNCATE VariantsTop@."; *)
          variants_top
      else
        let h = h - 1 in
        variants @@ MCons.map (MOPP.map (variants_args_truncate h)) m

  and variants_args_truncate h = function
    | NakedOnly as varg -> varg
    | NakedOrWithArg v -> NakedOrWithArg (truncate h v)
    | WithArgOnly v -> WithArgOnly (truncate h v)

  let rec widen_aux h
      {
        height = _;
        bools = bools1;
        ints = ints1;
        closures = cs1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        height = _;
        bools = bools2;
        ints = ints2;
        closures = cs2;
        tuples = tuples2;
        variants = variants2;
      } =
    make
      ~bools:(Bools.widen bools1 bools2)
      ~ints:(Ints.widen ints1 ints2)
      ~closures:(clos_widen_aux h cs1 cs2)
      ~tuples:(tuples_widen_aux h tuples1 tuples2)
      ~variants:(variants_widen_aux h variants1 variants2)

  and clos_widen_aux h c1 c2 =
    match (c1, c2) with
    | ClosTop, _ | _, ClosTop -> clos_top
    | Clos (_, m1), Clos (_, m2) ->
      if h <= 0
      then if clos_is_bot c2 then c1 else clos_top
      else
        let m =
          M.merge
            (fun _t oenv1 oenv2 ->
              match (oenv1, oenv2) with
              | Some _, None -> oenv1
              | None, Some env2 -> Some (Env.map (truncate (h - 1)) env2)
              | Some env1, Some env2 ->
                Some
                  (Env.union
                     (fun _x cs1 cs2 -> Some (widen_aux (h - 1) cs1 cs2))
                     env1
                     env2)
              | None, None -> assert false)
            m1
            m2
        in
        clos m

  and tuples_widen_aux h om1 om2 =
    match (om1, om2) with
    | TuplesTop, _ | _, TuplesTop -> tuples_top
    | Tuples (_, m1), Tuples (_, m2) ->
      if h <= 0
      then if tuples_is_bot om2 then om1 else tuples_top
      else
        let m =
          MInt.merge
            (fun _i omopp1 omopp2 ->
              match (omopp1, omopp2) with
              | Some _, None -> omopp1
              | None, Some mopp2 ->
                Some (MOPP.map (List.map (truncate (h - 1))) mopp2)
              | Some mopp1, Some mopp2 ->
                Some
                  (MOPP.merge
                     (fun _mopp ol1 ol2 ->
                       match (ol1, ol2) with
                       | Some _, None -> ol1
                       | None, Some l2 -> Some (List.map (truncate (h - 1)) l2)
                       | Some l1, Some l2 ->
                         Some
                           (List.map2
                              (fun v1 v2 -> widen_aux (h - 1) v1 v2)
                              l1
                              l2)
                       | None, None -> assert false)
                     mopp1
                     mopp2)
              | None, None -> assert false)
            m1
            m2
        in
        tuples m

  and variants_widen_aux h om1 om2 =
    match (om1, om2) with
    | VariantsTop, _ | _, VariantsTop -> variants_top
    | Variants (_, m1), Variants (_, m2) ->
      if h <= 0
      then if variants_is_bot om2 then om1 else variants_top
      else
        let m =
          MCons.merge
            (fun _c omopp1 omopp2 ->
              match (omopp1, omopp2) with
              | Some _, None -> omopp1
              | None, Some mopp2 ->
                Some (MOPP.map (variants_args_truncate (h - 1)) mopp2)
              | Some mopp1, Some mopp2 ->
                Some
                  (MOPP.merge
                     (fun _opp oargs1 oargs2 ->
                       match (oargs1, oargs2) with
                       | Some _, None -> oargs1
                       | None, Some args2 ->
                         Some (variants_args_truncate (h - 1) args2)
                       | Some args1, Some args2 ->
                         Some (variants_args_widen_aux (h - 1) args1 args2)
                       | None, None -> assert false)
                     mopp1
                     mopp2)
              | None, None -> assert false)
            m1
            m2
        in
        variants m

  and variants_args_widen_aux h varg1 varg2 =
    match (varg1, varg2) with
    | NakedOnly, NakedOnly -> NakedOnly
    | NakedOnly, (NakedOrWithArg v | WithArgOnly v)
    | (NakedOrWithArg v | WithArgOnly v), NakedOnly -> NakedOrWithArg v
    | WithArgOnly v1, WithArgOnly v2 -> WithArgOnly (widen_aux h v1 v2)
    | NakedOrWithArg v1, WithArgOnly v2
    | WithArgOnly v1, NakedOrWithArg v2
    | NakedOrWithArg v1, NakedOrWithArg v2 -> NakedOrWithArg (widen_aux h v1 v2)

  let widen r1 r2 = widen_aux (height r1) r1 r2

  (* Two abstract values are similar if their heights differ by at
     least 2, or if they have comparable shapes *)
  let sim r1 r2 =
    let n1 = height r1
    and n2 = height r2 in
    abs (n1 - n2) >= Options.sim_threshold || leq_gen ~weak:true r1 r2
end
