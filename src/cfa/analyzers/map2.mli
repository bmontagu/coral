(** [Make(X1)(X2)] creates a module of maps whose keys are of type
   [X1.t * X2.t] and whose implementation is based on the nesting of
   maps [Map.Make(X1)] and [Map.Make(X2)]. *)
module Make (X1 : sig
  include Map.OrderedType

  val pp : Format.formatter -> t -> unit
end) (X2 : sig
  include Map.OrderedType

  val pp : Format.formatter -> t -> unit
end) : sig
  type key = X1.t * X2.t
  type 'a t

  val empty : 'a t
  val find_opt : key -> 'a t -> 'a option
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val cardinal : 'a t -> int

  val fold_similar :
    (key -> 'a -> 'b -> 'b) -> (X2.t -> X2.t -> bool) -> key -> 'a t -> 'b -> 'b
  (** [fold_similar f sim2 (k1, k2) m acc] folds the function [f] on
     the map [m] with accumulator [acc] only for the keys [(k1', k2')]
     in [m] such that [k1' = k1] and [sim2 k2 k2' = true]. *)

  val to_seq : 'a t -> (key * 'a) Seq.t
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end
