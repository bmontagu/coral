(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Lang.Ast

(** Abstract interface for abstract domains for the Nabla analyzers *)
module type ABS_DOMAIN = sig
  type ints
  type bools
  type annot
  type t

  val compare : t -> t -> int
  val bot : t
  val is_bot : t -> bool
  val top : t
  val singleton_closure : annot option -> NablaFun.t -> t Env.t -> t
  val is_fun : t -> bool
  val ints : ints -> t
  val is_int : t -> bool
  val get_ints : t -> ints * t
  val bools : bools -> t
  val get_bools : t -> bools * t
  val is_bool : t -> bool
  val mem_bool : bool -> t -> bool
  val singleton_tuple : PP.t option -> int -> t list -> t

  val get_tuple : int -> t -> ((PP.t option * t list) list * t) option
  (** [get_tuple n v] either returns [None] if [v] contains no record of
   width [n], or returns [Some (vss, rem)] where [vss] is an
   association list between optional program points (that denote de
   creation points of the [n]-ary tuples) and the list of its
   arguments [vs]. [vs] is a list of length [n] that contains the
   possible arguments of the tuple. The remainder [rem] is the
   abstract value for of all the remaining possible values in [v]. *)

  val singleton_variant : PP.t option -> Cons.t -> t option -> t
  val get_variant_naked : Cons.t -> t -> (PP.t option list * t) option
  val get_variant_with_arg : Cons.t -> t -> ((PP.t option * t) list * t) option
  val leq : t -> t -> bool
  val join : t -> t -> t
  val get_closures : t -> ((NablaFun.t * t Env.t) list option * t) option

  val closures_join_map :
    'a -> ('a -> 'a -> 'a) -> 'a -> (NablaFun.t -> t Env.t -> 'a) -> t -> 'a

  val widen : t -> t -> t
  val meet : t -> t -> t
  val sim : t -> t -> bool
  val pp : Format.formatter -> t -> unit
end

(** Generic functor that implements a Nabla analyzis *)
module Analyze
    (_ : Logs.S)
    (_ : sig
      val refine_branches : bool
      val branch_is_context : bool
      val let_is_context : bool
      val finer_context : bool
      val no_env_restrict : bool
      val kill_unreachable : bool
      val record_closure_creation_context : bool
      val record_tuple_creation_location : bool
      val record_variant_creation_location : bool
      val cache_non_calls : bool
      val cache_widened_calls_only : bool
      val no_alarms : bool
      val disprove_alarms : bool
      val debug : bool
    end)
    (_ : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (Calls : Calls.S)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (_ : ABS_DOMAIN
           with type bools := Bools.t
            and type ints := Ints.t
            and type annot := Calls.t) : sig
  module Abs : sig
    type t

    val get_alarms : t -> (PP.t * string) list
    val pp : Format.formatter -> t -> unit
  end

  module Entry : sig
    type t

    val pp : Format.formatter -> t -> unit
  end

  val eval : Lang.Ast.Term.t -> Abs.t * Entry.t Memo.Statistics.t
end
