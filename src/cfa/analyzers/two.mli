(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** The two-point lattice *)

type t

val compare : t -> t -> int
val bot : t
val top : t
val is_bot : t -> bool
val is_top : t -> bool
val leq : t -> t -> bool
val join : t -> t -> t
val widen : t -> t -> t
val meet : t -> t -> t
