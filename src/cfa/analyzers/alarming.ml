(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

open Lang.Ast

module Make (Options : sig
  val no_alarms : bool
end) (Calls : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (VAbs : sig
  type t

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val meet : t -> t -> t
end) =
struct
  module Alarm = struct
    type t = {
      non_functional: bool;
      non_numeric: bool;
      non_boolean: bool;
      non_exhaustive_match: bool;
      value: VAbs.t;
    }

    let bot =
      {
        non_functional = false;
        non_numeric = false;
        non_boolean = false;
        non_exhaustive_match = false;
        value = VAbs.bot;
      }

    let is_bot
        {
          non_functional;
          non_numeric;
          non_boolean;
          non_exhaustive_match;
          value;
        } =
      (not non_functional)
      && (not non_numeric)
      && (not non_boolean)
      && (not non_exhaustive_match)
      && VAbs.is_bot value

    let non_boolean v = { bot with non_boolean = true; value = v }
    let non_numeric v = { bot with non_numeric = true; value = v }
    let non_functional v = { bot with non_functional = true; value = v }

    let non_exhaustive_match v =
      { bot with non_exhaustive_match = true; value = v }

    let bool_leq b1 b2 = (not b1) || b2
    let bool_join b1 b2 = b1 || b2
    let bool_meet b1 b2 = b1 && b2

    let leq
        {
          non_functional = nf1;
          non_numeric = nn1;
          non_boolean = nb1;
          non_exhaustive_match = nem1;
          value = v1;
        }
        {
          non_functional = nf2;
          non_numeric = nn2;
          non_boolean = nb2;
          non_exhaustive_match = nem2;
          value = v2;
        } =
      bool_leq nf1 nf2
      && bool_leq nn1 nn2
      && bool_leq nb1 nb2
      && bool_leq nem1 nem2
      && VAbs.leq v1 v2

    let join_gen join
        {
          non_functional = nf1;
          non_numeric = nn1;
          non_boolean = nb1;
          non_exhaustive_match = nem1;
          value = v1;
        }
        {
          non_functional = nf2;
          non_numeric = nn2;
          non_boolean = nb2;
          non_exhaustive_match = nem2;
          value = v2;
        } =
      {
        non_functional = bool_join nf1 nf2;
        non_numeric = bool_join nn1 nn2;
        non_boolean = bool_join nb1 nb2;
        non_exhaustive_match = bool_join nem1 nem2;
        value = join v1 v2;
      }

    let join = join_gen VAbs.join
    let widen = join_gen VAbs.widen

    let meet
        {
          non_functional = nf1;
          non_numeric = nn1;
          non_boolean = nb1;
          non_exhaustive_match = nem1;
          value = v1;
        }
        {
          non_functional = nf2;
          non_numeric = nn2;
          non_boolean = nb2;
          non_exhaustive_match = nem2;
          value = v2;
        } =
      {
        non_functional = bool_meet nf1 nf2;
        non_numeric = bool_meet nn1 nn2;
        non_boolean = bool_meet nb1 nb2;
        non_exhaustive_match = bool_meet nem1 nem2;
        value = VAbs.meet v1 v2;
      }

    let pp fmt
        {
          non_boolean;
          non_numeric;
          non_functional;
          non_exhaustive_match;
          value;
        } =
      if non_boolean
      then
        Format.fprintf fmt "ALARM: a boolean was expected for this expression@.";
      if non_numeric
      then
        Format.fprintf
          fmt
          "ALARM: an integer was expected for this expression@.";
      if non_functional
      then
        Format.fprintf
          fmt
          "ALARM: a function was expected for this expression@.";
      if non_exhaustive_match
      then Format.fprintf fmt "@[ALARM: this match might not be exhaustive@.";
      Format.fprintf
        fmt
        "The following abstract value was inferred for this expression:@.@[%a@]"
        VAbs.pp
        value
  end

  module Alarms = struct
    module Ctxt = struct
      type t = PP.t * Calls.t [@@deriving ord]
    end

    module M = Map.Make (Ctxt)

    type t = Alarm.t M.t

    let bindings m =
      let module PPs = Map.Make (PP) in
      M.fold
        (fun (pp, ctxt) a acc ->
          PPs.update
            pp
            (function
              | None -> Some [(ctxt, a)]
              | Some l -> Some ((ctxt, a) :: l))
            acc)
        m
        PPs.empty
      |> PPs.bindings

    let bot = M.empty
    let is_bot = M.is_empty
    let singleton pp calls a = M.singleton (pp, calls) a

    let leq m1 m2 =
      M.for_all
        (fun ctxt a1 ->
          match M.find_opt ctxt m2 with
          | None -> false
          | Some a2 -> Alarm.leq a1 a2)
        m1

    let join_gen join m1 m2 =
      M.union (fun _ctxt a1 a2 -> Some (join a1 a2)) m1 m2

    let join = join_gen Alarm.join
    let widen = join_gen Alarm.widen

    let meet m1 m2 =
      M.merge
        (fun _ o1 o2 ->
          match (o1, o2) with
          | None, _ | _, None -> None
          | Some v1, Some v2 ->
            let v12 = Alarm.meet v1 v2 in
            if Alarm.is_bot v12 then None else Some v12)
        m1
        m2

    module PPs = Set.Make (PP)

    let count_program_points_in_alarms m =
      PPs.cardinal @@ M.fold (fun (loc, _) _ acc -> PPs.add loc acc) m PPs.empty

    let pp fmt m =
      let open Format in
      pp_print_list
        ~pp_sep:(fun fmt () -> fprintf fmt "@.")
        (fun fmt ((loc, call), alarm) ->
          fprintf
            fmt
            "@[%a@]@.@[In the calling context:@ %a@]@.@[%a@]@."
            PP.pp_long
            loc
            Calls.pp
            call
            Alarm.pp
            alarm)
        fmt
        (M.bindings m);
      let nb_alarms = M.cardinal m
      and nb_program_points = count_program_points_in_alarms m in
      fprintf
        fmt
        "@.TOTAL: %i alarm%s raised, for %i program point%s.@."
        nb_alarms
        (if nb_alarms <= 1 then " was" else "s were")
        nb_program_points
        (if nb_program_points <= 1 then "" else "s")
  end

  type 'a m = {
    values: 'a;
    alarms: Alarms.t;
  }

  type t = VAbs.t m

  let pp fmt { values; alarms } =
    if Alarms.is_bot alarms
    then VAbs.pp fmt values
    else Format.fprintf fmt "%a@.%a" Alarms.pp alarms VAbs.pp values

  let bot = { values = VAbs.bot; alarms = Alarms.bot }
  let is_bot { values; alarms } = VAbs.is_bot values && Alarms.is_bot alarms

  let leq { values = v1; alarms = a1 } { values = v2; alarms = a2 } =
    VAbs.leq v1 v2 && Alarms.leq a1 a2

  let join_gen vjoin ajoin { values = v1; alarms = a1 }
      { values = v2; alarms = a2 } =
    { values = vjoin v1 v2; alarms = ajoin a1 a2 }

  let join = join_gen VAbs.join Alarms.join
  let widen = join_gen VAbs.widen Alarms.widen

  let meet { values = v1; alarms = a1 } { values = v2; alarms = a2 } =
    { values = VAbs.meet v1 v2; alarms = Alarms.meet a1 a2 }

  let make alarms values = { alarms; values }

  let alarm pp calls a =
    if Options.no_alarms
    then { values = (); alarms = Alarms.bot }
    else { values = (); alarms = Alarms.singleton pp calls a }

  let pp_warns fmt warns =
    let open Format in
    pp_print_list
      (fun fmt (ctxt, a) ->
        fprintf
          fmt
          "@[In the calling context:@ %a@]@.@[%a@]@."
          Calls.pp
          ctxt
          Alarm.pp
          a)
      fmt
      warns

  let get_alarms a =
    a.alarms
    |> Alarms.bindings
    |> List.map (fun (pp, warns) ->
           let msg =
             let open Format in
             asprintf "%a" pp_warns warns
           in
           (pp, msg))

  let return v = { bot with values = v }

  let bind { values = v1; alarms = a1 } f =
    let { values = v2; alarms = a2 } = f v1 in
    { values = v2; alarms = Alarms.join a1 a2 }

  let map { values; alarms } f = { values = f values; alarms }

  let plus { values = v1; alarms = a1 } { values = v2; alarms = a2 } =
    { values = (v1, v2); alarms = Alarms.join a1 a2 }

  let ( let* ) = bind
  let ( let+ ) = map
  let ( and+ ) = plus
  let run { values; alarms } = (alarms, values)
end
