(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

(** Abstract domain for closures and values, with recursive knots. *)

let debug_shrinking = false

(* ensure that we effectively use comparison on integers, and not
   polymorphic comparisons *)
let ( = ) : int -> int -> bool = ( = )
let ( <> ) : int -> int -> bool = ( <> )
let ( <= ) : int -> int -> bool = ( <= )
let ( < ) : int -> int -> bool = ( < )
let max : int -> int -> int = Int.max
let min : int -> int -> int = Int.min
let ( <=> ) = Bool.equal

let list_is_empty = function
  | [] -> true
  | _ -> false

open Lang.Ast

module type OrderedHashedTyped = sig
  type t

  val compare : t -> t -> int
  val hash_fold : t -> Hash.state -> Hash.state
end

module MInts = Ptmap
(** Maps indexed by integers *)

(** Sets of integers *)
module SInts = struct
  include Ptset

  (* TODO: remove once this is implemented in the Ptset library *)
  let to_seq t = fold Seq.cons t Seq.empty
end

(** Pairs of integers *)
module Ints2 : Map.OrderedType with type t = int * int = struct
  type t = int * int [@@deriving ord]
end

module SInts2 = Set.Make (Ints2)
(** Sets of pairs of integers *)

module MInts2 = Map.Make (Ints2)
(** Maps indexed by pairs of integers *)

(** Options of program points *)
module OPP = struct
  type t = PP.t option [@@deriving ord]

  let hash_fold t s =
    match t with
    | None -> Hash.fold 0 s
    | Some pp -> Hash.fold 1 @@ PP.hash_fold pp s
end

module MOPP = Map.Make (OPP)
(** Maps indexed by options of program points *)

module MCons = Map.Make (Cons)
(** Maps indexed by variant constructors *)

(** Domain of booleans where the default (bottom) value is [true] *)
module BoolDomainDefaultTrue = struct
  type t = bool

  let bot = true
  let leq b1 b2 = b1 || not b2
  let widen b1 b2 = b1 && b2
end

(** Whether to perform extra checking for wellformedness *)
let check = true

module AbsDomain
    (Options : sig
      val minimize : bool
      val occurs_threshold : int
      val sim_threshold : int
    end)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (Annot : OrderedHashedTyped)
    () =
struct
  module FunAnnot = struct
    type t = NablaFun.t * Annot.t option [@@deriving ord]

    let hash_fold (f, oannot) s =
      NablaFun.hash_fold f
      @@
      match oannot with
      | None -> Hash.fold 0 s
      | Some annot -> Hash.fold 1 @@ Annot.hash_fold annot s
  end

  module M = Map.Make (FunAnnot)

  type t = raw_t Hcons.hash_consed
  (** The type definition for abstract values. The encoding follows
     the locally-nameless style of binders. *)

  and raw_t =
    | Abs of {
        height: int;
        bools: Bools.t;
        ints: Ints.t;
        closures: closures;
        tuples: tuples;
        variants: variants;
      }
    | Mu of int (* height *) * t  (** recursive knot *)
    | Var of int  (** free variable, where the provided integer is a name *)
    | Idx of int
        (** bound variable, where the provided integer is a de Bruijn index *)

  and closures =
    | Clos of int * (* height *) t Env.t M.t
    | ClosTop

  and tuples =
    | Tuples of int * (* height *) t list MOPP.t MInts.t
    | TuplesTop

  and variants =
    | Variants of int * (* height *) t variant_args MOPP.t MCons.t
    | VariantsTop

  and 'a variant_args =
    | NakedOnly
    | WithArgOnly of 'a
    | NakedOrWithArg of 'a

  module T = struct
    type nonrec t = t

    let compare t1 t2 = Int.compare (Hcons.tag t1) (Hcons.tag t2)
    let equal = ( == )
    let hash_fold t s = Hash.fold (Hcons.tag t) s
    let hash t = Hash.(finalize @@ hash_fold t seed)
  end

  let compare = T.compare

  (** Memoization helpers *)
  module Memoize = struct
    let memo_int f =
      let m = ref Ptmap.empty in
      fun x ->
        match Ptmap.find x !m with
        | v -> v
        | exception Not_found ->
          let v = f x in
          m := Ptmap.add x v !m;
          v

    module HT = Hashtbl.Make (T)

    let memo f =
      let h = HT.create 127 in
      fun x ->
        match HT.find h x with
        | v -> v
        | exception Not_found ->
          let v = f x in
          HT.add h x v;
          v

    module T2 = struct
      type nonrec t = t * t

      let equal (t1, t2) (t3, t4) = T.equal t1 t3 && T.equal t2 t4

      let hash_fold (t1, t2) s =
        Hash.fold (Hcons.tag t1) @@ Hash.fold (Hcons.tag t2) s

      let hash t = Hash.finalize @@ hash_fold t Hash.seed
    end

    module HT2 = Hashtbl.Make (T2)

    let memo2 f =
      let h = HT2.create 127 in
      fun x1 x2 ->
        let p = (x1, x2) in
        match HT2.find h p with
        | v -> v
        | exception Not_found ->
          let v = f x1 x2 in
          HT2.add h p v;
          v

    module IntT = struct
      type nonrec t = int * t

      let equal (n1, t1) (n2, t2) = Int.equal n1 n2 && T.equal t1 t2
      let hash_fold (n, t) s = Hash.fold n @@ Hash.fold (Hcons.tag t) s
      let hash t = Hash.finalize @@ hash_fold t Hash.seed
    end

    module HIntT = Hashtbl.Make (IntT)

    let memo_int_t f =
      let h = HIntT.create 127 in
      fun i x ->
        let p = (i, x) in
        match HIntT.find h p with
        | v -> v
        | exception Not_found ->
          let v = f i x in
          HIntT.add h p v;
          v

    module SIntT = Hashset.Make (IntT)

    let memo_rec_int_t_unit func =
      let s = SIntT.create 127 in
      let rec f i x =
        let p = (i, x) in
        if SIntT.mem s p
        then ()
        else begin
          func f i x;
          SIntT.add s p
        end
      in
      f
  end

  module H : sig
    val build : raw_t -> t
  end = struct
    module T = struct
      type t = raw_t

      let closures_equal c1 c2 =
        c1 == c2
        ||
        match (c1, c2) with
        | ClosTop, ClosTop -> true
        | Clos (_, m1), Clos (_, m2) ->
          m1 == m2 || M.equal (fun e1 e2 -> Env.equal ( == ) e1 e2) m1 m2
        | _, _ -> false

      let tuples_equal t1 t2 =
        t1 == t2
        ||
        match (t1, t2) with
        | TuplesTop, TuplesTop -> true
        | Tuples (_, m1), Tuples (_, m2) ->
          m1 == m2
          || MInts.equal
               (fun mopp1 mopp2 ->
                 MOPP.equal (fun l1 l2 -> List.equal ( == ) l1 l2) mopp1 mopp2)
               m1
               m2
        | _, _ -> false

      let variants_equal v1 v2 =
        v1 == v2
        ||
        match (v1, v2) with
        | VariantsTop, VariantsTop -> true
        | Variants (_, m1), Variants (_, m2) ->
          m1 == m2
          || MCons.equal
               (fun mopp1 mopp2 ->
                 MOPP.equal
                   (fun varg1 varg2 ->
                     varg1 == varg2
                     ||
                     match (varg1, varg2) with
                     | NakedOnly, NakedOnly -> true
                     | WithArgOnly t1, WithArgOnly t2
                     | NakedOrWithArg t1, NakedOrWithArg t2 -> t1 == t2
                     | _, _ -> false)
                   mopp1
                   mopp2)
               m1
               m2
        | _, _ -> false

      let equal t1 t2 =
        match (t1, t2) with
        | ( Abs
              {
                height = _;
                bools = b1;
                ints = i1;
                closures = c1;
                tuples = t1;
                variants = v1;
              },
            Abs
              {
                height = _;
                bools = b2;
                ints = i2;
                closures = c2;
                tuples = t2;
                variants = v2;
              } ) ->
          Bools.equal b1 b2
          && Ints.equal i1 i2
          && closures_equal c1 c2
          && tuples_equal t1 t2
          && variants_equal v1 v2
        | Mu (_, t1), Mu (_, t2) -> t1 == t2
        | Var x1, Var x2 -> x1 = x2
        | Idx i1, Idx i2 -> i1 = i2
        | _, _ -> false

      let closures_hash_fold c acc =
        match c with
        | ClosTop -> Hash.fold 0 acc
        | Clos (_, m) ->
          Hash.(
            fold 1
            @@ M.fold
                 (fun term env acc ->
                   FunAnnot.hash_fold term
                   @@ Env.fold
                        (fun x t acc ->
                          Var.hash_fold x @@ fold (Hcons.tag t) acc)
                        env
                        acc)
                 m
                 acc)

      let tuples_hash_fold t acc =
        match t with
        | TuplesTop -> Hash.fold 0 acc
        | Tuples (_, m) ->
          Hash.(
            fold 1
            @@ MInts.fold
                 (fun i mopp acc ->
                   fold i
                   @@ MOPP.fold
                        (fun opp ts acc ->
                          OPP.hash_fold opp
                          @@ List.fold_left
                               (fun acc t -> fold (Hcons.tag t) acc)
                               acc
                               ts)
                        mopp
                        acc)
                 m
                 acc)

      let variants_hash_fold t acc =
        match t with
        | VariantsTop -> Hash.fold 0 acc
        | Variants (_, m) ->
          Hash.(
            fold 1
            @@ MCons.fold
                 (fun c mopp acc ->
                   Cons.hash_fold c
                   @@ MOPP.fold
                        (fun opp varg acc ->
                          OPP.hash_fold opp
                          @@
                          match varg with
                          | NakedOnly -> fold 0 acc
                          | WithArgOnly t -> fold 1 @@ fold (Hcons.tag t) acc
                          | NakedOrWithArg t -> fold 2 @@ fold (Hcons.tag t) acc)
                        mopp
                        acc)
                 m
                 acc)

      let hash t =
        Hash.finalize
        @@
        match t with
        | Abs { height = _; bools; ints; closures; tuples; variants } ->
          Hash.(
            fold 0
            @@ Bools.hash_fold bools
            @@ Ints.hash_fold ints
            @@ closures_hash_fold closures
            @@ tuples_hash_fold tuples
            @@ variants_hash_fold variants seed)
        | Mu (_, t) -> Hash.(fold 1 @@ fold (Hcons.tag t) seed)
        | Var x -> Hash.(fold 2 @@ fold x seed)
        | Idx i -> Hash.(fold 3 @@ fold i seed)
    end

    include Hcons.Make (T) ()

    let hcons_tbl = create 127
    let build v = hashcons hcons_tbl v
  end

  let build = H.build

  let height_clos = function
    | Clos (h, _) -> h
    | ClosTop -> 0

  let height_tuples = function
    | Tuples (h, _) -> h
    | TuplesTop -> 0

  let height_variants = function
    | Variants (h, _) -> h
    | VariantsTop -> 0

  let height t =
    match Hcons.node t with
    | Abs
        { height; bools = _; ints = _; closures = _; tuples = _; variants = _ }
    | Mu (height, _) -> height
    | Var _ | Idx _ -> 0

  let clos_is_bot = function
    | Clos (_, m) -> M.is_empty m
    | ClosTop -> false

  let tuples_is_bot = function
    | Tuples (_h, m) -> MInts.is_empty m
    | TuplesTop -> false

  let variants_is_bot = function
    | Variants (_, m) -> MCons.is_empty m
    | VariantsTop -> false

  let is_bot t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants } ->
      Bools.is_bot bools
      && Ints.is_bot ints
      && clos_is_bot closures
      && tuples_is_bot tuples
      && variants_is_bot variants
    | Mu _ | Var _ | Idx _ -> false

  let clos_is_top = function
    | ClosTop -> true
    | Clos _ -> false

  let tuples_is_top = function
    | Tuples _ -> false
    | TuplesTop -> true

  let variants_is_top = function
    | Variants _ -> false
    | VariantsTop -> true

  let is_top t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants } ->
      Bools.is_top bools
      && Ints.is_top ints
      && clos_is_top closures
      && tuples_is_top tuples
      && variants_is_top variants
    | Mu _ | Var _ | Idx _ -> false

  let rec is_bool t =
    match Hcons.node t with
    | Abs { height = _; bools = _; ints; closures; tuples; variants } ->
      Ints.is_bot ints
      && clos_is_bot closures
      && tuples_is_bot tuples
      && variants_is_bot variants
    | Mu (_, t) -> is_bool t
    | Var _ | Idx _ -> assert false

  let rec is_int t =
    match Hcons.node t with
    | Abs { height = _; bools; ints = _; closures; tuples; variants } ->
      Bools.is_bot bools
      && clos_is_bot closures
      && tuples_is_bot tuples
      && variants_is_bot variants
    | Mu (_, t) -> is_int t
    | Var _ | Idx _ -> assert false

  let rec is_fun t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures = _; tuples; variants } ->
      Bools.is_bot bools
      && Ints.is_bot ints
      && tuples_is_bot tuples
      && variants_is_bot variants
    | Mu (_, t) -> is_fun t
    | Var _ | Idx _ -> assert false

  let normalize_clos_map m =
    M.filter (fun _f env -> Env.for_all (fun _x v -> not @@ is_bot v) env) m

  let compute_clos_height m =
    if M.is_empty m
    then 0
    else
      1
      + M.fold
          (fun _ env acc ->
            Env.fold (fun _x tx acc -> max (height tx) acc) env acc)
          m
          0

  let clos m =
    let m = normalize_clos_map m in
    let h = compute_clos_height m in
    Clos (h, m)

  let normalize_tuples_map m =
    MInts.filter_map
      (fun _i mopp ->
        let mopp =
          MOPP.filter
            (fun _opp l -> List.for_all (fun v -> not @@ is_bot v) l)
            mopp
        in
        if MOPP.is_empty mopp then None else Some mopp)
      m

  let compute_tuples_height m =
    if MInts.is_empty m
    then 0
    else
      1
      + MInts.fold
          (fun _i mopp acc ->
            MOPP.fold
              (fun _opp l acc ->
                max acc (List.fold_left (fun h v -> max h (height v)) 0 l))
              mopp
              acc)
          m
          0

  let tuples m =
    let m = normalize_tuples_map m in
    let h = compute_tuples_height m in
    Tuples (h, m)

  let normalize_variants_map m =
    MCons.filter_map
      (fun _c mopp ->
        let mopp =
          MOPP.filter
            (fun _opp varg ->
              match varg with
              | NakedOnly -> true
              | WithArgOnly arg -> not @@ is_bot arg
              | NakedOrWithArg _ -> true)
            mopp
        in
        if MOPP.is_empty mopp then None else Some mopp)
      m

  let compute_variants_height m =
    if MCons.is_empty m
    then 0
    else
      1
      + MCons.fold
          (fun _c mopp acc ->
            MOPP.fold
              (fun _opp varg acc ->
                match varg with
                | NakedOnly -> acc
                | WithArgOnly arg | NakedOrWithArg arg -> max (height arg) acc)
              mopp
              acc)
          m
          0

  let variants m =
    let m = normalize_variants_map m in
    let h = compute_variants_height m in
    Variants (h, m)

  let make ~bools ~ints ~closures ~tuples ~variants =
    build
    @@ Abs
         {
           height =
             max (height_clos closures)
             @@ max (height_tuples tuples) (height_variants variants);
           bools;
           ints;
           closures;
           tuples;
           variants;
         }

  let clos_bot = Clos (0, M.empty)
  let clos_top = ClosTop
  let tuples_bot = Tuples (0, MInts.empty)
  let tuples_top = TuplesTop
  let variants_bot = Variants (0, MCons.empty)
  let variants_top = VariantsTop

  let bot =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let top =
    make
      ~bools:Bools.top
      ~ints:Ints.top
      ~closures:clos_top
      ~tuples:tuples_top
      ~variants:variants_top

  (** Transforms an integer into a lowercase letter, possibly with an
     integer suffix *)
  let int_to_char i = (Char.chr (Char.code 'a' + (i mod 26)), i / 26)

  let pp_int_var prefix fmt x =
    let c, n = int_to_char x in
    if n = 0
    then Format.fprintf fmt "%s%c" prefix c
    else Format.fprintf fmt "%s%c%i" prefix c n

  let pp fmt v =
    let n = ref 0 in
    let rec pp env fmt v =
      if is_bot v
      then Format.fprintf fmt "⊥"
      else if is_top v
      then Format.fprintf fmt "⊤"
      else
        match Hcons.node v with
        | Abs { height = _; bools; ints; closures; tuples; variants } ->
          let open Format in
          fprintf fmt "@[<hv 0>{@[<hv 1>";
          if not @@ Bools.is_bot bools
          then fprintf fmt "@ @[bools =@ %a@]" Bools.pp bools;
          if not @@ Ints.is_bot ints
          then fprintf fmt "@ @[ints =@ %a@]" Ints.pp ints;
          if not @@ clos_is_bot closures
          then fprintf fmt "@ @[closures =@ %a@]" (pp_closures env) closures;
          if not @@ tuples_is_bot tuples
          then fprintf fmt "@ @[tuples =@ %a@]" (pp_tuples env) tuples;
          if not @@ variants_is_bot variants
          then fprintf fmt "@ @[variants =@ %a@]" (pp_variants env) variants;
          fprintf fmt "@]@ }@]"
        | Var x -> pp_int_var "_" fmt x
        | Idx n -> Format.pp_print_string fmt (Ralist.nth env n)
        | Mu (_h, t) ->
          let var = Format.asprintf "%a" (pp_int_var "'") !n in
          incr n;
          Format.fprintf fmt "@[%a@ as %s@]" (pp (Ralist.cons var env)) t var;
          decr n
    and pp_closures env fmt = function
      | ClosTop -> Format.pp_print_string fmt "⊤"
      | Clos (_h, m) ->
        if M.is_empty m
        then Format.pp_print_string fmt "∅"
        else
          Format.fprintf
            fmt
            "@[<hv 0>{@[<hv 1> %a@]@ }@]"
            (Format.pp_print_list
               ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
               (fun fmt ((x, _annot), mx) ->
                 Format.fprintf
                   fmt
                   "@[%a@ %a@]"
                   (NablaFun.pp_rec true)
                   x
                   (Env.pp (pp env))
                   mx))
            (M.bindings m)
    and pp_tuples env fmt = function
      | TuplesTop -> Format.pp_print_string fmt "⊤"
      | Tuples (_h, m) ->
        if MInts.is_empty m
        then Format.pp_print_string fmt "∅"
        else
          Format.fprintf
            fmt
            "@[<hv 0>{@[<hv 1> %a@]@ }@]"
            (Format.pp_print_list
               ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
               (fun fmt (i, mopp) ->
                 Format.pp_print_list
                   ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
                   (fun fmt (_opp, li) ->
                     if list_is_empty li
                     then Format.fprintf fmt "%i: ()" i
                     else
                       Format.fprintf
                         fmt
                         "@[%i:@ %a@]"
                         i
                         (Format.pp_print_list
                            ~pp_sep:(fun fmt () -> Format.fprintf fmt "@ × ")
                            (pp env))
                         li)
                   fmt
                   (MOPP.bindings mopp)))
            (MInts.bindings m)
    and pp_variants env fmt = function
      | VariantsTop -> Format.pp_print_string fmt "⊤"
      | Variants (_h, m) ->
        if MCons.is_empty m
        then Format.pp_print_string fmt "∅"
        else
          Format.fprintf
            fmt
            "@[<hv 0>{@[<hv 1> %a@]@ }@]"
            (Format.pp_print_list
               ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
               (fun fmt (c, mopp) ->
                 Format.pp_print_list
                   ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
                   (fun fmt (_opp, varg) ->
                     Format.fprintf fmt "@[";
                     begin
                       match varg with
                       | NakedOnly -> Format.fprintf fmt "@[%a:@ .@]" Cons.pp c
                       | WithArgOnly arg ->
                         if not @@ is_bot arg
                         then
                           Format.fprintf
                             fmt
                             "@[%a:@ %a@]"
                             Cons.pp
                             c
                             (pp env)
                             arg
                       | NakedOrWithArg arg ->
                         Format.fprintf fmt "@[%a:@ .@]" Cons.pp c;
                         if not @@ is_bot arg
                         then
                           Format.fprintf
                             fmt
                             "@[%a:@ %a@]"
                             Cons.pp
                             c
                             (pp env)
                             arg
                     end;
                     Format.fprintf fmt "@]")
                   fmt
                   (MOPP.bindings mopp)))
            (MCons.bindings m)
    in
    pp Ralist.nil fmt v

  (** [is_free_var x t] tells whether [x] is a free variable in [t] *)
  let rec is_free_var x t =
    match Hcons.node t with
    | Idx _ -> false
    | Var y -> x = y
    | Mu (_, t) -> is_free_var x t
    | Abs { height = _; bools = _; ints = _; closures; tuples; variants } ->
      clos_is_free_var x closures
      || tuples_is_free_var x tuples
      || variants_is_free_var x variants

  and clos_is_free_var x = function
    | ClosTop -> false
    | Clos (_, m) ->
      M.exists (fun _ env -> Env.exists (fun _ t -> is_free_var x t) env) m

  and tuples_is_free_var x = function
    | TuplesTop -> false
    | Tuples (_, m) ->
      MInts.exists
        (fun _ mopp ->
          MOPP.exists (fun _ l -> List.exists (fun t -> is_free_var x t) l) mopp)
        m

  and variants_is_free_var x = function
    | VariantsTop -> false
    | Variants (_, m) ->
      MCons.exists
        (fun _ mopp ->
          MOPP.exists (fun _ args -> variants_args_is_free_var x args) mopp)
        m

  and variants_args_is_free_var x varg =
    match varg with
    | NakedOnly -> false
    | WithArgOnly arg | NakedOrWithArg arg -> is_free_var x arg

  (** [check_wf ~closed msg n t] checks whether [t] is locally-closed
     at level [n] (i.e. locally-closed for [n = 0]). Additionally, it
     checks wheter [t] has no free variable when [closed = true]. *)
  let check_wf =
    if check
    then (
      let exception FVAR in
      let exception BVAR in
      let check closed =
        Memoize.memo_rec_int_t_unit @@ fun check ->
        let clos_check lvl = function
          | ClosTop -> ()
          | Clos (_, m) ->
            M.iter (fun _ env -> Env.iter (fun _ t -> check lvl t) env) m
        and tuples_check lvl = function
          | TuplesTop -> ()
          | Tuples (_, m) ->
            MInts.iter
              (fun _ mopp ->
                MOPP.iter (fun _ l -> List.iter (fun t -> check lvl t) l) mopp)
              m
        and variants_check lvl = function
          | VariantsTop -> ()
          | Variants (_, m) ->
            MCons.iter
              (fun _ mopp ->
                MOPP.iter
                  (fun _ varg ->
                    match varg with
                    | NakedOnly -> ()
                    | WithArgOnly arg | NakedOrWithArg arg -> check lvl arg)
                  mopp)
              m
        in
        fun lvl t ->
          match Hcons.node t with
          | Var _ -> if closed then raise FVAR
          | Idx n -> if not (0 <= n && n < lvl) then raise BVAR
          | Mu (_, t) -> check (lvl + 1) t
          | Abs { height = _; bools = _; ints = _; closures; tuples; variants }
            ->
            clos_check lvl closures;
            tuples_check lvl tuples;
            variants_check lvl variants
      in
      let check_closed = check true
      and check_not_closed = check false in
      fun ~closed msg n t ->
        try if closed then check_closed n t else check_not_closed n t with
        | FVAR ->
          Format.eprintf
            "ERROR in NablaRec.%s: ill-formed abstract value (free variable \
             found)@.@[%a@]@."
            msg
            pp
            t;
          assert false
        | BVAR ->
          Format.eprintf
            "ERROR in NablaRec.%s: ill-formed abstract value (bound variable \
             found)@.@[%a@]@."
            msg
            pp
            t;
          assert false)
    else fun ~closed:_ _msg _n _t -> ()

  (** [close_aux n x t] turns the variable [x] into a de Bruijn binder
     in [t] considered at depth [n], and returns [true] iff [x] was
     found. This function is only used by the function [mu] defined
     hereafter. *)
  let close_aux n x t =
    let found = ref false in
    let rec close_aux n x t =
      match Hcons.node t with
      | Var y ->
        if x = y
        then begin
          found := true;
          build @@ Idx n
        end
        else t
      | Idx m -> if m < n then t else build @@ Idx (m + 1)
      | Mu (h, t) -> build @@ Mu (h, close_aux (n + 1) x t)
      | Abs { height; bools; ints; closures; tuples; variants } ->
        build
        @@ Abs
             {
               height;
               bools;
               ints;
               closures = close_closures_aux n x closures;
               tuples = close_tuples_aux n x tuples;
               variants = close_variants_aux n x variants;
             }
    and close_closures_aux n x = function
      | ClosTop as c -> c
      | Clos (h, m) -> Clos (h, M.map (Env.map (close_aux n x)) m)
    and close_tuples_aux n x = function
      | TuplesTop as t -> t
      | Tuples (h, m) ->
        Tuples (h, MInts.map (MOPP.map (List.map (close_aux n x))) m)
    and close_variants_aux n x = function
      | VariantsTop as t -> t
      | Variants (h, m) ->
        Variants
          ( h,
            MCons.map
              (MOPP.map (function
                  | NakedOnly -> NakedOnly
                  | WithArgOnly arg -> WithArgOnly (close_aux n x arg)
                  | NakedOrWithArg arg -> NakedOrWithArg (close_aux n x arg)))
              m )
    in
    let t' = close_aux n x t in
    (t', !found)

  (** [close x t] closes the variables [x] in the locally-closed term
     [t], i.e. turns the variable [x] into the 0 de Bruijn index. *)
  let close =
    Memoize.memo_int_t @@ fun x t ->
    check_wf ~closed:false "close (arg)" 0 t;
    let res = close_aux 0 x t in
    check_wf ~closed:false "close (res)" 1 (fst res);
    if check && is_free_var x (fst res)
    then
      Format.eprintf
        "ERROR in NablaRec.close (res): variable %a should not be free \
         in@.@[%a@]@."
        (pp_int_var "_")
        x
        pp
        t;
    res

  let var x = build @@ Var x

  (** [mu x t] creates a recursive abstract value, by binding the
     variable [x] in the abstract value [t]. If [n] is not found in
     [t], then [t] is returned. Avoids the creation of non-contractive
     values. *)
  let mu =
    Memoize.memo_int_t @@ fun x t ->
    let res =
      match Hcons.node t with
      | Var y -> if x = y then bot else t
      | Idx _ -> assert false
      | _ ->
        let t', found = close x t in
        if found then build @@ Mu (height t', t') else t
    in
    if check && is_free_var x res
    then
      Format.eprintf
        "ERROR in NablaRec.mu (res): variable %a should not be free \
         in@.@[%a@]@."
        (pp_int_var "_")
        x
        pp
        t;
    res

  (** [open_rec t' n t] substitutes the de Bruijn index [n] in [t]
     with the locally closed abstract value [t']. *)
  let rec open_rec t' n t =
    match Hcons.node t with
    | Var _ -> t
    | Idx m -> if m < n then t else if m = n then t' else build @@ Idx (m - 1)
    | Mu (_h, t) ->
      let t1 = open_rec t' (n + 1) t in
      build @@ Mu (height t1, t1)
    | Abs { height = _; bools; ints; closures; tuples; variants } ->
      make
        ~bools
        ~ints
        ~closures:(open_rec_closures t' n closures)
        ~tuples:(open_rec_tuples t' n tuples)
        ~variants:(open_rec_variants t' n variants)

  and open_rec_closures t' n c =
    match c with
    | ClosTop as c -> c
    | Clos (_h, m) -> clos @@ M.map (Env.map (open_rec t' n)) m

  and open_rec_tuples t' n c =
    match c with
    | TuplesTop as t -> t
    | Tuples (_h, m) ->
      tuples @@ MInts.map (MOPP.map (List.map (open_rec t' n))) m

  and open_rec_variants t' n c =
    match c with
    | VariantsTop as t -> t
    | Variants (_h, m) ->
      variants
      @@ MCons.map
           (MOPP.map (function
               | NakedOnly -> NakedOnly
               | WithArgOnly arg -> WithArgOnly (open_rec t' n arg)
               | NakedOrWithArg arg -> NakedOrWithArg (open_rec t' n arg)))
           m

  (** [open0 t' t] substitutes the de Bruijn index [0] in [t] with the
     locally closed abstract value [t']. *)
  let open0 =
    Memoize.memo2 @@ fun t' t ->
    check_wf ~closed:false "open0 (arg1)" 0 t';
    check_wf ~closed:false "open0 (arg2)" 1 t;
    let res = open_rec t' 0 t in
    check_wf ~closed:false "open0 (res)" 0 res;
    res

  (** [open_var x t] opens a value, by assigning the variable [x] to
     the 0 de Bruijn index of [t]. *)
  let open_var x t = open0 (build @@ Var x) t

  let singleton_closure annot t env =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:(clos (M.singleton (t, annot) env))
      ~tuples:tuples_bot
      ~variants:variants_bot

  let ints i =
    make
      ~bools:Bools.bot
      ~ints:i
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let rec get_ints t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants } ->
      (ints, make ~bools ~ints:Ints.bot ~closures ~tuples ~variants)
    | Mu (_h, t') -> get_ints (open0 t t')
    | Var _ | Idx _ -> assert false

  let bools b =
    make
      ~bools:b
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:variants_bot

  let rec get_bools t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants } ->
      (bools, make ~bools:Bools.bot ~ints ~closures ~tuples ~variants)
    | Mu (_h, t') -> get_bools (open0 t t')
    | Var _ | Idx _ -> assert false

  let singleton_tuple opp i l =
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:(tuples @@ MInts.singleton i (MOPP.singleton opp l))
      ~variants:variants_bot

  let make_constant_list =
    let rec loop n v acc = if n <= 0 then acc else loop (n - 1) v (v :: acc) in
    fun n v -> loop n v []

  let rec get_tuple n t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples = t'; variants } -> (
      match t' with
      | TuplesTop ->
        let rem = t in
        Some ([(None, make_constant_list n top)], rem)
      | Tuples (_h, m) -> (
        match MInts.find_opt n m with
        | None -> None
        | Some mopp ->
          let vs = MOPP.bindings mopp in
          let rem =
            make
              ~bools
              ~ints
              ~closures
              ~tuples:(tuples @@ MInts.remove n m)
              ~variants
          in
          Some (vs, rem)))
    | Mu (_h, t') -> get_tuple n (open0 t t')
    | Var _ | Idx _ -> assert false

  let singleton_variant opp c oarg =
    let arg =
      match oarg with
      | None -> NakedOnly
      | Some arg -> WithArgOnly arg
    in
    make
      ~bools:Bools.bot
      ~ints:Ints.bot
      ~closures:clos_bot
      ~tuples:tuples_bot
      ~variants:(variants @@ MCons.singleton c (MOPP.singleton opp arg))

  let rec get_variant_with_arg c t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants = t' } -> (
      match t' with
      | VariantsTop ->
        let rem = t in
        Some ([(None, top)], rem)
      | Variants (_h, m) -> (
        match MCons.find_opt c m with
        | None -> None
        | Some mopp ->
          let args =
            MOPP.fold
              (fun opp varg acc ->
                match varg with
                | NakedOnly -> acc
                | WithArgOnly arg | NakedOrWithArg arg ->
                  if is_bot arg then acc else (opp, arg) :: acc)
              mopp
              []
          in
          if list_is_empty args
          then None
          else
            let rem =
              make
                ~bools
                ~ints
                ~closures
                ~tuples
                ~variants:
                  (variants
                  @@ MCons.add
                       c
                       (MOPP.filter_map
                          (fun _opp varg ->
                            match varg with
                            | NakedOnly | NakedOrWithArg _ -> Some NakedOnly
                            | WithArgOnly _ -> None)
                          mopp)
                       m)
            in
            Some (args, rem)))
    | Mu (_h, t') -> get_variant_with_arg c (open0 t t')
    | Var _ | Idx _ -> assert false

  let rec get_variant_naked c t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants = t' } -> (
      match t' with
      | VariantsTop ->
        let rem = t in
        Some ([None], rem)
      | Variants (_h, m) -> (
        match MCons.find_opt c m with
        | None -> None
        | Some mopp ->
          if MOPP.for_all
               (fun _opp varg ->
                 match varg with
                 | NakedOnly | NakedOrWithArg _ -> false
                 | WithArgOnly _ -> true)
               mopp
          then None
          else
            let pps =
              MOPP.fold
                (fun opp varg acc ->
                  match varg with
                  | NakedOnly | NakedOrWithArg _ -> opp :: acc
                  | WithArgOnly _ -> acc)
                mopp
                []
            in
            if list_is_empty pps
            then None
            else
              let rem =
                make
                  ~bools
                  ~ints
                  ~closures
                  ~tuples
                  ~variants:
                    (variants
                    @@ MCons.add
                         c
                         (MOPP.filter_map
                            (fun _opp varg ->
                              match varg with
                              | NakedOnly -> None
                              | NakedOrWithArg arg | WithArgOnly arg ->
                                Some (WithArgOnly arg))
                            mopp)
                         m)
              in
              Some (pps, rem)))
    | Mu (_h, t') -> get_variant_naked c (open0 t t')
    | Var _ | Idx _ -> assert false

  module AtomicPath = struct
    type t =
      | InVariant of Cons.t * PP.t option
          (** Path within a variant: which constructor, produced at which
       program point *)
      | InClos of FunAnnot.t * Var.t
          (** Path within a closure: which function, and which variable in
       the environment *)
      | InTuple of int * PP.t option * int
          (** Path within a tuple: which arity, and which position in the
       tuple *)
    [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | InVariant (c, _) -> fprintf fmt "@@%a" Cons.pp c
      | InClos ((f, _), _) -> fprintf fmt "[%a]" (NablaFun.pp_rec false) f
      | InTuple (_, _, j) -> fprintf fmt ".%i" j
  end

  module Unif = struct
    type var = int

    type node = {
      bools: Bools.t;
      ints: Ints.t;
      closures: node_closures;
      tuples: node_tuples;
      variants: node_variants;
    }

    and node_closures =
      | ClosTop
      | Clos of var Env.t M.t

    and node_tuples =
      | TuplesTop
      | Tuples of var list MOPP.t MInts.t

    and node_variants =
      | VariantsTop
      | Variants of var variant_args MOPP.t MCons.t

    type graph = node Uf.t

    let clos_is_empty = function
      | ClosTop -> false
      | Clos m -> M.is_empty m

    let tuples_is_empty = function
      | TuplesTop -> false
      | Tuples m -> MInts.is_empty m

    let variants_is_empty = function
      | VariantsTop -> false
      | Variants m -> MCons.is_empty m

    let node_is_empty ~weak { bools; ints; closures; tuples; variants } =
      (weak || (Bools.is_bot bools && Ints.is_bot ints))
      && clos_is_empty closures
      && tuples_is_empty tuples
      && variants_is_empty variants

    let is_empty ~weak graph n =
      node_is_empty ~weak (Option.get @@ Uf.get_term n graph)

    let clos_leq_head clos1 clos2 =
      match (clos1, clos2) with
      | _, ClosTop -> true
      | ClosTop, _ -> false
      | Clos m1, Clos m2 -> M.for_all (fun c _ -> M.mem c m2) m1

    let tuples_leq_head tuples1 tuples2 =
      match (tuples1, tuples2) with
      | _, TuplesTop -> true
      | TuplesTop, _ -> false
      | Tuples m1, Tuples m2 ->
        MInts.for_all
          (fun c mopp1 ->
            match MInts.find_opt c m2 with
            | None -> false
            | Some mopp2 -> MOPP.for_all (fun opp _ -> MOPP.mem opp mopp2) mopp1)
          m1

    let variants_leq_head variants1 variants2 =
      match (variants1, variants2) with
      | _, VariantsTop -> true
      | VariantsTop, _ -> false
      | Variants m1, Variants m2 ->
        MCons.for_all
          (fun c mopp1 ->
            match MCons.find_opt c m2 with
            | None -> false
            | Some mopp2 ->
              MOPP.for_all
                (fun opp varg1 ->
                  match MOPP.find_opt opp mopp2 with
                  | None -> false
                  | Some varg2 -> begin
                    match (varg1, varg2) with
                    | WithArgOnly _, _ | _, (NakedOnly | NakedOrWithArg _) ->
                      true
                    | (NakedOnly | NakedOrWithArg _), WithArgOnly _ -> false
                  end)
                mopp1)
          m1

    let node_leq_head ~weak
        {
          bools = bools1;
          ints = ints1;
          closures = clos1;
          tuples = tuples1;
          variants = variants1;
        }
        {
          bools = bools2;
          ints = ints2;
          closures = clos2;
          tuples = tuples2;
          variants = variants2;
        } =
      (weak || (Bools.leq bools1 bools2 && Ints.leq ints1 ints2))
      && clos_leq_head clos1 clos2
      && tuples_leq_head tuples1 tuples2
      && variants_leq_head variants1 variants2

    let leq_head ~weak graph t1 t2 =
      let node1 = Option.get @@ Uf.get_term t1 graph
      and node2 = Option.get @@ Uf.get_term t2 graph in
      node_leq_head ~weak node1 node2

    let join_vars ~widen (graph : graph) x1 x2 =
      let bools_join = if widen then Bools.widen else Bools.join
      and ints_join = if widen then Ints.widen else Ints.join
      and clos_join c1 c2 =
        match (c1, c2) with
        | (ClosTop as c), _ | _, (ClosTop as c) ->
          (* Format.eprintf "UNIFY TOP@."; *)
          (c, [])
        | Clos m1, Clos m2 ->
          let eqs = ref [] in
          let m12 =
            M.union
              (fun _f env1 env2 ->
                (* gather the equalities between variables *)
                ignore
                @@ Env.union
                     (fun _x var1 var2 ->
                       eqs := (var1, var2) :: !eqs;
                       None)
                     env1
                     env2;
                Some env1)
              m1
              m2
          in
          (Clos m12, !eqs)
      and tuples_join t1 t2 =
        match (t1, t2) with
        | (TuplesTop as t), _ | _, (TuplesTop as t) ->
          (* Format.eprintf "UNIFY TOP@."; *)
          (t, [])
        | Tuples m1, Tuples m2 ->
          let eqs = ref [] in
          let m12 =
            MInts.union
              (fun _i mopp1 mopp2 ->
                Some
                  (MOPP.union
                     (fun _opp l1 l2 ->
                       (* gather the equalities between variables *)
                       List.iter2
                         (fun var1 var2 -> eqs := (var1, var2) :: !eqs)
                         l1
                         l2;
                       Some l1)
                     mopp1
                     mopp2))
              m1
              m2
          in
          (Tuples m12, !eqs)
      and variants_join t1 t2 =
        match (t1, t2) with
        | (VariantsTop as t), _ | _, (VariantsTop as t) ->
          (* Format.eprintf "UNIFY TOP@."; *)
          (t, [])
        | Variants m1, Variants m2 ->
          let eqs = ref [] in
          let m12 =
            MCons.union
              (fun _c mopp1 mopp2 ->
                Some
                  (MOPP.union
                     (fun _opp varg1 varg2 ->
                       match (varg1, varg2) with
                       | NakedOnly, NakedOnly -> Some NakedOnly
                       | NakedOnly, (NakedOrWithArg var | WithArgOnly var)
                       | (NakedOrWithArg var | WithArgOnly var), NakedOnly ->
                         Some (NakedOrWithArg var)
                       | WithArgOnly var1, WithArgOnly var2 ->
                         (* gather the equalities between variables *)
                         eqs := (var1, var2) :: !eqs;
                         Some (WithArgOnly var1)
                       | NakedOrWithArg var1, WithArgOnly var2
                       | WithArgOnly var1, NakedOrWithArg var2
                       | NakedOrWithArg var1, NakedOrWithArg var2 ->
                         (* gather the equalities between variables *)
                         eqs := (var1, var2) :: !eqs;
                         Some (NakedOrWithArg var1))
                     mopp1
                     mopp2))
              m1
              m2
          in
          (Variants m12, !eqs)
      in
      let local_solve t1 t2 =
        match (t1, t2) with
        | ( {
              bools = bools1;
              ints = ints1;
              closures = closures1;
              tuples = tuples1;
              variants = variants1;
            },
            {
              bools = bools2;
              ints = ints2;
              closures = closures2;
              tuples = tuples2;
              variants = variants2;
            } ) ->
          let closures12, eqs_clos = clos_join closures1 closures2 in
          let tuples12, eqs_tuples = tuples_join tuples1 tuples2 in
          let variants12, eqs_variants = variants_join variants1 variants2 in
          let res =
            {
              bools = bools_join bools1 bools2;
              ints = ints_join ints1 ints2;
              closures = closures12;
              tuples = tuples12;
              variants = variants12;
            }
          and eqs = eqs_variants @ eqs_tuples @ eqs_clos in
          (res, eqs)
      in
      Uf.unify_vars local_solve x1 x2 graph

    (** [meet_vars graph x1 x2] adds to [graph] new nodes that
       represent the intersections of the abstract values pointed by
       [x1] and [x2]. It works by building the product of the automata
       starting at [x1] and [x2]. *)
    let meet_vars r (graph : graph) x1 x2 =
      let get_var =
        let cache = ref MInts2.empty in
        fun x1 x2 ->
          let p = (x1, x2) in
          try MInts2.find p !cache
          with Not_found ->
            let n = !r in
            incr r;
            cache := MInts2.add p n !cache;
            Uf.register n graph;
            n
      in
      let clos_meet c1 c2 =
        match (c1, c2) with
        | ClosTop, c | c, ClosTop -> (c, [])
        | Clos m1, Clos m2 ->
          let eqs = ref [] in
          let m12 =
            M.merge
              (fun _f oenv1 oenv2 ->
                match (oenv1, oenv2) with
                | Some env1, Some env2 ->
                  (* gather the equalities between variables *)
                  let env12 =
                    Env.union
                      (fun _x var1 var2 ->
                        eqs := (var1, var2) :: !eqs;
                        Some (get_var var1 var2))
                      env1
                      env2
                  in
                  Some env12
                | _, (None as none) | (None as none), _ -> none)
              m1
              m2
          in
          (Clos m12, !eqs)
      in
      let tuples_meet t1 t2 =
        match (t1, t2) with
        | TuplesTop, t | t, TuplesTop -> (t, [])
        | Tuples m1, Tuples m2 ->
          let eqs = ref [] in
          let m12 =
            MInts.merge
              (fun _i omopp1 omopp2 ->
                match (omopp1, omopp2) with
                | Some mopp1, Some mopp2 ->
                  let mopp12 =
                    MOPP.merge
                      (fun _opp ol1 ol2 ->
                        match (ol1, ol2) with
                        | Some l1, Some l2 ->
                          (* gather the equalities between variables *)
                          let l12 =
                            List.map2
                              (fun var1 var2 ->
                                eqs := (var1, var2) :: !eqs;
                                get_var var1 var2)
                              l1
                              l2
                          in
                          Some l12
                        | _, (None as none) | (None as none), _ -> none)
                      mopp1
                      mopp2
                  in
                  if MOPP.is_empty mopp12 then None else Some mopp12
                | _, (None as none) | (None as none), _ -> none)
              m1
              m2
          in
          (Tuples m12, !eqs)
      in
      let variants_meet t1 t2 =
        match (t1, t2) with
        | VariantsTop, t | t, VariantsTop -> (t, [])
        | Variants m1, Variants m2 ->
          let eqs = ref [] in
          let m12 =
            MCons.merge
              (fun _c omopp1 omopp2 ->
                match (omopp1, omopp2) with
                | Some mopp1, Some mopp2 ->
                  let mopp12 =
                    MOPP.merge
                      (fun _opp oargs1 oargs2 ->
                        match (oargs1, oargs2) with
                        | Some varg1, Some varg2 -> begin
                          match (varg1, varg2) with
                          | NakedOnly, WithArgOnly _ | WithArgOnly _, NakedOnly
                            -> None
                          | NakedOnly, NakedOnly
                          | NakedOrWithArg _, NakedOnly
                          | NakedOnly, NakedOrWithArg _ -> Some NakedOnly
                          | WithArgOnly var1, NakedOrWithArg var2
                          | NakedOrWithArg var1, WithArgOnly var2
                          | WithArgOnly var1, WithArgOnly var2 ->
                            (* gather the equalities between variables *)
                            eqs := (var1, var2) :: !eqs;
                            Some (WithArgOnly (get_var var1 var2))
                          | NakedOrWithArg var1, NakedOrWithArg var2 ->
                            (* gather the equalities between variables *)
                            eqs := (var1, var2) :: !eqs;
                            Some (NakedOrWithArg (get_var var1 var2))
                        end
                        | _, (None as none) | (None as none), _ -> none)
                      mopp1
                      mopp2
                  in
                  if MOPP.is_empty mopp12 then None else Some mopp12
                | _, (None as none) | (None as none), _ -> none)
              m1
              m2
          in
          (Variants m12, !eqs)
      in
      let local_solve x t1 t2 =
        match (t1, t2) with
        | ( {
              bools = bools1;
              ints = ints1;
              closures = closures1;
              tuples = tuples1;
              variants = variants1;
            },
            {
              bools = bools2;
              ints = ints2;
              closures = closures2;
              tuples = tuples2;
              variants = variants2;
            } ) ->
          let closures12, clos_eqs = clos_meet closures1 closures2 in
          let tuples12, tuples_eqs = tuples_meet tuples1 tuples2 in
          let variants12, variants_eqs = variants_meet variants1 variants2 in
          let res =
            {
              bools = Bools.meet bools1 bools2;
              ints = Ints.meet ints1 ints2;
              closures = closures12;
              tuples = tuples12;
              variants = variants12;
            }
          and eqs = variants_eqs @ tuples_eqs @ clos_eqs in
          Uf.set_term x res graph;
          (res, eqs)
      in
      let started = ref SInts.empty in
      let rec process todo =
        match todo with
        | [] -> ()
        | (x1, x2) :: todo -> (
          let x12 = get_var x1 x2 in
          match Uf.get_term x12 graph with
          | Some _ ->
            (* already handled *)
            process todo
          | None ->
            (* not handled yet *)
            let v1 = Option.get @@ Uf.get_term x1 graph
            and v2 = Option.get @@ Uf.get_term x2 graph in
            let res, more_todo = local_solve x12 v1 v2 in
            Uf.set_term x12 res graph;
            let todo =
              if SInts.mem x12 !started
              then todo
              else begin
                started := SInts.add x12 !started;
                more_todo @ todo
              end
            in
            process todo)
      in
      process [(x1, x2)];
      get_var x1 x2

    module MemoFix =
      Memo.Fix.Priority.Make (Logs.Silent) (Int) (MInts) (BoolDomainDefaultTrue)

    (** [is_empty_raw_with_hyp hyp graph t] tests whether the node [t]
        in the graph [graph] is semantically empty, assuming the nodes
        in the list [hyp] are already semantically empty *)
    let is_empty_raw_with_hyp hyp graph t =
      let is_hyp = List.mem t hyp in
      let is_empty =
        MemoFix.solve ~debug:false ~transient:false @@ fun is_empty t ->
        let clos_is_empty = function
          | ClosTop -> false
          | Clos m ->
            M.for_all
              (fun (f, _annot) env ->
                (* fixpoints through recursive functions are non-empty *)
                (not @@ NablaFun.is_recursive f)
                && Env.exists (fun _ v -> is_empty v) env)
              m
        and tuples_is_empty = function
          | TuplesTop -> false
          | Tuples m ->
            MInts.for_all
              (fun _ mopp1 ->
                MOPP.for_all (fun _ l1 -> List.exists is_empty l1) mopp1)
              m
        and variants_is_empty = function
          | VariantsTop -> false
          | Variants m ->
            MCons.for_all
              (fun _ mopp1 ->
                MOPP.for_all
                  (fun _ varg1 ->
                    match varg1 with
                    | NakedOnly | NakedOrWithArg _ -> false
                    | WithArgOnly v1 -> is_empty v1)
                  mopp1)
              m
        in
        is_hyp
        ||
        match Uf.get_term t graph with
        | Some { bools; ints; closures; tuples; variants } ->
          Bools.is_bot bools
          && Ints.is_bot ints
          && clos_is_empty closures
          && tuples_is_empty tuples
          && variants_is_empty variants
        | None -> assert false
      in
      fst @@ is_empty t

    (** [is_empty_raw graph t] tests whether the node [t] is
        semantically empty in the graph [graph] *)
    let is_empty_raw graph t = is_empty_raw_with_hyp [] graph t

    let encode n (graph : graph) t =
      let fresh_var () =
        let i = !n in
        incr n;
        i
      in
      let rec encode env t : int =
        let var = fresh_var () in
        Uf.register var graph;
        encode_ env var t
      (* [encode_ env dst t] encodes [t] as a graph with destination
         note [dst], and environment on bound variables [env] *)
      and encode_ env dst (t : t) : int =
        match Hcons.node t with
        | Idx n -> Ralist.nth env n
        | Var _ -> assert false
        | Mu (_, t) -> encode_ (Ralist.cons dst env) dst t
        | Abs { height = _; bools; ints; closures; tuples; variants } ->
          let t' : node =
            {
              bools;
              ints;
              closures = clos_encode env closures;
              tuples = tuples_encode env tuples;
              variants = variants_encode env variants;
            }
          in
          Uf.set_term dst t' graph;
          dst
      and clos_encode env : closures -> node_closures = function
        | ClosTop -> ClosTop
        | Clos (_, m) -> Clos (M.map (Env.map (encode env)) m)
      and tuples_encode env : tuples -> node_tuples = function
        | TuplesTop -> TuplesTop
        | Tuples (_, m) ->
          Tuples (MInts.map (MOPP.map (List.map (encode env))) m)
      and variants_encode env : variants -> node_variants = function
        | VariantsTop -> VariantsTop
        | Variants (_, m) ->
          Variants
            (MCons.map
               (MOPP.map (function
                   | NakedOnly -> NakedOnly
                   | NakedOrWithArg arg -> NakedOrWithArg (encode env arg)
                   | WithArgOnly arg -> WithArgOnly (encode env arg)))
               m)
      in
      encode Ralist.empty t

    let decode (graph : graph) i =
      let cache = ref MInts.empty in
      let loops = ref SInts.empty in
      let rec decode i : t =
        (* IMPORTANT: always work with representants! *)
        let i = Uf.find i graph in
        match MInts.find_opt i !cache with
        | None -> (
          (* not explored yet *)
          let old_cache = !cache in
          cache := MInts.add i None !cache;
          match Uf.get_term i graph with
          | None -> assert false (* we forgot to set a term in some node *)
          | Some { bools; ints; closures; tuples; variants } ->
            let closures = clos_decode closures in
            let tuples = tuples_decode tuples in
            let variants = variants_decode variants in
            let is_recursive = SInts.mem i !loops in
            let res = make ~bools ~ints ~closures ~tuples ~variants in
            let res =
              if is_recursive
              then if is_empty_raw graph i then bot else mu i res
              else res
            in
            (* do not forget to discard the new changes in the cache
               if [i] is a recursive variable, otherwise [i] could
               escape its scope *)
            cache :=
              MInts.add
                i
                (Some res)
                (if is_recursive then old_cache else !cache);
            res)
        | Some (Some t) ->
          (* we have detected sharing *)
          t
        | Some None ->
          (* we have detected a cycle *)
          loops := SInts.add i !loops;
          begin
            match Uf.get_term i graph with
            | Some _ -> ()
            | None -> assert false
          end;
          build @@ Var i
      and clos_decode : node_closures -> closures = function
        | ClosTop -> ClosTop
        | Clos m -> clos @@ M.map (Env.map decode) m
      and tuples_decode : node_tuples -> tuples = function
        | TuplesTop -> TuplesTop
        | Tuples m -> tuples @@ MInts.map (MOPP.map (List.map decode)) m
      and variants_decode : node_variants -> variants = function
        | VariantsTop -> VariantsTop
        | Variants m ->
          variants
          @@ MCons.map
               (MOPP.map (function
                   | NakedOnly -> NakedOnly
                   | NakedOrWithArg var -> NakedOrWithArg (decode var)
                   | WithArgOnly var -> WithArgOnly (decode var)))
               m
      in
      decode i

    module Minimize = struct
      module AtomicPathMap = Map.Make (AtomicPath)

      module Node_as_OT : Map.OrderedType with type t = node = struct
        type t = node

        let compare_shallow_closures clos1 clos2 =
          match (clos1, clos2) with
          | ClosTop, ClosTop -> 0
          | Clos _, ClosTop -> 1
          | ClosTop, Clos _ -> -1
          | Clos m1, Clos m2 ->
            let env_is_empty _ env = Env.is_empty env in
            let m1 = M.filter env_is_empty m1
            and m2 = M.filter env_is_empty m2 in
            M.compare (fun _ _ -> 0) m1 m2

        let compare_shallow_tuples tuples1 tuples2 =
          match (tuples1, tuples2) with
          | TuplesTop, TuplesTop -> 0
          | Tuples _, TuplesTop -> 1
          | TuplesTop, Tuples _ -> -1
          | Tuples m1, Tuples m2 ->
            let m1 = Option.value (MInts.find_opt 0 m1) ~default:MOPP.empty
            and m2 = Option.value (MInts.find_opt 0 m2) ~default:MOPP.empty in
            MOPP.compare (fun _ _ -> 0) m1 m2

        let compare_shallow_variants variants1 variants2 =
          match (variants1, variants2) with
          | VariantsTop, VariantsTop -> 0
          | Variants _, VariantsTop -> 1
          | VariantsTop, Variants _ -> -1
          | Variants m1, Variants m2 ->
            let keep_only_naked_cons _ mopp =
              let mopp =
                MOPP.filter
                  (fun _ varg ->
                    match varg with
                    | NakedOnly | NakedOrWithArg _ -> true
                    | WithArgOnly _ -> false)
                  mopp
              in
              if MOPP.is_empty mopp then None else Some mopp
            in
            let m1 = MCons.filter_map keep_only_naked_cons m1
            and m2 = MCons.filter_map keep_only_naked_cons m2 in
            MCons.compare
              (fun mopp1 mopp2 -> MOPP.compare (fun _ _ -> 0) mopp1 mopp2)
              m1
              m2

        let compare
            {
              bools = bools1;
              ints = ints1;
              closures = clos1;
              tuples = tuples1;
              variants = variants1;
            }
            {
              bools = bools2;
              ints = ints2;
              closures = clos2;
              tuples = tuples2;
              variants = variants2;
            } =
          let b = Bools.compare bools1 bools2 in
          if b <> 0
          then b
          else
            let i = Ints.compare ints1 ints2 in
            if i <> 0
            then i
            else
              let c = compare_shallow_closures clos1 clos2 in
              if c <> 0
              then c
              else
                let t = compare_shallow_tuples tuples1 tuples2 in
                if t <> 0
                then t
                else compare_shallow_variants variants1 variants2
      end

      module ShallowNodeMap = Map.Make (Node_as_OT)

      module IntClasses = struct
        module C = Set.Make (SInts)

        let find_class i classes =
          let exception Found of SInts.t in
          try
            C.iter (fun s -> if SInts.mem i s then raise (Found s)) classes;
            assert false
          with Found s -> s

        let pp fmt cl =
          let open Format in
          fprintf
            fmt
            "@[{@ %a@ }@]"
            (pp_print_seq
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               (fun fmt s ->
                 fprintf
                   fmt
                   "@[{@ %a@ }@]"
                   (pp_print_seq
                      ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
                      pp_print_int)
                   (SInts.to_seq s)))
            (C.to_seq cl)
      end

      let deps_and_initial_classes graph root =
        let seen = ref SInts.empty
        and smp : SInts.t ShallowNodeMap.t ref =
          ref ShallowNodeMap.empty (* shallow node map *)
        and deps : int AtomicPathMap.t MInts.t ref =
          ref MInts.empty (* dependencies from the nodes to their children *)
        in
        let add_smp node i =
          smp :=
            ShallowNodeMap.update
              node
              (function
                | None -> Some (SInts.singleton i)
                | Some s -> Some (SInts.add i s))
              !smp
        and add_deps parent atomic_path child =
          deps :=
            MInts.update
              parent
              (function
                | None -> Some (AtomicPathMap.singleton atomic_path child)
                | Some parents ->
                  Some
                    (AtomicPathMap.update
                       atomic_path
                       (function
                         | None -> Some child
                         | Some _ -> assert false)
                       parents))
              !deps
        in
        let rec browse i =
          let i = Uf.find i graph in
          if SInts.mem i !seen
          then ()
          else begin
            seen := SInts.add i !seen;
            match Uf.get_term i graph with
            | None -> assert false
            | Some ({ bools = _; ints = _; closures; tuples; variants } as node)
              ->
              add_smp node i;
              clos_browse i closures;
              tuples_browse i tuples;
              variants_browse i variants
          end
        and clos_browse parent = function
          | ClosTop -> ()
          | Clos m ->
            M.iter
              (fun f env ->
                Env.iter
                  (fun x i ->
                    add_deps parent (AtomicPath.InClos (f, x)) i;
                    browse i)
                  env)
              m
        and tuples_browse parent = function
          | TuplesTop -> ()
          | Tuples m ->
            MInts.iter
              (fun n mopp ->
                MOPP.iter
                  (fun opp is ->
                    List.iteri
                      (fun j i ->
                        add_deps parent (AtomicPath.InTuple (n, opp, j)) i;
                        browse i)
                      is)
                  mopp)
              m
        and variants_browse parent = function
          | VariantsTop -> ()
          | Variants m ->
            MCons.iter
              (fun c mopp ->
                MOPP.iter
                  (fun opp varg ->
                    match varg with
                    | NakedOnly -> ()
                    | NakedOrWithArg i | WithArgOnly i ->
                      add_deps parent (AtomicPath.InVariant (c, opp)) i;
                      browse i)
                  mopp)
              m
        in
        browse root;
        let classes, rem =
          ShallowNodeMap.fold
            (fun _ s (classes, rem) ->
              (IntClasses.C.add s classes, SInts.diff rem s))
            !smp
            (IntClasses.C.empty, !seen)
        in
        let classes =
          if SInts.is_empty rem then classes else IntClasses.C.add rem classes
        in
        (!deps, classes)

      let debug_minimize = false

      exception Found_split of SInts.t * SInts.t * SInts.t

      let rec simul_iter (deps : int AtomicPathMap.t MInts.t) classes =
        match
          IntClasses.C.iter
            (fun class_ ->
              SInts.iter
                (fun i ->
                  (* we have found no split yet in this class *)
                  let class_remainder = SInts.remove i class_ in
                  if not @@ SInts.is_empty class_remainder
                  then
                    let i_children =
                      Option.value ~default:AtomicPathMap.empty
                      @@ MInts.find_opt i deps
                    in
                    AtomicPathMap.iter
                      (fun path i_child ->
                        let i_child_class =
                          IntClasses.find_class i_child classes
                        in
                        let new_class1, new_class2 =
                          SInts.fold
                            (fun j (new_class1, new_class2) ->
                              (* we have j <> i *)
                              let j_children =
                                Option.value ~default:AtomicPathMap.empty
                                @@ MInts.find_opt j deps
                              in
                              match AtomicPathMap.find_opt path j_children with
                              | None ->
                                (* j has no child along path, so it
                                   cannot safely stay within the
                                   same class as i *)
                                if debug_minimize
                                then
                                  Format.eprintf
                                    "Found path clash: %i clashes with %i@."
                                    j
                                    i;
                                (new_class1, SInts.add j new_class2)
                              | Some (j_child : int) ->
                                (* j has a child along path *)
                                if SInts.mem j_child i_child_class
                                then begin
                                  (* this is ok: j and i can stay in
                                     the same class *)
                                  (SInts.add j new_class1, new_class2)
                                end
                                else begin
                                  (* we have found a clash *)
                                  if debug_minimize
                                  then
                                    Format.eprintf
                                      "Found class clash: %i clashes with %i@."
                                      j
                                      i;
                                  (new_class1, SInts.add j new_class2)
                                end)
                            class_remainder
                            (SInts.singleton i, SInts.empty)
                        in
                        if not @@ SInts.is_empty new_class2
                        then
                          raise (Found_split (class_, new_class1, new_class2)))
                      i_children)
                class_)
            classes
        with
        | () ->
          (* at this point, no Found_split exception was raised, so
             all classes are consistent: we have found a
             simulation! *)
          classes
        | exception Found_split (old_class, new_class1, new_class2) ->
          (* classes are not consistent: let's perform a split and iterate *)
          let open Format in
          if debug_minimize
          then begin
            eprintf
              "@[Split found:@ @[{@ %a@ }@]@ @[{@ %a@ }@]@]@."
              (pp_print_seq
                 ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
                 pp_print_int)
              (SInts.to_seq new_class1)
              (pp_print_seq
                 ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
                 pp_print_int)
              (SInts.to_seq new_class2);
            eprintf "@[Old partition:@ @[%a@]@]@." IntClasses.pp classes
          end;
          let classes =
            IntClasses.C.(
              classes |> remove old_class |> add new_class1 |> add new_class2)
          in
          if debug_minimize
          then eprintf "@[New partition:@ @[%a@]@]@." IntClasses.pp classes;
          simul_iter deps classes

      let minimize (graph : graph) root =
        let deps, initial_classes = deps_and_initial_classes graph root in
        if debug_minimize
        then begin
          Format.eprintf "MINIMIZE START@.";
          Format.eprintf
            "Initial classes:@.@[%a@]@."
            IntClasses.pp
            initial_classes
        end;
        let simulation = simul_iter deps initial_classes in
        if debug_minimize
        then begin
          Format.eprintf "Final classes:@.@[%a@]@." IntClasses.pp simulation;
          Format.eprintf "MINIMIZE END@.@."
        end;
        IntClasses.C.iter
          (fun cl ->
            let i = SInts.choose cl in
            let cl = SInts.remove i cl in
            SInts.iter (fun j -> join_vars ~widen:false graph i j) cl)
          simulation
    end

    type path = AtomicPath.t list
    (** Paths are lists of atomic paths: the first element in that
   list is the atomic paths to follow *)

    let pp fmt = function
      | [] -> Format.pp_print_string fmt "ε"
      | path ->
        Format.pp_print_list ~pp_sep:(fun _fmt () -> ()) AtomicPath.pp fmt path

    (** [find_at_path g p x] finds in the graph [g] the node that is
       reached from [x] by following the path [p] *)
    let rec find_at_path (graph : graph) (path : path) x =
      match path with
      | [] ->
        (* Format.eprintf "Found: @[%a@]@." pp t; *)
        x
      | l :: ls -> (
        match Uf.get_term x graph with
        | None -> assert false
        | Some { bools = _; ints = _; closures; tuples; variants } -> (
          match l with
          | InClos (f, x) -> clos_find_at_path graph f x ls closures
          | InTuple (w, opp, p) -> tuples_find_at_path graph w opp p ls tuples
          | InVariant (c, opp) -> variants_find_at_path graph c opp ls variants)
        )

    and clos_find_at_path cache f x ls = function
      | ClosTop -> raise Not_found
      | Clos m -> m |> M.find f |> Env.find x |> find_at_path cache ls

    and tuples_find_at_path cache w opp i ls = function
      | TuplesTop -> raise Not_found
      | Tuples m ->
        m
        |> MInts.find w
        |> MOPP.find opp
        |> (fun l -> List.nth l i)
        |> find_at_path cache ls

    and variants_find_at_path cache c opp ls = function
      | VariantsTop -> raise Not_found
      | Variants m ->
        m
        |> MCons.find c
        |> MOPP.find opp
        |> (function
             | NakedOnly -> assert false
             | NakedOrWithArg v | WithArgOnly v -> v)
        |> find_at_path cache ls

    let find_at_path cache path t = find_at_path cache (List.rev path) t
  end

  let with_encoding f t =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t = Unif.encode n graph t in
    f graph enc_t;
    Unif.decode graph enc_t

  let with_encoding2 f t1 t2 =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode n graph t1 in
    let enc_t2 = Unif.encode n graph t2 in
    f graph enc_t1 enc_t2;
    Unif.decode graph enc_t1

  let with_encoding3 f t1 t2 =
    let graph = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode n graph t1 in
    let enc_t2 = Unif.encode n graph t2 in
    let enc_t12 = f n graph enc_t1 enc_t2 in
    Unif.decode graph enc_t12

  module MemoFix2 =
    Memo.Fix.Priority.Make (Logs.Silent) (Ints2) (MInts2)
      (BoolDomainDefaultTrue)

  (* [leq_gen_raw ~weak graph t1 t2] is an inclusion test between two
     abstract values [t1] and [t2] that are encoded in a same graph
     [graph]. If [weak = true], then the integer parts of [t1] and
     [t2] are ignored. *)
  let leq_gen_raw ~weak graph t1 t2 =
    let is_empty_raw = Memoize.memo_int @@ Unif.is_empty_raw graph in
    let leq =
      MemoFix2.solve ~debug:false ~transient:false @@ fun leq (t1, t2) ->
      let leq t1 t2 = leq (t1, t2) in
      let clos_leq (clos1 : Unif.node_closures) (clos2 : Unif.node_closures) =
        match (clos1, clos2) with
        | _, ClosTop -> true
        | ClosTop, Clos _ -> false
        | Clos m1, Clos m2 ->
          M.for_all
            (fun t env1 ->
              match M.find_opt t m2 with
              | None ->
                (not weak)
                && M.for_all
                     (fun (f, _) env ->
                       (* fixpoints through recursive functions are non-empty *)
                       (not @@ NablaFun.is_recursive f)
                       && Env.exists (fun _ v -> is_empty_raw v) env)
                     m1
              | Some env2 -> Env.equal leq env1 env2)
            m1
      and tuples_leq (t1 : Unif.node_tuples) (t2 : Unif.node_tuples) =
        match (t1, t2) with
        | _, TuplesTop -> true
        | TuplesTop, Tuples _ -> false
        | Tuples m1, Tuples m2 ->
          MInts.for_all
            (fun i mopp1 ->
              match MInts.find_opt i m2 with
              | None ->
                (not weak)
                && MOPP.for_all (fun _ l1 -> List.exists is_empty_raw l1) mopp1
              | Some mopp2 ->
                MOPP.for_all
                  (fun opp l1 ->
                    match MOPP.find_opt opp mopp2 with
                    | None -> (not weak) && List.exists is_empty_raw l1
                    | Some l2 -> List.for_all2 leq l1 l2)
                  mopp1)
            m1
      and variants_leq (t1 : Unif.node_variants) (t2 : Unif.node_variants) =
        match (t1, t2) with
        | _, VariantsTop -> true
        | VariantsTop, Variants _ -> false
        | Variants m1, Variants m2 ->
          MCons.for_all
            (fun c mopp1 ->
              match MCons.find_opt c m2 with
              | None ->
                (not weak)
                && MOPP.for_all
                     (fun _ varg1 ->
                       match varg1 with
                       | NakedOnly | NakedOrWithArg _ -> false
                       | WithArgOnly v1 -> is_empty_raw v1)
                     mopp1
              | Some mopp2 ->
                MOPP.for_all
                  (fun opp varg1 ->
                    match MOPP.find_opt opp mopp2 with
                    | None -> begin
                      (not weak)
                      &&
                      match varg1 with
                      | NakedOnly | NakedOrWithArg _ -> false
                      | WithArgOnly v1 -> is_empty_raw v1
                    end
                    | Some varg2 -> begin
                      match (varg1, varg2) with
                      | NakedOnly, (NakedOnly | NakedOrWithArg _) -> true
                      | (NakedOrWithArg arg1 | WithArgOnly arg1), NakedOnly ->
                        (not weak) && is_empty_raw arg1
                      | (NakedOrWithArg _ | NakedOnly), WithArgOnly _ -> false
                      | ( WithArgOnly arg1,
                          (NakedOrWithArg arg2 | WithArgOnly arg2) )
                      | NakedOrWithArg arg1, NakedOrWithArg arg2 ->
                        leq arg1 arg2
                    end)
                  mopp1)
            m1
      in
      match (Uf.get_term t1 graph, Uf.get_term t2 graph) with
      | ( Some
            {
              Unif.bools = bools1;
              ints = ints1;
              closures = clos1;
              tuples = tuples1;
              variants = variants1;
            },
          Some
            {
              bools = bools2;
              ints = ints2;
              closures = clos2;
              tuples = tuples2;
              variants = variants2;
            } ) ->
        Bools.leq bools1 bools2
        && (if weak
            then Ints.is_bot ints1 || (not @@ Ints.is_bot ints2)
            else Ints.leq ints1 ints2)
        && clos_leq clos1 clos2
        && tuples_leq tuples1 tuples2
        && variants_leq variants1 variants2
      | None, _ | _, None -> assert false
    in
    fst @@ leq (t1, t2)

  (* [leq_gen ~weak t1 t2] is an inclusion test between [t1] and [t2].
     If [weak = true], then the integer parts of [t1] and [t2] are
     ignored. *)
  let leq_gen ~weak t1 t2 =
    let graph : Unif.node Uf.t = Uf.init () in
    let n = ref 0 in
    let enc_t1 = Unif.encode n graph t1 in
    let enc_t2 = Unif.encode n graph t2 in
    leq_gen_raw ~weak graph enc_t1 enc_t2

  let leq =
    let leq = Memoize.memo2 @@ leq_gen ~weak:false in
    fun t1 t2 -> t1 == t2 || leq t1 t2

  let leq_weak =
    let leq = Memoize.memo2 @@ leq_gen ~weak:true in
    fun t1 t2 -> t1 == t2 || leq t1 t2

  let leq_gen ~weak t1 t2 = if weak then leq_weak t1 t2 else leq t1 t2

  let minimize =
    if Options.minimize
    then (
      Memoize.memo @@ fun t ->
      let t' = with_encoding Unif.Minimize.minimize t in
      if check && (not @@ leq t' t)
      then
        Format.eprintf
          "WARNING: minimization creates a precision loss:@.Before \
           minimization:@.@[%a@]@.After minimization:@.@[%a@]@."
          pp
          t
          pp
          t';
      t')
    else Fun.id

  let join_gen ~widen t1 t2 =
    if t1 == t2
    then t1
    else with_encoding2 (Unif.join_vars ~widen) t1 t2 |> minimize

  let join =
    let join = Memoize.memo2 @@ join_gen ~widen:false in
    fun t1 t2 -> if t1 == t2 then t1 else join t1 t2

  let meet =
    let meet = Memoize.memo2 @@ with_encoding3 Unif.meet_vars in
    fun t1 t2 -> if t1 == t2 then t1 else meet t1 t2

  let rec mem_bool b t =
    match Hcons.node t with
    | Abs
        { height = _; bools; ints = _; closures = _; tuples = _; variants = _ }
      -> Bools.mem b bools
    | Mu (_h, t0) ->
      (* we can avoid a costly call to [open0] here *)
      mem_bool b t0
    | Idx _ | Var _ ->
      (* this case should never happen *)
      assert false

  let rec get_closures t =
    match Hcons.node t with
    | Abs { height = _; bools; ints; closures; tuples; variants } -> (
      let rem = make ~bools ~ints ~closures:clos_bot ~tuples ~variants in
      match closures with
      | ClosTop -> Some (None, rem)
      | Clos (_h, m) ->
        let l = M.fold (fun (f, _annot) env acc -> (f, env) :: acc) m [] in
        Some (Some l, rem))
    | Mu (_h, t') -> get_closures (open0 t t')
    | Var _ | Idx _ -> assert false

  let rec closures_join_map bot join fclos_top fclos t =
    match Hcons.node t with
    | Abs
        { height = _; bools = _; ints = _; closures; tuples = _; variants = _ }
      -> (
      match closures with
      | ClosTop -> fclos_top
      | Clos (_h, m) ->
        M.fold (fun (t, _annot) c acc -> join (fclos t c) acc) m bot)
    | Mu (_h, t0) -> closures_join_map bot join fclos_top fclos (open0 t t0)
    | Idx _ | Var _ ->
      (* this case should never happen *)
      assert false

  (** [truncate h t] truncates [t] at height [h] by replacing
      sub-terms that are too high with [top] *)
  let truncate n h t =
    let fresh_var () =
      let var = !n in
      incr n;
      var
    in
    let rec truncate h t =
      if height t <= h
      then t
      else
        match Hcons.node t with
        | Idx _ -> assert false (* this case should never happen *)
        | Var _ -> t
        | Mu (_, t) ->
          let var = fresh_var () in
          mu var @@ truncate h (open_var var t)
        | Abs { height = _; bools; ints; closures; tuples; variants } ->
          make
            ~bools
            ~ints
            ~closures:(clos_truncate h closures)
            ~tuples:(tuples_truncate h tuples)
            ~variants:(variants_truncate h variants)
    and clos_truncate h c =
      match c with
      | ClosTop -> clos_top
      | Clos (h', m) ->
        if h' <= h
        then c
        else if h <= 0
        then
          if clos_is_bot c
          then c
          else (* Format.eprintf "TRUNCATE ClosTop@."; *)
            clos_top
        else
          let h = h - 1 in
          clos (M.map (Env.map (truncate h)) m)
    and tuples_truncate h t =
      match t with
      | TuplesTop -> tuples_top
      | Tuples (h', m) ->
        if h' <= h
        then t
        else if h <= 0
        then
          if tuples_is_bot t
          then t
          else (* Format.eprintf "TRUNCATE TuplesTop@."; *)
            tuples_top
        else
          let h = h - 1 in
          tuples (MInts.map (MOPP.map (List.map (truncate h))) m)
    and variants_truncate h t =
      match t with
      | VariantsTop -> variants_top
      | Variants (h', m) ->
        if h' <= h
        then t
        else if h <= 0
        then
          if variants_is_bot t
          then t
          else (* Format.eprintf "TRUNCATE VariantsTop@."; *)
            variants_top
        else
          let h = h - 1 in
          variants
            (MCons.map
               (MOPP.map (function
                   | NakedOnly -> NakedOnly
                   | NakedOrWithArg arg -> NakedOrWithArg (truncate h arg)
                   | WithArgOnly arg -> WithArgOnly (truncate h arg)))
               m)
    in
    truncate h t

  module PMap = Map.Make (AtomicPath)
  (** Maps indexed by atomic paths. They are used to compute how
     many times an atomic path occurs in an abstract value. *)

  module Path = struct
    (** [count_occurs_in_paths t] produces a map whose keys are atomic
       paths, that associates to each atomic path how many times it
       occurs in [t]. The output map only contains strictly positive
       integers. *)
    let count_occurs_in_paths t =
      let join_maps m1 m2 =
        PMap.union
          (fun _ (n1, h1) (n2, h2) -> Some (max n1 n2, max h1 h2))
          m1
          m2
      and count_path p h m =
        PMap.update
          p
          (function
            | None -> Some (1, h)
            | Some (n, h') -> Some (n + 1, max h' h))
          m
      in
      let rec count h t =
        match Hcons.node t with
        | Var _ | Idx _ -> PMap.empty
        | Mu (_, t) -> count h t
        | Abs { height = _; bools = _; ints = _; closures; tuples; variants } ->
          join_maps (clos_count h closures)
          @@ join_maps (tuples_count h tuples) (variants_count h variants)
      and clos_count h = function
        | ClosTop -> PMap.empty
        | Clos (_, m) ->
          M.fold
            (fun f env acc ->
              Env.fold
                (fun x t acc ->
                  count (h + 1) t
                  |> count_path (AtomicPath.InClos (f, x)) h
                  |> join_maps acc)
                env
                acc)
            m
            PMap.empty
      and tuples_count h = function
        | TuplesTop -> PMap.empty
        | Tuples (_, m) ->
          MInts.fold
            (fun w mopp acc ->
              MOPP.fold
                (fun opp l acc ->
                  fst
                  @@ List.fold_left
                       (fun (acc, i) t ->
                         ( count (h + 1) t
                           |> count_path (AtomicPath.InTuple (w, opp, i)) h
                           |> join_maps acc,
                           i + 1 ))
                       (acc, 0)
                       l)
                mopp
                acc)
            m
            PMap.empty
      and variants_count h = function
        | VariantsTop -> PMap.empty
        | Variants (_, m) ->
          MCons.fold
            (fun c mopp acc ->
              MOPP.fold
                (fun opp varg acc ->
                  match varg with
                  | NakedOnly -> acc
                  | NakedOrWithArg arg | WithArgOnly arg ->
                    count (h + 1) arg
                    |> count_path (AtomicPath.InVariant (c, opp)) h
                    |> join_maps acc)
                mopp
                acc)
            m
            PMap.empty
      in
      count t

    (** [max_occurs t] counts the maximum number of occurrences of any
       atomic path in [t] *)
    let max_occurs = Memoize.memo (count_occurs_in_paths 0)
  end

  let clash_positions graph v1 v2 =
    let cache = ref SInts2.empty in
    let depth = ref MInts.empty in
    let record_depth t n =
      depth :=
        MInts.update
          t
          (function
            | None -> Some n
            | Some n' -> Some (min n n'))
          !depth
    and get_depth t = MInts.find t !depth in
    let add_clash ctxt t1 t2 acc =
      (ctxt, get_depth t1, get_depth t2, t2) :: acc
    in
    let rec browse n1 n2 ctxt t1 t2 acc =
      if SInts2.mem (t1, t2) !cache
      then acc
      else begin
        cache := SInts2.add (t1, t2) !cache;
        record_depth t1 n1;
        record_depth t2 n2;
        match (Uf.get_term t1 graph, Uf.get_term t2 graph) with
        | ( Some
              {
                Unif.bools = _;
                ints = _;
                closures = clos1;
                tuples = tuples1;
                variants = variants1;
              },
            Some
              ({
                 bools = _;
                 ints = _;
                 closures = clos2;
                 tuples = tuples2;
                 variants = variants2;
               } as node2) ) ->
          let n1 = get_depth t1
          and n2 = get_depth t2 in
          if n1 <> n2
          then
            (* depths are not synchronized anymore: we should stop *)
            if n1 < n2 && (not @@ Unif.node_is_empty ~weak:true node2)
            then (* we have found a clash *)
              add_clash ctxt t1 t2 acc
            else acc
          else
            acc
            |> browse_clos n1 n2 ctxt t1 clos1 t2 clos2
            |> browse_tuples n1 n2 ctxt t1 tuples1 t2 tuples2
            |> browse_variants n1 n2 ctxt t1 variants1 t2 variants2
        | None, _ | _, None -> assert false
      end
    and browse_clos n1 n2 ctxt t1 clos1 t2 clos2 acc =
      match (clos1, clos2) with
      | _, ClosTop | ClosTop, Clos _ -> acc
      | Clos m1, Clos m2 ->
        let n1' = n1 + 1
        and n2' = n2 + 1 in
        M.fold
          (fun t env2 acc ->
            match M.find_opt t m1 with
            | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
            | Some env1 ->
              Env.fold
                (fun x t2' acc ->
                  let t1' = Env.find x env1 in
                  browse
                    n1'
                    n2'
                    ((AtomicPath.InClos (t, x), n2, t2) :: ctxt)
                    t1'
                    t2'
                    acc)
                env2
                acc)
          m2
          acc
    and browse_tuples n1 n2 ctxt t1 tuples1 t2 tuples2 acc =
      let n1' = n1 + 1
      and n2' = n2 + 1 in
      match (tuples1, tuples2) with
      | _, TuplesTop | TuplesTop, Tuples _ -> acc
      | Tuples m1, Tuples m2 ->
        MInts.fold
          (fun i mopp2 acc ->
            match MInts.find_opt i m1 with
            | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
            | Some mopp1 ->
              MOPP.fold
                (fun opp l2 acc ->
                  match MOPP.find_opt opp mopp1 with
                  | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
                  | Some l1 ->
                    let acc =
                      if n1 = n2
                         && not
                              (List.exists (Unif.is_empty ~weak:true graph) l1
                              <=> List.exists
                                    (Unif.is_empty ~weak:true graph)
                                    l2)
                      then add_clash ctxt t1 t2 acc
                      else acc
                    in
                    snd
                    @@ List.fold_left2
                         (fun (j, acc) t1' t2' ->
                           ( j + 1,
                             browse
                               n1'
                               n2'
                               ((AtomicPath.InTuple (i, opp, j), n2, t2) :: ctxt)
                               t1'
                               t2'
                               acc ))
                         (0, acc)
                         l1
                         l2)
                mopp2
                acc)
          m2
          acc
    and browse_variants n1 n2 ctxt t1 variants1 t2 variants2 acc =
      let n1' = n1 + 1
      and n2' = n2 + 1 in
      match (variants1, variants2) with
      | _, VariantsTop | VariantsTop, Variants _ -> acc
      | Variants m1, Variants m2 ->
        MCons.fold
          (fun c mopp2 acc ->
            match MCons.find_opt c m1 with
            | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
            | Some mopp1 ->
              MOPP.fold
                (fun opp varg2 acc ->
                  match MOPP.find_opt opp mopp1 with
                  | None -> if n1 = n2 then add_clash ctxt t1 t2 acc else acc
                  | Some varg1 -> begin
                    match (varg1, varg2) with
                    | NakedOnly, NakedOnly -> acc
                    | WithArgOnly t1', WithArgOnly t2'
                    | NakedOrWithArg t1', NakedOrWithArg t2' ->
                      let acc =
                        if n1 = n2
                           && not
                                (Unif.is_empty ~weak:true graph t1'
                                <=> Unif.is_empty ~weak:true graph t2')
                        then add_clash ctxt t1 t2 acc
                        else acc
                      in
                      browse
                        n1'
                        n2'
                        ((AtomicPath.InVariant (c, opp), n2, t2) :: ctxt)
                        t1'
                        t2'
                        acc
                    | NakedOrWithArg t1', WithArgOnly t2'
                    | WithArgOnly t1', NakedOrWithArg t2' ->
                      let acc =
                        if n1 = n2 then add_clash ctxt t1 t2 acc else acc
                      in
                      browse
                        n1'
                        n2'
                        ((AtomicPath.InVariant (c, opp), n2, t2) :: ctxt)
                        t1'
                        t2'
                        acc
                    | NakedOnly, WithArgOnly _ | WithArgOnly _, NakedOnly ->
                      if n1 = n2 then add_clash ctxt t1 t2 acc else acc
                    | NakedOnly, NakedOrWithArg t'
                    | NakedOrWithArg t', NakedOnly ->
                      if n1 = n2 && Unif.is_empty ~weak:true graph t'
                      then add_clash ctxt t1 t2 acc
                      else acc
                  end)
                mopp2
                acc)
          m2
          acc
    in
    browse 0 0 [] v1 v2 []

  let path_of_ctxt ctxt = List.map (fun (p, _, _) -> p) ctxt

  let rec refold0 graph ctxt orig_depth pos_depth pos (success, failures) =
    match ctxt with
    | [] -> (success, failures)
    | (_atomic_path, ancestor_depth, ancestor) :: ctxt ->
      if orig_depth >= ancestor_depth
      then
        if leq_gen_raw ~weak:true graph pos ancestor
           && (* prevent folding nodes that could create a
                 semantically empty recursive node *)
           (not @@ Unif.is_empty_raw_with_hyp [pos] graph ancestor)
        then begin
          if debug_shrinking
          then
            Format.eprintf
              "  %a (orig_depth=%i, ancestor_depth=%i, pos_depth=%i)@."
              Unif.pp
              (List.rev (path_of_ctxt ctxt))
              orig_depth
              ancestor_depth
              pos_depth;
          Unif.join_vars ~widen:false graph ancestor pos;
          (* XXX: should we use widening here? *)
          (true, [])
        end
        else
          let failures =
            if orig_depth < pos_depth
               || Unif.leq_head ~weak:true graph pos ancestor
                  && (* prevent folding nodes that could create a
                        semantically empty recursive node *)
                  (not @@ Unif.is_empty_raw_with_hyp [pos] graph ancestor)
            then begin
              let path = (* atomic_path :: *) path_of_ctxt ctxt in
              if debug_shrinking
              then
                Format.eprintf
                  "  possible replacement at %a@."
                  Unif.pp
                  (List.rev path);
              path :: failures
            end
            else failures
          in
          refold0 graph ctxt orig_depth pos_depth pos (success, failures)
      else refold0 graph ctxt orig_depth pos_depth pos (success, failures)

  let refold graph ctxt orig_depth depth pos =
    if debug_shrinking
    then
      Format.eprintf
        "Folds at position %a:@."
        Unif.pp
        (List.rev (path_of_ctxt ctxt));
    refold0 graph ctxt orig_depth depth pos (false, [])

  let perform_refolds t1 t2 =
    let graph = Uf.init ()
    and n = ref 0 in
    let t1_enc = Unif.encode n graph t1 in
    let t2_enc = Unif.encode n graph t2 in
    let positions = clash_positions graph t1_enc t2_enc in
    let positions =
      List.sort
        (fun (_ctxt1, orig_depth1, depth1, _node1)
             (_ctxt2, orig_depth2, depth2, _node2) ->
          let n = Int.compare depth1 depth2 in
          if n <> 0 then n else Int.compare orig_depth1 orig_depth2)
        positions
    in
    let success, progressed, failures =
      (* XXX: use failures to try some more refolds when [success=false]! *)
      List.fold_left
        (fun (success_acc, progress_acc, failures_acc)
             (ctxt, depth_orig, depth, pos) ->
          let success, failures = refold graph ctxt depth_orig depth pos in
          if success
          then (success_acc, true, failures_acc)
          else
            let pos_path = path_of_ctxt ctxt in
            let failures =
              List.map (fun ancestor_path -> (pos_path, ancestor_path)) failures
            in
            (false, progress_acc, failures @ failures_acc))
        (true, false, [])
        positions
    in
    let t2 = Unif.decode graph t2_enc in
    (success, progressed, failures, t2)

  let occurs_leq m (m1 : (int * int) PMap.t) (m2 : (int * int) PMap.t) =
    PMap.for_all
      (fun p (n1, _h1) ->
        match PMap.find_opt p m2 with
        | None -> n1 <= m
        | Some (n2, _h2) -> n1 <= max m n2)
      m1

  let rec shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t =
    if max_iter < 0
    then begin
      Format.eprintf
        "INFO: NablaRec.shrink: maximum number of iterations was reached@.";
      truncate (ref 0) max_height t
    end
    else begin
      (* Format.eprintf "INFO: NablaRec.shrink: START perform_refolds@."; *)
      let success, progressed, failures, t = perform_refolds t_orig t in
      (* Format.eprintf "INFO: NablaRec.shrink: END perform_refolds@."; *)
      check_wf ~closed:true "shrink loop" 0 t;
      if success
      then t
      else if progressed
      then begin
        Format.eprintf "INFO: NablaRec.shrink: unsuccessful but progress made@.";
        let max_iter = max_iter - 1 in
        shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t
      end
      else if list_is_empty failures
      then t
      else begin
        Format.eprintf "INFO: NableRec.shrink: handling failures@.";
        let t =
          with_encoding
            (fun graph enc_t ->
              List.iter
                (fun (from, to_) ->
                  (* [from] is deeper in the tree than [to_], thus
                     [to_] should serve as the base for widening *)
                  try
                    let to_node = Unif.find_at_path graph to_ enc_t
                    and from_node = Unif.find_at_path graph from enc_t in
                    Unif.join_vars
                      ~widen:false (* XXX: should we force widening here? *)
                      graph
                      to_node
                      from_node
                  with Not_found ->
                    (* unification has created some ClosTop *)
                    ())
                failures)
            t
        in
        check_wf ~closed:true "shrink loop" 0 t;
        shrink ~max_iter ~occurs ~occurs_threshold ~max_height t_orig t
      end
    end

  let widen =
    Memoize.memo2 @@ fun t1 t2 ->
    let t12 = join_gen ~widen:true t1 t2 in
    if check && (not @@ leq t1 t12)
    then Format.eprintf "ERROR: NablaRec.widen: widening not expansive (LHS)@.";
    if check && (not @@ leq t2 t12)
    then Format.eprintf "ERROR: NablaRec.widen: widening not expansive (RHS)@.";
    let h1 = height t1
    and occurs1 = Path.max_occurs t1
    and occurs12 = Path.max_occurs t12 in
    let occurs_threshold = Options.occurs_threshold in
    if occurs_leq occurs_threshold occurs12 occurs1 || height t12 <= h1
    then begin
      (* Format.eprintf *)
      (* "@[WIDEN h = %i@ @[t1 =@ @[%a@]@]@ @[t2 =@ @[%a@]@]@.result \ *)
         (*    =@.@[%a@]@.@.@]" *)
      (*   h1 *)
      (*   pp *)
      (*   t1 *)
      (*   pp *)
      (*   t2 *)
      (*   pp *)
      (*   t12; *)
      t12
    end
    else begin
      if debug_shrinking
      then
        Format.eprintf
          "@[SHRINK h = %i@ @[orig =@ @[%a@]@]@ @[t =@ @[%a@]@]@.@]"
          h1
          pp
          t1
          pp
          t12;
      let res =
        shrink
          ~max_iter:(3 * (h1 + 1))
          ~occurs:occurs1
          ~occurs_threshold
          ~max_height:h1
          t1
          t12
      in
      let res = minimize res in
      if debug_shrinking
      then Format.eprintf "SHRINK result =@.@[%a@]@.@." pp res;
      if check && (not @@ leq t12 res)
      then
        Format.eprintf
          "ERROR: NablaRec.widen: shrinking not expansive@.before \
           =@.@[%a@]@.after =@.@[%a@]@."
          pp
          t12
          pp
          res;
      res
    end

  (* Two abstract values are similar if their heights differ by at
     least 2, or if they have comparable shapes *)
  let sim t1 t2 =
    let n1 = height t1
    and n2 = height t2 in
    abs (n1 - n2) >= Options.sim_threshold || leq_gen ~weak:true t1 t2
end

module Tests = struct
  module Annots = Calls.CyclicCallSites (struct
    let max_nesting = 0
    let max_last = None
  end)

  module D =
    AbsDomain
      (struct
        let minimize = true
        let occurs_threshold = 0
        let sim_threshold = 0
      end)
      (AbstractDomains.Bools)
      (AbstractDomains.Intervals)
      (Annots)
      ()

  let leq = D.leq
  let minimize = D.minimize

  let join t1 t2 =
    (* disable minimization for the purpose of tests *)
    D.(with_encoding2 (Unif.join_vars ~widen:false) t1 t2)

  let meet t1 t2 =
    (* disable minimization for the purpose of tests *)
    D.(with_encoding3 Unif.meet_vars t1 t2)

  let variants l =
    let v =
      D.variants
      @@ List.fold_right
           (fun (x, v) acc ->
             MCons.add
               (Cons.of_string x)
               (MOPP.singleton None
               @@
               match v with
               | None -> D.NakedOnly
               | Some w -> D.WithArgOnly w)
               acc)
           l
           MCons.empty
    in
    D.make
      ~bools:AbstractDomains.Bools.bot
      ~ints:AbstractDomains.Intervals.bot
      ~closures:D.clos_bot
      ~tuples:D.tuples_bot
      ~variants:v

  let tuples l =
    let v =
      D.tuples
      @@ List.fold_right
           (fun vs acc ->
             MInts.add (List.length vs) (MOPP.singleton None vs) acc)
           l
           MInts.empty
    in
    D.make
      ~bools:AbstractDomains.Bools.bot
      ~ints:AbstractDomains.Intervals.bot
      ~closures:D.clos_bot
      ~tuples:v
      ~variants:D.variants_bot

  let print_bool b = Format.printf "%s@." (if b then "true" else "false")
  let print t = Format.printf "@[%a@]" D.pp t

  let peano =
    (* peano numbers *)
    let peano = 0 in
    D.mu peano @@ variants [("Z", None); ("S", Some (D.var peano))]

  let peano_bis =
    (* peano numbers, with an external unrolling *)
    let peano = 0 in
    variants
      [
        ("Z", None);
        ( "S",
          Some (D.mu peano @@ variants [("Z", None); ("S", Some (D.var peano))])
        );
      ]

  let peano_ter =
    (* peano numners, with an internal unrolling *)
    let peano = 0 in
    D.mu peano
    @@ variants
         [
           ("Z", None);
           ("S", Some (variants [("Z", None); ("S", Some (D.var peano))]));
         ]

  let%expect_test "peano" =
    print peano;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano minimize" =
    print @@ minimize peano;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano_bis" =
    print peano_bis;
    [%expect
      {| { variants = { S: { variants = { S: 'a, Z: . } } as 'a, Z: . } } |}]

  let%expect_test "peano_bis minimize" =
    print @@ minimize peano_bis;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano_ter" =
    print peano_ter;
    [%expect
      {| { variants = { S: { variants = { S: 'a, Z: . } }, Z: . } } as 'a |}]

  let%expect_test "peano_ter minimize" =
    print @@ minimize peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let peano2 =
    (* even peano numbers *)
    let peano2 = 0 in
    D.mu peano2
    @@ variants
         [("Z", None); ("S", Some (variants [("S", Some (D.var peano2))]))]

  let peano2_bis =
    (* even peano numbers, with an external unrolling *)
    let peano2 = 0 in
    variants
      [
        ("Z", None);
        ( "S",
          Some
            (variants
               [
                 ( "S",
                   Some
                     (D.mu peano2
                     @@ variants
                          [
                            ("Z", None);
                            ("S", Some (variants [("S", Some (D.var peano2))]));
                          ]) );
               ]) );
      ]

  let peano2_ter =
    (* even peano numbers, with an internal unrolling *)
    let peano2 = 0 in
    D.mu peano2
    @@ variants
         [
           ("Z", None);
           ( "S",
             Some
               (variants
                  [
                    ( "S",
                      Some
                        (variants
                           [
                             ("Z", None);
                             ("S", Some (variants [("S", Some (D.var peano2))]));
                           ]) );
                  ]) );
         ]

  let%expect_test "peano2" =
    print peano2;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2 minimize" =
    print @@ minimize peano2;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2_bis" =
    print peano2_bis;
    [%expect
      {|
      {
        variants =
        { S:
          {
            variants =
            { S: { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a }
          },
          Z: .
        }
      } |}]

  let%expect_test "peano2_bis minimize" =
    print @@ minimize peano2_bis;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2_ter" =
    print peano2_ter;
    [%expect
      {|
      {
        variants =
        { S:
          { variants = { S: { variants = { S: { variants = { S: 'a } }, Z: . } } }
          },
          Z: .
        }
      } as 'a |}]

  let%expect_test "peano2_ter minimize" =
    print @@ minimize peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano <= peano_bis" =
    print_bool @@ leq peano peano_bis;
    [%expect {| true |}]

  let%expect_test "peano_bis <= peano" =
    print_bool @@ leq peano_bis peano;
    [%expect {| true |}]

  let%expect_test "peano <= peano_ter" =
    print_bool @@ leq peano peano_ter;
    [%expect {| true |}]

  let%expect_test "peano_ter <= peano" =
    print_bool @@ leq peano_ter peano;
    [%expect {| true |}]

  let%expect_test "peano_bis <= peano_ter" =
    print_bool @@ leq peano_bis peano_ter;
    [%expect {| true |}]

  let%expect_test "peano_ter <= peano_bis" =
    print_bool @@ leq peano_ter peano_bis;
    [%expect {| true |}]

  let%expect_test "peano2 <= peano2_bis" =
    print_bool @@ leq peano2 peano2_bis;
    [%expect {| true |}]

  let%expect_test "peano2_bis <= peano2" =
    print_bool @@ leq peano2_bis peano2;
    [%expect {| true |}]

  let%expect_test "peano2 <= peano2_ter" =
    print_bool @@ leq peano2 peano2_ter;
    [%expect {| true |}]

  let%expect_test "peano2_ter <= peano2" =
    print_bool @@ leq peano2_ter peano2;
    [%expect {| true |}]

  let%expect_test "peano2_bis <= peano2_ter" =
    print_bool @@ leq peano2_bis peano2_ter;
    [%expect {| true |}]

  let%expect_test "peano2_ter <= peano2_bis" =
    print_bool @@ leq peano2_ter peano2_bis;
    [%expect {| true |}]

  let%expect_test "peano2 <= peano" =
    print_bool @@ leq peano2 peano;
    [%expect {| true |}]

  let%expect_test "peano </= peano2" =
    print_bool @@ leq peano peano2;
    [%expect {| false |}]

  let%expect_test "peano /\\ peano_bis" =
    print @@ meet peano peano_bis;
    [%expect
      {| { variants = { S: { variants = { S: 'a, Z: . } } as 'a, Z: . } } |}];
    print @@ minimize @@ meet peano peano_bis;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano /\\ peano_ter" =
    print @@ meet peano peano_ter;
    [%expect
      {| { variants = { S: { variants = { S: 'a, Z: . } }, Z: . } } as 'a |}];
    print @@ minimize @@ meet peano peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano_bis /\\ peano_ter" =
    print @@ meet peano_bis peano_ter;
    [%expect
      {|
      {
        variants =
        { S: { variants = { S: { variants = { S: 'a, Z: . } }, Z: . } } as 'a, Z: .
        }
      } |}];
    print @@ minimize @@ meet peano_bis peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano \\/ peano_bis" =
    print @@ join peano peano_bis;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano \\/ peano_ter" =
    print @@ join peano peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano_bis \\/ peano_ter" =
    print @@ join peano_bis peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano2 /\\ peano2_bis" =
    print @@ meet peano2 peano2_bis;
    [%expect
      {|
        {
          variants =
          { S:
            {
              variants =
              { S: { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a }
            },
            Z: .
          }
        } |}];
    print @@ minimize @@ meet peano2 peano2_bis;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2 /\\ peano2_ter" =
    print @@ meet peano2 peano2_ter;
    [%expect
      {|
        {
          variants =
          { S:
            { variants = { S: { variants = { S: { variants = { S: 'a } }, Z: . } } }
            },
            Z: .
          }
        } as 'a |}];
    print @@ minimize @@ meet peano2 peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2_bis /\\ peano2_ter" =
    print @@ meet peano2_bis peano2_ter;
    [%expect
      {|
      {
        variants =
        { S:
          {
            variants =
            { S:
              {
                variants =
                { S:
                  {
                    variants =
                    { S: { variants = { S: { variants = { S: 'a } }, Z: . } } }
                  },
                  Z: .
                }
              } as 'a
            }
          },
          Z: .
        }
      } |}];
    print @@ minimize @@ meet peano2_bis peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2 \\/ peano2_bis" =
    print @@ join peano2 peano2_bis;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2 \\/ peano2_ter" =
    print @@ join peano2 peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano2_bis \\/ peano2_ter" =
    print @@ join peano2_bis peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano /\\ peano2" =
    print @@ meet peano peano2;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let%expect_test "peano \\/ peano2" =
    print @@ join peano peano2;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano_bis /\\ peano_ter" =
    print @@ meet peano_bis peano_ter;
    [%expect
      {|
      {
        variants =
        { S: { variants = { S: { variants = { S: 'a, Z: . } }, Z: . } } as 'a, Z: .
        }
      } |}];
    print @@ minimize @@ meet peano_bis peano_ter;
    [%expect {| { variants = { S: 'a, Z: . } } as 'a |}]

  let%expect_test "peano2_bis /\\ peano2_ter" =
    print @@ meet peano2_bis peano2_ter;
    [%expect
      {|
      {
        variants =
        { S:
          {
            variants =
            { S:
              {
                variants =
                { S:
                  {
                    variants =
                    { S: { variants = { S: { variants = { S: 'a } }, Z: . } } }
                  },
                  Z: .
                }
              } as 'a
            }
          },
          Z: .
        }
      } |}];
    print @@ minimize @@ meet peano2_bis peano2_ter;
    [%expect {| { variants = { S: { variants = { S: 'a } }, Z: . } } as 'a |}]

  let empty_variant = D.mu 0 @@ variants [("C", Some (D.var 0))]

  let%expect_test "minimize empty variant" =
    (* FIXME *)
    print @@ minimize empty_variant;
    [%expect {| ⊥ |}]

  let%expect_test "empty variant <= empty" =
    (* FIXME *)
    print_bool @@ leq empty_variant D.bot;
    [%expect {| true |}]

  let empty_variant2 =
    D.mu 0
    @@ D.mu 1
    @@ variants [("C1", Some (D.var 0)); ("C2", Some (D.var 1))]

  let%expect_test "minimize empty variant 2" =
    (* FIXME *)
    print @@ minimize empty_variant2;
    [%expect {| ⊥ |}]

  let%expect_test "empty variant 2 <= empty" =
    (* FIXME *)
    print_bool @@ leq empty_variant2 D.bot;
    [%expect {| true |}]

  let empty_tuple = D.mu 0 @@ tuples [[D.var 0; D.var 0]]

  let%expect_test "minimize empty tuple" =
    (* FIXME *)
    print @@ minimize empty_tuple;
    [%expect {| ⊥ |}]

  let%expect_test "empty tuple <= empty" =
    (* FIXME *)
    print_bool @@ leq empty_tuple D.bot;
    [%expect {| true |}]

  let empty_tuple2 = D.mu 0 @@ D.mu 1 @@ tuples [[D.var 0; D.var 1]]

  let%expect_test "minimize empty tuple 2" =
    (* FIXME *)
    print @@ minimize empty_tuple2;
    [%expect {| ⊥ |}]

  let%expect_test "empty tuple 2 <= empty" =
    (* FIXME *)
    print_bool @@ leq empty_tuple2 D.bot;
    [%expect {| true |}]

  (* type of peano numbers encoded as tuples, and with an unnecessary
     external unfolding *)
  let peano_tuple = D.mu 0 @@ tuples [[]; [tuples [[]; [D.var 0]]]]

  let%expect_test "minimize peano tuple" =
    print @@ minimize peano_tuple;
    [%expect {| { tuples = { 0: (), 1: 'a } } as 'a |}]

  (* type of peano numbers encoded as tuples, and with an unnecessary
     internal unfolding *)
  let peano_tuple_bis = D.mu 0 @@ tuples [[]; [tuples [[]; [D.var 0]]]]

  let%expect_test "minimize peano tuple bis" =
    print @@ minimize peano_tuple_bis;
    [%expect {| { tuples = { 0: (), 1: 'a } } as 'a |}]
end
