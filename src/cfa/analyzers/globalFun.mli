(** Module for lambdas and recursive lambdas *)

open Lang.Ast

type t =
  | Lam of PP.t * PP.t * Var.t * Term.t
      (** [Lam (loc, xloc, x, t)]:
      - [loc] is the location of the Lambda
      - [xloc] is the location of the variable [x] of the lambda
      - [x] is the variable of the lambda
      - [t] is the body of the lambda
  *)
  | FixLam of PP.t * PP.t * Var.t * PP.t * Var.t * Term.t
      (** [FixLam (loc, floc, f, xloc, x, t)]:
      - [loc] is the location of the Lambda
      - [floc] is the location of the self-variable [f] of the lambda
      - [f] is the self-variable of the lambda
      - [xloc] is the location of the variable [x] of the lambda
      - [x] is the variable of the lambda
      - [t] is the body of the lambda
  *)

val compare : t -> t -> int
val fv : t -> Vars.t

val pp_rec : bool -> Format.formatter -> t -> unit
(** [pp_rec b] is a pretty-printer for Funs. If [b = true], then the
   printed result is enclosed in parentheses. *)
