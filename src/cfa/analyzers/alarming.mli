(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

open Lang.Ast

module Make (_ : sig
  val no_alarms : bool
end) (Calls : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) (VAbs : sig
  type t

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val meet : t -> t -> t
end) : sig
  module Alarm : sig
    type t

    val non_boolean : VAbs.t -> t
    val non_numeric : VAbs.t -> t
    val non_functional : VAbs.t -> t
    val non_exhaustive_match : VAbs.t -> t
  end

  module Alarms : sig
    type t

    val is_bot : t -> bool
    val pp : Format.formatter -> t -> unit
    val join : t -> t -> t
  end

  type 'a m
  type t = VAbs.t m

  val bot : t
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val make : Alarms.t -> 'a -> 'a m
  val alarm : PP.t -> Calls.t -> Alarm.t -> unit m
  val get_alarms : t -> (PP.t * string) list
  val run : 'a m -> Alarms.t * 'a
  val return : 'a -> 'a m
  val bind : 'a m -> ('a -> 'b m) -> 'b m
  val ( let* ) : 'a m -> ('a -> 'b m) -> 'b m
  val map : 'a m -> ('a -> 'b) -> 'b m
  val ( let+ ) : 'a m -> ('a -> 'b) -> 'b m
  val plus : 'a m -> 'b m -> ('a * 'b) m
  val ( and+ ) : 'a m -> 'b m -> ('a * 'b) m
end
