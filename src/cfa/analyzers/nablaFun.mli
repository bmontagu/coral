(** Module for lambdas and recursive lambdas *)

open Lang.Ast

type t =
  | Lam of PP.t * Var.t * Term.t
      (** [Lam (loc, x, t)]:
      - [loc] is the location of the Lambda
      - [x] is the variable of the lambda
      - [t] is the body of the lambda
  *)
  | FixLam of PP.t * Var.t * Var.t * Term.t
      (** [FixLam (loc, floc, f, xloc, x, t)]:
      - [loc] is the location of the Lambda
      - [f] is the self-variable of the lambda
      - [x] is the variable of the lambda
      - [t] is the body of the lambda
  *)

val hash_fold : t -> Hash.state -> Hash.state
val compare : t -> t -> int
val fv : t -> Vars.t

val pp_rec : bool -> Format.formatter -> t -> unit
(** [pp_rec b] is a pretty-printer for Funs. If [b = true], then the
   printed result is enclosed in parentheses. *)

val is_recursive : t -> bool
