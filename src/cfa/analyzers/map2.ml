(** [Make(X1)(X2)] creates a module of maps whose keys are of type
   [X1.t * X2.t] and whose implementation is based on the nesting of
   maps [Map.Make(X1)] and [Map.Make(X2)]. *)
module Make (X1 : sig
  include Map.OrderedType

  val pp : Format.formatter -> t -> unit
end) (X2 : sig
  include Map.OrderedType

  val pp : Format.formatter -> t -> unit
end) =
struct
  module M1 = Map.Make (X1)
  module M2 = Map.Make (X2)

  type key = X1.t * X2.t
  type 'a t = 'a M2.t M1.t

  let empty = M1.empty

  let find_opt (k1, k2) m1 =
    match M1.find_opt k1 m1 with
    | None as o -> o
    | Some m2 -> M2.find_opt k2 m2

  let update (k1, k2) f m1 =
    M1.update
      k1
      (function
        | None -> (
          match f None with
          | None as o -> o
          | Some v -> Some (M2.singleton k2 v))
        | Some m2 ->
          let m2' = M2.update k2 f m2 in
          if M2.is_empty m2' then None else Some m2')
      m1

  let merge f m1 m1' =
    M1.merge
      (fun k1 o1 o1' ->
        match (o1, o1') with
        | Some m2, Some m2' ->
          let m = M2.merge (fun k2 o2 o2' -> f (k1, k2) o2 o2') m2 m2' in
          if M2.is_empty m then None else Some m
        | Some m2, None ->
          let m =
            M2.fold
              (fun k2 v acc ->
                match f (k1, k2) (Some v) None with
                | Some v' -> M2.add k2 v' acc
                | None -> acc)
              m2
              M2.empty
          in
          if M2.is_empty m then None else Some m
        | None, Some m2' ->
          let m =
            M2.fold
              (fun k2 v acc ->
                match f (k1, k2) None (Some v) with
                | Some v' -> M2.add k2 v' acc
                | None -> acc)
              m2'
              M2.empty
          in
          if M2.is_empty m then None else Some m
        | None, None -> assert false)
      m1
      m1'

  let cardinal m1 = M1.fold (fun _ m2 n -> n + M2.cardinal m2) m1 0

  let fold f m1 acc =
    M1.fold
      (fun k1 m2 acc -> M2.fold (fun k2 v acc -> f (k1, k2) v acc) m2 acc)
      m1
      acc

  (** [fold_similar f sim2 (k1, k2) m acc] folds the function [f] on
     the map [m] with accumulator [acc] only for the keys [(k1', k2')]
     in [m] such that [k1' = k1] and [sim2 k2 k2' = true]. *)
  let fold_similar f sim2 (k1, k2) m1 acc =
    match M1.find_opt k1 m1 with
    | None -> acc
    | Some m2 ->
      M2.fold
        (fun k2' v acc -> if sim2 k2 k2' then f (k1, k2') v acc else acc)
        m2
        acc

  let to_seq m1 =
    m1
    |> M1.to_seq
    |> Seq.concat_map (fun (x1, m2) ->
           m2 |> M2.to_seq |> Seq.map (fun (x2, v) -> ((x1, x2), v)))

  let pp pp_val fmt m1 =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@ ")
      (fun fmt ((k1, k2), v) ->
        Format.fprintf fmt "@[%a %a@ ->@ %a@]" X1.pp k1 X2.pp k2 pp_val v)
      fmt
      (List.concat_map
         (fun (k1, m2) ->
           List.map (fun (k2, v) -> ((k1, k2), v)) (M2.bindings m2))
         (M1.bindings m1))
end
