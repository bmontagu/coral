open Lang.Ast
(** Module for lambdas and recursive lambdas *)

type t =
  | Lam of PP.t * Var.t * Term.t
  | FixLam of PP.t * Var.t * Var.t * Term.t

let hash_fold t s =
  match t with
  | Lam (loc, _, _) -> Hash.fold 0 @@ PP.hash_fold loc s
  | FixLam (loc, _, _, _) -> Hash.fold 1 @@ PP.hash_fold loc s

let compare f1 f2 =
  match (f1, f2) with
  | Lam (loc1, _, _), Lam (loc2, _, _)
  | FixLam (loc1, _, _, _), FixLam (loc2, _, _, _) -> PP.compare loc1 loc2
  | Lam _, FixLam _ -> -1
  | FixLam _, Lam _ -> 1

let pp_rec parens fmt =
  let open Format in
  function
  | Lam (loc, x, t) ->
    if parens
    then fprintf fmt "@[(fun[%a]@ %a@ ->@ %a)@]" PP.pp loc Var.pp x Term.pp t
    else fprintf fmt "@[fun[%a]@ %a@ ->@ %a@]" PP.pp loc Var.pp x Term.pp t
  | FixLam (loc, f, x, t) ->
    if parens
    then
      fprintf
        fmt
        "@[(rec fun[%a]@ %a@ %a@ ->@ %a)@]"
        PP.pp
        loc
        Var.pp
        f
        Var.pp
        x
        Term.pp
        t
    else
      fprintf
        fmt
        "@[rec fun[%a]@ %a@ %a@ ->@ %a@]"
        PP.pp
        loc
        Var.pp
        f
        Var.pp
        x
        Term.pp
        t

let pp = pp_rec false

let fv =
  let module MiniTerm = struct
    type nonrec t = t

    let compare = compare
    let pp = pp
  end in
  let module TMemo =
    Memo.Make (Logs.Silent) (MiniTerm) (MapMake (MiniTerm)) (Vars)
  in
  TMemo.memo @@ function
  | Lam (_loc, x, t) -> Vars.remove x (Term.fv t)
  | FixLam (_loc, f, x, t) -> Vars.remove f (Vars.remove x (Term.fv t))

let is_recursive = function
  | FixLam _ -> true
  | Lam _ -> false
