(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type COMPARABLE = sig
  type t

  val compare : t -> t -> int
end

module AbsDomain
    (_ : sig
      val sim_threshold : int
    end)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (Annot : COMPARABLE) :
  NablaCommon.ABS_DOMAIN
    with type bools := Bools.t
     and type ints := Ints.t
     and type annot := Annot.t
