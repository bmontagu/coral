(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** The two-point lattice *)

type t =
  | Bot
  | Top
[@@deriving ord]

let bot = Bot
let top = Top

let is_bot = function
  | Bot -> true
  | Top -> false

let is_top = function
  | Bot -> false
  | Top -> true

let leq v1 v2 =
  match (v1, v2) with
  | Bot, _ | _, Top -> true
  | Top, Bot -> false

let join v1 v2 =
  match (v1, v2) with
  | Bot, v | v, Bot -> v
  | Top, Top -> top

let widen = join

let meet v1 v2 =
  match (v1, v2) with
  | Top, v | v, Top -> v
  | Bot, Bot -> bot
