(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

module Analyze
    (_ : Logs.S)
    (_ : sig
      val refine_branches : bool
      val branch_is_context : bool
      val let_is_context : bool
      val finer_context : bool
      val no_env_restrict : bool
      val kill_unreachable : bool
      val record_closure_creation_context : bool
      val cache_non_calls : bool
      val cache_widened_calls_only : bool
      val no_alarms : bool
      val debug : bool
      val print_memo_table : bool
    end)
    (_ : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (_ : Calls.S)
    (_ : AbstractDomains.Sigs.BOOL_DOMAIN)
    (_ : AbstractDomains.Sigs.INT_DOMAIN) : sig
  module CtxtEnv : sig
    type t

    val pp : Format.formatter -> t -> unit
  end

  module Abs : sig
    type t

    val get_alarms : t -> (Lang.Ast.PP.t * string) list
    val pp : Format.formatter -> t -> unit
  end

  module Entry : sig
    type t

    val pp : Format.formatter -> t -> unit
  end

  val eval : Lang.Ast.Term.t -> Abs.t * Entry.t Memo.Statistics.t
end
