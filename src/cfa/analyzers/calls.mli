(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type S = sig
  type t

  val hash_fold : t -> Hash.state -> Hash.state
  val compare : t -> t -> int
  val is_max : t -> bool
  val empty : t
  val extend : t -> Lang.Ast.PP.t -> Lang.Ast.PP.t option -> t
  val pp : Format.formatter -> t -> unit
end

(** Module implementing the "max k cyclic call sites" strategy with
   "at most l last call sites" in the case of recursive calls. *)
module CyclicCallSites (_ : sig
  val max_nesting : int

  val max_last : int option
  (** If [max_last = Some 0], this is equivalent to the "max k
          cyclic call sites policy". If [max_last = None] is provided,
          then the bound is chosen dynamically, taking the length of
          the last call site repetition as a value. *)
end) : S

(** Module implementing the "last k call sites" strategy, as in k-CFA *)
module LastCallSites (_ : sig
  val max_length : int
end) : S
