(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2023
*)

open Lang.Ast

module type ABS_DOMAIN = sig
  type ints
  type bools
  type annot
  type t

  val compare : t -> t -> int
  val bot : t
  val is_bot : t -> bool
  val top : t
  val singleton_closure : annot option -> NablaFun.t -> t Env.t -> t
  val is_fun : t -> bool
  val ints : ints -> t
  val is_int : t -> bool
  val get_ints : t -> ints * t
  val bools : bools -> t
  val get_bools : t -> bools * t
  val is_bool : t -> bool
  val mem_bool : bool -> t -> bool
  val singleton_tuple : PP.t option -> int -> t list -> t

  val get_tuple : int -> t -> ((PP.t option * t list) list * t) option
  (** [get_tuple n v] either returns [None] if [v] contains no record of
   width [n], or returns [Some (vss, rem)] where [vss] is an
   association list between optional program points (that denote de
   creation points of the [n]-ary tuples) and the list of its
   arguments [vs]. [vs] is a list of length [n] that contains the
   possible arguments of the tuple. The remainder [rem] is the
   abstract value for of all the remaining possible values in [v]. *)

  val singleton_variant : PP.t option -> Cons.t -> t option -> t
  val get_variant_naked : Cons.t -> t -> (PP.t option list * t) option
  val get_variant_with_arg : Cons.t -> t -> ((PP.t option * t) list * t) option
  val leq : t -> t -> bool
  val join : t -> t -> t
  val get_closures : t -> ((NablaFun.t * t Env.t) list option * t) option

  val closures_join_map :
    'a -> ('a -> 'a -> 'a) -> 'a -> (NablaFun.t -> t Env.t -> 'a) -> t -> 'a

  val widen : t -> t -> t
  val meet : t -> t -> t
  val sim : t -> t -> bool
  val pp : Format.formatter -> t -> unit
end

module Analyze
    (Log : Logs.S)
    (Options : sig
      val refine_branches : bool
      val branch_is_context : bool
      val let_is_context : bool
      val finer_context : bool
      val no_env_restrict : bool
      val kill_unreachable : bool
      val record_closure_creation_context : bool
      val record_tuple_creation_location : bool
      val record_variant_creation_location : bool
      val cache_non_calls : bool
      val cache_widened_calls_only : bool
      val no_alarms : bool
      val disprove_alarms : bool
      val debug : bool
    end)
    (Solver : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (Calls : Calls.S)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (VAbs : ABS_DOMAIN
              with type bools := Bools.t
               and type ints := Ints.t
               and type annot := Calls.t) =
struct
  module Abs = Alarming.Make (Options) (Calls) (VAbs)

  module VEnv = struct
    type t = VAbs.t Env.t option
    (** Sets of environments of variables: [None] is the bottom element,
     and [Some Env.empty] is the top element. Variables that are not
     recorded in environments are implicitly set to [VAbs.top]. *)

    let top = Some Env.empty
    let bot = None

    (** Inclusion of two environments *)
    let leq oenv1 oenv2 =
      match (oenv1, oenv2) with
      | None, _ -> true
      | Some _, None -> false
      | Some env1, Some env2 ->
        Env.for_all
          (fun x v2 ->
            match Env.find_opt x env1 with
            | None -> VAbs.leq VAbs.top v2
            | Some v1 -> VAbs.leq v1 v2)
          env2

    (** Meet of two environments *)
    let meet oenv1 oenv2 =
      match (oenv1, oenv2) with
      | None, _ | _, None -> None
      | Some env1, Some env2 -> (
        let exception Inconsistent in
        try
          Some
            (Env.union
               (fun _x v1 v2 ->
                 let v12 = VAbs.meet v1 v2 in
                 if VAbs.is_bot v12 then raise Inconsistent else Some v12)
               env1
               env2)
        with Inconsistent -> None)

    (** Join of two environments *)
    let join oenv1 oenv2 =
      match (oenv1, oenv2) with
      | None, o | o, None -> o
      | Some env1, Some env2 ->
        Some
          (Env.merge
             (fun _x ov1 ov2 ->
               match (ov1, ov2) with
               | (None as none), _ | _, (None as none) -> none
               | Some v1, Some v2 -> Some (VAbs.join v1 v2))
             env1
             env2)

    (** Widening of two environments *)
    let widen oenv1 oenv2 =
      match (oenv1, oenv2) with
      | None, env | env, None -> env
      | Some env1, Some env2 ->
        Some
          (Env.merge
             (fun _x ov1 ov2 ->
               match (ov1, ov2) with
               | (None as none), _ | _, (None as none) -> none
               | Some v1, Some v2 -> Some (VAbs.widen v1 v2))
             env1
             env2)
  end

  let b_true = Bools.singleton true
  let abs_true = VAbs.bools b_true
  let b_false = Bools.singleton false
  let abs_false = VAbs.bools b_false

  let make_closure_annot calls =
    if Options.record_closure_creation_context then Some calls else None

  let make_tuple_pp pp =
    if Options.record_tuple_creation_location then Some pp else None

  let make_variant_pp pp =
    if Options.record_variant_creation_location then Some pp else None

  (** Memoization module for the function [backward] *)
  module MemoFixBackward = struct
    module AbsEnvAbs = struct
      type t = VAbs.t Env.t * VAbs.t [@@deriving ord]

      let pp fmt (env, vout) =
        Format.fprintf
          fmt
          "@[{ @[@[env =@ %a@]@ @[vout =@ %a@]@]@ }@]"
          (Env.pp VAbs.pp)
          env
          VAbs.pp
          vout
    end

    module CallsTerm = struct
      type t = Calls.t * Term.t [@@deriving ord]

      let pp fmt (calls, t) =
        Format.fprintf fmt "@[@[[%a]@]@ %a@]" Calls.pp calls Term.pp t
    end

    module AbsClosure = struct
      type t = (Calls.t * Term.t) * (VAbs.t Env.t * VAbs.t) [@@deriving ord]

      let widen (ct1, (env1, vout1)) (_ct2, (env2, vout2)) =
        let env12 = Env.union (fun _ a1 a2 -> Some (VAbs.widen a1 a2)) env1 env2
        and vout12 = VAbs.widen vout1 vout2 in
        (ct1, (env12, vout12))
    end

    module M = struct
      include Map2.Make (CallsTerm) (AbsEnvAbs)

      let sim (env1, v1) (env2, v2) =
        Env.for_all (fun x a1 -> VAbs.sim a1 (Env.find x env2)) env1
        && VAbs.sim v1 v2

      let fold_similar f k m = fold_similar f sim k m
    end

    include Solver (Logs.Silent) (AbsClosure) (M) (VEnv)
  end

  (** [add_bindings new_bindings env] adds to [env] the bindings in
     [new_bindings]. The new bindings will replace existing ones. *)
  let add_bindings new_bindings env = Env.fold Env.add new_bindings env

  (** [remove_bindings bindings_to_remove env] removes from [env] the
     bindings in [bindings_to_remove], if they exist. *)
  let remove_bindings bindings_to_remove env =
    Env.fold (fun x _ env -> Env.remove x env) bindings_to_remove env

  (** [get_envs_from_simple_pattern input pat] returns the environment
     that binds the variables in [pat] to the sub-parts of the
     abstract value [input]. *)
  let get_envs_from_simple_pattern input pat =
    let open Abs in
    match pat with
    | Term.PWildcard -> return Env.empty
    | PVar (_id, x) -> return (Env.singleton x input)

  (** [get_envs_from_pattern backward calls env t0 input pat] returns
     the possible environments [Some (refined_env, branch_env)] (or
     [None] if this is not possible), and the remainder [rem], i.e.
     the values that remain to be matched from [input], after matching
     against the pattern [p] has failed on the term [t0].
     [refined_env] is a refined version of [env] with the same
     variables, whereas [branch_env] is the environment that binds the
     variables as defined by the pattern [pat] *)
  let get_envs_from_pattern backward calls env t0 input pat =
    let open Abs in
    match pat with
    | Term.PSimple (Any, p) ->
      if Options.kill_unreachable && VAbs.is_bot input
      then return (None, VAbs.bot)
      else
        let* new_env = get_envs_from_simple_pattern input p in
        return (Some (env, new_env), VAbs.bot)
    | Term.PSimple (Bool, p) ->
      let b, rem = VAbs.get_bools input in
      if Options.kill_unreachable && Bools.is_bot b
      then return (None, input)
      else
        let refined_env =
          if Options.refine_branches
          then
            let learned = backward calls env t0 (VAbs.bools b) in
            VEnv.meet (Some env) learned
          else Some env
        in
        let* envs =
          match refined_env with
          | None -> return None
          | Some refined_env ->
            let* branch_env = get_envs_from_simple_pattern (VAbs.bools b) p in
            return @@ Some (refined_env, branch_env)
        in
        return (envs, rem)
    | Term.PSimple (Int, p) ->
      let i, rem = VAbs.get_ints input in
      if Options.kill_unreachable && Ints.is_bot i
      then return (None, input)
      else
        let refined_env =
          if Options.refine_branches
          then
            let learned = backward calls env t0 (VAbs.ints i) in
            VEnv.meet (Some env) learned
          else Some env
        in
        let* envs =
          match refined_env with
          | None -> return None
          | Some refined_env ->
            let* branch_env = get_envs_from_simple_pattern (VAbs.ints i) p in
            return @@ Some (refined_env, branch_env)
        in
        return (envs, rem)
    | Term.PTuple (n, l) -> (
      match VAbs.get_tuple n input with
      | None -> return (None, input)
      | Some (args, rem) ->
        if Options.kill_unreachable
           && List.for_all (fun (_, vs) -> List.exists VAbs.is_bot vs) args
        then return (None, rem)
        else
          let refined_env =
            if Options.refine_branches
            then
              let tuple_n =
                List.fold_left
                  (fun acc (opp, vs) ->
                    VAbs.join acc @@ VAbs.singleton_tuple opp n vs)
                  VAbs.bot
                  args
              in
              let learned = backward calls env t0 tuple_n in
              VEnv.meet (Some env) learned
            else Some env
          in
          let* envs =
            match refined_env with
            | None -> return None
            | Some refined_env -> (
              let* branch_env =
                List.fold_left
                  (fun env_acc (_opp, vs) ->
                    let* env_acc = env_acc in
                    let* env =
                      List.fold_left2
                        (fun m pi vi ->
                          let* acc = m in
                          let* new_env = get_envs_from_simple_pattern vi pi in
                          return @@ VEnv.meet (Some new_env) acc)
                        (return @@ Some Env.empty)
                        l
                        vs
                    in
                    return @@ VEnv.join env_acc env)
                  (return VEnv.bot)
                  args
              in
              match branch_env with
              | None as none -> return none
              | Some branch_env -> return @@ Some (refined_env, branch_env))
          in
          return (envs, rem))
    | PVariant (c, None) -> (
      match VAbs.get_variant_naked c input with
      | None -> return (None, input)
      | Some (pps, rem) ->
        let refined_env =
          if Options.refine_branches
          then
            let cons_c =
              List.fold_left
                (fun acc opp ->
                  VAbs.join acc @@ VAbs.singleton_variant opp c None)
                VAbs.bot
                pps
            in
            let learned = backward calls env t0 cons_c in
            VEnv.meet (Some env) learned
          else Some env
        in
        let envs =
          match refined_env with
          | None -> None
          | Some refined_env -> Some (refined_env, Env.empty)
        in
        return (envs, rem))
    | PVariant (c, Some p) -> (
      match VAbs.get_variant_with_arg c input with
      | None -> return (None, input)
      | Some (vs, rem) ->
        let refined_env =
          if Options.refine_branches
          then
            let cons_c =
              List.fold_left
                (fun acc (opp, v) ->
                  VAbs.join acc @@ VAbs.singleton_variant opp c (Some v))
                VAbs.bot
                vs
            in
            let learned = backward calls env t0 cons_c in
            VEnv.meet (Some env) learned
          else Some env
        in
        let* envs =
          match refined_env with
          | None -> return None
          | Some refined_env ->
            let v =
              List.fold_left (fun acc (_opp, v) -> VAbs.join acc v) VAbs.bot vs
            in
            let* branch_env = get_envs_from_simple_pattern v p in
            return @@ Some (refined_env, branch_env)
        in
        return (envs, rem))

  let backward_solve f =
    let curry f calls env t vout = f ((calls, t), (env, vout)) in
    let curry1 f widen calls env t vout = f widen ((calls, t), (env, vout)) in
    let uncurry f g ((calls, t), (env, vout)) = f (curry1 g) calls env t vout in
    curry @@ MemoFixBackward.solve ~debug:false ~transient:false (uncurry f)

  (** [backward eval calls env t v] is a backward analyzer, that
     returns necessary informations about the variables of [t], when
     we know it forward evaluated to [v] using [eval]. This function
     is useful to refine the knowledge after a branch has been taken
     in the forward analyzer. *)
  let backward eval =
    backward_solve @@ fun request ->
    let rec backward calls env t vout =
      let backward calls env t vout =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if Options.cache_non_calls
        then request false calls env t vout
        else backward calls env t vout
      and backward_widen calls env t vout =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if (not Options.cache_widened_calls_only) || Calls.is_max calls
        then request (Calls.is_max calls) calls env t vout
        else backward calls env t vout
      in
      match t.Term.term with
      | Term.Var x -> Some (Env.singleton x vout)
      | Term.Int n ->
        let v = VAbs.(meet vout (ints (Ints.singleton n))) in
        if VAbs.is_bot v then VEnv.bot else VEnv.top
      | Term.UnknownInt ->
        let v = VAbs.(meet vout @@ ints Ints.(interval min_int max_int)) in
        if VAbs.is_bot v then VEnv.bot else VEnv.top
      | Term.Bool b ->
        let v = VAbs.(meet vout (bools (Bools.singleton b))) in
        if VAbs.is_bot v then VEnv.bot else VEnv.top
      | Term.UnknownBool ->
        let v = VAbs.(meet vout (bools Bools.top)) in
        if VAbs.is_bot v then VEnv.bot else VEnv.top
      | Term.Unop (NEG, t) ->
        let i, _ = VAbs.get_ints vout in
        backward calls env t VAbs.(ints @@ Ints.Transfer.neg i)
      | Term.Binop (PLUS, t1, t2) ->
        let i, _ = VAbs.get_ints vout in
        let v1 = eval calls env t1 in
        let i1, _ = VAbs.get_ints v1 in
        let v2 = eval calls env t2 in
        let i2, _ = VAbs.get_ints v2 in
        let env1 = backward calls env t1 VAbs.(ints @@ Ints.Transfer.minus i i2)
        and env2 =
          backward calls env t2 VAbs.(ints @@ Ints.Transfer.minus i i1)
        in
        VEnv.meet env1 env2
      | Term.Binop (MINUS, t1, t2) ->
        let i, _ = VAbs.get_ints vout in
        let v1 = eval calls env t1 in
        let i1, _ = VAbs.get_ints v1 in
        let v2 = eval calls env t2 in
        let i2, _ = VAbs.get_ints v2 in
        let env1 = backward calls env t1 VAbs.(ints @@ Ints.Transfer.plus i i2)
        and env2 =
          backward calls env t2 VAbs.(ints @@ Ints.Transfer.(minus i1 i))
        in
        VEnv.meet env1 env2
      | Term.Binop (TIMES, t1, t2) ->
        let i, _ = VAbs.get_ints vout in
        if Ints.is_bot i
        then VEnv.bot
        else
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let env1 = backward calls env t1 VAbs.(ints i1)
          and env2 = backward calls env t2 VAbs.(ints i2) in
          VEnv.meet env1 env2
      | Term.Binop (LE, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.le i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.Binop (LT, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.lt i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.Binop (GE, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.ge i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.Binop (GT, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.gt i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.Binop (EQ, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.eq i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.Binop (NEQ, t1, t2) ->
        let backward_when b =
          let v1 = eval calls env t1 in
          let i1, _ = VAbs.get_ints v1 in
          let v2 = eval calls env t2 in
          let i2, _ = VAbs.get_ints v2 in
          let ints1, ints2 = Ints.Transfer.neq i1 i2 b in
          let env1 = backward calls env t1 (VAbs.ints ints1)
          and env2 = backward calls env t2 (VAbs.ints ints2) in
          VEnv.meet env1 env2
        in
        let if_true =
          if VAbs.mem_bool true vout then backward_when true else None
        and if_false =
          if VAbs.mem_bool false vout then backward_when false else None
        in
        VEnv.join if_true if_false
      | Term.BOrElse (t1, t2) ->
        let if_true =
          if VAbs.mem_bool true vout
          then
            let env1_t = backward calls env t1 abs_true
            and env1_f = backward calls env t1 abs_false
            and env2_t = backward calls env t2 abs_true in
            VEnv.join env1_t (VEnv.meet env1_f env2_t)
          else None
        and if_false =
          if VAbs.mem_bool false vout
          then
            let env1_f = backward calls env t1 abs_false
            and env2_f = backward calls env t2 abs_false in
            VEnv.meet env1_f env2_f
          else None
        in
        VEnv.join if_true if_false
      | Term.BAndAlso (t1, t2) ->
        let if_true =
          if VAbs.mem_bool true vout
          then
            let env1_t = backward calls env t1 abs_true
            and env2_t = backward calls env t2 abs_true in
            VEnv.meet env1_t env2_t
          else None
        and if_false =
          if VAbs.mem_bool false vout
          then
            let env1_t = backward calls env t1 abs_true
            and env1_f = backward calls env t1 abs_false
            and env2_f = backward calls env t2 abs_false in
            VEnv.join env1_f (VEnv.meet env1_t env2_f)
          else None
        in
        VEnv.join if_true if_false
      | Term.Lam _ | Term.FixLam _ -> begin
        match VAbs.get_closures vout with
        | None -> VEnv.bot
        | Some (None, _rem) ->
          (* we got clos_top *)
          VEnv.top
        | Some (Some l, _rem) ->
          List.fold_left
            (fun acc (f, f_env) ->
              match f with
              | NablaFun.Lam (id, _, _) | FixLam (id, _, _, _) ->
                if PP.compare id t.id = 0
                then
                  let r_env =
                    (* [env] restricted to the variables of [f_env] *)
                    Env.merge
                      (fun _ ov1 ov2 ->
                        match (ov1, ov2) with
                        | None, _ -> ov1
                        | Some _, Some _ -> ov2
                        | Some _, None ->
                          (* this should not happen: it would be a
                             binding for a free variable of the
                             closure, but that is not available in
                             the environment *)
                          assert false)
                      f_env
                      env
                  in
                  VEnv.join acc (VEnv.meet (Some f_env) (Some r_env))
                else acc)
            VEnv.bot
            l
      end
      | Term.Match (t0, branches) ->
        let v0 = eval calls env t0 in
        if VAbs.is_bot v0
        then VEnv.bot
        else
          snd
          @@ List.fold_left
               (fun (i, env_acc) (pati, ti) ->
                 match
                   snd
                   @@ Abs.run
                   @@ get_envs_from_pattern backward calls env t0 v0 pati
                 with
                 | None, _rem -> (i + 1, env_acc)
                 | Some (refined_env, branch_env), _rem ->
                   let oenv =
                     let oenv1 = Some refined_env in
                     let oenv2 =
                       let calls =
                         if Options.branch_is_context
                         then
                           Calls.extend
                             calls
                             t.id
                             (if Options.finer_context then Some i else None)
                         else calls
                       in
                       match
                         backward
                           calls
                           (add_bindings branch_env refined_env)
                           ti
                           vout
                       with
                       | None -> VEnv.bot
                       | Some env2 -> Some (remove_bindings branch_env env2)
                     in
                     VEnv.meet oenv1 oenv2
                   in
                   (i + 1, VEnv.join env_acc oenv))
               (0, VEnv.bot)
               branches
      | Term.If (t1, t2, t3) ->
        let env_true =
          let test = backward calls env t1 abs_true in
          match VEnv.meet test (Some env) with
          | None -> VEnv.bot
          | Some refined_env ->
            let branch = backward calls refined_env t2 vout in
            VEnv.meet test branch
        in
        let env_false =
          let test = backward calls env t1 abs_false in
          match VEnv.meet test (Some env) with
          | None -> VEnv.bot
          | Some refined_env ->
            let branch = backward calls refined_env t3 vout in
            VEnv.meet test branch
        in
        VEnv.join env_true env_false
      | Term.Let (_xloc, x, t1, t2) -> (
        let v1 = eval calls env t1 in
        let env2o = backward calls (Env.add x v1 env) t2 vout in
        match env2o with
        | None -> VEnv.bot
        | Some env2 ->
          let env1o =
            match Env.find_opt x env2 with
            | Some vx -> backward calls env t1 vx
            | None -> VEnv.top
          in
          VEnv.meet env1o (Some (Env.remove x env2)))
      | Term.App (t1, t2) ->
        let v1 = eval calls env t1 in
        let v2 = eval calls env t2 in
        VAbs.closures_join_map
          VEnv.bot
          VEnv.join
          VEnv.top
          (fun t0 env0 ->
            match t0 with
            | Lam (loc, x, t') -> (
              let calls' =
                Calls.extend
                  calls
                  t.Term.id
                  (if Options.finer_context then Some loc else None)
              in
              let oenv1 = backward_widen calls' (Env.add x v2 env0) t' vout in
              match oenv1 with
              | None -> VEnv.bot
              | Some env1 -> begin
                match Env.find_opt x env1 with
                | None -> VEnv.top
                | Some vx -> backward calls env t2 vx
              end)
            | FixLam (loc, f, x, t') -> (
              let calls' =
                Calls.extend
                  calls
                  t.Term.id
                  (if Options.finer_context then Some loc else None)
              in
              let annot = make_closure_annot calls' in
              let oenv1 =
                backward_widen
                  calls'
                  (Env.add
                     x
                     v2
                     (Env.add
                        f
                        (VAbs.singleton_closure
                           annot
                           t0
                           (Vars.restrict_env (NablaFun.fv t0) env0))
                        env0))
                  t'
                  vout
              in
              match oenv1 with
              | None -> VEnv.bot
              | Some env1 -> begin
                match Env.find_opt x env1 with
                | None -> VEnv.top
                | Some vx -> backward calls env t2 vx
              end))
          v1
      | Term.Tuple (n, ts) -> (
        match VAbs.get_tuple n vout with
        | Some (args, _rem) ->
          List.fold_left
            (fun acc0 (_opp, vs) ->
              VEnv.join acc0
              @@ List.fold_left2
                   (fun macc t v ->
                     let acc = macc
                     and env = backward calls env t v in
                     VEnv.meet acc env)
                   VEnv.top
                   ts
                   vs)
            VEnv.bot
            args
        | None -> None)
      | Term.Variant (c, None) -> (
        match VAbs.get_variant_naked c vout with
        | Some _rem -> VEnv.top
        | None -> VEnv.bot)
      | Term.Variant (c, Some tc) -> (
        match VAbs.get_variant_with_arg c vout with
        | Some (vs, _rem) ->
          List.fold_left
            (fun acc (_opp, vc) -> VEnv.join acc @@ backward calls env tc vc)
            VEnv.bot
            vs
        | None -> VEnv.bot)
    in
    backward

  (** Memoization module for the function [eval] *)
  module MemoFixEval = struct
    module AbsEnv = struct
      type t = VAbs.t Env.t [@@deriving ord]

      let pp = Env.pp VAbs.pp
    end

    module CallsTerm = struct
      type t = Calls.t * Term.t [@@deriving ord]

      let pp fmt (calls, t) =
        Format.fprintf fmt "@[@[[%a]@]@ %a@]" Calls.pp calls Term.pp t
    end

    module AbsClosure = struct
      type t = (Calls.t * Term.t) * VAbs.t Env.t [@@deriving ord]

      let widen (ct1, env1) (_ct2, env2) =
        (ct1, Env.union (fun _ a1 a2 -> Some (VAbs.widen a1 a2)) env1 env2)
    end

    module M = struct
      include Map2.Make (CallsTerm) (AbsEnv)

      let sim env1 env2 =
        Env.for_all (fun x a1 -> VAbs.sim a1 (Env.find x env2)) env1

      let fold_similar f k m = fold_similar f sim k m
    end

    include Solver (Log) (AbsClosure) (M) (Abs)
  end

  module EntryClassic = struct
    type t = ((Calls.t * Lang.Ast.Term.t) * MemoFixEval.AbsEnv.t) * Abs.t

    let pp fmt (((c, t), e), a) =
      Format.fprintf
        fmt
        "@[@[ctxt =@ @[%a@],@ loc =@ @[%a@],@ term =@ @[%a@],@ env =@ @[%a@]] \
         ====>@ @[%a@]@]"
        Calls.pp
        c
        PP.pp
        t.Term.id
        Term.pp
        t
        MemoFixEval.AbsEnv.pp
        e
        Abs.pp
        a

    (** Helper function to define the memoized fixpoint *)
    let eval_solve f =
      let curry f calls env t = f ((calls, t), env)
      and curry1 f b calls env t = f b ((calls, t), env) in
      let uncurry2 f g ((calls, t), env) = f (curry1 g) calls env t in
      curry
      @@ MemoFixEval.solve ~debug:Options.debug ~transient:false
      @@ uncurry2 f
  end
  [@@warning "-32-34"]

  module MemoFixEvalMFG = struct
    module AbsEnv = struct
      type t = VAbs.t Env.t

      let pp = Env.pp VAbs.pp
      let is_bot env = Env.exists (fun _ v -> VAbs.is_bot v) env
      let leq env1 env2 = Env.equal VAbs.leq env1 env2

      let join_gen join env1 env2 =
        Env.union (fun _x v1 v2 -> Some (join v1 v2)) env1 env2

      let join env1 env2 = join_gen VAbs.join env1 env2
      let widen env1 env2 = join_gen VAbs.widen env1 env2
      let meet env1 env2 = join_gen VAbs.meet env1 env2
    end

    module CallsTerm = struct
      type t = Calls.t * Term.t [@@deriving ord]
    end

    module M = Map.Make (CallsTerm)
    module MFG = Function_graphs.Elementary.Make (AbsEnv) (Abs)
    include Memo.Fix.Priority.MakeMFG (Log) (CallsTerm) (M) (MFG)
  end
  [@@warning "-32"]

  module EntryMFG = struct
    type t = (Calls.t * Lang.Ast.Term.t) * MemoFixEvalMFG.MFG.t

    let pp fmt ((c, t), g) =
      Format.fprintf
        fmt
        "@[@[ctxt =@ @[%a@],@ loc =@ @[%a@],@ term =@ @[%a@],@ graph =@ @[%a@]]"
        Calls.pp
        c
        PP.pp
        t.Term.id
        Term.pp
        t
        MemoFixEvalMFG.MFG.pp
        g

    (** Helper function to define the memoized fixpoint *)
    let eval_solve f =
      let curry f calls env t = f (calls, t) env
      and curry1 f _ calls env t = f (calls, t) env in
      let uncurry2 f g (calls, t) env = f (curry1 g) calls env t in
      curry
      @@ MemoFixEvalMFG.solve ~debug:Options.debug ~transient:false
      @@ uncurry2 f
  end
  [@@warning "-32-34"]

  (* XXX choose EntryClassic or EntryMFG here to use the legacy method
     for input widening of the one based on minimal function graphs *)
  module Entry = EntryClassic

  (** The main abstract evaluator *)
  let eval =
    Entry.eval_solve @@ fun request ->
    let rec eval calls env t =
      let eval calls env t =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if Options.cache_non_calls
        then request false calls env t
        else eval calls env t
      and eval_widen calls env t =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if (not Options.cache_widened_calls_only) || Calls.is_max calls
        then request (Calls.is_max calls) calls env t
        else eval calls env t
      in
      let backward calls env t vout =
        fst
        @@ backward
             (fun calls env t -> snd @@ Abs.run (eval_widen calls env t))
             calls
             env
             t
             vout
      in
      let open Abs in
      match t.Term.term with
      | Term.Bool b -> return @@ VAbs.bools @@ Bools.singleton b
      | Term.Lam (_xloc, x, t0) ->
        let env = Vars.restrict_env (Term.fv t) env in
        let annot = make_closure_annot calls in
        return @@ VAbs.singleton_closure annot (NablaFun.Lam (t.id, x, t0)) env
      | Term.FixLam (_floc, f, _xloc, x, t0) ->
        let env = Vars.restrict_env (Term.fv t) env in
        let annot = make_closure_annot calls in
        return
        @@ VAbs.singleton_closure annot (NablaFun.FixLam (t.id, f, x, t0)) env
      | Term.UnknownInt -> return @@ VAbs.ints Ints.(interval min_int max_int)
      | Term.UnknownBool -> return @@ VAbs.bools Bools.top
      | Term.If (t1, t2, t3) ->
        let term_id = t.id in
        let* ts1 = eval calls env t1 in
        let ts1_can_be_bool =
          let b, _ = VAbs.get_bools ts1 in
          not @@ Bools.is_bot b
        in
        let* () =
          if ts1_can_be_bool && (not @@ VAbs.is_bool ts1)
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t1 (snd @@ VAbs.get_bools ts1)
            in
            if disproved
            then return ()
            else alarm t1.id calls Alarm.(non_boolean ts1)
          else return ()
        in
        if Options.kill_unreachable && not ts1_can_be_bool
        then return VAbs.bot
        else
          let* ts2 =
            let calls' =
              if Options.branch_is_context
              then
                Calls.extend
                  calls
                  term_id
                  (if Options.finer_context
                   then Some 0 (* here 0 stands for "true" *)
                   else None)
              else calls
            in
            if VAbs.mem_bool true ts1
            then
              if Options.refine_branches
              then
                let learned = backward calls env t1 abs_true in
                match VEnv.meet (Some env) learned with
                | None -> return VAbs.bot
                | Some env' -> eval calls' env' t2
              else eval calls' env t2
            else return VAbs.bot
          in
          let* ts3 =
            let calls' =
              if Options.branch_is_context
              then
                Calls.extend
                  calls
                  term_id
                  (if Options.finer_context
                   then Some 1 (* here 1 stands for "false" *)
                   else None)
              else calls
            in
            if VAbs.mem_bool false ts1
            then
              if Options.refine_branches
              then
                let learned = backward calls env t1 abs_false in
                match VEnv.meet (Some env) learned with
                | None -> return VAbs.bot
                | Some env' -> eval calls' env' t3
              else eval calls' env t3
            else return VAbs.bot
          in
          return @@ VAbs.join ts2 ts3
      | Term.BOrElse (t1, t2) ->
        let term_id = t.id in
        let* ts1 = eval calls env t1 in
        let* () =
          let can_be_bool =
            let b, _ = VAbs.get_bools ts1 in
            not @@ Bools.is_bot b
          in
          if can_be_bool && (not @@ VAbs.is_bool ts1)
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t1 (snd @@ VAbs.get_bools ts1)
            in
            if disproved
            then return ()
            else alarm t1.id calls Alarm.(non_boolean ts1)
          else return ()
        in
        let calls' =
          if Options.branch_is_context
          then
            Calls.extend
              calls
              term_id
              (if Options.finer_context
               then Some 1 (* here 1 stands for "false" *)
               else None)
          else calls
        in
        if VAbs.mem_bool true ts1
        then
          if VAbs.mem_bool false ts1
          then
            let+ ts2 = eval calls' env t2 in
            VAbs.join abs_true ts2
          else return abs_true
        else if VAbs.mem_bool false ts1
        then
          if Options.refine_branches
          then
            let learned = backward calls env t1 abs_false in
            match VEnv.meet (Some env) learned with
            | None -> return VAbs.bot
            | Some env' -> eval calls' env' t2
          else eval calls' env t2
        else return VAbs.bot
      | Term.BAndAlso (t1, t2) ->
        let term_id = t.id in
        let* ts1 = eval calls env t1 in
        let* () =
          let can_be_bool =
            let b, _ = VAbs.get_bools ts1 in
            not @@ Bools.is_bot b
          in
          if can_be_bool && (not @@ VAbs.is_bool ts1)
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t1 (snd @@ VAbs.get_bools ts1)
            in
            if disproved
            then return ()
            else alarm t1.id calls Alarm.(non_boolean ts1)
          else return ()
        in
        let calls' =
          if Options.branch_is_context
          then
            Calls.extend
              calls
              term_id
              (if Options.finer_context
               then Some 0 (* here 0 stands for "true" *)
               else None)
          else calls
        in
        if VAbs.mem_bool false ts1
        then
          if VAbs.mem_bool true ts1
          then
            let+ ts2 = eval calls' env t2 in
            VAbs.join abs_false ts2
          else return abs_false
        else if VAbs.mem_bool true ts1
        then
          if Options.refine_branches
          then
            let learned = backward calls env t1 abs_true in
            match VEnv.meet (Some env) learned with
            | None -> return VAbs.bot
            | Some env' -> eval calls' env' t2
          else eval calls' env t2
        else return VAbs.bot
      | Term.Var x -> return @@ Env.find x env
      | Term.App (t1, t2) ->
        let* res =
          let* ts1 = eval calls env t1 in
          let ts1_can_be_fun =
            VAbs.closures_join_map false ( || ) true (fun _ _ -> true) ts1
          in
          let* () =
            if ts1_can_be_fun && (not @@ VAbs.is_fun ts1)
            then
              (* WARNING: do not try to disprove the warning, as it
                 may create an undetected cycle between the two
                 solvers (for forward and backward analysis). One such
                 example is [church.simpl] with [cyclic-call-sites:1].
              *)
              let non_clos =
                match VAbs.get_closures ts1 with
                | None -> ts1
                | Some (_clos, rem) -> rem
              in
              let disproved =
                false (* we keep the dead code here on purpose *)
                && Options.disprove_alarms
                && Option.is_none
                   @@ VEnv.meet (Some env)
                   @@ backward calls env t1 non_clos
              in
              if disproved
              then return ()
              else alarm t1.id calls Alarm.(non_functional ts1)
            else return ()
          in
          if Options.kill_unreachable && not ts1_can_be_fun
          then return VAbs.bot
          else
            let* ts2 = eval calls env t2 in
            if Options.kill_unreachable && VAbs.is_bot ts2
            then return ts2
            else
              VAbs.closures_join_map
                Abs.bot
                Abs.join
                (return VAbs.top)
                (fun t0 env0 ->
                  match t0 with
                  | Lam (loc, x, t') ->
                    let calls' =
                      Calls.extend
                        calls
                        t.Term.id
                        (if Options.finer_context then Some loc else None)
                    in
                    eval_widen calls' (Env.add x ts2 env0) t'
                  | FixLam (loc, f, x, t') ->
                    let calls' =
                      Calls.extend
                        calls
                        t.Term.id
                        (if Options.finer_context then Some loc else None)
                    in
                    let annot = make_closure_annot calls' in
                    eval_widen
                      calls'
                      (Env.add
                         x
                         ts2
                         (Env.add
                            f
                            (VAbs.singleton_closure
                               annot
                               t0
                               (Vars.restrict_env (NablaFun.fv t0) env0))
                            env0))
                      t')
                ts1
        in
        return res
      | Term.Let (_xloc, x, t1, t2) ->
        let* ts1 = eval calls env t1 in
        if Options.kill_unreachable && VAbs.is_bot ts1
        then return ts1
        else
          let calls' =
            if Options.let_is_context
            then Calls.extend calls t.Term.id None
            else calls
          in
          eval calls' (Env.add x ts1 env) t2
      | Term.Int n -> return @@ VAbs.ints @@ Ints.singleton n
      | Term.Binop (op, t1, t2) -> (
        let* ts1 = eval calls env t1 in
        let ts1_can_be_int =
          let i, _ = VAbs.get_ints ts1 in
          not @@ Ints.is_bot i
        in
        let* () =
          if ts1_can_be_int && (not @@ VAbs.is_int ts1)
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t1 (snd @@ VAbs.get_ints ts1)
            in
            if disproved
            then return ()
            else alarm t1.id calls Alarm.(non_numeric ts1)
          else return ()
        in
        if Options.kill_unreachable && not ts1_can_be_int
        then return VAbs.bot
        else
          let* ts2 = eval calls env t2 in
          let ts2_can_be_int =
            let i, _ = VAbs.get_ints ts2 in
            not @@ Ints.is_bot i
          in
          let* () =
            if ts2_can_be_int && (not @@ VAbs.is_int ts2)
            then
              let disproved =
                Options.disprove_alarms
                && Option.is_none
                   @@ VEnv.meet (Some env)
                   @@ backward calls env t2 (snd @@ VAbs.get_ints ts2)
              in
              if disproved
              then return ()
              else alarm t2.id calls Alarm.(non_numeric ts2)
            else return ()
          in
          if Options.kill_unreachable && not ts2_can_be_int
          then return VAbs.bot
          else
            let i1, _ = VAbs.get_ints ts1 in
            let i2, _ = VAbs.get_ints ts2 in
            return
            @@
            match op with
            | PLUS -> VAbs.ints @@ Ints.Transfer.plus i1 i2
            | MINUS -> VAbs.ints @@ Ints.Transfer.minus i1 i2
            | TIMES -> VAbs.ints @@ Ints.Transfer.mult i1 i2
            | LE ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.le i1 i2 true, Ints.Transfer.le i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | LT ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.lt i1 i2 true, Ints.Transfer.lt i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | GE ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.ge i1 i2 true, Ints.Transfer.ge i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | GT ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.gt i1 i2 true, Ints.Transfer.gt i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | EQ ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.eq i1 i2 true, Ints.Transfer.eq i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | NEQ ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                (Ints.Transfer.neq i1 i2 true, Ints.Transfer.neq i1 i2 false)
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false)
      | Term.Unop (NEG, t1) ->
        let* ts1 = eval calls env t1 in
        let ts1_can_be_int =
          let i, _ = VAbs.get_ints ts1 in
          not @@ Ints.is_bot i
        in
        let* () =
          if ts1_can_be_int && (not @@ VAbs.is_int ts1)
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t1 (snd @@ VAbs.get_ints ts1)
            in
            if disproved
            then return ()
            else alarm t1.id calls Alarm.(non_numeric ts1)
          else return ()
        in
        if Options.kill_unreachable && not ts1_can_be_int
        then return VAbs.bot
        else
          let i1, _ = VAbs.get_ints ts1 in
          return @@ VAbs.ints @@ Ints.Transfer.neg i1
      | Term.Tuple (n, ts) ->
        let* l =
          List.fold_left
            (fun ml t ->
              let* l = ml in
              let* v = eval calls env t in
              return (v :: l))
            (return [])
            ts
        in
        let opp = make_tuple_pp t.id in
        return @@ VAbs.singleton_tuple opp n (List.rev l)
      | Term.Variant (c, None) ->
        let opp = make_variant_pp t.id in
        return @@ VAbs.singleton_variant opp c None
      | Term.Variant (c, Some t) ->
        let* v = eval calls env t in
        let opp = make_variant_pp t.id in
        return @@ VAbs.singleton_variant opp c (Some v)
      | Term.Match (t0, bs) ->
        let eval_branch term_id input i (pi, ti) =
          (* returns the output value of the branch, and the
             remainder, i.e. the values that remain to be matched from
             [input], after matching against the pattern [p] has
             failed *)
          let* envs, rem =
            get_envs_from_pattern backward calls env t0 input pi
          in
          let* res =
            match envs with
            | None -> return VAbs.bot
            | Some (refined_env, branch_env) ->
              let calls =
                if Options.branch_is_context
                then
                  Calls.extend
                    calls
                    term_id
                    (if Options.finer_context then Some i else None)
                else calls
              in
              eval calls (add_bindings branch_env refined_env) ti
          in
          return (res, rem)
        in
        let eval_branches term_id input bs =
          (* returns the output value of the whole pattern matching,
             and the remainder, i.e. the values that remain to be
             matched from [input], after pattern matching has failed
          *)
          List.fold_left
            (fun acc b ->
              let* i, res, rem = acc in
              let* res_b, rem_b = eval_branch term_id rem (i + 1) b in
              return (i + 1, VAbs.join res res_b, rem_b))
            (return (0, VAbs.bot, input))
            bs
        in
        let term_id = t.id in
        let* v = eval calls env t0 in
        let* _n, res, rem = eval_branches term_id v bs in
        let* () =
          if not @@ VAbs.is_bot rem
          then
            let disproved =
              Options.disprove_alarms
              && Option.is_none
                 @@ VEnv.meet (Some env)
                 @@ backward calls env t0 rem
            in
            if disproved
            then return ()
            else alarm term_id calls Alarm.(non_exhaustive_match rem)
          else return ()
        in
        return res
    in
    eval

  (** Evaluator in the empty environment, in the empty calling context *)
  let eval t = eval Calls.empty Env.empty t
end
