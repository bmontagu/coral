(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Lang.Ast

module type COMPARABLE = sig
  type t

  val compare : t -> t -> int
end

(** The abstract domain for values, parameterized by the contexts for
   closures, the domain for booleans and the domain for integers. *)
module AbsDomain
    (CtxtEnvs : sig
      type var
      type elt
      type t

      val leq : t -> t -> bool
      val singleton : elt -> t
      val join : t -> t -> t
      val meet : t -> t -> t
      val pp : Format.formatter -> t -> unit
      val fold_vars : (var -> 'a -> 'a) -> t -> 'a -> 'a
    end)
    (TupleSet : sig
      type var
      type elt
      type t

      val leq : t -> t -> bool
      val singleton : elt -> t
      val join : t -> t -> t
      val meet : t -> t -> t
      val join_map : bot:'a -> join:('a -> 'a -> 'a) -> (elt -> 'a) -> t -> 'a
      val pp : Format.formatter -> t -> unit
      val fold_vars : (var -> 'a -> 'a) -> t -> 'a -> 'a
    end)
    (VariantSet : sig
      type var
      type elt
      type t

      val leq : t -> t -> bool
      val singleton : elt -> t
      val join : t -> t -> t
      val meet : t -> t -> t
      val join_map : bot:'a -> join:('a -> 'a -> 'a) -> (elt -> 'a) -> t -> 'a
      val pp : Format.formatter -> t -> unit
      val fold_vars : (var -> 'a -> 'a) -> t -> 'a -> 'a
    end)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (Annot : COMPARABLE) : sig
  module FunAnnot : COMPARABLE

  type t = {
    bools: Bools.t;
    ints: Ints.t;
    closures: closures;
    tuples: tuples;
    variants: variants;
  }

  and closures = Clos of CtxtEnvs.t Map.Make(FunAnnot).t [@@unboxed]
  and tuples = Tuples of TupleSet.t Ptmap.t [@@unboxed]
  and variants = Variants of VariantSet.t Map.Make(Cons).t [@@unboxed]

  val bot : t
  val is_bot : t -> bool
  val singleton_closure : Annot.t option -> GlobalFun.t -> CtxtEnvs.elt -> t
  val singleton_tuple : int -> TupleSet.elt -> t
  val singleton_variant : Cons.t -> VariantSet.elt -> t
  val ints : Ints.t -> t
  val get_ints : t -> Ints.t
  val is_int : t -> bool
  val bools : Bools.t -> t
  val get_bools : t -> Bools.t
  val is_bool : t -> bool
  val mem_bool : bool -> t -> bool
  val is_fun : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t

  val closures_join_map :
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    (GlobalFun.t -> CtxtEnvs.t -> 'a) ->
    t ->
    'a

  val closures_iter : (GlobalFun.t -> CtxtEnvs.t -> unit) -> t -> unit

  val tuples_join_map :
    int -> join:('a -> 'a -> 'a) -> bot:'a -> (TupleSet.elt -> 'a) -> t -> 'a

  val variants_join_map :
    Cons.t ->
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    (VariantSet.elt -> 'a) ->
    t ->
    'a

  val meet : t -> t -> t
  val widen : t -> t -> t
  val pp : Format.formatter -> t -> unit

  val fold_vars :
    (CtxtEnvs.var -> 'a -> 'a) ->
    (TupleSet.var -> 'a -> 'a) ->
    (VariantSet.var -> 'a -> 'a) ->
    t ->
    'a ->
    'a
end = struct
  module FunAnnot = struct
    type t = GlobalFun.t * Annot.t option [@@deriving ord]
  end

  module M = Map.Make (FunAnnot)
  module MInt = Ptmap
  module MCons = Map.Make (Cons)

  type t = {
    bools: Bools.t;
    ints: Ints.t;
    closures: closures;
    tuples: tuples;
    variants: variants;
  }
  (** The possible booleans, integers, and closures *)

  (** A closure is a map that associates to every possible function an
     abstract context *)
  and closures = Clos of CtxtEnvs.t M.t [@@unboxed]

  and tuples = Tuples of TupleSet.t MInt.t [@@unboxed]
  and variants = Variants of VariantSet.t MCons.t [@@unboxed]

  let clos_bot = Clos M.empty
  let tuples_bot = Tuples MInt.empty
  let variants_bot = Variants MCons.empty

  let bot =
    {
      bools = Bools.bot;
      ints = Ints.bot;
      closures = clos_bot;
      tuples = tuples_bot;
      variants = variants_bot;
    }

  let clos_is_bot = function
    | Clos m -> M.is_empty m

  let tuples_is_bot = function
    | Tuples m -> MInt.is_empty m

  let variants_is_bot = function
    | Variants m -> MCons.is_empty m

  let is_bot { bools; ints; closures; tuples; variants } =
    Bools.is_bot bools
    && Ints.is_bot ints
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let clos_is_top = function
    | Clos _ -> false

  let tuples_is_top = function
    | Tuples _ -> false

  let variants_is_top = function
    | Variants _ -> false

  let is_top { bools; ints; closures; tuples; variants } =
    Bools.is_top bools
    && Ints.is_top ints
    && clos_is_top closures
    && tuples_is_top tuples
    && variants_is_top variants

  let singleton_closure annot t env =
    {
      bools = Bools.bot;
      ints = Ints.bot;
      closures = Clos (M.singleton (t, annot) (CtxtEnvs.singleton env));
      tuples = tuples_bot;
      variants = variants_bot;
    }

  let singleton_tuple i l =
    {
      bools = Bools.bot;
      ints = Ints.bot;
      closures = clos_bot;
      tuples = Tuples (MInt.singleton i (TupleSet.singleton l));
      variants = variants_bot;
    }

  let singleton_variant c s =
    {
      bools = Bools.bot;
      ints = Ints.bot;
      closures = clos_bot;
      tuples = tuples_bot;
      variants = Variants (MCons.singleton c (VariantSet.singleton s));
    }

  let ints i =
    {
      bools = Bools.bot;
      ints = i;
      closures = clos_bot;
      tuples = tuples_bot;
      variants = variants_bot;
    }

  let get_ints { bools = _; ints; closures = _; tuples = _; variants = _ } =
    ints

  let is_int { bools; ints = _; closures; tuples; variants } =
    Bools.is_bot bools
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let bools b =
    {
      bools = b;
      ints = Ints.bot;
      closures = clos_bot;
      tuples = tuples_bot;
      variants = variants_bot;
    }

  let get_bools { bools; ints = _; closures = _; tuples = _; variants = _ } =
    bools

  let is_bool { bools = _; ints; closures; tuples; variants } =
    Ints.is_bot ints
    && clos_is_bot closures
    && tuples_is_bot tuples
    && variants_is_bot variants

  let is_fun { bools; ints; closures = _; tuples; variants } =
    Bools.is_bot bools
    && Ints.is_bot ints
    && tuples_is_bot tuples
    && variants_is_bot variants

  let rec join
      {
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    {
      bools = Bools.join bools1 bools2;
      ints = Ints.join ints1 ints2;
      closures = clos_join clos1 clos2;
      tuples = tuples_join tuples1 tuples2;
      variants = variants_join variants1 variants2;
    }

  and clos_join c1 c2 =
    match (c1, c2) with
    | Clos m1, Clos m2 ->
      let m =
        M.union (fun _t envs1 envs2 -> Some (CtxtEnvs.join envs1 envs2)) m1 m2
      in
      Clos m

  and tuples_join t1 t2 =
    match (t1, t2) with
    | Tuples m1, Tuples m2 ->
      let m =
        MInt.union (fun _i ts1 ts2 -> Some (TupleSet.join ts1 ts2)) m1 m2
      in
      Tuples m

  and variants_join t1 t2 =
    match (t1, t2) with
    | Variants m1, Variants m2 ->
      let m =
        MCons.union (fun _c ts1 ts2 -> Some (VariantSet.join ts1 ts2)) m1 m2
      in
      Variants m

  let rec meet
      {
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    {
      bools = Bools.meet bools1 bools2;
      ints = Ints.meet ints1 ints2;
      closures = clos_meet clos1 clos2;
      tuples = tuples_meet tuples1 tuples2;
      variants = variants_meet variants1 variants2;
    }

  and clos_meet c1 c2 =
    match (c1, c2) with
    | Clos m1, Clos m2 ->
      let m =
        M.merge
          (fun _t oenvs1 oenvs2 ->
            match (oenvs1, oenvs2) with
            | None, _ | _, None -> None
            | Some envs1, Some envs2 -> Some (CtxtEnvs.meet envs1 envs2))
          m1
          m2
      in
      Clos m

  and tuples_meet t1 t2 =
    match (t1, t2) with
    | Tuples m1, Tuples m2 ->
      let m =
        MInt.merge
          (fun _i ots1 ots2 ->
            match (ots1, ots2) with
            | None, _ | _, None -> None
            | Some ts1, Some ts2 -> Some (TupleSet.meet ts1 ts2))
          m1
          m2
      in
      Tuples m

  and variants_meet t1 t2 =
    match (t1, t2) with
    | Variants m1, Variants m2 ->
      let m =
        MCons.merge
          (fun _c ots1 ots2 ->
            match (ots1, ots2) with
            | None, _ | _, None -> None
            | Some ts1, Some ts2 -> Some (VariantSet.meet ts1 ts2))
          m1
          m2
      in
      Variants m

  let rec leq
      {
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    Bools.leq bools1 bools2
    && Ints.leq ints1 ints2
    && clos_leq clos1 clos2
    && tuples_leq tuples1 tuples2
    && variants_leq variants1 variants2

  and clos_leq c1 c2 =
    match (c1, c2) with
    | Clos m1, Clos m2 ->
      M.for_all
        (fun t envs1 ->
          match M.find_opt t m2 with
          | None -> false
          | Some envs2 -> CtxtEnvs.leq envs1 envs2)
        m1

  and tuples_leq t1 t2 =
    match (t1, t2) with
    | Tuples m1, Tuples m2 ->
      MInt.for_all
        (fun i ts1 ->
          match MInt.find_opt i m2 with
          | None -> false
          | Some ts2 -> TupleSet.leq ts1 ts2)
        m1

  and variants_leq t1 t2 =
    match (t1, t2) with
    | Variants m1, Variants m2 ->
      MCons.for_all
        (fun c ts1 ->
          match MCons.find_opt c m2 with
          | None -> false
          | Some ts2 -> VariantSet.leq ts1 ts2)
        m1

  let mem_bool b { bools; ints = _; closures = _; tuples = _; variants = _ } =
    Bools.mem b bools

  let closures_join_map ~join ~bot fclos r =
    match r.closures with
    | Clos m -> M.fold (fun (t, _annot) c acc -> join (fclos t c) acc) m bot

  let closures_iter f r =
    match r.closures with
    | Clos m -> M.iter (fun (x, _annot) -> f x) m

  let tuples_join_map n ~join ~bot fclos r =
    match r.tuples with
    | Tuples m -> (
      match MInt.find_opt n m with
      | None -> bot
      | Some ts -> TupleSet.join_map ~join ~bot fclos ts)

  let variants_join_map c ~join ~bot fclos r =
    match r.variants with
    | Variants m -> (
      match MCons.find_opt c m with
      | None -> bot
      | Some vs -> VariantSet.join_map ~join ~bot fclos vs)

  let widen
      {
        bools = bools1;
        ints = ints1;
        closures = clos1;
        tuples = tuples1;
        variants = variants1;
      }
      {
        bools = bools2;
        ints = ints2;
        closures = clos2;
        tuples = tuples2;
        variants = variants2;
      } =
    {
      bools = Bools.widen bools1 bools2;
      ints = Ints.widen ints1 ints2;
      closures =
        clos_join clos1 clos2
        (* no widening on closures: it is a finite-height domain *);
      tuples =
        tuples_join tuples1 tuples2
        (* no widening on tuples: it is a finite-height domain *);
      variants =
        variants_join variants1 variants2
        (* no widening on variants: it is a finite-height domain *);
    }

  let rec pp fmt ({ bools; ints; closures; tuples; variants } as v) =
    if is_bot v
    then Format.fprintf fmt "⊥"
    else if is_top v
    then Format.fprintf fmt "⊤"
    else
      let open Format in
      fprintf fmt "@[<hv 0>{@[<hv 1>";
      if not @@ Bools.is_bot bools
      then fprintf fmt "@ @[bools =@ %a@]" Bools.pp bools;
      if not @@ Ints.is_bot ints
      then fprintf fmt "@ @[ints =@ %a@]" Ints.pp ints;
      if not @@ clos_is_bot closures
      then fprintf fmt "@ @[closures =@ %a@]" pp_closures closures;
      if not @@ tuples_is_bot tuples
      then fprintf fmt "@ @[tuples =@ %a@]" pp_tuples tuples;
      if not @@ variants_is_bot variants
      then fprintf fmt "@ @[variants =@ %a@]" pp_variants variants;
      fprintf fmt "@]@ }@]"

  and pp_closures fmt = function
    | Clos m ->
      if M.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt ((x, _annot), mx) ->
               Format.fprintf
                 fmt
                 "@[%a@ %a@]"
                 (GlobalFun.pp_rec true)
                 x
                 CtxtEnvs.pp
                 mx))
          (M.bindings m)

  and pp_tuples fmt = function
    | Tuples m ->
      if MInt.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (i, ts) ->
               Format.fprintf fmt "@[%i:@ %a@]" i TupleSet.pp ts))
          (MInt.bindings m)

  and pp_variants fmt = function
    | Variants m ->
      if MCons.is_empty m
      then Format.pp_print_string fmt "∅"
      else
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (c, ts) ->
               Format.fprintf fmt "@[%a:@ %a@]" Cons.pp c VariantSet.pp ts))
          (MCons.bindings m)

  let fold_vars fclos ftuples fvariants
      {
        bools = _;
        ints = _;
        closures = Clos m;
        tuples = Tuples im;
        variants = Variants cm;
      } acc =
    acc
    |> M.fold (fun _f envs acc -> CtxtEnvs.fold_vars fclos envs acc) m
    |> MInt.fold (fun _i ts acc -> TupleSet.fold_vars ftuples ts acc) im
    |> MCons.fold (fun _c vs acc -> VariantSet.fold_vars fvariants vs acc) cm
end

(** Main analyzer module, parameterized by logging functions, options,
   the domain for calls, the domain for booleans, and the domain for
   integers. *)
module Analyze
    (Log : Logs.S)
    (Options : sig
      val refine_branches : bool
      val branch_is_context : bool
      val let_is_context : bool
      val finer_context : bool
      val no_env_restrict : bool
      val kill_unreachable : bool
      val record_closure_creation_context : bool
      val cache_non_calls : bool
      val cache_widened_calls_only : bool
      val no_alarms : bool
      val debug : bool
      val print_memo_table : bool
    end)
    (Solver : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (Calls : Calls.S)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN) =
struct
  (** Abstract contexts, i.e. environments that map variables to pairs
     of a program point and a calling context. *)
  module CtxtEnv = struct
    type var = PP.t * Calls.t [@@deriving ord]
    type t = var Env.t [@@deriving ord]

    let empty = Env.empty

    let pp fmt e =
      Env.pp
        (fun fmt (pp, call) ->
          Format.fprintf fmt "@[%a @@ %a@]" PP.pp pp Calls.pp call)
        fmt
        e

    let fold_vars f t acc = Env.fold (fun _x var acc -> f var acc) t acc
  end

  (** Sets of abstract contexts *)
  module CtxtEnvs = struct
    module S = Set.Make (CtxtEnv)

    type t = S.t
    type elt = S.elt
    type var = CtxtEnv.var

    let singleton = S.singleton
    let leq = S.subset
    let join = S.union
    let meet = S.inter

    let join_map join map envs bot =
      S.fold (fun env acc -> join acc (map env)) envs bot

    let iter = S.iter

    let pp fmt s =
      let l = S.elements s in
      let fmt_string =
        match l with
        | [_] -> ("@[%a@]" : _ format)
        | _ -> "@[<hv 0>{@[<hv 1> %a@]@ }@]"
      in
      Format.fprintf
        fmt
        fmt_string
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           CtxtEnv.pp)
        l

    let fold_vars f t acc =
      S.fold (fun env acc -> CtxtEnv.fold_vars f env acc) t acc
  end

  module Tuple = struct
    type var = PP.t * Calls.t [@@deriving ord]
    type t = var list [@@deriving ord]

    (* let empty = Env.empty *)

    let pp fmt = function
      | [] -> Format.fprintf fmt "()"
      | l ->
        Format.fprintf
          fmt
          "@[<hv 0>(@[<hv 1> %a@]@ )@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (pp, call) ->
               Format.fprintf fmt "@[%a @@ %a@]" PP.pp pp Calls.pp call))
          l

    let fold_vars f t acc = List.fold_left (fun acc var -> f var acc) acc t
  end

  module TupleSet = struct
    module S = Set.Make (Tuple)

    type t = S.t
    type elt = S.elt
    type var = Tuple.var

    let singleton = S.singleton
    let leq = S.subset
    let join = S.union
    let meet = S.inter

    let join_map ~bot ~join map envs =
      S.fold (fun env acc -> join acc (map env)) envs bot

    (* let iter = S.iter *)

    let pp fmt s =
      let l = S.elements s in
      let fmt_string =
        match l with
        | [_] -> ("@[%a@]" : _ format)
        | _ -> "@[{%a}@]"
      in
      Format.fprintf
        fmt
        fmt_string
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           Tuple.pp)
        l

    let fold_vars f t acc =
      S.fold (fun ti acc -> Tuple.fold_vars f ti acc) t acc
  end

  module Names = struct
    type t = PP.t * Calls.t [@@deriving ord]
  end

  module VariantSet = struct
    module S = Set.Make (Names)

    type var = PP.t * Calls.t
    type elt = var option

    type t = {
      naked: Two.t;
      arg: S.t;
    }

    let singleton = function
      | None -> { naked = Two.top; arg = S.empty }
      | Some v -> { naked = Two.bot; arg = S.singleton v }

    let leq { naked = naked1; arg = arg1 } { naked = naked2; arg = arg2 } =
      Two.leq naked1 naked2 && S.subset arg1 arg2

    let join { naked = naked1; arg = arg1 } { naked = naked2; arg = arg2 } =
      { naked = Two.join naked1 naked2; arg = S.union arg1 arg2 }

    let meet { naked = naked1; arg = arg1 } { naked = naked2; arg = arg2 } =
      { naked = Two.meet naked1 naked2; arg = S.inter arg1 arg2 }

    let join_map ~bot ~join map { naked; arg = names } =
      let acc = if Two.is_bot naked then bot else join bot (map None) in
      S.fold (fun name acc -> join acc (map (Some name))) names acc

    let pp fmt { naked; arg } =
      Format.fprintf fmt "@[";
      if not @@ Two.is_bot naked then Format.fprintf fmt "@[.@]";
      if (not @@ Two.is_bot naked) && (not @@ S.is_empty arg)
      then Format.fprintf fmt ",@ ";
      if not @@ S.is_empty arg
      then
        Format.fprintf
          fmt
          "@[<hv 0>{@[<hv 1> %a@]@ }@]"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
             (fun fmt (pp, call) ->
               Format.fprintf fmt "@[%a @@ %a@]" PP.pp pp Calls.pp call))
          (S.elements arg);
      Format.fprintf fmt "@]"

    let fold_vars f { naked = _; arg } acc = S.fold f arg acc
  end

  module VAbs =
    AbsDomain (CtxtEnvs) (TupleSet) (VariantSet) (Bools) (Ints) (Calls)
  (** The abstract domain for sets of values: the context part of
     abstract closures are elements in [CtxtEnvs.t]. *)

  module Common =
    GlobalCommon.Make (Log) (Options) (Solver) (Calls) (CtxtEnv) (VAbs)

  module GEnv = Common.GEnv

  let solve = Common.solve

  (** Meet of two global environments *)
  let env_meet ~narrow:_ oenv1 oenv2 =
    match (oenv1, oenv2) with
    | None, _ | _, None -> None
    | Some env1, Some env2 -> (
      let exception Inconsistent in
      try
        Some
          (GEnv.union
             (fun (_i, c) v1 v2 ->
               let v12 =
                 if Calls.is_max c
                    (* it is unsound to meet variables for maximal call sites *)
                 then VAbs.join v1 v2
                 else VAbs.meet v1 v2
               in
               if VAbs.is_bot v12 then raise Inconsistent else Some v12)
             env1
             env2)
      with Inconsistent -> None)

  (** Join of two global environments *)
  let env_join oenv1 oenv2 =
    match (oenv1, oenv2) with
    | None, o | o, None -> o
    | Some env1, Some env2 ->
      Some
        (GEnv.merge
           (fun _x ov1 ov2 ->
             match (ov1, ov2) with
             | Some v1, Some v2 -> Some (VAbs.join v1 v2)
             | None, _ | _, None -> None)
           env1
           env2)

  let b_true = Bools.singleton true
  let abs_true = VAbs.bools b_true
  let b_false = Bools.singleton false
  let abs_false = VAbs.bools b_false

  module Abs = Common.AlarmGEnvAbs

  module Entry = struct
    type t = ((Calls.t * Lang.Ast.Term.t * CtxtEnv.t) * unit) * Abs.t

    let pp fmt (((c, t, e), ()), a) =
      Format.fprintf
        fmt
        "@[@[ctxt =@ @[%a@],@ loc =@ @[%a@],@ term =@ @[%a@],@ env =@ @[%a@]] \
         ====>@ @[%a@]@]"
        Calls.pp
        c
        PP.pp
        t.Term.id
        Term.pp
        t
        CtxtEnv.pp
        e
        Abs.pp
        a
  end

  (** [backward eval ctxtenv t v] is a backward analyzer, that returns
     necessary informations about the variables of [t], when we know
     it forward evaluated to [v] using [eval]. It is useful to refine
     the knowledge after a branch has been taken in the forward
     analyzer. It returns a global environment that can only contain
     the variables of [t], and for which something was learned. *)
  let rec backward eval ctxtenv t v =
    let open Abs in
    if VAbs.is_bot v
    then return None
    else
      match t.Term.term with
      | Term.Var x -> return @@ Some (GEnv.singleton (Env.find x ctxtenv) v)
      | Term.Int n ->
        let v = VAbs.(meet v (ints (Ints.singleton n))) in
        return @@ if VAbs.is_bot v then None else Some GEnv.bot
      | Term.UnknownInt ->
        let v = VAbs.(meet v @@ ints Ints.(interval min_int max_int)) in
        return @@ if VAbs.is_bot v then None else Some GEnv.bot
      | Term.Bool b ->
        let v = VAbs.(meet v (bools (Bools.singleton b))) in
        return @@ if VAbs.is_bot v then None else Some GEnv.bot
      | Term.UnknownBool ->
        let v = VAbs.(meet v (bools Bools.top)) in
        return @@ if VAbs.is_bot v then None else Some GEnv.bot
      | Term.Unop (NEG, t) ->
        backward eval ctxtenv t VAbs.(ints @@ Ints.Transfer.neg v.ints)
      | Term.Binop (PLUS, t1, t2) ->
        let* v1 = eval t1 in
        let* v2 = eval t2 in
        let+ env1 =
          backward
            eval
            ctxtenv
            t1
            VAbs.(ints @@ Ints.Transfer.minus v.ints v2.ints)
        and+ env2 =
          backward
            eval
            ctxtenv
            t2
            VAbs.(ints @@ Ints.Transfer.minus v.ints v1.ints)
        in
        env_meet ~narrow:false env1 env2
      | Term.Binop (MINUS, t1, t2) ->
        let* v1 = eval t1 in
        let* v2 = eval t2 in
        let+ env1 =
          backward
            eval
            ctxtenv
            t1
            VAbs.(ints @@ Ints.Transfer.plus v.ints v2.ints)
        and+ env2 =
          backward
            eval
            ctxtenv
            t2
            VAbs.(ints @@ Ints.Transfer.(minus v1.ints v.ints))
        in
        env_meet ~narrow:false env1 env2
      | Term.Binop (LE, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.le v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.Binop (LT, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.lt v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.Binop (GE, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.ge v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.Binop (GT, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.gt v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.Binop (EQ, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.eq v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.Binop (NEQ, t1, t2) ->
        let backward_when b =
          let* v1 = eval t1 in
          let* v2 = eval t2 in
          let ints1, ints2 = Ints.Transfer.neq v1.ints v2.ints b in
          let+ env1 = backward eval ctxtenv t1 (VAbs.ints ints1)
          and+ env2 = backward eval ctxtenv t2 (VAbs.ints ints2) in
          env_meet ~narrow:false env1 env2
        in
        let+ if_true =
          if VAbs.mem_bool true v then backward_when true else return None
        and+ if_false =
          if VAbs.mem_bool false v then backward_when false else return None
        in
        env_join if_true if_false
      | Term.BOrElse (t1, t2) ->
        let+ if_true =
          if VAbs.mem_bool true v
          then
            let+ env1_t = backward eval ctxtenv t1 abs_true
            and+ env1_f = backward eval ctxtenv t1 abs_false
            and+ env2_t = backward eval ctxtenv t2 abs_true in
            env_join env1_t (env_meet ~narrow:false env1_f env2_t)
          else return None
        and+ if_false =
          if VAbs.mem_bool false v
          then
            let+ env1_f = backward eval ctxtenv t1 abs_false
            and+ env2_f = backward eval ctxtenv t2 abs_false in
            env_meet ~narrow:false env1_f env2_f
          else return None
        in
        env_join if_true if_false
      | Term.BAndAlso (t1, t2) ->
        let+ if_true =
          if VAbs.mem_bool true v
          then
            let+ env1_t = backward eval ctxtenv t1 abs_true
            and+ env2_t = backward eval ctxtenv t2 abs_true in
            env_meet ~narrow:false env1_t env2_t
          else return None
        and+ if_false =
          if VAbs.mem_bool false v
          then
            let+ env1_t = backward eval ctxtenv t1 abs_true
            and+ env1_f = backward eval ctxtenv t1 abs_false
            and+ env2_f = backward eval ctxtenv t2 abs_false in
            env_join env1_f (env_meet ~narrow:false env1_t env2_f)
          else return None
        in
        env_join if_true if_false
      | Term.Binop (TIMES, _, _)
      | Term.Lam _
      | Term.FixLam _
      | Term.If _
      | Term.App _
      | Term.Let _
      | Term.Tuple _
      | Term.Variant _
      | Term.Match _ -> return @@ Some GEnv.bot

  let make_annot calls =
    if Options.record_closure_creation_context && Calls.is_max calls
    then Some calls
    else None

  (** The abstract evaluator: given a global environment, a calling
     context and a local environment, and a term, it computes an
     abstract value for the output of the term and a new global
     environment. This new environment will be used to feed a Kleene
     iterator. We use the same widening-aware fixpoint evaluator as
     for the ∇­CFA implementation, but we actually never make use of
     widening in this particular instance. *)
  let eval genv cache =
    solve @@ fun request ->
    let rec eval calls env t =
      let eval calls env t =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if Options.cache_non_calls
        then request false calls env t
        else eval calls env t
      in
      let request calls env t =
        let env =
          if Options.no_env_restrict
          then env
          else Vars.restrict_env (Term.fv t) env
        in
        if (not Options.cache_widened_calls_only) || Calls.is_max calls
        then request false calls env t (* never perform widening *)
        else eval calls env t
      in
      let open Abs in
      match t.Term.term with
      | Term.Bool b -> return @@ VAbs.bools @@ Bools.singleton b
      | Term.Lam (xloc, x, t0) ->
        let annot = make_annot calls in
        let env = Vars.restrict_env (Term.fv t) env in
        return
        @@ VAbs.singleton_closure annot (GlobalFun.Lam (t.id, xloc, x, t0)) env
      | Term.FixLam (floc, f, xloc, x, t0) ->
        let annot = make_annot calls in
        let env = Vars.restrict_env (Term.fv t) env in
        return
        @@ VAbs.singleton_closure
             annot
             (GlobalFun.FixLam (t.id, floc, f, xloc, x, t0))
             env
      | Term.UnknownInt -> return @@ VAbs.ints Ints.(interval min_int max_int)
      | Term.UnknownBool -> return @@ VAbs.bools Bools.top
      | Term.If (t1, t2, t3) ->
        let term_id = t.id in
        let* v1 = eval calls env t1 in
        let v1_can_be_bool = not @@ Bools.is_bot @@ VAbs.get_bools v1 in
        let* () =
          if v1_can_be_bool && (not @@ VAbs.is_bool v1)
          then alarm t1.id calls Alarm.(non_boolean v1)
          else return ()
        in
        if Options.kill_unreachable && not v1_can_be_bool
        then return VAbs.bot
        else
          let+ ts2 =
            let calls' =
              if Options.branch_is_context
              then
                Calls.extend
                  calls
                  term_id
                  (if Options.finer_context then Some 0 else None)
              else calls
            in
            if VAbs.mem_bool true v1
            then
              let* genv2, v2 = with_recorded @@ eval calls' env t2 in
              if Options.refine_branches
              then
                let* learned = backward (eval calls env) env t1 abs_true in
                match env_meet ~narrow:true (Some genv2) learned with
                | None -> return v2
                | Some genv' ->
                  let* () = record_genv genv' in
                  return v2
              else return v2
            else return VAbs.bot
          and+ ts3 =
            let calls' =
              if Options.branch_is_context
              then
                Calls.extend
                  calls
                  term_id
                  (if Options.finer_context then Some 1 else None)
              else calls
            in
            if VAbs.mem_bool false v1
            then
              let* genv3, v3 = with_recorded @@ eval calls' env t3 in
              if Options.refine_branches
              then
                let* learned = backward (eval calls env) env t1 abs_false in
                match env_meet ~narrow:true (Some genv3) learned with
                | None -> return v3
                | Some genv' ->
                  let* () = record_genv genv' in
                  return v3
              else return v3
            else return VAbs.bot
          in
          VAbs.join ts2 ts3
      | Term.BOrElse (t1, t2) ->
        let* v1 = eval calls env t1 in
        let* () =
          let can_be_bool = not @@ Bools.is_bot @@ VAbs.get_bools v1 in
          if can_be_bool && (not @@ VAbs.is_bool v1)
          then alarm t1.id calls Alarm.(non_boolean v1)
          else return ()
        in
        if VAbs.mem_bool true v1
        then
          if VAbs.mem_bool false v1
          then
            (* TODO: refine branch *)
            let* v2 = eval calls env t2 in
            return @@ VAbs.join abs_true v2
          else return abs_true
        else if VAbs.mem_bool false v1
        then (* TODO: refine branch *)
          eval calls env t2
        else return VAbs.bot
      | Term.BAndAlso (t1, t2) ->
        let* v1 = eval calls env t1 in
        let* () =
          let can_be_bool = not @@ Bools.is_bot @@ VAbs.get_bools v1 in
          if can_be_bool && (not @@ VAbs.is_bool v1)
          then alarm t1.id calls Alarm.(non_boolean v1)
          else return ()
        in
        if VAbs.mem_bool false v1
        then
          if VAbs.mem_bool true v1
          then
            let* v2 = eval calls env t2 in
            return @@ VAbs.join abs_false v2
          else return abs_false
        else if VAbs.mem_bool true v1
        then eval calls env t2
        else return VAbs.bot
      | Term.Var x -> return @@ GEnv.get (Env.find x env) genv
      | Term.App (t1, t2) ->
        let* v1 = eval calls env t1 in
        let v1_can_be_fun =
          VAbs.closures_join_map ~join:( || ) ~bot:false (fun _ _ -> true) v1
        in
        let* () =
          if v1_can_be_fun && (not @@ VAbs.is_fun v1)
          then alarm t1.id calls Alarm.(non_functional v1)
          else return ()
        in
        if Options.kill_unreachable && not v1_can_be_fun
        then return VAbs.bot
        else
          let* v2 = eval calls env t2 in
          if Options.kill_unreachable && VAbs.is_bot v2
          then return v2
          else
            VAbs.closures_join_map
              ~join
              ~bot
              (fun t0 envs0 ->
                match t0 with
                | Lam (loc, xloc, x, t') ->
                  let calls' =
                    Calls.extend
                      calls
                      t.Term.id
                      (if Options.finer_context then Some loc else None)
                  in
                  let* () = record_parameter_instance xloc calls' v2 in
                  CtxtEnvs.join_map
                    join
                    (fun env0 ->
                      request calls' (Env.add x (xloc, calls') env0) t')
                    envs0
                    bot
                | FixLam (loc, floc, f, xloc, x, t') ->
                  let calls' =
                    Calls.extend
                      calls
                      t.Term.id
                      (if Options.finer_context then Some loc else None)
                  in
                  let annot = make_annot calls' in
                  CtxtEnvs.join_map
                    join
                    (fun env0 ->
                      let clos =
                        VAbs.singleton_closure
                          annot
                          t0
                          (Vars.restrict_env (GlobalFun.fv t0) env0)
                      in
                      let* () = record_parameter_instance floc calls' clos in
                      let* () = record_parameter_instance xloc calls' v2 in
                      let new_env =
                        env0
                        |> Env.add f (floc, calls')
                        |> Env.add x (xloc, calls')
                      in
                      request calls' new_env t')
                    envs0
                    bot)
              v1
      | Term.Let (xloc, x, t1, t2) ->
        let* v1 = eval calls env t1 in
        if Options.kill_unreachable && VAbs.is_bot v1
        then return v1
        else
          let calls' =
            if Options.let_is_context
            then Calls.extend calls t.Term.id None
            else calls
          in
          let* () = record_parameter_instance xloc calls v1 in
          eval calls' (Env.add x (xloc, calls') env) t2
      | Term.Int n -> return @@ VAbs.ints @@ Ints.singleton n
      | Term.Binop (op, t1, t2) ->
        let* v1 = eval calls env t1 in
        let v1_can_be_int = not @@ Ints.is_bot @@ VAbs.get_ints v1 in
        let* () =
          if v1_can_be_int && (not @@ VAbs.is_int v1)
          then alarm t1.id calls Alarm.(non_numeric v1)
          else return ()
        in
        if Options.kill_unreachable && not v1_can_be_int
        then return VAbs.bot
        else
          let* v2 = eval calls env t2 in
          let* () =
            let can_be_int = not @@ Ints.is_bot @@ VAbs.get_ints v2 in
            if can_be_int && (not @@ VAbs.is_int v2)
            then alarm t2.id calls Alarm.(non_numeric v2)
            else return ()
          in
          return
            (match op with
            | PLUS -> VAbs.ints @@ Ints.Transfer.plus v1.VAbs.ints v2.VAbs.ints
            | MINUS ->
              VAbs.ints @@ Ints.Transfer.minus v1.VAbs.ints v2.VAbs.ints
            | TIMES -> VAbs.ints @@ Ints.Transfer.mult v1.VAbs.ints v2.VAbs.ints
            | LE ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.le v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.le v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | LT ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.lt v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.lt v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | GE ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.ge v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.ge v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | GT ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.gt v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.gt v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | EQ ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.eq v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.eq v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false
            | NEQ ->
              let (int1_t, int2_t), (int1_f, int2_f) =
                ( Ints.Transfer.neq v1.VAbs.ints v2.VAbs.ints true,
                  Ints.Transfer.neq v1.VAbs.ints v2.VAbs.ints false )
              in
              let when_true =
                if Ints.(is_bot int1_t || is_bot int2_t)
                then Bools.bot
                else b_true
              and when_false =
                if Ints.(is_bot int1_f || is_bot int2_f)
                then Bools.bot
                else b_false
              in
              VAbs.bools @@ Bools.join when_true when_false)
      | Term.Unop (NEG, t1) ->
        let* v1 = eval calls env t1 in
        let* () =
          let can_be_int = not @@ Ints.is_bot @@ VAbs.get_ints v1 in
          if can_be_int && (not @@ VAbs.is_int v1)
          then alarm t1.id calls Alarm.(non_numeric v1)
          else return ()
        in
        return @@ VAbs.ints @@ Ints.Transfer.neg v1.VAbs.ints
      | Term.Tuple (n, ts) ->
        let* vs =
          List.fold_left
            (fun macc t ->
              let* acc = macc in
              let* v = eval calls env t in
              let* () = record_produced_value t.id calls v in
              return ((t.id, calls) :: acc))
            (return [])
            ts
        in
        let vs = List.rev vs in
        return @@ VAbs.singleton_tuple n vs
      | Term.Variant (c, None) -> return @@ VAbs.singleton_variant c None
      | Term.Variant (c, Some t) ->
        let* v = eval calls env t in
        let* () = record_produced_value t.id calls v in
        return @@ VAbs.singleton_variant c (Some (t.id, calls))
      | Term.Match (t0, bs) ->
        let get_envs_from_simple_pattern input p env =
          match p with
          | Term.PWildcard -> return env
          | PVar (id, x) ->
            (* a value is bound to a variable *)
            let* () = record_parameter_instance id calls input in
            return (Env.add x (id, calls) env)
        in
        (* XXX: refine branches + learn_from_patterns *)
        let get_envs_from_pattern input p env =
          match p with
          | Term.PSimple (Any, p) ->
            let* env = get_envs_from_simple_pattern input p env in
            return [env]
          | Term.PSimple (Int, p) ->
            let* env =
              get_envs_from_simple_pattern VAbs.(ints @@ get_ints input) p env
            in
            return [env]
          | Term.PSimple (Bool, p) ->
            let* env =
              get_envs_from_simple_pattern VAbs.(bools @@ get_bools input) p env
            in
            return [env]
          | Term.PTuple (n, l) ->
            VAbs.tuples_join_map
              n
              ~bot:(return [])
              ~join:(fun m1 m2 ->
                let* envs1 = m1 in
                let* envs2 = m2 in
                return @@ envs1 @ envs2)
              (fun vs ->
                List.fold_left2
                  (fun m pi xi ->
                    let vi = GEnv.get xi cache in
                    let* envs = m in
                    List.fold_left
                      (fun acc env ->
                        let* envs = acc in
                        let* env = get_envs_from_simple_pattern vi pi env in
                        return @@ (env :: envs))
                      (return [])
                      envs)
                  (return [env])
                  l
                  vs)
              input
          | Term.PVariant (c, None) ->
            VAbs.variants_join_map
              c
              ~bot:(return [])
              ~join:(fun m1 m2 ->
                let* envs1 = m1 in
                let* envs2 = m2 in
                return @@ envs1 @ envs2)
              (function
                | None -> return [env]
                | Some _ -> return [])
              input
          | Term.PVariant (c, Some p) ->
            VAbs.variants_join_map
              c
              ~bot:(return [])
              ~join:(fun m1 m2 ->
                let* envs1 = m1 in
                let* envs2 = m2 in
                return @@ envs1 @ envs2)
              (function
                | None -> return []
                | Some var ->
                  let v = GEnv.get var cache in
                  let* env = get_envs_from_simple_pattern v p env in
                  return [env])
              input
        in
        let eval_branch term_id input i (pi, ti) =
          let* envs = get_envs_from_pattern input pi env in
          List.fold_left
            (fun macc env ->
              let* vacc = macc in
              let calls =
                if Options.branch_is_context
                then
                  Calls.extend
                    calls
                    term_id
                    (if Options.finer_context then Some i else None)
                else calls
              in
              let* vi = eval calls env ti in
              return @@ VAbs.join vi vacc)
            (return VAbs.bot)
            envs
        in
        let eval_branches term_id input bs =
          List.fold_left
            (fun acc b ->
              let* i, res = acc in
              let* res_b = eval_branch term_id input (i + 1) b in
              return (i + 1, VAbs.join res res_b))
            (return (0, VAbs.bot))
            bs
        in
        let term_id = t.id in
        let* v = eval calls env t0 in
        let* _n, res = eval_branches term_id v bs in
        (* let* () =
         *   if not matched
         *   then alarm term_id calls Alarm.(non_exhaustive_match v)
         *   else return ()
         * in *)
        return res
    in
    eval

  (** Prints statistics about the global environment on [stderr] *)
  let print_stats genv cache =
    if Options.debug
    then begin
      Log.eprintf "Bindings in global environment: %i@." (GEnv.cardinal genv);
      let cache_entries = GEnv.cardinal cache in
      if cache_entries <> 0
      then Log.eprintf "Bindings in global cache: %i@." cache_entries
    end

  (** Checks whether there is a cycle in the global environment.
     Cycles are innocuous, but they are hints that indicate where the
     ∇-CFA analysis may be less precise. *)
  let check_genv genv =
    let finished = ref Common.BoolGEnv.empty in
    let exception Cycle in
    let exception Unbound in
    let rec browse name abs =
      let open Common.BoolGEnv in
      match find_opt name !finished with
      | None ->
        (* not visited yet *)
        finished := add name false !finished;
        VAbs.closures_iter
          (fun _fun envs ->
            CtxtEnvs.iter
              (fun env ->
                Env.iter
                  (fun _x name' ->
                    match GEnv.find_opt name' genv with
                    | Some abs' -> browse name' abs'
                    | None -> raise Unbound)
                  env)
              envs)
          abs;
        finished := add name true !finished
      | Some true ->
        (* visited and finished: sharing detected *)
        ()
      | Some false ->
        (* visited and not finished: cycle detected *)
        raise Cycle
    in
    try GEnv.iter browse genv with
    | Cycle -> Log.eprintf "INFO: Cycle found in global environment.@."
    | Unbound ->
      Log.eprintf
        "ERROR: Unbound entry in global environment. This should not happen.@."

  (** The final evaluator: computes a fixpoint of the inner evaluator *)
  let eval t =
    Common.make_eval
      ~print_stats
      ~check_genv
      ~whole_genv_cache:Options.print_memo_table
      eval
      t
end
