(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2021
*)

(** Unification engine, based on a Union-Find data structure. *)

type var = int
type 'a t

val init : unit -> 'a t
val register : var -> 'a t -> unit
val find : var -> 'a t -> var
val equivalent : var -> var -> 'a t -> bool
val union : var -> var -> 'a t -> unit
val get_term : var -> 'a t -> 'a option
val set_term : var -> 'a -> 'a t -> unit

val unify_vars :
  ('a -> 'a -> 'a * (var * var) list) -> var -> var -> 'a t -> unit
