(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Lang.Ast

module Make
    (_ : Logs.S)
    (_ : sig
      val no_alarms : bool
      val debug : bool
    end)
    (_ : Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING)
    (Calls : Calls.S)
    (CtxtEnv : sig
      type t

      val empty : t
      val compare : t -> t -> int
      val pp : Format.formatter -> t -> unit
    end)
    (Abs : sig
      type t

      val bot : t
      val is_bot : t -> bool
      val leq : t -> t -> bool
      val join : t -> t -> t
      val widen : t -> t -> t
      val meet : t -> t -> t
      val pp : Format.formatter -> t -> unit

      val fold_vars :
        (PP.t * Calls.t -> 'a -> 'a) ->
        (PP.t * Calls.t -> 'a -> 'a) ->
        (PP.t * Calls.t -> 'a -> 'a) ->
        t ->
        'a ->
        'a
      (** [fold fenv fcache] folds over the variables of an abstract
         value, using [fenv] to fold over the variables that are
         recorded in the environment (e.g. the variables that are in
         the environments of abstract closures), and using [fcache] to
         fold over the variables that are recorded in the cache (e.g.
         the variables that are recorded in abstract tuples). *)
    end) : sig
  module GEnv : sig
    type key = PP.t * Calls.t
    type value = Abs.t
    type t

    val bot : t
    val get : key -> t -> value
    val singleton : key -> value -> t
    val find_opt : key -> t -> value option
    val union : (key -> value -> value -> value option) -> t -> t -> t

    val merge :
      (key -> value option -> value option -> value option) -> t -> t -> t

    val iter : (key -> value -> unit) -> t -> unit
    val cardinal : t -> int
  end

  module BoolGEnv : sig
    type key = PP.t * Calls.t
    type value = bool
    type t

    val empty : t
    val find_opt : key -> t -> value option
    val add : key -> value -> t -> t
  end

  (** Tuples of a global environment, an abstract value and a log of
     alarms: such tuples are the results of the analyzer. *)
  module AlarmGEnvAbs : sig
    type 'a m
    type t = Abs.t m

    module Alarm : sig
      type t

      val non_boolean : Abs.t -> t
      val non_numeric : Abs.t -> t
      val non_functional : Abs.t -> t
      val non_exhaustive_match : Abs.t -> t
    end

    val bot : t
    val leq : t -> t -> bool
    val join : t -> t -> t
    val widen : t -> t -> t
    val pp : Format.formatter -> t -> unit
    val return : 'a -> 'a m
    val ( let* ) : 'a m -> ('a -> 'b m) -> 'b m
    val ( let+ ) : 'a m -> ('a -> 'b) -> 'b m
    val ( and+ ) : 'a m -> 'b m -> ('a * 'b) m
    val with_recorded : 'a m -> (GEnv.t * 'a) m
    val record_genv : GEnv.t -> unit m
    val record_parameter_instance : PP.t -> Calls.t -> Abs.t -> unit m
    val record_produced_value : PP.t -> Calls.t -> Abs.t -> unit m
    val alarm : PP.t -> Calls.t -> Alarm.t -> unit m
    val get_alarms : t -> (PP.t * string) list
  end

  val solve :
    ((bool -> Calls.t -> CtxtEnv.t -> Term.t -> AlarmGEnvAbs.t) ->
    Calls.t ->
    CtxtEnv.t ->
    Term.t ->
    AlarmGEnvAbs.t) ->
    Calls.t ->
    CtxtEnv.t ->
    Term.t ->
    AlarmGEnvAbs.t
    * (((Calls.t * Lang.Ast.Term.t * CtxtEnv.t) * unit) * AlarmGEnvAbs.t)
      Memo.Statistics.t

  val make_eval :
    print_stats:(GEnv.t -> GEnv.t -> unit) ->
    check_genv:(GEnv.t -> unit) ->
    whole_genv_cache:bool ->
    (GEnv.t ->
    GEnv.t ->
    Calls.t ->
    CtxtEnv.t ->
    'term ->
    AlarmGEnvAbs.t
    * (((Calls.t * Lang.Ast.Term.t * CtxtEnv.t) * unit) * AlarmGEnvAbs.t)
      Memo.Statistics.t) ->
    'term ->
    AlarmGEnvAbs.t
    * (((Calls.t * Lang.Ast.Term.t * CtxtEnv.t) * unit) * AlarmGEnvAbs.t)
      Memo.Statistics.t
end
