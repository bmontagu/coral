(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type OrderedHashedTyped = sig
  type t

  val compare : t -> t -> int
  val hash_fold : t -> Hash.state -> Hash.state
end

(** Abstract domain for closures and values, with recursive knots. *)
module AbsDomain
    (_ : sig
      val minimize : bool
      val occurs_threshold : int
      val sim_threshold : int
    end)
    (Bools : AbstractDomains.Sigs.BOOL_DOMAIN)
    (Ints : AbstractDomains.Sigs.INT_DOMAIN)
    (Annot : OrderedHashedTyped)
    () :
  NablaCommon.ABS_DOMAIN
    with type bools := Bools.t
     and type ints := Ints.t
     and type annot := Annot.t
