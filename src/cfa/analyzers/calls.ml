(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type S = sig
  type t

  val hash_fold : t -> Hash.state -> Hash.state
  val compare : t -> t -> int
  val is_max : t -> bool
  val empty : t
  val extend : t -> Lang.Ast.PP.t -> Lang.Ast.PP.t option -> t
  val pp : Format.formatter -> t -> unit
end

let pp_call_string fmt l =
  let open Lang.Ast in
  (Format.pp_print_list
     ~pp_sep:(fun fmt () -> Format.pp_print_string fmt " # ")
     (fun fmt (pp1, pp2o) ->
       match pp2o with
       | None -> PP.pp fmt pp1
       | Some pp2 -> Format.fprintf fmt "%a[%a]" PP.pp pp1 PP.pp pp2))
    fmt
    (List.rev l)

let rec keep n = function
  | [] -> []
  | x :: l -> if n <= 0 then [] else x :: keep (n - 1) l

let pp_opp_list_hash_fold l s =
  List.fold_left
    (fun s v ->
      Hash.fold 1
      @@
      match v with
      | loc1, oloc2 -> (
        Lang.Ast.PP.hash_fold loc1
        @@
        match oloc2 with
        | None -> Hash.fold 0 s
        | Some loc2 -> Hash.fold 1 @@ Lang.Ast.PP.hash_fold loc2 s))
    (Hash.fold 0 s)
    l

(** Module implementing the "max k cyclic call sites" strategy with
   "at most l last call sites" in the case of recursive calls. *)
module CyclicCallSites (Config : sig
  val max_nesting : int

  val max_last : int option
  (** If [max_last = Some 0], this is equivalent to the "max k
          cyclic call sites policy". If [max_last = None] is provided,
          then the bound is chosen dynamically, taking the length of
          the last call site repetition as a value. *)
end) : S = struct
  open Lang.Ast

  type t =
    | Calls of (PP.t * PP.t option) list
    | CallsMax of
        (PP.t * PP.t option) list * int * (PP.t * PP.t option) list * int
  [@@deriving ord]

  let hash_fold t s =
    match t with
    | Calls l -> Hash.fold 0 @@ pp_opp_list_hash_fold l s
    | CallsMax (l1, i, l2, j) ->
      Hash.fold 1
      @@ pp_opp_list_hash_fold l1
      @@ Hash.fold i
      @@ pp_opp_list_hash_fold l2
      @@ Hash.fold j s

  let pp fmt = function
    | Calls calls -> (
      match calls with
      | [] -> Format.pp_print_string fmt "ε"
      | _ -> pp_call_string fmt calls)
    | CallsMax (prefix, _repetition_len, suffix, _suffix_len) -> (
      match (prefix, suffix) with
      | [], [] -> Format.pp_print_char fmt '*'
      | [], _ -> Format.fprintf fmt "* # %a" pp_call_string suffix
      | _, [] -> Format.fprintf fmt "%a # *" pp_call_string prefix
      | _, _ ->
        Format.fprintf
          fmt
          "%a # * # %a"
          pp_call_string
          prefix
          pp_call_string
          suffix)

  let empty = Calls []

  let is_max = function
    | CallsMax (_prefix, repetition_len, _suffix, suffix_len) ->
      suffix_len >= repetition_len
    | Calls _ -> false

  let cmp_call = [%derive.ord: PP.t * PP.t option]

  let count c calls =
    List.fold_left
      (fun (n, found, idx) c' ->
        if cmp_call c c' = 0
        then (n + 1, true, if found then idx else idx + 1)
        else (n, found, if found then idx else idx + 1))
      (0, false, 0)
      calls

  let extend calls c1 c2o =
    match calls with
    | CallsMax (prefix, repetition_len, suffix, suffix_len) ->
      let c = (c1, c2o) in
      CallsMax
        ( prefix,
          repetition_len,
          keep repetition_len (c :: suffix),
          min repetition_len (suffix_len + 1) )
    | Calls l ->
      let c = (c1, c2o) in
      let repeat, _found, index = count c l in
      if repeat >= Config.max_nesting
      then
        let index =
          match Config.max_last with
          | None -> index
          | Some n -> n
        in
        CallsMax (c :: l, index, [], 0)
      else Calls (c :: l)
end

(** Module implementing the "last k call sites" strategy, as in k-CFA *)
module LastCallSites (Config : sig
  val max_length : int
end) : S = struct
  open Lang.Ast

  type t = int * (PP.t * PP.t option) list [@@deriving ord]

  let hash_fold (i, l) s = Hash.fold i @@ pp_opp_list_hash_fold l s

  let pp fmt (len, calls) =
    if len < Config.max_length
    then
      match calls with
      | [] -> Format.pp_print_string fmt "ε"
      | _ -> pp_call_string fmt calls
    else
      match calls with
      | [] -> Format.pp_print_char fmt '*'
      | _ -> Format.fprintf fmt "* # %a" pp_call_string calls

  let empty = (0, [])
  let is_max (n, _) = n >= Config.max_length

  let extend_gen max (n, l) c1 c2o =
    let c = (c1, c2o) in
    if n < max then (n + 1, c :: l) else (n, keep n (c :: l))

  let extend = extend_gen Config.max_length
end
