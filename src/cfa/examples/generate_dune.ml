(*****************************************************************************)

(* configuration for analyzer "nabla" *)
module ConfigNabla = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-nabla"

  (* extention for the expected output files *)
  let expected_ext = ".expected-nabla"

  (* name of the executable to run for each test *)
  let exe = "cfa-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags =
    [
      "--debug";
      "--analysis=nabla";
      "--solver=priority";
      "--integer-domain=intervals";
      "--call-domain=cyclic-call-sites:1";
      "--branch-is-context";
      "--kill-unreachable";
      (* "--share-integers-program-points"; *)
      "--share-variables-program-points";
      "--refine-branches";
      "--disprove-alarms";
    ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = ["negative_normal_form"]
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigNabla) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "nabla-rec" *)
module ConfigNablaRec = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-nabla-rec"

  (* extention for the expected output files *)
  let expected_ext = ".expected-nabla-rec"

  (* name of the executable to run for each test *)
  let exe = "cfa-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags =
    [
      "--debug";
      "--analysis=nabla-rec";
      "--solver=priority";
      "--integer-domain=intervals";
      "--call-domain=cyclic-call-sites:1";
      "--branch-is-context";
      "--kill-unreachable";
      (* "--share-integers-program-points"; *)
      "--share-variables-program-points";
      "--refine-branches";
      "--disprove-alarms";
    ]

  (* flags that should be given to specific files *)
  let file_specific_flags =
    [
      ( ["even_odd"; "even_odd_pair"; "eval_expr_monadic_variant_keys"],
        "--sim-threshold=2" :: default_flags );
      (["peano_add"; "peano_add2"], "--occurs-threshold=2" :: default_flags);
    ]

  (* files in the following list are excluded *)
  let excludes = ["sliding_window"]
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigNablaRec) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "nabla-rec" with dynamic call sites policy *)
module ConfigNablaRecDyn = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-nabla-rec-dyn"

  (* extention for the expected output files *)
  let expected_ext = ".expected-nabla-rec-dyn"

  (* name of the executable to run for each test *)
  let exe = "cfa-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags =
    [
      "--debug";
      "--analysis=nabla-rec";
      "--solver=priority";
      "--integer-domain=intervals";
      "--call-domain=cyclic-call-sites:1+dyn";
      "--branch-is-context";
      "--kill-unreachable";
      (* "--share-integers-program-points"; *)
      "--share-variables-program-points";
      "--refine-branches";
      "--disprove-alarms";
    ]

  (* flags that should be given to specific files *)
  let file_specific_flags =
    [
      ( ["even_odd"; "even_odd_pair"; "eval_expr_monadic_variant_keys"],
        "--sim-threshold=2" :: default_flags );
    ]

  (* files in the following list are excluded *)
  let excludes = ["negative_normal_form"; "skolemize"]
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigNablaRecDyn) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "global" *)
module ConfigGlobal = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-global"

  (* extention for the expected output files *)
  let expected_ext = ".expected-global"

  (* name of the executable to run for each test *)
  let exe = "cfa-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags =
    [
      "--debug";
      "--analysis=global";
      "--solver=naive";
      "--integer-domain=intervals";
      "--call-domain=last-call-sites:1";
      (* "--kill-unreachable"; *)
      (* "--share-integers-program-points"; *)
      "--share-variables-program-points";
      "--branch-is-context";
      "--refine-branches";
    ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes =
    [
      "heintze_mcallester_500";
      "heintze_mcallester_600";
      "heintze_mcallester_700";
      "heintze_mcallester_800";
      "heintze_mcallester_900";
      "heintze_mcallester_1000";
    ]
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigGlobal) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "global-independent-attribute" *)
module ConfigGlobalIndependentAttribute = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-global-independent-attribute"

  (* extention for the expected output files *)
  let expected_ext = ".expected-global-independent-attribute"

  (* name of the executable to run for each test *)
  let exe = "cfa-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags =
    [
      "--debug";
      "--analysis=global-independent-attribute";
      "--solver=naive";
      "--integer-domain=intervals";
      "--call-domain=last-call-sites:1";
      (* "--kill-unreachable"; *)
      (* "--share-integers-program-points"; *)
      "--share-variables-program-points";
      "--branch-is-context";
      "--refine-branches";
    ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes =
    [
      "heintze_mcallester_500";
      "heintze_mcallester_600";
      "heintze_mcallester_700";
      "heintze_mcallester_800";
      "heintze_mcallester_900";
      "heintze_mcallester_1000";
    ]
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigGlobalIndependentAttribute) in
  M.generate_rules stdout Filename.current_dir_name
