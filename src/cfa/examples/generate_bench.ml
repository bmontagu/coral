open CfaLib.Main

module Preferences = struct
  let print_git_version = false
  let print_date = false
  let print_running_times = false
end

module Prog = struct
  type t = {
    name: string;
    path: string;
  }
end

module Analyzer = struct
  type t = {
    heuristics: CallDomain.t;
    kind: AnalysisKind.t;
  }

  let name { heuristics; kind } =
    let h =
      match heuristics with
      | LastCallSites n -> string_of_int n
      | CyclicCallSites (n, Some 0) -> string_of_int n ^ "*"
      | CyclicCallSites (n, Some l) -> string_of_int n ^ "*+" ^ string_of_int l
      | CyclicCallSites (n, None) -> string_of_int n ^ "*+dyn"
    and k =
      match kind with
      | Nabla -> "∇CFA"
      | NablaRec -> "∇CFA-rec"
      | Global -> "CFA"
      | GlobalIndependentAttribute -> "CFA-IA"
    in
    h ^ "-" ^ k

  let tex_name { heuristics; kind } =
    let h =
      match heuristics with
      | LastCallSites n -> "$" ^ string_of_int n ^ "$"
      | CyclicCallSites (n, Some 0) -> "$" ^ string_of_int n ^ "^\\ast$"
      | CyclicCallSites (n, Some l) ->
        "$" ^ string_of_int n ^ "^\\ast+" ^ string_of_int l ^ "$"
      | CyclicCallSites (n, None) ->
        "$" ^ string_of_int n ^ "^\\ast+\\mathsf{dyn}$"
    and k =
      match kind with
      | Nabla -> "$\\nabla$CFA"
      | NablaRec -> "$\\nabla$CFA-rec"
      | Global -> "CFA"
      | GlobalIndependentAttribute -> "CFA-IA"
    in
    h ^ "-" ^ k

  let options { heuristics; kind } =
    let open Options in
    {
      refine_branches = true;
      (* To get a fair comparison, we enable branch refinement
            for every analyzer. It used to be enabled only for Nabla:
         begin match kind with
           | Nabla -> true
           | Global | GlobalIndependentAttribute -> false
         end;
      *)
      branch_is_context = true;
      (* To get a fair comparison, we enable this flag
            for every analyzer. It used to be enabled only for Nabla:
         begin match heuristics with
           | LastCallSites _ | CyclicCallSites 0 -> false
           | CyclicCallSites _ -> true
         end;
      *)
      let_is_context = false;
      finer_context = false;
      no_env_restrict = false;
      kill_unreachable = false;
      record_closure_creation_context = false;
      record_tuple_creation_location = false;
      record_variant_creation_location = false;
      cache_non_calls = true;
      cache_widened_calls_only = false;
      no_alarms = false;
      disprove_alarms = false;
      minimize = true;
      debug = false;
      print_memo_table = false;
      share_integers_program_points = false;
      share_variables_program_points = true;
      call_domain = heuristics;
      integer_domain =
        begin
          match kind with
          | Nabla | NablaRec -> IntegerDomain.Intervals
          | Global | GlobalIndependentAttribute -> IntegerDomain.Flat
        end;
      analysis_kind = kind;
      solver = Naive;
      occurs_threshold = 0;
      sim_threshold = 4;
    }

  let pp_kind fmt =
    let open Format in
    function
    | AnalysisKind.Nabla -> fprintf fmt "nabla"
    | AnalysisKind.NablaRec -> fprintf fmt "nabla-rec"
    | Global -> fprintf fmt "global"
    | GlobalIndependentAttribute -> fprintf fmt "global-independent-attribute"

  let pp_options fmt options =
    let open Format in
    let open Options in
    if options.refine_branches then fprintf fmt " --refine-branches";
    if options.branch_is_context then fprintf fmt " --branch-is-context";
    if options.let_is_context then fprintf fmt " --let-is-context";
    if options.no_env_restrict then fprintf fmt " --no-env-restrict";
    if options.debug then fprintf fmt " --debug";
    fprintf fmt " --call-domain=%a" CallDomain.printer options.call_domain;
    fprintf
      fmt
      " --integer-domain=%a"
      IntegerDomain.printer
      options.integer_domain;
    fprintf fmt " --%a" pp_kind options.analysis_kind

  let pp_cli_options fmt analyzer = pp_options fmt (options analyzer)
end

let base_dir =
  if Array.length Sys.argv <> 2
  then begin
    Format.eprintf
      "Please provide exactly one argument: the path of the directories that \
       contain the examples.@.";
    exit 1
  end;
  let dir = Sys.argv.(1) in
  if not @@ Sys.file_exists dir
  then begin
    Format.eprintf "Path %s does not exist.@." dir;
    exit 1
  end;
  if not @@ Sys.is_directory dir
  then begin
    Format.eprintf "Path %s is not a directory.@." dir;
    exit 1
  end;
  dir

let full_name prog = Filename.concat base_dir prog.Prog.path

let check_progs progs =
  let n = ref 0 in
  let check prog =
    incr n;
    let name = full_name prog in
    if not @@ Sys.file_exists name
    then begin
      Format.eprintf "File %s does not exist.@." name;
      exit 1
    end;
    if Sys.is_directory name
    then begin
      Format.eprintf "File %s is a directory.@." name;
      exit 1
    end
  in
  List.iter check progs;
  Format.eprintf "%i files are registered in the benchmark.@." !n

let make_suite progs analyzers =
  List.fold_right
    (fun prog rows ->
      List.fold_right
        (fun analyzer cols -> (prog, analyzer) :: cols)
        analyzers
        []
      :: rows)
    progs
    []

let exec_one (prog, analyzer) =
  Format.eprintf "%s: %s... @?" prog.Prog.path (Analyzer.name analyzer);
  let options = Analyzer.options analyzer
  and name = full_name prog in
  let res, tm = Time.call (fun () -> cfa_value options name) in
  Format.eprintf "DONE (%a)@." Time.pp tm;
  (res, tm)

let exec_suite suite = List.map (List.map exec_one) suite

let pp_analyzer fmt analyzer =
  Format.fprintf fmt "\\multicolumn{5}{c}{%s}" (Analyzer.tex_name analyzer)

let pp_stats fmt stats =
  let open Memo.Statistics in
  Format.fprintf
    fmt
    "%i & %i & %i (%i) & %i"
    stats.states
    stats.edges
    stats.max_fanout
    stats.med_fanout
    stats.iterations

let pp_stat_cols fmt _ = Format.fprintf fmt "S & E & F & I & R"

let comment s =
  let lines = String.split_on_char '\n' s in
  let lines = List.map (fun s -> "% " ^ s) lines in
  String.concat "\n" lines

let pp_result fmt (Pack (v, pp, stats, _pp_entry), tm) =
  let v_string = Format.asprintf "%a" pp v in
  if Preferences.print_running_times
  then
    Format.fprintf
      fmt
      "%% Time: %a@.  %a &@.%s@."
      Time.pp
      tm
      pp_stats
      stats
      (comment v_string)
  else Format.fprintf fmt "%a &@.%s@." pp_stats stats (comment v_string)

let print_line prog line =
  Format.printf
    "  \\texttt{%s} &@.  %a@."
    prog.Prog.name
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt " &@.  ")
       pp_result)
    line

let print_lines progs suite =
  List.iter2
    (fun prog line ->
      print_line prog line;
      Format.printf " \\\\@.")
    progs
    suite

let print_table progs analyzers suite =
  let open Format in
  eprintf "Printing table... @?";
  List.iter
    (fun analyzer ->
      printf
        "%% Options for %s: %a@."
        (Analyzer.tex_name analyzer)
        Analyzer.pp_cli_options
        analyzer)
    analyzers;
  printf "%%@.";
  printf "%% S: states, E: edges, F: max fanout, I: iterations, R: result@.";
  printf "%%@.";
  printf "\\noindent@.";
  printf "S: number of States in the abstract call graph\\\\@.";
  printf "E: number of Edges in the abstract call graph\\\\@.";
  printf
    "F: maximal Fanout degree in the abstract call graph (and the median \
     fanout degree)\\\\@.";
  printf "I: number of Iterations before a fixpoint was reached\\\\@.";
  printf
    "R: analysis Result (results are pretty-printed in the \\LaTeX{} source \
     file)\\\\@.";
  printf "\\begin{tabular}{l*{%i}{rrrrc}}@." (List.length analyzers);
  printf "  \\toprule@.";
  printf
    "  Program & %a \\\\@."
    (pp_print_list
       ~pp_sep:(fun fmt () -> pp_print_string fmt " & ")
       pp_analyzer)
    analyzers;
  printf "  \\midrule@.";
  printf
    "  & %a \\\\@."
    (pp_print_list
       ~pp_sep:(fun fmt () -> pp_print_string fmt " & ")
       pp_stat_cols)
    analyzers;
  printf "  \\midrule@.";
  print_lines progs suite;
  printf "  \\bottomrule@.";
  printf "\\end{tabular}@.";
  eprintf "DONE@."

let pp_date fmt tm =
  let mo =
    match tm.Unix.tm_mon with
    | 0 -> "jan"
    | 1 -> "feb"
    | 2 -> "mar"
    | 3 -> "apr"
    | 4 -> "may"
    | 5 -> "jun"
    | 6 -> "jul"
    | 7 -> "aug"
    | 8 -> "sep"
    | 9 -> "oct"
    | 10 -> "nov"
    | 11 -> "dec"
    | _ -> assert false
  and y = 1900 + tm.Unix.tm_year
  and d = tm.Unix.tm_mday
  and h = tm.Unix.tm_hour
  and m = tm.Unix.tm_min
  and s = tm.Unix.tm_sec in
  Format.fprintf fmt "%i %s %i %i:%i:%i" y mo d h m s

let print_date () =
  if Preferences.print_date
  then Format.printf "%% Date: %a@." pp_date (Unix.gmtime @@ Unix.time ())

let get_git_revision () =
  let chn = Unix.open_process_in "git rev-parse HEAD" in
  let s = try input_line chn with End_of_file -> "ERROR" in
  close_in chn;
  s

let print_git_revision () =
  if Preferences.print_git_version
  then Format.printf "%% Git revision: %s@." (get_git_revision ())

let analyzers =
  let open Analyzer in
  let open CallDomain in
  let open AnalysisKind in
  [
    { heuristics = LastCallSites 0; kind = Global };
    { heuristics = LastCallSites 1; kind = Global };
    { heuristics = CyclicCallSites (1, Some 0); kind = Global };
    { heuristics = LastCallSites 1; kind = Nabla };
    { heuristics = CyclicCallSites (1, Some 0); kind = Nabla };
    { heuristics = LastCallSites 1; kind = NablaRec };
    { heuristics = CyclicCallSites (1, Some 0); kind = NablaRec };
    { heuristics = CyclicCallSites (1, None); kind = NablaRec };
  ]

let progs =
  let open Prog in
  [
    { name = "ack"; path = "ack.simpl" };
    { name = "ack\\_cps"; path = "ack_cps.simpl" };
    { name = "binomial"; path = "binomial.simpl" };
    { name = "blur"; path = "blur.simpl" };
    { name = "church"; path = "church.simpl" };
    { name = "delta\\_delta"; path = "delta_delta.simpl" };
    { name = "eta"; path = "eta.simpl" };
    { name = "facehugger"; path = "facehugger.simpl" };
    { name = "fact"; path = "fact.simpl" };
    { name = "fact\\_cps"; path = "fact_cps.simpl" };
    { name = "fact\\_tailrec"; path = "fact_tailrec.simpl" };
    { name = "hmca100"; path = "heintze_mcallester_100.simpl" };
    { name = "hmca200"; path = "heintze_mcallester_200.simpl" };
    { name = "hmca300"; path = "heintze_mcallester_300.simpl" };
    { name = "hmca400"; path = "heintze_mcallester_400.simpl" };
    { name = "hmca500"; path = "heintze_mcallester_500.simpl" };
    { name = "hmca600"; path = "heintze_mcallester_600.simpl" };
    { name = "kcfa2"; path = "kcfa2.simpl" };
    { name = "kcfa3"; path = "kcfa3.simpl" };
    { name = "mc91"; path = "mc91.simpl" };
    { name = "mc91\\_cps"; path = "mc91_cps.simpl" };
    { name = "mc91\\_tailrec"; path = "mc91_tailrec.simpl" };
    { name = "mj09"; path = "mj09.simpl" };
    { name = "sat"; path = "sat.simpl" };
    { name = "sat3"; path = "sat3.simpl" };
    { name = "sat4"; path = "sat4.simpl" };
    { name = "shivers"; path = "shivers_infinite.simpl" };
    { name = "shivers2"; path = "shivers_infinite2.simpl" };
    { name = "tak"; path = "tak.simpl" };
    { name = "tak\\_cps"; path = "tak_cps.simpl" };
    { name = "tak\\_4d"; path = "tak_4d.simpl" };
  ]

let () =
  check_progs progs;
  print_date ();
  print_git_revision ();
  make_suite progs analyzers |> exec_suite |> print_table progs analyzers
