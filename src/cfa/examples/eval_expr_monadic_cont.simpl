(* A simple evaluator for arithmetic expressions with variables, that
   uses an error monad encoded with two continuations (one that is
   used in case of success, and one that is used in case of failure)
*)

val return x = fun success error -> success x
val error = fun success error -> error ()
val bind m f = fun success error ->
  m (fun x -> f x success error) error
val run m = m (fun x -> Ok x) (fun unit -> Error)

(* Environments are association lists, that may have duplicate
entries: the first entry is always selected. *)
val empty_env = Nil
val add_env x v env = Cons ((x, v), env)
val rec find_env x env = match env with
| Nil -> error
| Cons p -> match p with (q, env') ->
  match q with (y, v) ->
    if x = y
    then return v
    else find_env x env'
  end
  end
end

(* Evaluator for arithmetic expressions of type
type t = Var of int | Int of int | Plus of t * t | Minus of t * t
*)
val rec eval_expr env e = match e with
| Var x -> find_env x env
| Num i -> return i
| Plus p -> match p with (e1, e2) ->
  bind (eval_expr env e1) (fun n1 ->
    bind (eval_expr env e2) (fun n2 ->
      return (n1 + n2)
    )
  )
  end
| Minus p -> match p with (e1, e2) ->
  bind (eval_expr env e1) (fun n1 ->
    bind (eval_expr env e2) (fun n2 ->
      return (n1 - n2)
    )
  )
  end
end
val eval env expr = run (eval_expr env expr)

val x = 0
val y = x+1
val z = y+1
val t = 42
val example_expr1 = Plus (Var x, Minus (Num 42, Var y)) (* x + (42 - y) *)
val example_expr2 = Plus (Minus (Num 42, Num 27), Num 27) (* (42 - 27) + 27 *)
val example_expr3 = Plus (Minus (Num 42, Var t), Num 27) (* (42 - t) + 27 *)
val example_env = add_env z 3 (add_env x 27 (add_env y 36 empty_env))
(* z -> 3, x -> 27, y -> 36 *)

val result1 = eval example_env example_expr1 (* should return Ok 33 *)
val result2 = eval example_env example_expr2 (* should return Ok 42 *)
val result3 = eval example_env example_expr3 (* should return Error *)
val result = (result1, result2, result3)
