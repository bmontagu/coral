(* computes the maximal number in a tree using a visitor pattern *)

val truncate_to_range a b x =
  if x < a
  then a
  else if x > b
  then b
  else x

val truncate = truncate_to_range 12 42

val max x y = if x < y then y else x

val tree_max init =
  let rec tree_max t = match t with
  | E -> init
  | N p -> match p with (t1, x, t2) -> max x (max (tree_max t1) (tree_max t2))
  end end in
  tree_max

val rec div2 n =
  if n <= 1
  then 0
  else 1 + div2 (n-2)

val rec make n x =
  if n <= 0
  then E
  else
    let n2 = div2 n in
    let m2 = if n2 = 0 then n - 1 else n - n2 in
    let y = truncate (x+1) in
    N (make n2 y, x, make m2 y)

val fst p = match p with (x, _) -> x end
val snd p = match p with (_, y) -> y end

(* accept function for the visitor pattern *)
val rec accept visitor t = match t with
| E -> fst visitor accept ()
| N p -> match p with (l, x, r) ->
  snd visitor accept l x r
end end

(* visitor to compute the maximum of a tree *)
val rec visit m =
((fun accept unit -> m),
 (fun accept l x r ->
   max x (max (accept (visit m) l) (accept (visit m) r))
 )
)

val tree_max n t =
  accept (visit n) t

val result = tree_max 0 (make ?int 0)
