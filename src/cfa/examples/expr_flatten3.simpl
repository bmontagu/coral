(* Example extracted from "Analyse Statique de Transformations Pour l'Élimination de Motif",
   P. Lermusiaux
   Ph.D. thesis, 2022 *)

(* type expr = Int of int | Lst of list
   and list = Nil | Cons of expr * list
*)

val make_expr make_list n =
  if ?bool
  then Int (?int)
  else List (make_list (n-1))

val rec make_list n =
  if ?bool
  then Nil
  else Cons (make_expr make_list (n-1), make_list (n-1))

(* concatenation of two lists *)
val rec fconcat flatten l1 l2 = match l1 with
| Nil -> flatten l2
| Cons p -> match p with
  | (e, l1') -> match e with
    | Int n -> Cons (Int n, fconcat flatten l1' l2)
    | List l1'' -> fconcat flatten l1'' (Cons (List l1', l2))
    end
  end
end

(* flattens a list of expressions, so that intermediate lists are eliminated *)
val rec flatten l = match l with
| Nil -> Nil
| Cons p -> match p with
  | (e, l') -> match e with
    | Int n -> Cons (Int n, flatten l')
    | List l'' -> fconcat flatten l'' l'
    end
  end
end

(* result is a list of elements of the form Int _ exclusively *)
val result = flatten (make_list ?int)

(* check returns true iff there is no nested list *)
val rec check l = match l with
| Nil -> true
| Cons p -> match p with
  | (e, l') ->
    let b = match e with
    | Int _ -> true
    | List _ -> false
    end in
    b && check l'
  end
end

(* this should always return true *)
val check_result = check result

val final_result = (result, check_result)
