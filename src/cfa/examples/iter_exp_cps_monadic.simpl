(* Exponential iteration of the identity, using the continuation monad *)
val return x = fun k -> k x
val bind m f = fun k -> m (fun x -> f x k)
val run m = m (fun x -> x)

val compose f g x = f (g x)

(* [iter n f] is the function f that is iterated 2^n times *)
val rec iter n f =
  if n <= 0 then return f
  else
    bind (iter (n-1) f)
    (fun g -> return (compose g g))

(* Iterations of the identity, called on 0 *)
val result = run (iter ?int (fun x -> x)) 0
