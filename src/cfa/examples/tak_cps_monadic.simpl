(* Takeuchi's function
  I. Takeuchi, “On a recursive function that does almost recusion only”,
  Electrical Communica-tion Laboratory, Nippon Telephone and Telegraph Co.,
  Tokyo, Japan (1978)

  The other functions are described in
  Donald E. Knuth,
  "Textbook examples of recursion",
  arXiv cs/9301113 (1991)
  https://arxiv.org/abs/cs/9301113

  Version using the continuation monad
 *)

val return x = fun k -> k x
val bind m f = fun k -> m (fun x -> f x k)
val run m = m (fun x -> x)

val rec tak x y z =
  if x <= y
  then return y
  else
    bind ((tak (x - 1) y z)) (fun x1 ->
      bind (tak (y - 1) z x) (fun y1 ->
        bind (tak (z - 1) x y) (fun z1 ->
          tak x1 y1 z1
        )
      )
    )

val result_tak = run (tak ?int ?int ?int)
