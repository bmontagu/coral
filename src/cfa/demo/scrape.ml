module Config = struct
  let project = "JavaScript demo of CFA"

  let dir =
    Filename.(concat (Sys.getcwd ()) @@ concat parent_dir_name "examples")

  let extension = ".simpl"

  (* exclude from the list the file that demand too many recursion
     levels for the JS interpreter... *)
  let exclude_list =
    [
      "heintze_mcallester_200.simpl";
      "heintze_mcallester_300.simpl";
      "heintze_mcallester_400.simpl";
      "heintze_mcallester_500.simpl";
      "heintze_mcallester_600.simpl";
      "heintze_mcallester_700.simpl";
      "heintze_mcallester_800.simpl";
      "heintze_mcallester_900.simpl";
      "heintze_mcallester_1000.simpl";
    ]

  let value_name = "examples"
  let value_type = `Array
  let out_channel = stdout
end

let () =
  let module Scrap = Scraper.Make (Config) in
  Scrap.run ()
