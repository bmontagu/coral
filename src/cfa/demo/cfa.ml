open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
open JsDemo

let ( >>= ) m f =
  match m with
  | Error _ as err -> err
  | Ok v -> f v

let return x = Ok x

module Options : OPTIONS = struct
  module Examples = struct
    let collection = Examples.examples
    let default = "mc91"
    let label = "Example programs:"
  end

  module Color = struct
    let normal_text = CSS.Color.(Name Black)
    let error_text = CSS.Color.(Name Red)
    let disabled_text = CSS.Color.(Name Gray)
  end

  module Button = struct
    let label = "Analyse!"
    let dom_id = "analyse-button"
    let dom_class = ["analyse-button"]
  end

  module Output = struct
    let label = "Output:"
    let dom_id = "output"
    let dom_class = ["output"]
    let font_size = 16
  end

  module Editor = struct
    let dom_id = "editor"
    let dom_class = ["editor"]
    let tab_size = 2
    let theme = "ace/theme/chrome"
    let mode = "ace/mode/ocaml"
    let font_size = 16
  end

  module OptionsDialog = struct
    type value = CfaLib.Main.Options.t

    let items = []
    let modify_onchange _ = ()

    let analysis, get_analysis, modify_onchange_analysis =
      let open CfaLib.Main.AnalysisKind in
      Utils.make_select
        ~name:"Analysis:"
        ~min
        ~max
        ~of_enum
        ~to_string
        ~default:(to_enum NablaRec)

    let items = analysis :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_analysis x

    let solver_domain, get_solver, modify_onchange_solver =
      let open CfaLib.Main.Solver in
      Utils.make_select
        ~name:"Fixpoint solver:"
        ~min
        ~max
        ~of_enum
        ~to_string
        ~default:(to_enum Priority)

    let items = solver_domain :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_solver x

    let integer_domain, get_integer_domain, modify_onchange_integer_domain =
      let open CfaLib.Main.IntegerDomain in
      Utils.make_select
        ~name:"Integer domain:"
        ~min
        ~max
        ~of_enum
        ~to_string
        ~default:(to_enum Intervals)

    let items = integer_domain :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_integer_domain x

    let call_domain, get_call_domain, modify_onchange_call_domain =
      let min = 0
      and max = 14
      and of_enum =
        let open CfaLib.Main.CallDomain in
        function
        | 0 -> Some (LastCallSites 0)
        | 1 -> Some (LastCallSites 1)
        | 2 -> Some (LastCallSites 2)
        | 3 -> Some (CyclicCallSites (0, Some 0))
        | 4 -> Some (CyclicCallSites (1, Some 0))
        | 5 -> Some (CyclicCallSites (2, Some 0))
        | 6 -> Some (CyclicCallSites (0, Some 1))
        | 7 -> Some (CyclicCallSites (1, Some 1))
        | 8 -> Some (CyclicCallSites (2, Some 1))
        | 9 -> Some (CyclicCallSites (0, Some 2))
        | 10 -> Some (CyclicCallSites (1, Some 2))
        | 11 -> Some (CyclicCallSites (2, Some 2))
        | 12 -> Some (CyclicCallSites (0, None))
        | 13 -> Some (CyclicCallSites (1, None))
        | 14 -> Some (CyclicCallSites (2, None))
        | _ -> None
      and to_string = function
        | CfaLib.Main.CallDomain.LastCallSites n ->
          Format.asprintf "last %i call sites" n
        | CfaLib.Main.CallDomain.CyclicCallSites (n, Some 0) ->
          Format.asprintf "max %i cyclic call sites" n
        | CfaLib.Main.CallDomain.CyclicCallSites (n, Some l) ->
          Format.asprintf "max %i cyclic call sites + %i last call sites" n l
        | CfaLib.Main.CallDomain.CyclicCallSites (n, None) ->
          Format.asprintf "max %i cyclic call sites + dyn last call sites" n
      in
      Utils.make_select
        ~name:"Call sites approximation strategy:"
        ~min
        ~max
        ~of_enum
        ~to_string
        ~default:5

    let items = call_domain :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_call_domain x

    let refine_branches, get_refine_branches, modify_onchange_refine_branches =
      Utils.make_checkbox ~name:"Refine branches" ~checked:true

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_refine_branches x

    let items = refine_branches :: items

    let ( branch_is_context,
          get_branch_is_context,
          modify_onchange_branch_is_context ) =
      Utils.make_checkbox ~name:"Branch is context" ~checked:true

    let items = branch_is_context :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_branch_is_context x

    let let_is_context, get_let_is_context, modify_onchange_let_is_context =
      Utils.make_checkbox ~name:"Let binding is context" ~checked:false

    let items = let_is_context :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_let_is_context x

    let finer_context, get_finer_context, modify_onchange_finer_context =
      Utils.make_checkbox ~name:"Finer contexts" ~checked:false

    let items = finer_context :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_finer_context x

    let no_env_restrict, get_no_env_restrict, modify_onchange_no_env_restrict =
      Utils.make_checkbox ~name:"No environment restriction" ~checked:false

    let items = no_env_restrict :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_no_env_restrict x

    let kill_unreachable, get_kill_unreachable, modify_onchange_kill_unreachable
        =
      Utils.make_checkbox ~name:"Kill unreachable parts" ~checked:true

    let items = kill_unreachable :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_kill_unreachable x

    let ( record_closure_creation_context,
          get_record_closure_creation_context,
          modify_onchange_record_closure_creation_context ) =
      Utils.make_checkbox ~name:"Record closure creation context" ~checked:false

    let items = record_closure_creation_context :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_record_closure_creation_context x

    let ( record_tuple_creation_location,
          get_record_tuple_creation_location,
          modify_onchange_record_tuple_creation_location ) =
      Utils.make_checkbox ~name:"Record tuple creation location" ~checked:false

    let items = record_tuple_creation_location :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_record_tuple_creation_location x

    let ( record_variant_creation_location,
          get_record_variant_creation_location,
          modify_onchange_record_variant_creation_location ) =
      Utils.make_checkbox
        ~name:"Record variant creation location"
        ~checked:false

    let items = record_variant_creation_location :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_record_variant_creation_location x

    let cache_non_calls, get_cache_non_calls, modify_onchange_cache_non_calls =
      Utils.make_checkbox ~name:"Cache non calls" ~checked:false

    let items = cache_non_calls :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_cache_non_calls x

    let ( cache_widened_calls_only,
          get_cache_widened_calls_only,
          modify_onchange_cache_widened_calls_only ) =
      Utils.make_checkbox ~name:"Cache widened calls only" ~checked:false

    let items = cache_widened_calls_only :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_cache_widened_calls_only x

    let no_alarms, get_no_alarms, modify_onchange_no_alarms =
      Utils.make_checkbox ~name:"No alarms" ~checked:false

    let items = no_alarms :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_no_alarms x

    let disprove_alarms, get_disprove_alarms, modify_onchange_disprove_alarms =
      Utils.make_checkbox ~name:"Disprove alarms" ~checked:true

    let items = disprove_alarms :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_disprove_alarms x

    let minimize, get_minimize, modify_onchange_minimize =
      Utils.make_checkbox
        ~name:"Minimize abstract values (∇-rec only)"
        ~checked:true

    let items = minimize :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_minimize x

    let debug, get_debug, modify_onchange_debug =
      Utils.make_checkbox ~name:"Debug" ~checked:false

    let items = debug :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_debug x

    let print_memo_table, get_print_memo_table, modify_onchange_print_memo_table
        =
      Utils.make_checkbox ~name:"Print memo table" ~checked:false

    let items = print_memo_table :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_print_memo_table x

    let ( share_integers_program_points,
          get_share_integers_program_points,
          modify_onchange_share_integers_program_points ) =
      Utils.make_checkbox
        ~name:"Share program points for identical integers"
        ~checked:false

    let items = share_integers_program_points :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_share_integers_program_points x

    let ( share_variables_program_points,
          get_share_variables_program_points,
          modify_onchange_share_variables_program_points ) =
      Utils.make_checkbox
        ~name:"Share program points for identical variables of the same scope"
        ~checked:true

    let items = share_variables_program_points :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_share_variables_program_points x

    let occurs_threshold, get_occurs_threshold, modify_onchange_occurs_threshold
        =
      Utils.make_select
        ~name:"Occurrence threshold (for ∇/∇-rec only):"
        ~min:0
        ~max:3
        ~of_enum:(fun n -> Some n)
        ~to_string:(fun n -> if n = 0 then "0 (disabled)" else string_of_int n)
        ~default:2

    let items = occurs_threshold :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_occurs_threshold x

    let sim_threshold, get_sim_threshold, modify_onchange_sim_threshold =
      Utils.make_select
        ~name:"Similarity threshold (for ∇/∇-rec only):"
        ~min:0
        ~max:6
        ~of_enum:(fun n -> Some n)
        ~to_string:(fun n -> if n = 0 then "0 (disabled)" else string_of_int n)
        ~default:2

    let items = sim_threshold :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_sim_threshold x

    let html_elt =
      let open Html in
      fieldset
        ~legend:(legend [txt "Analyser options:"])
        ~a:[a_id "options"]
        (List.rev items)

    let get_value () =
      let open CfaLib.Main.Options in
      {
        analysis_kind = get_analysis ();
        solver = get_solver ();
        integer_domain = get_integer_domain ();
        call_domain = get_call_domain ();
        refine_branches = get_refine_branches ();
        branch_is_context = get_branch_is_context ();
        let_is_context = get_let_is_context ();
        finer_context = get_finer_context ();
        no_env_restrict = get_no_env_restrict ();
        kill_unreachable = get_kill_unreachable ();
        record_closure_creation_context = get_record_closure_creation_context ();
        record_tuple_creation_location = get_record_tuple_creation_location ();
        record_variant_creation_location =
          get_record_variant_creation_location ();
        cache_non_calls = get_cache_non_calls ();
        cache_widened_calls_only = get_cache_widened_calls_only ();
        no_alarms = get_no_alarms ();
        disprove_alarms = get_disprove_alarms ();
        minimize = get_minimize ();
        debug = get_debug ();
        print_memo_table = get_print_memo_table ();
        share_integers_program_points = get_share_integers_program_points ();
        share_variables_program_points = get_share_variables_program_points ();
        occurs_threshold = get_occurs_threshold ();
        sim_threshold = get_sim_threshold ();
      }

    let set_onchange = modify_onchange
  end

  let loc_to_ace loc =
    let to_loc pos =
      let open Lexing in
      (pos.pos_lnum, pos.pos_cnum - pos.pos_bol)
    in
    {
      Ezjs_ace.Ace.loc_start = to_loc (Location.get_start loc);
      loc_end = to_loc (Location.get_end loc);
    }

  let pp_to_ace p =
    let dummy =
      let p = (-1, -1) in
      Ezjs_ace.Ace.{ loc_start = p; loc_end = p }
    in
    match Lang.Ast.PP.(Map.find_opt p (get_location_map ())) with
    | Some loc -> loc_to_ace loc
    | None -> dummy

  let analyzer options input =
    let module F = struct
      let buf = Buffer.create 1024
      let std_formatter = Format.formatter_of_buffer buf
      let err_formatter = Format.formatter_of_buffer buf
    end in
    let module Log = Logs.Make (F) in
    let open CfaLib.Main in
    let module Analyzer = (val Options.get_analysis_module (module Log) options)
    in
    let run () =
      let open Lang in
      CfaLib.Driver.parse Parser.program "" (`String input) >>= fun prog ->
      Ast.Outer.check prog >>= fun () ->
      let prog =
        let open Options in
        Ast.Term.translate
          ~share_integers:options.share_integers_program_points
          ~share_variables:options.share_variables_program_points
          prog
      in
      return @@ Analyzer.eval prog
    in
    let mres, duration = Time.call run in
    Result.map_error (fun (loc, msg) ->
        let s =
          Log.eprintf "%a@.%s@." Location.pp loc msg;
          Buffer.contents F.buf
        in
        (loc_to_ace loc, s))
    @@ Result.map
         (fun (res, stats) ->
           let msg =
             Log.printf
               "%a@.@.(* Running time: %a *)@.(* Statistics:@.   %a *)@."
               Analyzer.Abs.pp
               res
               Time.pp
               duration
               (Memo.Statistics.pp
                  ~print_memo_table:options.print_memo_table
                  Analyzer.Entry.pp)
               stats;
             Buffer.contents F.buf
           and warnings =
             Analyzer.Abs.get_alarms res
             |> List.map (fun (p, msg) ->
                    let msg =
                      let loc =
                        match
                          Lang.Ast.PP.(Map.find_opt p (get_location_map ()))
                        with
                        | Some loc -> loc
                        | None -> Location.dummy
                      in
                      Format.asprintf "%a@.%s" Location.pp loc msg
                    in
                    (pp_to_ace p, msg))
           in
           (msg, warnings))
         mres
end

let () =
  let module Demo = Make (Options) in
  Demo.setup ()
