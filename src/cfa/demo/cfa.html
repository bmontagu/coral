<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Control-Flow Analyzers</title>
    <link href="style.css" rel="stylesheet" style="text/css">
  </head>
  <body>
    <h1>Control-Flow Analyzers for the λ-calculus</h1>

    <p>
      This web-page embeds prototypes of static Analyzers for the
      untyped λ-calculus, extended with booleans and arithmetic
      operations, as described in the
      paper <a href="https://people.irisa.fr/Benoit.Montagu/papers/pldi2021_trace_based_cfa.pdf">Trace-Based
      Control-Flow Analysis</a>, as well as improvements of these
      analyzers, that have not been published yet.
    </p>
    <p>
      These analyzers are experiments for
      the <a href="https://salto.gitlabpages.inria.fr/">Salto
      project</a>, that aims at developing a static analyzer for OCaml
      programs.
    </p>
    <p>
      <em>Warning</em>: the analyzers can take a very long time to run
    in your browser! Do not be surprised if your browser warns you
    that some script is taking a lot of time.
    </p>

    <p>
      By default, the output text only displays the analysis result
      for <strong>the last declaration</strong> of the program. If
      the <code>Print memo table</code> option is checked, then the
      whole memoization table (and the whole global environment in the
      case of global analyses) is also printed. Note that the
      memoization table only records by default the program points
      that are function calls. It is possible to
      record <emph>every</emph> program point by enabling
      the <code>Cache non calls</code> options.
    </p>

    <script src="ace-src/ace.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="cfa.bc.js"></script>

    <h2>
      Documentation
    </h2>

    <h3>
      Options
    </h3>

    <p>The available analyses are:</p>
    <ul>
      <li>
        <code>nabla</code>: ∇-CFA, a control-flow analysis based on
        widening
      </li>
      <li>
        <code>nabla-rec</code>: ∇rec-CFA, a control-flow analysis
        based on widening, that employs abstract values that might be
        recursive.
      </li>
      <li>
        <code>global</code>: a classic CFA based on a global
        environment (as used in constraint-based CFA)
      </li>
      <li>
        <code>global-independent-attribute</code>: a variation of
        the <code>global</code> analysis, where each function is
        accompanied with only one environment of abstract values. This
        is in contrast with the <code>global</code> analysis, that
        associates each function to a set of possible environments.
        Not described in the paper.
      </li>
    </ul>

    <p>
      The primitive operations <code>?int</code>
      and <code>?bool</code> produce an unknown integer or boolean,
      respectively, in a non-deterministic manner. The following
      integer operations
      are <code>+</code>, <code>-</code>, <code>*</code>, <code>&lt;=</code>, <code>&lt;</code>, <code>&gt;=</code>, <code>&gt;</code>
      and <code>=</code>. The semantics of arithmetic operations is
      that of unbounded integers. The analyzers perform approximations
      when the capacity of an integer might exceed the precision
      provided by your machine or your browser.
    </p>

    <p>
      You are free to choose the abstract domain to represent sets of
      integers:
    </p>
    <ul>
      <li>
        <code>two-points</code>: ⊥ and ⊤.
      </li>
      <li>
        <code>flat</code>: the flat domain of integers, with ⊥ and ⊤.
      </li>
      <li>
        <code>signs</code>: the domain of signs. It has the abstract
        elements: ⊥,
        ⊤, <code>]-∞,0]</code>, <code>]-∞,-1]</code>, <code>{0}</code>, <code>]-∞,-1]∪[1,+∞[</code>, <code>[0,+∞[</code>,
        and <code>[1,+∞[</code>.
      </li>
      <li>
        <code>intervals</code>: the domain of intervals.
      </li>
    </ul>

    <p>
      You are free to choose the following approximations for call sites:
    </p>
    <ul>
      <li>
        <code>last 0 call sites</code>: no distinction of call sites,
        as in 0-CFA.
      </li>
      <li>
        <code>last 1 call sites</code>: only the most recent call site
        is distinguished, as in 1-CFA.
      </li>
      <li>
        <code>last 2 call sites</code>: only the two most recent call
        sites are distinguished, as in 2-CFA.
      </li>
      <li>
        <code>max 0 cyclic call sites</code>: occurrences of call
        sites are limited to 0. This is the same as 0-CFA.
      </li>
      <li>
        <code>max 1 cyclic call sites</code>: occurrences of call
        sites are limited to at most 1.
      </li>
      <li>
        <code>max 2 cyclic call sites</code>: occurrences of call
        sites are limited to at most 2.
      </li>
      <li>
        <code>max 0 cyclic call sites + 1 last call sites</code>:
        occurrences of call sites are limited to 0, and in addition
        the last 1 call site is distinguished.
      </li>
      <li>
        <code>max 1 cyclic call sites + 1 last call sites</code>:
        occurrences of call sites are limited to at most 1, and in
        addition the last 1 call site is distinguished.
      </li>
      <li>
        <code>max 2 cyclic call sites + 1 last call sites</code>:
        occurrences of call sites are limited to at most 2, and in
        addition the last 1 call site is distinguished.
      </li>
      <li>
        <code>max 0 cyclic call sites + 2 last call sites</code>:
        occurrences of call sites are limited to 0, and in addition
        the last 2 call sites are distinguished.
      </li>
      <li>
        <code>max 1 cyclic call sites + 2 last call sites</code>:
        occurrences of call sites are limited to at most 1, and in
        addition the last 2 call sites are distinguished.
      </li>
      <li>
        <code>max 2 cyclic call sites + 2 last call sites</code>:
        occurrences of call sites are limited to at most 2, and in
        addition the last 2 call sites are distinguished.
      </li>
      <li>
        <code>max 0 cyclic call sites + dyn last call sites</code>:
        occurrences of call sites are limited to 0, and in addition
        the number of the last call sites that are distinguished is
        dynamically computed (it is equal to the length of the call
        string where the last repeated call site occurs, starting from
        the end of the call string).
      </li>
      <li>
        <code>max 1 cyclic call sites + dyn last call sites</code>:
        occurrences of call sites are limited to at most 1, and in
        addition the number of the last call sites that are
        distinguished is dynamically computed (it is equal to the
        length of the call string where the last repeated call site
        occurs, starting from the end of the call string).
      </li>
      <li>
        <code>max 2 cyclic call sites + dyn last call sites</code>:
        occurrences of call sites are limited to at most 2, and in
        addition the number of the last call sites that are
        distinguished is dynamically computed (it is equal to the
        length of the call string where the last repeated call site
        occurs, starting from the end of the call string).
      </li>
    </ul>

    <p>
      The following options are available:
    </p>
    <ul>
      <li>
        <code>Refine branches</code>: refine the knowledge in the
        branches of a test.
      </li>
      <li>
        <code>Branch is context</code>: consider branches (<code>if
        ... then ... else ...</code>) as a call site for the two
        branches.
      </li>
      <li>
        <code>Let binding is context</code>:
        consider <code>let</code>-binding (<code>let ... in
        ...</code>) as a call site for the body of
        the <code>let</code>.
      </li>
      <li>
        <code>Finer contexts</code>: record in calling contexts which
        functions are called, and in branching contexts which branches
        are taken.
      </li>
      <li>
        <code>No environment restriction</code>: do not restrict the
        environment to the free variables of the program that is being
        analyzed.
      </li>
      <li>
        <code>Kill unreachable parts</code>: do not analyze parts of
        the code that are not reachable.
      </li>
      <li>
        <code>Record closure creation context</code>: Record in
         abstract values for closures the calling context in which the
         closure was created. WARNING: this option can dramatically
         increase the computation time, especially for the nabla and
         nabla-rec analyses.
      </li>
      <li>
        <code>Record tuple creation location</code>: Record in
         abstract values for tuples the program point at which the
         tuple was created. This option is only available for the
         nabla and nabla-rec analyses. WARNING: this option can
         dramatically increase the computation time.
      </li>
      <li>
        <code>Record variant creation location</code>: Record in
         abstract values for variants the program point at which the
         variant was created. This option is only available for the
         nabla and nabla-rec analyses. WARNING: this option can
         dramatically increase the computation time.
      </li>
      <li>
        <code>Cache non calls</code>: Record as well program points
        that are not calls. With this flag on, you will reduce the
        cached graph by a large amount, which can save space and time.
      </li>
      <li>
        <code>Cache widened calls only</code>: among the calls, record
        only the widened calls in the cache (as opposed to all the
        calls). Without this flag off, you get a proper notion of
        abstract call graph. With this flag on, you might reduce the
        cached graph by a large amount, which can save space and time.
      </li>
      <li>
        <code>No alarms</code>: disable the inference of alarms by the
        analyzer.
      </li>
      <li>
        <code>Disprove alarms</code>: Try to disprove safety alarms by
         exploiting a backward analysis (for nabla/nabla-rec analyses
         only).
      </li>
      <li>
        <code>Minimize abstract values</code>: minimize the
        representation of abstract values produced by the ∇-rec
        analyzer. This produces more readable results.
      </li>
      <li>
        <code>Debug</code>: print some statistics about the analyses.
      </li>
      <li>
        <code>Print memo table</code>: print the final contents of the
        table that is used by the fixpoint solvers, that associate to
        sub-terms of the program and their contexts the abstract value
        that describes an over-approximated set of the values those
        sub-terms might produce.
      </li>
      <li>
        <code>Share program points for identical integers</code>:
        helps reduce the number of states to explore.
      </li>
      <li>
        <code>Share program points for identical variables of the same
        scope</code>: helps reduce the number of states to explore.
      </li>
      <li>
        <code>Occurrence threshold</code>: value that permits to delay
        the use of an aggressive widening, by permitting a maximum
        number of repetitions of some node along a path in abstract
        values.
      </li>
      <li>
        <code>Similarity threshold</code>: value that permits to use
        several incomparable environments. This improves the accuracy
        of the widening on inputs at the cost of introducing some
        disjunctions.
      </li>
      <li>
        <code>Use legacy shrinking fallback</code>: use an alternative
        heuristic for widening in ∇-rec, that was originally
        implemented.
      </li>
    </ul>

    <h3>
      Syntax of programs
    </h3>

    <p>
      Functions are the only supported recursive values in programs.
      Mutually recursive definitions are currently <em>not</em>
      supported.
    </p>

    <p>
      The syntax of programs is defined by the following EBNF grammar:
    </p>
    <pre>
program ::=                                                     (Program)
          | decl+

decl ::=                                                        (Declaration)
       | &quot;val&quot; &quot;rec&quot;? x x* &quot;=&quot; expr                             (Value declaration)

expr ::=                                                        (Expressions)
       | x                                                      (Variable)
       | i                                                      (Integer)
       | &quot;?int&quot;                                                 (Unknown integer)
       | expr binop expr                                        (Binary operation)
       | unop expr                                              (Unary operation)
       | &quot;true&quot;                                                 (True boolean)
       | &quot;false&quot;                                                (False boolean)
       | &quot;?bool&quot;                                                (Unknown integer)
       | &quot;fun&quot; x+ &quot;-&gt;&quot; expr                                     (Anonymous function)
       | expr expr                                              (Application)
       | &quot;if&quot; expr &quot;then&quot; expr &quot;else&quot; expr                      (Branching)
       | &quot;let&quot; &quot;rec&quot;? x x* &quot;=&quot; expr &quot;in&quot; expr                   (Let definition)
       | &quot;(&quot; expr &quot;)&quot;                                           (Parenthesized expression)
       | &quot;(&quot; expr (&quot;,&quot; expr)+ &quot;)&quot;                               (Tuple)
       | cons                                                   (Variant constructor with no argument)
       | cons expr                                              (Variant constructor with an argument)
       | &quot;match&quot; expr &quot;with&quot; &quot;|&quot;? branch (&quot;|&quot; branch)* &quot;end&quot;    (Pattern matching)

branch ::=                                                      (Pattern matching branch)
         | pat &quot;->&quot; expr

pat ::=                                                         (Pattern)
      | simple_pat                                              (Simple pattern)
      | &quot;()&quot;                                                    (Unit pattern)
      | &quot;(&quot; simpl_pat (&quot;,&quot; simpl_pat)+ &quot;)&quot;                      (Tuple pattern)
      | cons                                                    (Constructor pattern with no argument)
      | cons simpl_pat                                          (Constructor pattern with an argument)
      | &quot;?int&quot; (&quot;as&quot; simpl_pat)?                                (Pattern of integer type)
      | &quot;?bool&quot; (&quot;as&quot; simpl_pat)?                               (Pattern of boolean type)

simpl_pat ::=                                                   (Simple pattern)
            | &quot;_&quot;                                               (Wildcard)
            | x                                                 (Variable)

binop ::=                                                       (Binary operators)
        | &quot;+&quot;                                                   (Addition)
        | &quot;-&quot;                                                   (Subtraction)
        | &quot;*&quot;                                                   (Multiplication)
        | &quot;&amp;&amp;&quot;                                                  (Boolean conjunction)
        | &quot;||&quot;                                                  (Boolean disjunction)
        | &quot;&lt;&quot;                                                   (Lesser than integer test)
        | &quot;&lt;=&quot;                                                  (Lesser than or equal integer test)
        | &quot;&gt;&quot;                                                   (Greater than integer test)
        | &quot;&gt;=&quot;                                                  (Greater than or equal integer test)
        | &quot;=&quot;                                                   (Equality integer test)
        | &quot;&lt;&gt;&quot;                                                  (Inequality integer test)

unop ::=                                                        (Unary operators)
       | &quot;-&quot;                                                    (Negation)

x ::=                                                           (Variables)
    | low_alpha (alpha | digit | symb)*

cons ::=                                                        (Variant constructors)
    | up_alpha (alpha | digit | symb)*

i ::=                                                           (Integers)
    | digit+

up_alpha ::=                                                    (Uppercase alphabetic symbols)
        | [&apos;A&apos;-&apos;Z&apos;]
low_alpha ::=                                                   (Lowercase alphabetic symbols)
        | [&apos;a&apos;-&apos;z&apos;]
alpha ::=                                                       (Alphabetic symbols)
        | [&apos;a&apos;-&apos;z&apos;] | [&apos;A&apos;-&apos;Z&apos;]

digit ::=                                                       (Digit)
      | [&apos;0&apos;-&apos;9&apos;]

symb ::=                                                        (Symbol)
      | &quot;_&quot;
      | &quot;&apos;&quot;
    </pre>

    <h3>
      Syntax of outputs
    </h3>

    <p>
      The results are of the form <samp>{ bools = <var>B</var> ints
        = <var>I</var> closures = <var>C</var> tuples = <var>T</var>
        variants = <var>V</var> }</samp>, where:
    </p>
    <ul>
      <li>
        <var>B</var> is the set of possible booleans.
      </li>
      <li>
        <var>I</var> is the set of possible integers.
      </li>
      <li>
        <var>C</var> is the set of possible closures, that are
        composed of the piece of code of the closure and an abstract
        environment.
        <ul>
          <li>
            For the λ-abstractions in closures, we display their
            code, and the location at which they occur in the
            program text.
            <br>
            For example, <samp>λ[42:3] x -> x</samp> is the identify
            function that occurs at line 42, column 3 of the program
            text.
          </li>
          <li>
            The environments in closures differ, depending on the
            analysis and on the context sensitivity.
            The <code>global</code>
            and <code>global-independent-attribute</code> refer to
            pairs of a program point and an abstract call string.
            <br>
            For example, the <code>global</code> analysis may return
            the abstract closure <samp>(λ[42:3] x -> y) {[y -> 6:8 @
            23:9], [y -> 6:8 @ 23:9 @ 10:4 # *]}</samp>. It is a
            closure with two possible environments, that give an
            abstract value to the local variable <samp>y</samp>. The
            first possible value is the instance of the binding
            variable at line 6 column 8 in the calling context
            composed of the single application site located at line
            23 column 9. The second possible abstract value
            for <samp>y</samp> is the instance of the same binder for
            the call sites that is composed of at least two calls, and
            starting with a call at line 23 column 9, then a call at
            line 10 column 4.
          </li>
        </ul>
      </li>
      <li><var>T</var> is the set of possible tuples, and are
      classified by their width. For example, <samp>{ 0: (); 3: { ints
      = [0,6] } × { bools = {true} } × ints = ⊤ }</samp> denotes a
      tuple whose width is either 0 (and thus accepts no further
      argument) or 3, and whose first, second, and third arguments are
      an integer between 0 and 6, the <samp>true</samp> boolean, and
      any integer, respectively.
      </li>
      <li><var>V</var> is the set of possible variants, and are
      classified by their head constructor. For example, <samp>{ A: {
      ints = ⊤ } B: . C: { bools = {false} } }</samp> denotes either a
      variant <samp>A</samp> with an integer argument, or a
      variant <samp>B</samp> with no argument (as indicated by the
      dot), or a variant <samp>C</samp> whose argument is the
      boolean <samp>false</samp>.
      </li>
    </ul>
    <p>
      The symbols ⊥ or ∅ always indicates the empty set. The symbol ⊤
      indicates the full set.
    </p>
    <p>
      For the <code>nabla-rec</code> analysis, the inferred abstract
      values may contain cycles, thus denoting inductively defined
      sets of values. For example, the abstract value <samp>v = {
      variants = { Nil: . Cons: { tuples = {2: { ints = ⊤ } × &apos;a
      } } } } as &apos;a</samp> denotes lists of integers,
      where <samp>Nil</samp> is the constructor for the empty list,
      and <samp>Cons</samp> is the constructor for creating a
      non-empty list. The abstract value <samp>v</samp> is such
      that <samp>v = { variants = { Nil: . Cons: { tuples = {2: { ints
      = ⊤ } × v } } } }</samp>. The syntax for cyclic abstract values
      follows the style that is used in OCaml to display
      equi-recursive types.
    </p>
    <p>
      For the <code>global</code>
      and <code>global-independent-attribute</code> analyses, parts of
      the <em>global environment</em> and of the <em>global store</em>
      are also printed. The printed parts are reduced to the slices of
      the global environment and of the global store that are strictly
      necessary to interpret the abstract value for the
      output, <em>i.e.</em>, the parts that resolve the <em>name
      indirections</em> used by the analyses.
    </p>
    <p>
      The output also contains some statistics about this run of the analyzer:
    </p>
    <ul>
      <li>
        <code>Running time</code>: how much time it took for the
        analyzer to complete.
      </li>
      <li>
        <code>States</code>: how many states were explored in the
        abstract call graph.
      </li>
      <li>
        <code>Edges</code>: how many distinct edges were taken in
        the exploration of the abstract call graph.
      </li>
      <li>
        <code>Max fanout</code>: the maximal fanout degree of the
        abstract call graph. This gives an indication of the size of
        the disjunctions in the abstract domains. The median fanout is
        also displayed, enclosed in parentheses.
      </li>
      <li>
        <code>Iterations</code>: how many iterations were necessary
        to reach a fixpoint.
      </li>
      <li>
        <code>Calls</code>: how many times the functional that
        performs the analysis of sub-terms has been called.
      </li>
    </ul>

    <hr>

    <p>
      Author: <a href="https://people.irisa.fr/Benoit.Montagu/">Benoît
        Montagu</a>
      <br/>
      Copyright © <a href="https://www.inria.fr">Inria</a> 2020-2023
      <br/>
      Powered by <a href="https://ocaml.org/">OCaml</a>,
      <a href="https://ocsigen.org/js_of_ocaml/dev/manual/overview">Js_of_ocaml</a>
      and the <a href="https://ace.c9.io/">ACE code editor</a>.
    </p>

  </body>
</html>

<!--  LocalWords:  untyped booleans Salto OCaml memoization CFA nabla
 -->
<!--  LocalWords:  boolean fixpoint disjunctions EBNF decl expr binop
 -->
<!--  LocalWords:  unop Unary bool simpl disjunction symb bools ints
 -->
<!--  LocalWords:  equi indirections fanout Benoît Montagu Inria Js
 -->
<!--  LocalWords:  ocaml
 -->
