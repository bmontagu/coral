(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

open Cmdliner

let analysis_kind_t =
  let analyses_with_doc =
    let open CfaLib.Main.AnalysisKind in
    [
      (Nabla, "nabla", "Analyzer with widening, implements the ∇-CFA analysis.");
      ( NablaRec,
        "nabla-rec",
        "Analyzer with widening, implements the ∇-CFA analysis with recursive \
         abstract values." );
      ( Global,
        "global",
        "Global analyzer, naive implementation of a classical CFA." );
      ( GlobalIndependentAttribute,
        "global-independent-attribute",
        "Global analyzer with independent attribute abstraction of contexts, \
         same as --global but only one environment is associated to a closure. \
         The bindings in the environment contain sets of abstract values." );
    ]
  in
  let analyses = List.map (fun (a, s, _) -> (s, a)) analyses_with_doc
  and doc =
    let open Format in
    asprintf
      "Analysis that should be performed. The possible analyses are: %a. %a"
      (pp_print_list
         ~pp_sep:(fun fmt () -> pp_print_string fmt ", ")
         (fun fmt (_a, s, _d) -> fprintf fmt "$(b,%s)" s))
      analyses_with_doc
      (pp_print_list
         ~pp_sep:(fun fmt () -> pp_print_string fmt " ")
         (fun fmt (_a, s, d) -> fprintf fmt "$(b,%s): %s" s d))
      analyses_with_doc
  in
  let open Arg in
  required & opt (some @@ enum analyses) None & info ~doc ["analysis"]

let solver_t =
  let solvers_with_doc =
    let open CfaLib.Main.Solver in
    [
      ( Naive,
        "Naive solver, that performs iterations until every value is stable, \
         always restarting from the root equation. Inefficient, yet simple." );
      ( TopDownLegacy,
        "A variant of the \"top-down\" solver. It uses a dependency graph to \
         avoid recomputing some equations. The detection of widening point is \
         dynamic, following a naive heuristic." );
      ( TopDown,
        "A variant of the \"top-down\" solver. It uses a dependency graph to \
         avoid recomputing some equations. The detection of widening point is \
         dynamic, following a heuristic based on which computation is \
         currently \"on the stack\", i.e., pending." );
      ( Priority,
        "Fixpoint solver that uses a dependency graph to avoid recomputing \
         some equations. It uses priorities to detect possible widening \
         points, and to decide which computation to schedule next." );
    ]
  in
  let doc =
    let open Format in
    asprintf
      "Solver that should be used by the analyses. The possible solvers are: \
       %a. %a"
      (pp_print_list
         ~pp_sep:(fun fmt () -> pp_print_string fmt ", ")
         (fun fmt (s, _d) -> fprintf fmt "$(b,%a)" CfaLib.Main.Solver.printer s))
      solvers_with_doc
      (pp_print_list
         ~pp_sep:(fun fmt () -> pp_print_string fmt " ")
         (fun fmt (s, d) ->
           fprintf fmt "$(b,%a): %s" CfaLib.Main.Solver.printer s d))
      solvers_with_doc
  in
  let open CfaLib.Main in
  let open Arg in
  value
  & opt (conv Solver.(parser, printer)) Solver.default
  & info ["solver"] ~docv:"SOLVER" ~doc

let integer_domain_t =
  let open Arg in
  let open CfaLib.Main in
  value
  & opt (conv IntegerDomain.(parser, printer)) IntegerDomain.default
  & info
      ["integer-domain"]
      ~docv:"INTEGER_DOMAIN"
      ~doc:
        ("The abstract domain to interpret sets of integers. Possible values: "
        ^ String.concat ", " IntegerDomain.examples
        ^ ".")

let call_domain_t =
  let open Arg in
  let open CfaLib.Main in
  value
  & opt (conv CallDomain.(parser, printer)) CallDomain.default
  & info
      ["call-domain"]
      ~docv:"CALL_DOMAIN"
      ~doc:
        ("The abstract domain to interpret calling contexts. Possible values: "
        ^ String.concat ", " CallDomain.examples
        ^ ".")

let refine_branches_t =
  let open Arg in
  value
  & flag
  & info
      ["refine-branches"]
      ~doc:
        "Refine the branches of if/then/else by performing a local backward \
         analysis."

let branch_is_context_t =
  let open Arg in
  value
  & flag
  & info
      ["branch-is-context"]
      ~doc:"Consider branching as creating a new context."

let let_is_context_t =
  let open Arg in
  value
  & flag
  & info
      ["let-is-context"]
      ~doc:"Consider let binding as creating a new context."

let finer_context_t =
  let open Arg in
  value
  & flag
  & info
      ["finer-context"]
      ~doc:
        "Record in calling contexts which functions are called, and in \
         branching contexts which branches are taken."

let no_env_restrict_t =
  let open Arg in
  value
  & flag
  & info
      ["no-env-restrict"]
      ~doc:
        "Do no restrict environments to only the free variables of a subterm \
         during its analysis (makes the analyses slower, because it hinders \
         some context sharing and memoization)."

let kill_unreachable_t =
  let open Arg in
  value
  & flag
  & info
      ["kill-unreachable"]
      ~doc:"Kill the analysis of unreachable parts of the code."

let record_closure_creation_context_t =
  let open Arg in
  value
  & flag
  & info
      ["record-closure-creation-context"]
      ~doc:
        "Record in abstract values for closures the calling context in which \
         the closure was created. WARNING: this option can dramatically \
         increase the computation time, especially for the nabla and nabla-rec \
         analyses."

let record_tuple_creation_location_t =
  let open Arg in
  value
  & flag
  & info
      ["record-tuple-creation-location"]
      ~doc:
        "Record in abstract values for tuples the program point at which the \
         tuple was created. This option is only available for the nabla and \
         nabla-rec analyses. WARNING: this option can dramatically increase \
         the computation time."

let record_variant_creation_location_t =
  let open Arg in
  value
  & flag
  & info
      ["record-variant-creation-location"]
      ~doc:
        "Record in abstract values for variants the program point at which the \
         variant was created. This option is only available for the nabla and \
         nabla-rec analyses. WARNING: this option can dramatically increase \
         the computation time."

let cache_non_calls_t =
  let open Arg in
  value
  & flag
  & info
      ["cache-non-calls"]
      ~doc:
        "Record as well program points that are not calls. With this flag on, \
         you will reduce the cached graph by a large amount, which can save \
         space and time."

let cache_widened_calls_only_t =
  let open Arg in
  value
  & flag
  & info
      ["cache-widened-calls-only"]
      ~doc:
        "Among the calls, record only the widened calls in the cache (as \
         opposed to all the calls). Without this flag off, you get a proper \
         notion of abstract call graph. With this flag on, you might reduce \
         the cached graph by a large amount, which can save space and time."

let no_alarms_t =
  let open Arg in
  value
  & flag
  & info ["no-alarms"] ~doc:"Disable the inference of alarms by the analyser"

let disprove_alarms_t =
  let open Arg in
  value
  & flag
  & info
      ["disprove-alarms"]
      ~doc:
        "Try to disprove safety alarms by exploiting a backward analysis (for \
         nabla/nabla-rec analyses only)."

let minimize_t =
  let open Arg in
  value
  & vflag
      true
      [
        ( true,
          info
            ["minimize"]
            ~doc:
              "Enables the minimization of abstract values (for nabla-rec \
               analysis only)." );
        ( false,
          info
            ["no-minimize"]
            ~doc:
              "Disables the minimization of abstract values (for nabla-rec \
               analysis only)." );
      ]

let debug_t =
  let open Arg in
  value
  & flag
  & info
      ["debug"]
      ~doc:"Output debug information about fixpoint iterations on stderr."

let print_memo_table_t =
  let open Arg in
  value
  & flag
  & info
      ["print-memo-table"]
      ~doc:
        "Print the whole memoization table, that was used during the \
         computation."

let share_integers_program_points_t =
  let open Arg in
  value
  & flag
  & info
      ["share-integers-program-points"]
      ~doc:"Do share program points for identical integers."

let share_variables_program_points_t =
  let open Arg in
  value
  & flag
  & info
      ["share-variables-program-points"]
      ~doc:
        "Do share program points for identical variables from the same scope."

let occurs_threshold_t =
  let open Arg in
  value
  & opt int 0
  & info
      ["occurs-threshold"]
      ~docv:"N"
      ~doc:
        "Minimum number of constructor/tuple/function occurrence along paths \
         before widening is triggered (for nabla/nabla-rec only)."

let sim_threshold_t =
  let open Arg in
  value
  & opt int 0
  & info
      ["sim-threshold"]
      ~docv:"N"
      ~doc:
        "Similarity threshold above which merging of cases should happen (for \
         nabla/nabla-rec only)."

let ( let+ ) v f =
  let open Term in
  const f $ v

let ( and+ ) v1 v2 =
  let open Term in
  const (fun x y -> (x, y)) $ v1 $ v2

let options_t : CfaLib.Main.Options.t Term.t =
  let+ refine_branches = refine_branches_t
  and+ branch_is_context = branch_is_context_t
  and+ let_is_context = let_is_context_t
  and+ finer_context = finer_context_t
  and+ no_env_restrict = no_env_restrict_t
  and+ kill_unreachable = kill_unreachable_t
  and+ record_closure_creation_context = record_closure_creation_context_t
  and+ record_tuple_creation_location = record_tuple_creation_location_t
  and+ record_variant_creation_location = record_variant_creation_location_t
  and+ cache_non_calls = cache_non_calls_t
  and+ cache_widened_calls_only = cache_widened_calls_only_t
  and+ no_alarms = no_alarms_t
  and+ disprove_alarms = disprove_alarms_t
  and+ minimize = minimize_t
  and+ debug = debug_t
  and+ print_memo_table = print_memo_table_t
  and+ integer_domain = integer_domain_t
  and+ call_domain = call_domain_t
  and+ analysis_kind = analysis_kind_t
  and+ solver = solver_t
  and+ share_integers_program_points = share_integers_program_points_t
  and+ share_variables_program_points = share_variables_program_points_t
  and+ occurs_threshold = occurs_threshold_t
  and+ sim_threshold = sim_threshold_t in
  {
    CfaLib.Main.Options.refine_branches;
    branch_is_context;
    let_is_context;
    finer_context;
    no_env_restrict;
    kill_unreachable;
    record_closure_creation_context;
    record_tuple_creation_location;
    record_variant_creation_location;
    cache_non_calls;
    cache_widened_calls_only;
    no_alarms;
    disprove_alarms;
    minimize;
    debug;
    print_memo_table;
    integer_domain;
    call_domain;
    analysis_kind;
    solver;
    share_integers_program_points;
    share_variables_program_points;
    occurs_threshold;
    sim_threshold;
  }

let file_t =
  let open Arg in
  required
  & pos 0 (some string) None
  & info [] ~docv:"FILE" ~doc:"File to analyse"

let cfa_t =
  let open Term in
  const CfaLib.Main.cfa_print $ options_t $ file_t

let cfa_info =
  let open Cmd in
  info "cfa" ~doc:"Control flow analysis for a simple functional language."

let () = exit @@ Cmd.(eval @@ v cfa_info cfa_t)
