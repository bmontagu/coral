(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

(** Variables *)
module Var : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
  val of_string : string -> t
  val hash_fold : t -> Hash.state -> Hash.state
end = struct
  type t = string

  let compare = String.compare
  let of_string s = s
  let pp = Format.pp_print_string

  let hash_fold s state =
    String.fold_left
      (fun state c -> Hash.fold (Char.code c) state)
      (Hash.fold 0 state)
      s
end

(** Variant constructors *)
module Cons : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
  val of_string : string -> t
  val hash_fold : t -> Hash.state -> Hash.state
end =
  Var

(** Environments, i.e. maps from variables to some values *)
module Env = struct
  include Map.Make (Var)

  let pp pp_elt fmt env =
    if is_empty env
    then Format.fprintf fmt "[]"
    else
      Format.fprintf
        fmt
        "@[[@[<hv 1> %a@]@ ]@]"
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ")
           (fun fmt (x, a) ->
             Format.fprintf fmt "@[<hv 2>%a ->@ %a@]" Var.pp x pp_elt a))
        (bindings env)
end

(** Sets of variables *)
module Vars : sig
  type t

  val empty : t
  val mem : Var.t -> t -> bool
  val singleton : Var.t -> t
  val add : Var.t -> t -> t
  val union : t -> t -> t
  val remove : Var.t -> t -> t
  val diff : t -> t -> t
  val pp : Format.formatter -> t -> unit
  val restrict_env : t -> 'a Env.t -> 'a Env.t
end = struct
  type t = unit Env.t

  let empty = Env.empty
  let mem = Env.mem
  let singleton x = Env.singleton x ()
  let add x vars = Env.add x () vars
  let union m1 m2 = Env.union (fun _ () () -> Some ()) m1 m2
  let remove = Env.remove

  let diff m1 m2 =
    Env.merge
      (fun _ o1 o2 ->
        match (o1, o2) with
        | Some (), Some () -> None
        | Some (), None -> Some ()
        | None, _ -> None)
      m1
      m2

  let pp fmt vars =
    let open Format in
    pp_print_list
      ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
      (fun fmt (x, ()) -> Var.pp fmt x)
      fmt
      (Env.bindings vars)

  let restrict_env fvs m =
    Env.merge
      (fun _x o ov ->
        match (o, ov) with
        | Some (), Some v -> Some v
        | None, Some _ -> None
        | Some (), None -> assert false
        | None, None -> None)
      fvs
      m
end

(** Binary operators *)
type binop =
  | PLUS
  | MINUS
  | TIMES
  | LE
  | LT
  | GE
  | GT
  | EQ
  | NEQ
[@@deriving ord]

let pp_binop fmt = function
  | PLUS -> Format.pp_print_string fmt "+"
  | MINUS -> Format.pp_print_string fmt "-"
  | TIMES -> Format.pp_print_string fmt "*"
  | LE -> Format.pp_print_string fmt "<="
  | LT -> Format.pp_print_string fmt "<"
  | GE -> Format.pp_print_string fmt ">="
  | GT -> Format.pp_print_string fmt ">"
  | EQ -> Format.pp_print_string fmt "="
  | NEQ -> Format.pp_print_string fmt "<>"

(** Unary operators *)
type unop = NEG [@@deriving ord]

let pp_unop fmt = function
  | NEG -> Format.pp_print_string fmt "-"

type basic_pattern =
  | Any
  | Bool
  | Int

(** Outer syntax *)
module Outer = struct
  type fun_arg = Var.t Location.located

  type parens_style =
    | Parentheses
    | BeginEnd

  type simple_pattern = basic_pattern * fun_arg option

  type pattern =
    | PVar of simple_pattern
    | PTuple of int * fun_arg option list
    | PVariant of Cons.t * fun_arg option option

  type expr = expr_ Location.located
  (** Expressions *)

  and expr_ =
    | Var of Var.t
    | Let of fun_arg * expr * expr
    | App of expr * expr
    | Fun of fun_arg * expr
    | FixFun of fun_arg * fun_arg * expr
    | Parens of parens_style * expr
    | UnknownInt
    | UnknownBool
    | Bool of bool
    | BOrElse of expr * expr
    | BAndAlso of expr * expr
    | IfThenElse of expr * expr * expr
    | Int of int
    | Binop of binop * expr * expr
    | Unop of unop * expr
    | Tuple of int * expr list
    | Variant of Cons.t * expr option
    | Match of expr * branch list

  and branch = pattern * expr

  (** Declarations *)
  type decl =
    | DeclVal of Var.t Location.located * expr
    | DeclFun of Var.t Location.located * fun_arg list Location.located * expr
    | DeclRecFun of
        Var.t Location.located * fun_arg list Location.located * expr

  type program = decl list
  (** Programs *)

  let pp_fun_arg fmt { Location.contents = x; _ } = Var.pp fmt x

  let pp_opt_fun_arg fmt = function
    | None -> Format.pp_print_char fmt '_'
    | Some x -> pp_fun_arg fmt x

  let pp_simple_pattern fmt = function
    | Any, None -> Format.pp_print_char fmt '_'
    | Any, Some x -> pp_fun_arg fmt x
    | Int, None -> Format.pp_print_string fmt "?int"
    | Int, Some x -> Format.fprintf fmt "?int as %a" pp_fun_arg x
    | Bool, None -> Format.pp_print_string fmt "?bool"
    | Bool, Some x -> Format.fprintf fmt "?bool as %a" pp_fun_arg x

  let pp_pattern fmt = function
    | PVar ox -> pp_simple_pattern fmt ox
    | PTuple (_n, []) -> Format.pp_print_string fmt "()"
    | PTuple (_n, l) ->
      let open Format in
      fprintf
        fmt
        "@[<hv 0>(@[<hv 1> %a@]@ )@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_opt_fun_arg)
        l
    | PVariant (c, None) -> Cons.pp fmt c
    | PVariant (c, Some ox) ->
      Format.fprintf fmt "@[%a@ %a@]" Cons.pp c pp_opt_fun_arg ox

  let rec pp_expr fmt loc_expr = pp_expr_ fmt loc_expr.Location.contents

  and pp_expr_ fmt =
    let open Format in
    function
    | Var x -> Var.pp fmt x
    | Let (x, e1, e2) ->
      fprintf fmt "@[let %a =@ %a@ in@ %a@]" pp_fun_arg x pp_expr e1 pp_expr e2
    | App (e1, e2) -> fprintf fmt "@[%a@ %a@]" pp_expr e1 pp_expr e2
    | Parens (Parentheses, e) -> fprintf fmt "@[(%a)@]" pp_expr e
    | Fun (fun_arg, e) ->
      fprintf fmt "@[<hv>fun@ %a ->@ %a@]" pp_fun_arg fun_arg pp_expr e
    | FixFun (f, fun_arg, e) ->
      fprintf
        fmt
        "@[<hv>fixfun@ %a %a@ ->@ %a@]"
        pp_fun_arg
        f
        pp_fun_arg
        fun_arg
        pp_expr
        e
    | Parens (BeginEnd, e) -> fprintf fmt "@[begin@ %a@ end@]" pp_expr e
    | UnknownInt -> fprintf fmt "?int"
    | UnknownBool -> fprintf fmt "?bool"
    | Bool b -> if b then fprintf fmt "true" else fprintf fmt "false"
    | BOrElse (e1, e2) -> fprintf fmt "@[(%a)@ ||@ (%a)@]" pp_expr e1 pp_expr e2
    | BAndAlso (e1, e2) ->
      fprintf fmt "@[(%a)@ &&@ (%a)@]" pp_expr e1 pp_expr e2
    | IfThenElse (e1, e2, e3) ->
      fprintf
        fmt
        "@[if@ %a@ then@ %a@ else@ %a@]"
        pp_expr
        e1
        pp_expr
        e2
        pp_expr
        e3
    | Int n -> pp_print_int fmt n
    | Binop (op, e1, e2) ->
      fprintf fmt "@[(%a)@ %a@ (%a)@]" pp_expr e1 pp_binop op pp_expr e2
    | Unop (op, e) -> fprintf fmt "@[%a@ (%a)@]" pp_unop op pp_expr e
    | Tuple (_n, es) ->
      fprintf
        fmt
        "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_expr)
        es
    | Variant (c, None) -> Cons.pp fmt c
    | Variant (c, Some e) -> fprintf fmt "@[%a@ %a@]" Cons.pp c pp_expr e
    | Match (e, bs) ->
      fprintf
        fmt
        "@[match@ @[%a@]@ with@ @[%a@]@ end@]"
        pp_expr
        e
        pp_branches
        bs

  and pp_branches fmt bs =
    let open Format in
    pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_branch fmt bs

  and pp_branch fmt (pat, e) =
    Format.fprintf fmt "@[| @[%a@]@ ->@ @[%a@]@]" pp_pattern pat pp_expr e

  let pp_decl fmt =
    let open Format in
    function
    | DeclVal (x, e) ->
      fprintf fmt "@[let@ %a@ =@ %a@]" Var.pp x.Location.contents pp_expr e
    | DeclFun (x, { Location.contents = args; _ }, e) ->
      fprintf
        fmt
        "@[let@ %a@ @[(%a)@]@ =@ %a@]"
        Var.pp
        x.Location.contents
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_fun_arg)
        args
        pp_expr
        e
    | DeclRecFun (x, { Location.contents = args; _ }, e) ->
      fprintf
        fmt
        "@[let rec@ %a@ @[(%a)@]@ =@ %a@]"
        Var.pp
        x.Location.contents
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_fun_arg)
        args
        pp_expr
        e

  let pp_program fmt decls =
    let open Format in
    fprintf
      fmt
      "@[<v>%a@]"
      (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@.@.") pp_decl)
      decls

  exception NonLinearVarInPattern of Location.t * Var.t

  let check_simple_pattern acc = function
    | None -> acc
    | Some { Location.contents = x; loc } ->
      if Vars.mem x acc
      then raise (NonLinearVarInPattern (loc, x))
      else Vars.add x acc

  let check_pattern acc = function
    | PVariant (_, None) -> acc
    | PVar (_, ox) | PVariant (_, Some ox) -> check_simple_pattern acc ox
    | PTuple (n, l) ->
      assert (n = List.length l);
      List.fold_left check_simple_pattern acc l

  exception Unbound of Location.t * Var.t

  (** Checks that all variables of a term are bound *)
  let rec check_term vars { Location.contents = t; loc } =
    match t with
    | Var x -> if not @@ Vars.mem x vars then raise @@ Unbound (loc, x)
    | UnknownInt | UnknownBool | Bool _ | Int _ -> ()
    | Let (x, t1, t2) ->
      check_term vars t1;
      check_term (Vars.add x.contents vars) t2
    | App (t1, t2) | BOrElse (t1, t2) | BAndAlso (t1, t2) | Binop (_, t1, t2) ->
      check_term vars t1;
      check_term vars t2
    | Fun (x, t) -> check_term (Vars.add x.contents vars) t
    | FixFun (f, x, t) ->
      check_term (Vars.add f.contents @@ Vars.add x.contents vars) t
    | Parens (_, t) | Unop (_, t) -> check_term vars t
    | IfThenElse (t1, t2, t3) ->
      check_term vars t1;
      check_term vars t2;
      check_term vars t3
    | Tuple (_n, ts) -> List.iter (check_term vars) ts
    | Variant (_c, None) -> ()
    | Variant (_c, Some t) -> check_term vars t
    | Match (e, bs) ->
      check_term vars e;
      check_branches vars bs

  and check_branches vars bs = List.iter (check_branch vars) bs

  and check_branch vars (pat, e) =
    let vars' = check_pattern Vars.empty pat in
    check_term (Vars.union vars vars') e

  (** [add_args args vars] adds the variables of the formal arguments
     [args] to the set of variables [vars] *)
  let add_args args vars =
    List.fold_left
      (fun acc x -> Vars.add x.Location.contents acc)
      vars
      args.Location.contents

  (** Checks that all variables of a program are bound *)
  let rec check_program vars = function
    | [] -> ()
    | DeclVal (x, e) :: decls ->
      check_term vars e;
      check_program (Vars.add x.contents vars) decls
    | DeclFun (f, args, e) :: decls ->
      check_term (add_args args vars) e;
      check_program (Vars.add f.contents vars) decls
    | DeclRecFun (f, args, e) :: decls ->
      check_term (Vars.add f.contents @@ add_args args vars) e;
      check_program (Vars.add f.contents vars) decls

  (** [check prog] checks that all variables of the program [prog] are bound *)
  let check prog =
    match check_program Vars.empty prog with
    | () -> Ok ()
    | exception Unbound (loc, x) ->
      let msg = Format.asprintf "Unbound variable: %a" Var.pp x in
      Error (loc, msg)
    | exception NonLinearVarInPattern (loc, x) ->
      let msg =
        Format.asprintf
          "This variable used more than once in a pattern: %a"
          Var.pp
          x
      in
      Error (loc, msg)
end

module MapMake (X : sig
  include Map.OrderedType
  include Memo.WITH_PP with type t := t
end) =
struct
  include Map.Make (X)

  let pp ppval fmt m =
    let open Format in
    pp_print_list
      ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
      (fun fmt (x, v) -> fprintf fmt "@[(@[%a@])@ -> @[%a@]@]" X.pp x ppval v)
      fmt
      (bindings m)
end

(** Program points *)
module PP = struct
  type t = int

  let compare = Int.compare
  let hash_fold = Hash.fold

  module Map = Ptmap

  let next_pp = ref 0
  let location_map = ref Map.empty
  let int_map = ref Map.empty

  (** Registers a program point for a location *)
  let register loc =
    let pp = !next_pp in
    incr next_pp;
    location_map := Map.add pp loc !location_map;
    pp

  (** Registers a program point for a boolean. We share the same
     identifier for identical booleans. We take negative numbers for
     booleans. It does not conflict with other identifiers, since they
     are non-negative. *)
  let register_bool loc b =
    let pp = if b then -1 else -2 in
    location_map :=
      Map.update
        pp
        (function
          | None -> Some loc
          | Some _ as o -> o)
        !location_map;
    pp

  (** Registers a program point for an integer. We share the same
     identifier for identical integers, when [share_integers = true] *)
  let register_int ~share_integers loc n =
    if share_integers
    then
      match Map.find_opt n !int_map with
      | None ->
        let pp = register loc in
        int_map := Map.add n pp !int_map;
        pp
      | Some pp -> pp
    else
      let pp = register loc in
      int_map := Map.add n pp !int_map;
      pp

  (** Gets the location map, that assigns resolves program points into
     locations, for terms that are not booleans or integers. *)
  let get_location_map () = !location_map

  let pp fmt i =
    match Map.find_opt i !location_map with
    | Some loc -> Format.pp_print_string fmt (Location.to_string_compact "" loc)
    | None -> Format.fprintf fmt "?%i" i

  let pp_long fmt i =
    match Map.find_opt i !location_map with
    | Some loc -> Location.pp fmt loc
    | None -> Format.fprintf fmt "?%i" i
end

(** Terms (inner syntax), annotated with unique program points. *)
module Term : sig
  type simple_pattern = private
    | PWildcard
    | PVar of PP.t * Var.t

  type pattern = private
    | PSimple of basic_pattern * simple_pattern
    | PTuple of int * simple_pattern list
    | PVariant of Cons.t * simple_pattern option

  type t = private {
    id: PP.t;
    term: t_;
  }

  and t_ = private
    | Bool of bool
    | UnknownBool
    | If of t * t * t
    | Var of Var.t
    | Lam of PP.t * Var.t * t
    | FixLam of PP.t * Var.t * PP.t * Var.t * t
    | App of t * t
    | Let of PP.t * Var.t * t * t
    | Int of int
    | UnknownInt
    | Binop of binop * t * t
    | Unop of unop * t
    | BOrElse of t * t
    | BAndAlso of t * t
    | Tuple of int * t list
    | Variant of Cons.t * t option
    | Match of t * branches

  and branches = branch list
  and branch = pattern * t

  val compare : t -> t -> int
  val hash_fold : t -> Hash.state -> Hash.state
  val fv : t -> Vars.t
  val pp_rec : bool -> Format.formatter -> t -> unit
  val pp : Format.formatter -> t -> unit

  val translate :
    share_integers:bool -> share_variables:bool -> Outer.program -> t
end = struct
  type simple_pattern =
    | PWildcard
    | PVar of PP.t * Var.t

  type pattern =
    | PSimple of basic_pattern * simple_pattern
    | PTuple of int * simple_pattern list
    | PVariant of Cons.t * simple_pattern option

  type t = {
    id: PP.t;
    term: t_;
  }

  and t_ =
    | Bool of bool
    | UnknownBool
    | If of t * t * t
    | Var of Var.t
    | Lam of PP.t * Var.t * t
    | FixLam of PP.t * Var.t * PP.t * Var.t * t
    | App of t * t
    | Let of PP.t * Var.t * t * t
    | Int of int
    | UnknownInt
    | Binop of binop * t * t
    | Unop of unop * t
    | BOrElse of t * t
    | BAndAlso of t * t
    | Tuple of int * t list
    | Variant of Cons.t * t option
    | Match of t * branches

  and branches = branch list
  and branch = pattern * t

  let hash_fold t s = Hash.fold t.id s

  (** Total order: since we give a unique identifier to each subterm,
     it suffices to compare the identifier. This saves a lot a time. *)
  let compare t1 t2 = PP.compare t1.id t2.id

  let pp_simple_pattern fmt = function
    | PWildcard -> Format.pp_print_char fmt '_'
    | PVar (_, x) -> Var.pp fmt x

  let pp_simple_pattern2 fmt = function
    | Any, p -> pp_simple_pattern fmt p
    | Int, PWildcard -> Format.pp_print_string fmt "?int"
    | Int, p -> Format.fprintf fmt "?int as %a" pp_simple_pattern p
    | Bool, PWildcard -> Format.pp_print_string fmt "?bool"
    | Bool, p -> Format.fprintf fmt "?bool as %a" pp_simple_pattern p

  let pp_pattern fmt = function
    | PSimple (bpat, ox) -> pp_simple_pattern2 fmt (bpat, ox)
    | PTuple (_n, []) -> Format.pp_print_string fmt "()"
    | PTuple (_n, l) ->
      let open Format in
      fprintf
        fmt
        "@[<hv 0>(@[<hv 1> %a@]@ )@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
           pp_simple_pattern)
        l
    | PVariant (c, None) -> Cons.pp fmt c
    | PVariant (c, Some spat) ->
      Format.fprintf fmt "@[%a@ %a@]" Cons.pp c pp_simple_pattern spat

  let rec pp_rec parens fmt { term = t; id = _ } =
    let open Format in
    match t with
    | Bool b -> pp_print_bool fmt b
    | If (t1, t2, t3) ->
      if parens
      then
        fprintf
          fmt
          "@[(if@ %a@ then@ %a@ else@ %a)@]"
          (pp_rec false)
          t1
          (pp_rec false)
          t2
          (pp_rec false)
          t3
      else
        fprintf
          fmt
          "@[if@ %a@ then@ %a@ else@ %a@]"
          (pp_rec false)
          t1
          (pp_rec false)
          t2
          (pp_rec false)
          t3
    | Var x -> Var.pp fmt x
    | Lam (_xloc, x, t) ->
      if parens
      then fprintf fmt "@[(fun@ %a@ ->@ %a)@]" Var.pp x (pp_rec false) t
      else fprintf fmt "@[fun@ %a@ ->@ %a@]" Var.pp x (pp_rec false) t
    | FixLam (_floc, f, _xloc, x, t) ->
      if parens
      then
        fprintf
          fmt
          "@[(rec fun@ %a@ %a@ ->@ %a)@]"
          Var.pp
          f
          Var.pp
          x
          (pp_rec false)
          t
      else
        fprintf
          fmt
          "@[rec fun@ %a@ %a@ ->@ %a@]"
          Var.pp
          f
          Var.pp
          x
          (pp_rec false)
          t
    | App (t1, t2) ->
      let parens1 =
        match t1.term with
        | Bool _
        | Var _
        | App _
        | UnknownInt
        | UnknownBool
        | Int _
        | Tuple _
        | Match _ -> false
        | FixLam _
        | Lam _
        | Let _
        | If _
        | Binop _
        | Unop _
        | BOrElse _
        | BAndAlso _
        | Variant _ -> true
      in
      let parens2 =
        match t2.term with
        | Bool _ | Var _ | UnknownInt | UnknownBool | Int _ | Tuple _ | Match _
          -> false
        | App _
        | FixLam _
        | Lam _
        | Let _
        | If _
        | Binop _
        | Unop _
        | BOrElse _
        | BAndAlso _
        | Variant _ -> true
      in
      if parens
      then fprintf fmt "@[(%a@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ %a@]" (pp_rec parens1) t1 (pp_rec parens2) t2
    | Let (_xloc, x, t1, t2) ->
      if parens
      then
        fprintf
          fmt
          "@[(let@ %a@ =@ %a@ in@ %a)@]"
          Var.pp
          x
          (pp_rec false)
          t1
          (pp_rec false)
          t2
      else
        fprintf
          fmt
          "@[let@ %a@ =@ %a@ in@ %a@]"
          Var.pp
          x
          (pp_rec false)
          t1
          (pp_rec false)
          t2
    | UnknownBool -> Format.pp_print_string fmt "bool?"
    | UnknownInt -> Format.pp_print_string fmt "int?"
    | Int n -> Format.pp_print_int fmt n
    | Binop (PLUS, t1, t2) ->
      let parens1 =
        match t1.term with
        | Int _ | UnknownInt | UnknownBool | Bool _ | Var _ | App _
        | Binop (TIMES, _, _)
        | Tuple _ | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2 =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | If _
        | Lam _
        | FixLam _
        | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Tuple _ | Match _ -> false
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2b =
        match t2.term with
        | Int _ | UnknownInt | UnknownBool | Bool _ | Var _ | App _
        | Binop (TIMES, _, _)
        | Tuple _ | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then fprintf fmt "@[(%a@ +@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ +@ %a@]" (pp_rec parens1) t1 (pp_rec parens2b) t2
    | Binop (MINUS, t1, t2) ->
      let parens1 =
        match t1.term with
        | Int _ | UnknownInt | UnknownBool | Bool _ | Var _ | App _
        | Binop (TIMES, _, _)
        | Tuple _ | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2 =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | If _
        | Lam _
        | FixLam _
        | Let _
        | Binop (TIMES, _, _)
        | Tuple _ | Match _ -> false
        | Binop ((PLUS | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2b =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then fprintf fmt "@[(%a@ -@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ -@ %a@]" (pp_rec parens1) t1 (pp_rec parens2b) t2
    | Binop (TIMES, t1, t2) ->
      let parens1 =
        match t1.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2 =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | If _
        | Lam _
        | FixLam _
        | Let _
        | Binop (TIMES, _, _)
        | Tuple _ | Match _ -> false
        | Binop ((PLUS | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2b =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then fprintf fmt "@[(%a@ *@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ *@ %a@]" (pp_rec parens1) t1 (pp_rec parens2b) t2
    | Binop (((LE | LT | GE | GT | EQ | NEQ) as op), t1, t2) ->
      let parens2b =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then
        fprintf
          fmt
          "@[(%a@ %a@ %a)@]"
          (pp_rec false)
          t1
          pp_binop
          op
          (pp_rec false)
          t2
      else
        fprintf
          fmt
          "@[%a@ %a@ %a@]"
          (pp_rec false)
          t1
          pp_binop
          op
          (pp_rec parens2b)
          t2
    | Unop (NEG, t1) ->
      let parens1 =
        match t1.term with
        | Int _ | UnknownInt | UnknownBool | Bool _ | Var _ | Tuple _ | Match _
          -> false
        | App _ | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then fprintf fmt "@[(-%a)@]" (pp_rec parens1) t1
      else fprintf fmt "@[-%a@]" (pp_rec parens1) t1
    | BOrElse (t1, t2) ->
      let parens1 =
        match t1.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2 =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | If _
        | Lam _
        | FixLam _
        | Let _
        | Tuple _
        | Match _ -> false
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2b =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | TIMES | MINUS | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then
        fprintf fmt "@[(%a@ ||@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ ||@ %a@]" (pp_rec parens1) t1 (pp_rec parens2b) t2
    | BAndAlso (t1, t2) ->
      let parens1 =
        match t1.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2 =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | If _
        | Lam _
        | FixLam _
        | Let _
        | Tuple _
        | Match _ -> false
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      and parens2b =
        match t2.term with
        | Int _
        | UnknownInt
        | UnknownBool
        | Bool _
        | Var _
        | App _
        | Tuple _
        | Match _ -> false
        | If _ | Lam _ | FixLam _ | Let _
        | Binop ((PLUS | MINUS | TIMES | LE | LT | GE | GT | EQ | NEQ), _, _)
        | Unop _ | BOrElse _ | BAndAlso _ | Variant _ -> true
      in
      if parens
      then
        fprintf fmt "@[(%a@ &&@ %a)@]" (pp_rec parens1) t1 (pp_rec parens2) t2
      else fprintf fmt "@[%a@ &&@ %a@]" (pp_rec parens1) t1 (pp_rec parens2b) t2
    | Tuple (_n, ts) ->
      fprintf
        fmt
        "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") (pp_rec false))
        ts
    | Variant (c, None) -> Cons.pp fmt c
    | Variant (c, Some t) ->
      if parens
      then fprintf fmt "@[(%a@ %a)@]" Cons.pp c (pp_rec false) t
      else fprintf fmt "@[%a@ %a@]" Cons.pp c (pp_rec false) t
    | Match (t, bs) ->
      fprintf
        fmt
        "@[match@ @[%a@]@ with@ @[%a@]@ end@]"
        (pp_rec false)
        t
        pp_branches
        bs

  and pp_branches fmt bs =
    let open Format in
    pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_branch fmt bs

  and pp_branch fmt (pat, e) =
    Format.fprintf
      fmt
      "@[| @[%a@]@ ->@ @[%a@]@]"
      pp_pattern
      pat
      (pp_rec false)
      e

  let pp = pp_rec false

  (** Free variables (with memoization) *)
  let fv =
    let module MiniTerm = struct
      type nonrec t = t

      let compare = compare
      let pp = pp
    end in
    let module TMemo =
      Memo.Make (Logs.Silent) (MiniTerm) (MapMake (MiniTerm)) (Vars)
    in
    TMemo.fix ~transient:true @@ fun fv { term = t; id = _ } ->
    match t with
    | Bool _ | UnknownInt | UnknownBool | Int _ | Variant (_, None) ->
      Vars.empty
    | If (t1, t2, t3) -> Vars.union (fv t1) @@ Vars.union (fv t2) (fv t3)
    | Var x -> Vars.singleton x
    | Lam (_xloc, x, t) -> Vars.remove x (fv t)
    | FixLam (_floc, f, _xloc, x, t) -> Vars.remove f (Vars.remove x (fv t))
    | App (t1, t2) | Binop (_, t1, t2) | BOrElse (t1, t2) | BAndAlso (t1, t2) ->
      Vars.union (fv t1) (fv t2)
    | Let (_xloc, x, t1, t2) -> Vars.union (fv t1) (Vars.remove x (fv t2))
    | Unop (_, t) -> fv t
    | Tuple (_n, ts) ->
      List.fold_left (fun vars t -> Vars.union vars (fv t)) Vars.empty ts
    | Variant (_, Some t) -> fv t
    | Match (t, bs) ->
      let fv_simple_pattern = function
        | PWildcard -> Vars.empty
        | PVar (_, x) -> Vars.singleton x
      in
      let fv_pattern = function
        | PTuple (_n, l) ->
          List.fold_left
            (fun vars p -> Vars.union vars (fv_simple_pattern p))
            Vars.empty
            l
        | PSimple (_, p) | PVariant (_, Some p) -> fv_simple_pattern p
        | PVariant (_, None) -> Vars.empty
      in
      let fv_branch (p, t) = Vars.diff (fv t) (fv_pattern p) in
      let fv_branches bs =
        List.fold_left
          (fun vars b -> Vars.union vars (fv_branch b))
          Vars.empty
          bs
      in
      Vars.union (fv t) (fv_branches bs)

  let translate_simple_pattern ~share_variables env = function
    | Some { Location.contents = x; loc } ->
      let id = PP.register loc in
      let env = if share_variables then Env.add x id env else env in
      (PVar (id, x), env)
    | None -> (PWildcard, env)

  let translate_pattern ~share_variables env = function
    | Outer.PVar (bpat, p) ->
      let p, env = translate_simple_pattern ~share_variables env p in
      (PSimple (bpat, p), env)
    | Outer.PTuple (n, l) ->
      let l, env =
        List.fold_left
          (fun (l, env) p ->
            let p, env = translate_simple_pattern ~share_variables env p in
            (p :: l, env))
          ([], env)
          l
      in
      (PTuple (n, List.rev l), env)
    | Outer.PVariant (c, None) -> (PVariant (c, None), env)
    | Outer.PVariant (c, Some spat) ->
      let spat, env = translate_simple_pattern ~share_variables env spat in
      (PVariant (c, Some spat), env)

  (** Translates outer expressions into terms that are annotated with
     unique identifers. Identical variables in the same scope share
     the same identifier, when [share_variables = true]. Identical
     integers also share the same identifier when [share_intergers =
     true]. Identical booleans always share the same identifier. *)
  let rec translate_term ~share_integers ~share_variables env
      { Location.loc; contents } =
    match contents with
    | Outer.Var x ->
      if share_variables
      then { id = Env.find x env; term = Var x }
      else { id = PP.register loc; term = Var x }
    | Outer.UnknownInt -> { id = PP.register loc; term = UnknownInt }
    | Outer.UnknownBool -> { id = PP.register loc; term = UnknownBool }
    | Outer.Let (x, t1, t2) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 =
        translate_term
          ~share_integers
          ~share_variables
          (Env.add x.contents (PP.register x.loc) env)
          t2
      in
      let id = PP.register loc in
      { id; term = Let (PP.register x.loc, x.contents, t1, t2) }
    | Outer.App (t1, t2) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 = translate_term ~share_integers ~share_variables env t2 in
      let id = PP.register loc in
      { id; term = App (t1, t2) }
    | Outer.Fun (x, t) ->
      let t =
        translate_term
          ~share_integers
          ~share_variables
          (Env.add x.contents (PP.register x.loc) env)
          t
      in
      let id = PP.register loc in
      { id; term = Lam (PP.register x.loc, x.contents, t) }
    | Outer.FixFun (f, x, t) ->
      let t =
        translate_term
          ~share_integers
          ~share_variables
          (Env.add f.contents (PP.register f.loc)
          @@ Env.add x.contents (PP.register x.loc)
          @@ env)
          t
      in
      let id = PP.register loc in
      {
        id;
        term =
          FixLam
            (PP.register f.loc, f.contents, PP.register x.loc, x.contents, t);
      }
    | Outer.Parens (_, t) ->
      translate_term ~share_integers ~share_variables env t
    | Outer.Bool b -> { id = PP.register_bool loc b; term = Bool b }
    | Outer.BOrElse (t1, t2) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 = translate_term ~share_integers ~share_variables env t2 in
      let id = PP.register loc in
      { id; term = BOrElse (t1, t2) }
    | Outer.BAndAlso (t1, t2) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 = translate_term ~share_integers ~share_variables env t2 in
      let id = PP.register loc in
      { id; term = BAndAlso (t1, t2) }
    | Outer.IfThenElse (t1, t2, t3) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 = translate_term ~share_integers ~share_variables env t2 in
      let t3 = translate_term ~share_integers ~share_variables env t3 in
      let id = PP.register loc in
      { id; term = If (t1, t2, t3) }
    | Outer.Int n ->
      { id = PP.register_int ~share_integers loc n; term = Int n }
    | Outer.Binop (op, t1, t2) ->
      let t1 = translate_term ~share_integers ~share_variables env t1 in
      let t2 = translate_term ~share_integers ~share_variables env t2 in
      let id = PP.register loc in
      { id; term = Binop (op, t1, t2) }
    | Outer.Unop (op, t) ->
      let t = translate_term ~share_integers ~share_variables env t in
      let id = PP.register loc in
      { id; term = Unop (op, t) }
    | Outer.Tuple (n, ts) ->
      let ts =
        List.map (translate_term ~share_integers ~share_variables env) ts
      in
      let id = PP.register loc in
      { id; term = Tuple (n, ts) }
    | Outer.Variant (c, ot) ->
      let ot =
        Option.map (translate_term ~share_integers ~share_variables env) ot
      in
      let id = PP.register loc in
      { id; term = Variant (c, ot) }
    | Outer.Match (t, bs) ->
      let t = translate_term ~share_integers ~share_variables env t in
      let bs = translate_branches ~share_integers ~share_variables env bs in
      let id = PP.register loc in
      { id; term = Match (t, bs) }

  and translate_branches ~share_integers ~share_variables env bs =
    List.map (translate_branch ~share_integers ~share_variables env) bs

  and translate_branch ~share_integers ~share_variables env (p, t) =
    let p, env = translate_pattern ~share_variables env p in
    let t = translate_term ~share_integers ~share_variables env t in
    (p, t)

  let rec translate_program = function
    | [] -> assert false
    | [Outer.DeclVal (_, t)] -> t
    | Outer.DeclVal (x, t) :: decls ->
      let t1 = t in
      let t2 = translate_program decls in
      let open Location in
      locate (from_pos (get_start x.loc, get_end t2.loc))
      @@ Outer.Let (x, t1, t2)
    | [Outer.DeclFun (_, args, t)] ->
      let open Location in
      List.fold_right
        (fun x acc ->
          locate (from_pos (get_start x.loc, get_end acc.loc))
          @@ Outer.Fun (x, acc))
        args.contents
        t
    | Outer.DeclFun (f, args, t) :: decls ->
      let open Location in
      let t1 =
        List.fold_right
          (fun x acc ->
            locate (from_pos (get_start x.loc, get_end acc.loc))
            @@ Outer.Fun (x, acc))
          args.contents
          t
      in
      let t2 = translate_program decls in
      locate (from_pos (get_start f.loc, get_end t2.loc))
      @@ Outer.Let (f, t1, t2)
    | Outer.DeclRecFun (_f, { contents = []; _ }, _t) :: _ -> assert false
    | [Outer.DeclRecFun (f, { contents = x :: xs; loc = args_loc }, t)] ->
      let open Location in
      locate (from_pos (get_start args_loc, get_end t.loc))
      @@ Outer.FixFun
           ( f,
             x,
             List.fold_right
               (fun x acc ->
                 locate (from_pos (get_start x.loc, get_end acc.loc))
                 @@ Outer.Fun (x, acc))
               xs
               t )
    | Outer.DeclRecFun (f, { contents = x :: xs; loc = args_loc }, t) :: decls
      ->
      let open Location in
      let t1 =
        locate (from_pos (get_start args_loc, get_end t.loc))
        @@ Outer.FixFun
             ( f,
               x,
               List.fold_right
                 (fun x acc ->
                   locate (from_pos (get_start x.loc, get_end acc.loc))
                   @@ Outer.Fun (x, acc))
                 xs
                 t )
      in
      let t2 = translate_program decls in
      locate (from_pos (get_start f.loc, get_end t2.loc))
      @@ Outer.Let (f, t1, t2)

  (** Translates programs into terms that are annotated with unique
     identifers. Identical variables in the same scope share the same
     identifier, when [share_variables = true]. Identical integers
     also share the same identifier when [share_intergers = true].
     Identical booleans always share the same identifier. *)
  let translate ~share_integers ~share_variables prog =
    translate_term
      ~share_integers
      ~share_variables
      Env.empty
      (translate_program prog)
end
