(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

%{
open Ast.Outer

let locate pos2 x = Location.(locate (from_pos pos2) x)

%}

%token EOF
%token VAL
%token LET EQUAL IN BEGIN END FUN REC
%token LPAR RPAR RARROW QMARKINT QMARKBOOL
%token BTRUE BFALSE BANDALSO BORELSE IF THEN ELSE MATCH WITH AS
%token LE LT GE GT NEQ PLUS MINUS STAR COMMA VBAR UNDERSCORE
%token<string> ID UPID
%token<int> INT

%start<Ast.Outer.program> program

%%

%inline var:
| x = ID
{ Ast.Var.of_string x }

%inline constructor:
| x = UPID
{ Ast.Cons.of_string x }

%inline located(x):
| x = x
{ locate $loc(x) x }

atomic_expr:
| e = atomic_expr_no_arith_unary
| e = arith_unary_expr
{ e }

arith_unary_expr:
| MINUS e = atomic_expr
{ locate $loc @@ Unop (NEG, e) }

atomic_expr_no_arith_unary:
| e = atomic_expr_no_tuples_no_arith_unary
{ e }
| c = constructor
{ locate $loc @@ Variant (c, None) }

atomic_expr_no_tuples_no_arith_unary:
| e = atomic_expr_no_cons_no_tuples_no_arith_unary
{ e }

atomic_expr_no_cons_no_tuples:
| e = atomic_expr_no_cons_no_tuples_no_arith_unary
| e = arith_unary_expr
{ e }

atomic_expr_no_cons_no_tuples_no_arith_unary:
| n = INT
{ locate $loc @@ Int n }
| BTRUE
{ locate $loc @@ Bool true }
| BFALSE
{ locate $loc @@ Bool false }
| x = var
{ locate $loc @@ Var x }
| QMARKINT
{ locate $loc @@ UnknownInt }
| QMARKBOOL
{ locate $loc @@ UnknownBool }
| e = delimited(BEGIN, expr, END)
{ locate $loc @@ Parens(BeginEnd, e) }
| e = delimited(LPAR, expr, RPAR)
{ locate $loc @@ Parens(Parentheses, e) }
| LPAR RPAR
{ locate $loc @@ Tuple (0, []) }
| LPAR e = expr COMMA es = separated_nonempty_list(COMMA, expr) RPAR
{ locate $loc @@ Tuple (1 + List.length es, e :: es) }

app_expr:
| e = atomic_expr_no_cons_no_tuples
{ e }
| e1 = app_expr e2 = atomic_expr_no_arith_unary
{ locate $loc @@ App(e1, e2) }
| c = constructor e = atomic_expr_no_arith_unary
{ locate $loc @@ Variant (c, Some e) }

div_expr:
| e = app_expr
{ e }
| c = constructor
{ locate $loc @@ Variant (c, None) }

factor_expr:
| e = div_expr
{ e }
| e1 = div_expr STAR e2 = factor_expr
{ locate $loc @@ Binop(TIMES, e1, e2) }

diff_expr:
| e = factor_expr
{ e }
| e1 = diff_expr MINUS e2 = factor_expr
{ locate $loc @@ Binop(MINUS, e1, e2) }

sum_expr:
| e = diff_expr
{ e }
| e1 = diff_expr PLUS e2 = sum_expr
{ locate $loc @@ Binop(PLUS, e1, e2) }

cmp:
| LE { LE }
| LT { LT }
| GE { GE }
| GT { GT }
| EQUAL { EQ }
| NEQ { NEQ }

cmp_expr:
| e = sum_expr
{ e }
| e1 = sum_expr op = cmp e2 = cmp_expr
{ locate $loc @@ Binop(op, e1, e2) }

bool_factor_expr:
| e = cmp_expr
{ e }
| e1 = cmp_expr BANDALSO e2 = bool_factor_expr
{ locate $loc @@ BAndAlso (e1, e2) }

bool_sum_expr:
| e = bool_factor_expr
{ e }
| e1 = bool_factor_expr BORELSE e2 = bool_sum_expr
{ locate $loc @@ BOrElse (e1, e2) }

expr:
| e = bool_sum_expr
{ e }
| LET x = located(var)
  EQUAL e1 = expr
  IN e2 = expr
{ locate $loc @@ Let(x, e1, e2)
}
| FUN xs = param+
  RARROW e = expr
{ List.fold_right (fun x acc ->
    let location =
      Location.(get_start x.loc, snd $loc)
    in
    locate location @@ Fun(x, acc))
    xs
    e
  |> fun e -> locate $loc e.contents
}
| LET f = located(var) xs = param+
  EQUAL e1 = expr
  IN e2 = expr
{ let e1' = List.fold_right (fun x acc ->
    let location =
      Location.(get_start x.loc, snd $loc)
    in
    locate location @@ Fun(x, acc))
    xs
    e1
  in
  locate $loc @@ Let(f, e1', e2)
}
| LET REC f = located(var) xs = param+
  EQUAL e1 = expr IN e2 = expr
{ let e1' =
    match xs with
    | [] -> assert false
    | args::argss ->
    let e =
      List.fold_right (fun x acc ->
        let location =
          Location.(get_start x.loc, snd $loc)
        in
        locate location @@ Fun(x, acc))
      argss
      e1
    in
    locate $loc @@ FixFun(f, args, e)
  in
  locate $loc @@ Let(f, e1', e2)
}
| IF e1 = expr THEN e2 = expr ELSE e3 = expr
{ locate $loc @@ IfThenElse (e1, e2, e3) }
| MATCH e = expr WITH bs = branches END
{ locate $loc @@ Match (e, bs) }

branches:
| VBAR? l = separated_list(VBAR, branch)
{ l }

branch:
| p = pattern RARROW e = expr
{ (p, e) }

pattern:
| p = simple_pattern2
{ PVar p }
| LPAR l = separated_list(COMMA, simple_pattern) RPAR
{ PTuple (List.length l, l) }
| c = constructor
{ PVariant (c, None) }
| c = constructor p = simple_pattern
{ PVariant (c, Some p) }

simple_pattern:
| UNDERSCORE
{ None }
| UNDERSCORE AS x = param
{ Some x }
| x = param
{ Some x }

simple_pattern2:
| UNDERSCORE
{ (Ast.Any, None) }
| UNDERSCORE AS x = param
{ (Ast.Any, Some x) }
| x = param
{ (Ast.Any, Some x) }
| QMARKBOOL
{ (Ast.Bool, None) }
| QMARKBOOL AS x = param
{ (Ast.Bool, Some x) }
| QMARKINT
{ (Ast.Int, None) }
| QMARKINT AS x = param
{ (Ast.Int, Some x) }

param:
| x = located(var)
{ x }

val_decl:
| VAL x = located(var)
  EQUAL e = expr
{ DeclVal(x, e) }
| VAL x = located(var) argss = located(param+)
  EQUAL e = expr
{ DeclFun(x, argss, e) }
| VAL REC x = located(var) argss = located(param+)
  EQUAL e = expr
{ DeclRecFun(x, argss, e) }

decl:
| d = val_decl
{ d }

program:
| ds = decl+ EOF
{ ds }
