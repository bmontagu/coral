(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

{
open Parser
exception Error of string * Location.t

let uniquify_id =
  let h = Hashtbl.create 128 in
  fun (id: string) ->
    match Hashtbl.find_opt h id with
    | Some id -> id
    | None -> Hashtbl.add h id id; id
}

let low_alpha = ['a'-'z']
let up_alpha = ['A'-'Z']
let alpha = low_alpha | up_alpha
let id_symbol = ['_' '\'']
let ext_alpha = id_symbol | alpha
let num = ['0'-'9']
let alpha_num = alpha | num
let ext_alpha_num = ext_alpha | num

let up_id = up_alpha ext_alpha_num*
let low_id = low_alpha ext_alpha_num*

let whitespace = [' ' '\t']+
let new_line = ('\r' '\n') | '\n'

let digits = '0' | ['1'-'9'] ['0'-'9']*

rule token = parse
| new_line { Lexing.new_line lexbuf; token lexbuf }
| whitespace { token lexbuf }
| "(*" { comment 0 lexbuf (* comment with 0 level of nesting *)}
| "val" { VAL }
| "let" { LET }
| '=' { EQUAL }
| "in" { IN }
| "begin" { BEGIN }
| "end" { END }
| "fun" { FUN }
| "rec" { REC }
| '(' { LPAR }
| ')' { RPAR }
| "->" { RARROW }
| "?int" { QMARKINT }
| "?bool" { QMARKBOOL }
| "true" { BTRUE }
| "false" { BFALSE }
| "as" { AS }
| "&&" { BANDALSO }
| "||" { BORELSE }
| "if" { IF }
| "then" { THEN }
| "else" { ELSE }
| "match" { MATCH }
| "with" { WITH }
| "<=" { LE }
| '<' { LT }
| ">=" { GE }
| '>' { GT }
| "<>" { NEQ }
| '+' { PLUS }
| '-' { MINUS }
| '*' { STAR }
| ',' { COMMA }
| '|' { VBAR }
| '_' { UNDERSCORE }
| up_id as id { UPID (uniquify_id id) }
| low_id as id { ID (uniquify_id id) }
| digits as x
{ try INT (int_of_string x)
  with Failure _ ->
    raise (Error (x, Location.from_lexbuf lexbuf))
}
| eof { EOF }
| _ as x
{ let s = String.make 1 x in
  raise (Error (s, Location.from_lexbuf lexbuf))
}

and comment n = parse
| new_line { Lexing.new_line lexbuf; comment n lexbuf }
| "*)" {
  if n = 0
  then (* there is no outer comment: get back to normal lexer *)
    token lexbuf
  else (* pop one comment nesting level *)
    comment (n-1) lexbuf
}
| "(*" { comment (n+1) lexbuf }
| eof {
  if n = 0
  then EOF
  else raise (Error ("EOF", Location.from_lexbuf lexbuf))
}
| ([^ '\r' '\n' '(' '*'] | '(' [^ '*'] | '*' [^ ')'])+ { comment n lexbuf }
