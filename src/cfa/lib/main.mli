(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

module IntegerDomain : sig
  type t =
    | TwoPoints
    | Flat
    | Signs
    | Intervals

  val min : int
  val max : int
  val to_enum : t -> int
  val of_enum : int -> t option
  val parser : string -> (t, [ `Msg of string ]) result
  val printer : Format.formatter -> t -> unit
  val to_string : t -> string
  val default : t
  val examples : string list
end

module CallDomain : sig
  type t =
    | LastCallSites of int
    | CyclicCallSites of int * int option

  val parser : string -> (t, [ `Msg of string ]) result
  val printer : Format.formatter -> t -> unit
  val default : t
  val examples : string list
end

module AnalysisKind : sig
  type t =
    | Nabla
    | NablaRec
    | Global
    | GlobalIndependentAttribute

  val min : int
  val max : int
  val to_enum : t -> int
  val of_enum : int -> t option
  val to_string : t -> string
end

module Solver : sig
  type t =
    | Naive
    | TopDownLegacy
    | TopDown
    | Priority

  val min : int
  val max : int
  val to_enum : t -> int
  val of_enum : int -> t option
  val to_string : t -> string
  val parser : string -> (t, [ `Msg of string ]) result
  val printer : Format.formatter -> t -> unit
  val default : t
end

module Options : sig
  type t = {
    refine_branches: bool;
    branch_is_context: bool;
    let_is_context: bool;
    finer_context: bool;
    no_env_restrict: bool;
    kill_unreachable: bool;
    record_closure_creation_context: bool;
    record_tuple_creation_location: bool;
    record_variant_creation_location: bool;
    cache_non_calls: bool;
    cache_widened_calls_only: bool;
    no_alarms: bool;
    disprove_alarms: bool;
    minimize: bool;
    debug: bool;
    print_memo_table: bool;
    integer_domain: IntegerDomain.t;
    call_domain: CallDomain.t;
    analysis_kind: AnalysisKind.t;
    solver: Solver.t;
    share_integers_program_points: bool;
    share_variables_program_points: bool;
    occurs_threshold: int;
    sim_threshold: int;
  }

  module type ANALYZER = sig
    module Abs : sig
      type t

      val get_alarms : t -> (Lang.Ast.PP.t * string) list
      val pp : Format.formatter -> t -> unit
    end

    module Entry : sig
      type t

      val pp : Format.formatter -> t -> unit
    end

    val eval : Lang.Ast.Term.t -> Abs.t * Entry.t Memo.Statistics.t
  end

  val get_analysis_module : (module Logs.S) -> t -> (module ANALYZER)
end

val cfa_print : Options.t -> string -> unit

type package =
  | Pack :
      'res
      * (Format.formatter -> 'res -> unit)
      * 'entry Memo.Statistics.t
      * (Format.formatter -> 'entry -> unit)
      -> package

val cfa_value : Options.t -> string -> package
