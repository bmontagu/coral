(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020-2021
*)

module IntegerDomain = struct
  type t =
    | TwoPoints
    | Flat
    | Signs
    | Intervals
  [@@deriving enum]

  let parser = function
    | "two-points" -> Ok TwoPoints
    | "flat" -> Ok Flat
    | "signs" -> Ok Signs
    | "intervals" -> Ok Intervals
    | s -> Error (`Msg ("Unknown domain for integers: " ^ s))

  let to_string = function
    | TwoPoints -> "two-points"
    | Flat -> "flat"
    | Signs -> "signs"
    | Intervals -> "intervals"

  let printer fmt x = Format.pp_print_string fmt (to_string x)

  let examples =
    List.map (Format.asprintf "%a" printer) [TwoPoints; Flat; Signs; Intervals]

  let default = Intervals

  let get_module =
    let open AbstractDomains in
    function
    | TwoPoints -> (module TwoPoints : Sigs.INT_DOMAIN)
    | Flat -> (module FlatInts : Sigs.INT_DOMAIN)
    | Signs -> (module Signs : Sigs.INT_DOMAIN)
    | Intervals -> (module Intervals : Sigs.INT_DOMAIN)
end

module CallDomain = struct
  type t =
    | LastCallSites of int
    | CyclicCallSites of int * int option

  let parser s =
    try
      match Scanf.sscanf s "last-call-sites:%u%!" (fun i -> i) with
      | i -> Ok (LastCallSites i)
      | (exception Scanf.Scan_failure _)
      | (exception Failure _)
      | (exception End_of_file) -> (
        match
          Scanf.sscanf s "cyclic-call-sites:%u+%u%!" (fun i j -> (i, j))
        with
        | i, j -> Ok (CyclicCallSites (i, Some j))
        | (exception Scanf.Scan_failure _)
        | (exception Failure _)
        | (exception End_of_file) -> (
          match Scanf.sscanf s "cyclic-call-sites:%u+dyn%!" (fun i -> i) with
          | i -> Ok (CyclicCallSites (i, None))
          | (exception Scanf.Scan_failure _)
          | (exception Failure _)
          | (exception End_of_file) -> (
            match Scanf.sscanf s "cyclic-call-sites:%u%!" (fun i -> i) with
            | i -> Ok (CyclicCallSites (i, Some 0)))))
    with Scanf.Scan_failure _ | Failure _ | End_of_file ->
      Error (`Msg ("Invalid argument: " ^ s))

  let printer fmt = function
    | LastCallSites n -> Format.fprintf fmt "last-call-sites:%i" n
    | CyclicCallSites (n, o) -> (
      match o with
      | None -> Format.fprintf fmt "cyclic-call-sites:%i+dyn" n
      | Some 0 -> Format.fprintf fmt "cyclic-call-sites:%i" n
      | Some l -> Format.fprintf fmt "cyclic-call-sites:%i+%i" n l)

  let examples =
    List.map
      (Format.asprintf "%a" printer)
      [
        CyclicCallSites (0, Some 0);
        CyclicCallSites (1, Some 0);
        CyclicCallSites (0, Some 1);
        CyclicCallSites (1, Some 1);
        CyclicCallSites (0, None);
        CyclicCallSites (1, None);
        LastCallSites 0;
        LastCallSites 1;
      ]

  let default = CyclicCallSites (1, Some 0)

  let get_module =
    let open Analyzers in
    function
    | CyclicCallSites (n, max_last) ->
      let module Options = struct
        let max_nesting = n
        let max_last = max_last
      end in
      let module M = Calls.CyclicCallSites (Options) in
      (module M : Calls.S)
    | LastCallSites n ->
      let module Options = struct
        let max_length = n
      end in
      let module M = Calls.LastCallSites (Options) in
      (module M : Calls.S)
end

module AnalysisKind = struct
  type t =
    | Nabla
    | NablaRec
    | Global
    | GlobalIndependentAttribute
  [@@deriving enum]

  let to_string = function
    | Nabla -> "nabla"
    | NablaRec -> "nabla-rec"
    | Global -> "global"
    | GlobalIndependentAttribute -> "global-independent-attribute"
end

module Solver = struct
  type t =
    | Naive
    | TopDownLegacy
    | TopDown
    | Priority
  [@@deriving enum]

  let to_string = function
    | Naive -> "naive"
    | TopDownLegacy -> "top-down-legacy"
    | TopDown -> "top-down"
    | Priority -> "priority"

  let parser = function
    | "naive" -> Ok Naive
    | "top-down-legacy" -> Ok TopDownLegacy
    | "top-down" -> Ok TopDown
    | "priority" -> Ok Priority
    | s -> Error (`Msg ("Unknown solver: " ^ s))

  let printer fmt x = Format.pp_print_string fmt (to_string x)
  let default = Priority

  module type SOLVER = Memo.NAIVE_SOLVER_WITH_INPUT_WIDENING

  let get_module =
    let open Memo.Fix in
    function
    | Naive -> (module Naive.MakeWithInputWidening : SOLVER)
    | TopDownLegacy -> (module TopDownLegacy.MakeWithInputWidening : SOLVER)
    | TopDown -> (module TopDown.MakeWithInputWidening : SOLVER)
    | Priority -> (module Priority.MakeWithInputWidening : SOLVER)
end

module Options = struct
  type t = {
    refine_branches: bool;
    branch_is_context: bool;
    let_is_context: bool;
    finer_context: bool;
    no_env_restrict: bool;
    kill_unreachable: bool;
    record_closure_creation_context: bool;
    record_tuple_creation_location: bool;
    record_variant_creation_location: bool;
    cache_non_calls: bool;
    cache_widened_calls_only: bool;
    no_alarms: bool;
    disprove_alarms: bool;
    minimize: bool;
    debug: bool;
    print_memo_table: bool;
    integer_domain: IntegerDomain.t;
    call_domain: CallDomain.t;
    analysis_kind: AnalysisKind.t;
    solver: Solver.t;
    share_integers_program_points: bool;
    share_variables_program_points: bool;
    occurs_threshold: int;
    sim_threshold: int;
  }

  module type S = sig
    val refine_branches : bool
    val branch_is_context : bool
    val let_is_context : bool
    val finer_context : bool
    val no_env_restrict : bool
    val kill_unreachable : bool
    val record_closure_creation_context : bool
    val record_tuple_creation_location : bool
    val record_variant_creation_location : bool
    val cache_non_calls : bool
    val cache_widened_calls_only : bool
    val no_alarms : bool
    val disprove_alarms : bool
    val minimize : bool
    val occurs_threshold : int
    val sim_threshold : int
    val debug : bool
    val print_memo_table : bool
  end

  let get_module options =
    let module Options = struct
      let refine_branches = options.refine_branches
      let branch_is_context = options.branch_is_context
      let let_is_context = options.let_is_context
      let finer_context = options.finer_context
      let no_env_restrict = options.no_env_restrict
      let kill_unreachable = options.kill_unreachable

      let record_closure_creation_context =
        options.record_closure_creation_context

      let record_tuple_creation_location =
        options.record_tuple_creation_location

      let record_variant_creation_location =
        options.record_variant_creation_location

      let cache_non_calls = options.cache_non_calls
      let cache_widened_calls_only = options.cache_widened_calls_only
      let no_alarms = options.no_alarms
      let disprove_alarms = options.disprove_alarms
      let minimize = options.minimize
      let occurs_threshold = options.occurs_threshold
      let sim_threshold = options.sim_threshold
      let debug = options.debug
      let print_memo_table = options.print_memo_table
    end in
    (module Options : S)

  module type ANALYZER = sig
    module Abs : sig
      type t

      val get_alarms : t -> (Lang.Ast.PP.t * string) list
      val pp : Format.formatter -> t -> unit
    end

    module Entry : sig
      type t

      val pp : Format.formatter -> t -> unit
    end

    val eval : Lang.Ast.Term.t -> Abs.t * Entry.t Memo.Statistics.t
  end

  let get_analysis_module (logs : (module Logs.S)) options =
    let module Log = (val logs) in
    let module Options = (val get_module options) in
    let module S = (val Solver.get_module options.solver) in
    let module I = (val IntegerDomain.get_module options.integer_domain) in
    let module C = (val CallDomain.get_module options.call_domain) in
    let module B = AbstractDomains.Bools in
    let open Analyzers in
    match options.analysis_kind with
    | Nabla ->
      let module D = Nabla.AbsDomain (Options) (B) (I) (C) in
      (module NablaCommon.Analyze (Log) (Options) (S) (C) (B) (I) (D) : ANALYZER)
    | NablaRec ->
      let module D = NablaRec.AbsDomain (Options) (B) (I) (C) () in
      (module NablaCommon.Analyze (Log) (Options) (S) (C) (B) (I) (D) : ANALYZER)
    | Global ->
      (module Global.Analyze (Log) (Options) (S) (C) (B) (I) : ANALYZER)
    | GlobalIndependentAttribute ->
      (module GlobalIndependentAttribute.Analyze (Log) (Options) (S) (C) (B) (I)
      : ANALYZER)
end

let ( >>= ) m f =
  match m with
  | Error _ as err -> err
  | Ok v -> f v

let return x = Ok x

type package =
  | Pack :
      'res
      * (Format.formatter -> 'res -> unit)
      * 'entry Memo.Statistics.t
      * (Format.formatter -> 'entry -> unit)
      -> package

type _ output_choice =
  | Value : package output_choice
  | Print : unit output_choice

let run (type res) (out : res output_choice) (options : Options.t)
    (pp : Format.formatter -> 'a -> unit)
    (pp_stat_entry : Format.formatter -> 'entry -> unit) : _ -> res = function
  | Error (loc, msg) ->
    Format.printf "%a@.%s@." Location.pp loc msg;
    exit 2
  | Ok (v, stats) -> (
    match out with
    | Print ->
      if options.print_memo_table
      then
        Format.printf
          "Statistics:@.@[%a@]@."
          (Memo.Statistics.pp
             ~print_memo_table:options.print_memo_table
             pp_stat_entry)
          stats;
      Format.printf "@[%a@]@." pp v
    | Value -> Pack (v, pp, stats, pp_stat_entry))

let cfa_gen out options file =
  let analyze pp pp_entry interpret =
    let open Lang in
    run out options pp pp_entry
    @@ ( Driver.parse_file Parser.program file >>= fun prog ->
         Ast.Outer.check prog >>= fun () ->
         let prog =
           let open Options in
           Ast.Term.translate
             ~share_integers:options.share_integers_program_points
             ~share_variables:options.share_variables_program_points
             prog
         in
         return @@ interpret prog )
  in
  let module Analyzer =
    (val Options.get_analysis_module (module Logs.Std) options)
  in
  let open Analyzer in
  analyze Abs.pp Entry.pp eval

let cfa_print = cfa_gen Print
let cfa_value = cfa_gen Value
