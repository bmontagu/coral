(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2019
*)

(** Updates the file name of positions in lexbuf to [name] *)
let set_pos_fname name lexbuf =
  let open Lexing in
  lexbuf.lex_start_p <- { lexbuf.lex_start_p with pos_fname = name };
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = name }

(** [parse parser name input] parses the input [input] named [name]
   using the parsing function [parser] *)
let parse parser name input =
  let lexbuf =
    match input with
    | `String s -> Lexing.from_string s
    | `Channel chn -> Lexing.from_channel chn
  in
  set_pos_fname name lexbuf;
  match parser Lang.Lexer.token lexbuf with
  | ast -> Ok ast
  | exception Lang.Lexer.Error (token, loc) ->
    let msg = Printf.sprintf "Error: unexpected character %s" token in
    Error (loc, msg)
  | exception Lang.Parser.Error ->
    let loc = Location.from_lexbuf lexbuf in
    let msg =
      let token = Lexing.lexeme lexbuf in
      Printf.sprintf "Error: unexpected token %s" token
    in
    Error (loc, msg)

(** [parse_file parser file] parses the file [file] using the parsing
   function [parser] *)
let parse_file parser filename =
  let chn = open_in filename in
  let res = parse parser filename (`Channel chn) in
  close_in chn;
  res
