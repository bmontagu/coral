(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t =
  | Bot
  | Int of int
  | Top
[@@deriving ord, eq]

let hash_fold t s =
  match t with
  | Bot -> Hash.fold 0 s
  | Int n -> Hash.fold 1 @@ Hash.fold n s
  | Top -> Hash.fold 2 s

let top = Top
let bot = Bot

let is_bot = function
  | Bot -> true
  | _ -> false

let is_top = function
  | Top -> true
  | _ -> false

let leq i1 i2 =
  match (i1, i2) with
  | Bot, _ | _, Top -> true
  | _, Bot | Top, _ -> false
  | Int n1, Int n2 -> n1 = n2

let mem n = function
  | Bot -> false
  | Top -> true
  | Int m -> m = n

let singleton n = Int n
let interval i j = if i <= j then if i = j then Int i else top else bot

let join i1 i2 =
  match (i1, i2) with
  | Bot, i | i, Bot | _, (Top as i) | (Top as i), _ -> i
  | Int n1, Int n2 -> if n1 = n2 then i1 else top

let widen = join

let meet i1 i2 =
  match (i1, i2) with
  | Top, i | i, Top | _, (Bot as i) | (Bot as i), _ -> i
  | Int n1, Int n2 -> if n1 = n2 then i1 else bot

let pp fmt i =
  let open Format in
  match i with
  | Bot -> pp_print_string fmt "∅"
  | Top -> pp_print_string fmt "⊤"
  | Int n -> fprintf fmt "{%i}" n

module Transfer = struct
  let plus i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> bot
    | Top, _ | _, Top -> top
    | Int n1, Int n2 ->
      let n12 = n1 + n2 in
      if 0 <= n1
      then if n1 <= n12 then Int n12 else top
      else if n12 <= n2
      then Int n12
      else top

  let neg = function
    | (Bot | Top) as i -> i
    | Int n -> if n = min_int then top else Int (-n)

  let minus i1 i2 = plus i1 (neg i2)

  let mult i1 i2 =
    match (i1, i2) with
    | (Bot as i), _ | _, (Bot as i) | (Int 0 as i), _ | _, (Int 0 as i) -> i
    | (Top as i), _ | _, (Top as i) -> i
    | Int n1, Int n2 ->
      if 0 <= n1
      then
        if 0 <= n2
        then if n2 <= max_int / n1 then Int (n1 * n2) else top
        else if (* n2 < 0 *)
                min_int / n1 <= n2
        then Int (n1 * n2)
        else top
      else if (* n1 < 0 *)
              0 <= n2
      then if min_int / n2 <= n1 then Int (n1 * n2) else top
      else if (* n2 < 0 *)
              n1 <= max_int / n2
      then Int (n1 * n2)
      else top

  let is_le i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> (bot, bot)
    | Int n1, Int n2 -> if n1 <= n2 then (i1, i2) else (bot, bot)
    | _ -> (i1, i2)

  let is_lt i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> (bot, bot)
    | Int n1, Int n2 -> if n1 < n2 then (i1, i2) else (bot, bot)
    | _ -> (i1, i2)

  let is_eq s1 s2 =
    let s = meet s1 s2 in
    (s, s)

  let swap_pair (a, b) = (b, a)
  let combine_pair f g (a1, b1) (a2, b2) = (f a1 a2, g b1 b2)

  let le i1 i2 = function
    | true -> is_le i1 i2
    | false -> swap_pair @@ is_lt i2 i1

  let lt i1 i2 = function
    | true -> is_lt i1 i2
    | false -> swap_pair @@ is_le i2 i1

  let ge i1 i2 b = swap_pair @@ le i2 i1 b
  let gt i1 i2 b = swap_pair @@ lt i2 i1 b

  let eq i1 i2 = function
    | true -> is_eq i1 i2
    | false -> combine_pair join join (is_lt i1 i2) (swap_pair @@ is_lt i2 i1)

  let neq i1 i2 b = eq i1 i2 (not b)
end
