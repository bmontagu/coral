(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t

val compare : t -> t -> int
val equal : t -> t -> bool
val hash_fold : t -> Hash.state -> Hash.state
val top : t
val bot : t
val is_bot : t -> bool
val is_top : t -> bool
val leq : t -> t -> bool
val mem : int -> t -> bool
val singleton : 'a -> t
val interval : 'a -> 'a -> t
val join : t -> t -> t
val widen : t -> t -> t
val meet : t -> t -> t
val pp : Format.formatter -> t -> unit

module Transfer : sig
  val plus : t -> t -> t
  val neg : t -> t
  val minus : t -> t -> t
  val mult : t -> t -> t
  val le : t -> t -> bool -> t * t
  val lt : t -> t -> bool -> t * t
  val ge : t -> t -> bool -> t * t
  val gt : t -> t -> bool -> t * t
  val eq : t -> t -> bool -> t * t
  val neq : t -> t -> bool -> t * t
end
