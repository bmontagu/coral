(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

(** The abstract domain of sings, with the following Hasse diagram:

           Top
           /|\
          / | \
         /  |  \
        /   |   \
     NegZ NegPos PosZ
      |\  /   \  /|
      | \/     \/ |
      | /\     /\ |
      |/  \   /  \|
      Neg  Zero  Pos
        \   |   /
         \  |  /
          \ | /
           \|/
           Bot
 *)

type t =
  | Bot
  | Neg
  | NegZ
  | Zero
  | Pos
  | PosZ
  | NegPos
  | Top
[@@deriving ord, eq]

let hash_fold t s =
  let i =
    match t with
    | Bot -> 0
    | Neg -> 1
    | NegZ -> 2
    | Zero -> 3
    | Pos -> 4
    | PosZ -> 5
    | NegPos -> 6
    | Top -> 7
  in
  Hash.fold i s

let bot = Bot

let is_bot = function
  | Bot -> true
  | _ -> false

let top = Top

let is_top = function
  | Top -> true
  | _ -> false

let singleton i = if i = 0 then Zero else if i < 0 then Neg else Pos

let mem i = function
  | Top -> true
  | Bot -> false
  | Zero -> i = 0
  | NegZ -> i <= 0
  | Neg -> i < 0
  | PosZ -> i >= 0
  | Pos -> i > 0
  | NegPos -> i <> 0

let leq s1 s2 =
  match (s1, s2) with
  | Bot, _
  | _, Top
  | Neg, (Neg | NegZ | NegPos)
  | NegZ, NegZ
  | Zero, (Zero | NegZ | PosZ)
  | Pos, (Pos | PosZ | NegPos)
  | PosZ, PosZ
  | NegPos, NegPos -> true
  | Top, _ | _, Bot | PosZ, _ | Pos, _ | Zero, _ | NegZ, _ | Neg, _ | NegPos, _
    -> false

let join s1 s2 =
  match (s1, s2) with
  | Bot, s
  | s, Bot
  | (Top as s), _
  | _, (Top as s)
  | Neg, (Neg as s)
  | NegZ, (NegZ as s)
  | Zero, (Zero as s)
  | Pos, (Pos as s)
  | PosZ, (PosZ as s)
  | NegPos, (NegPos as s)
  | (Pos | Zero), (PosZ as s)
  | (PosZ as s), (Pos | Zero)
  | (Neg | Zero), (NegZ as s)
  | (NegZ as s), (Neg | Zero)
  | (Pos | Neg), (NegPos as s)
  | (NegPos as s), (Pos | Neg) -> s
  | Neg, Zero | Zero, Neg -> NegZ
  | Pos, Zero | Zero, Pos -> PosZ
  | Pos, Neg | Neg, Pos -> NegPos
  | (Pos | PosZ), (Neg | NegZ)
  | (Neg | NegZ), (Pos | PosZ)
  | NegPos, (Zero | NegZ | PosZ)
  | (Zero | NegZ | PosZ), NegPos -> Top

let widen = join

let meet s1 s2 =
  match (s1, s2) with
  | (Bot as s), _
  | _, (Bot as s)
  | Top, s
  | s, Top
  | Neg, (Neg as s)
  | NegZ, (NegZ as s)
  | Zero, (Zero as s)
  | Pos, (Pos as s)
  | PosZ, (PosZ as s)
  | (NegPos as s), NegPos
  | ((Pos | Zero) as s), PosZ
  | PosZ, ((Pos | Zero) as s)
  | ((Neg | Zero) as s), NegZ
  | NegZ, ((Neg | Zero) as s) -> s
  | NegZ, PosZ | PosZ, NegZ -> Zero
  | (Neg | NegZ), NegPos | NegPos, (Neg | NegZ) -> Neg
  | (Pos | PosZ), NegPos | NegPos, (Pos | PosZ) -> Pos
  | (Pos | PosZ | Zero), Neg
  | Neg, (Pos | PosZ | Zero)
  | Pos, (NegZ | Zero)
  | (NegZ | Zero), Pos
  | Zero, NegPos
  | NegPos, Zero -> Bot

let pp fmt = function
  | Bot -> Format.pp_print_string fmt "∅"
  | Top -> Format.pp_print_string fmt "⊤"
  | Zero -> Format.pp_print_string fmt "{0}"
  | PosZ -> Format.pp_print_string fmt "[0, +∞]"
  | Pos -> Format.pp_print_string fmt "[1, +∞]"
  | NegZ -> Format.pp_print_string fmt "[-∞, 0]"
  | Neg -> Format.pp_print_string fmt "[-∞, -1]"
  | NegPos -> Format.pp_print_string fmt "[-∞, -1] ∪ [1, +∞]"

let interval l u =
  if l <= u
  then
    if l = 0
    then if u = 0 then Zero else PosZ
    else if 0 < l
    then Pos
    else if (* l < 0 *)
            u = 0
    then NegZ
    else if u < 0
    then Neg
    else (* 0 < u *)
      Top
  else Bot

module Transfer = struct
  let plus s1 s2 =
    match (s1, s2) with
    | ((Bot | Top) as s), _ | _, ((Bot | Top) as s) | Zero, s | s, Zero -> s
    | (Pos | PosZ), (PosZ as s)
    | (PosZ as s), Pos
    | Pos, (Pos as s)
    | (Neg | NegZ), (NegZ as s)
    | (NegZ as s), Neg
    | Neg, (Neg as s) -> s
    | (Pos | PosZ), (Neg | NegZ)
    | (Neg | NegZ), (Pos | PosZ)
    | (Neg | NegZ | Pos | PosZ), NegPos
    | NegPos, (Neg | NegZ | Pos | PosZ)
    | NegPos, NegPos -> Top

  let neg = function
    | (Bot | Top | Zero) as s -> s
    | Pos -> Neg
    | Neg -> Pos
    | PosZ -> NegZ
    | NegZ -> PosZ
    | NegPos -> NegPos

  let minus s1 s2 = plus s1 (neg s2)

  let mult s1 s2 =
    match (s1, s2) with
    | ((Bot | Zero) as s), _
    | _, ((Bot | Zero) as s)
    | (Top as s), _
    | _, (Top as s)
    | (PosZ as s), (PosZ | Pos)
    | Pos, (PosZ as s)
    | (NegZ as s), (PosZ | Pos)
    | (PosZ | Pos), (NegZ as s)
    | (Neg as s), Pos
    | Pos, (Neg as s)
    | (Pos as s), Pos
    | (Pos | Neg | NegPos), (NegPos as s)
    | (NegPos as s), (Pos | Neg)
    | (NegPos as s), (PosZ | NegZ)
    | (PosZ | NegZ), (NegPos as s) -> s
    | Neg, Neg -> Pos
    | Neg, NegZ | NegZ, Neg | NegZ, NegZ -> PosZ
    | Neg, PosZ | PosZ, Neg -> NegZ

  let below = function
    | (Bot | Top | Neg | NegZ) as s -> s
    | Zero -> NegZ
    | Pos | PosZ | NegPos -> Top

  let below_strict = function
    | (Bot | Top | Neg) as s -> s
    | Zero | NegZ -> Neg
    | Pos | PosZ | NegPos -> Top

  let above = function
    | (Bot | Top | Pos | PosZ) as s -> s
    | Zero -> PosZ
    | Neg | NegZ | NegPos -> Top

  let above_strict = function
    | (Bot | Top | Pos) as s -> s
    | Zero | PosZ -> Pos
    | Neg | NegZ | NegPos -> Top

  let is_le s1 s2 =
    let s1' = meet (below s2) s1
    and s2' = meet (above s1) s2 in
    if is_bot s1' || is_bot s2' then (bot, bot) else (s1', s2')

  let is_lt s1 s2 =
    let s1' = meet (below_strict s2) s1
    and s2' = meet (above_strict s1) s2 in
    if is_bot s1' || is_bot s2' then (bot, bot) else (s1', s2')

  let is_eq s1 s2 =
    let s = meet s1 s2 in
    (s, s)

  let swap_pair (a, b) = (b, a)
  let combine_pair f g (a1, b1) (a2, b2) = (f a1 a2, g b1 b2)

  let le s1 s2 = function
    | true -> is_le s1 s2
    | false -> swap_pair @@ is_lt s2 s1

  let lt s1 s2 = function
    | true -> is_lt s1 s2
    | false -> swap_pair @@ is_le s2 s1

  let ge i1 i2 b = swap_pair @@ le i2 i1 b
  let gt i1 i2 b = swap_pair @@ lt i2 i1 b

  let eq i1 i2 = function
    | true -> is_eq i1 i2
    | false -> combine_pair join join (is_lt i1 i2) (swap_pair @@ is_lt i2 i1)

  let neq i1 i2 b = eq i1 i2 (not b)
end
