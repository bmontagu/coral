(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t =
  | Top
  | True
  | False
  | Bot
[@@deriving ord, eq]

let hash_fold t s =
  let i =
    match t with
    | Top -> 0
    | True -> 1
    | False -> 2
    | Bot -> 3
  in
  Hash.fold i s

let top = Top
let bot = Bot

let is_bot = function
  | Bot -> true
  | True | False | Top -> false

let is_top = function
  | Top -> true
  | True | False | Bot -> false

let leq bs1 bs2 =
  match (bs1, bs2) with
  | Bot, _ | _, Top | False, False | True, True -> true
  | _, Bot | Top, _ | True, False | False, True -> false

let mem b = function
  | Bot -> false
  | Top -> true
  | True -> b
  | False -> not b

let singleton b = if b then True else False

let join bs1 bs2 =
  match (bs1, bs2) with
  | Bot, bs | bs, Bot | (True as bs), True | (False as bs), False -> bs
  | Top, _ | _, Top | True, False | False, True -> top

let widen = join

let meet bs1 bs2 =
  match (bs1, bs2) with
  | (Bot as bs), _
  | _, (Bot as bs)
  | Top, bs
  | bs, Top
  | (True as bs), True
  | (False as bs), False -> bs
  | True, False | False, True -> bot

let pp fmt =
  let open Format in
  function
  | Bot -> pp_print_string fmt "∅"
  | True -> pp_print_string fmt "{ true }"
  | False -> pp_print_string fmt "{ false }"
  | Top -> pp_print_string fmt "{ true, false }"
