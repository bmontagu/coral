(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module type ABS_DOMAIN = sig
  type concrete
  type t

  val compare : t -> t -> int
  val equal : t -> t -> bool
  val hash_fold : t -> Hash.state -> Hash.state
  val bot : t
  val is_bot : t -> bool
  val top : t
  val is_top : t -> bool
  val singleton : concrete -> t
  val mem : concrete -> t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : t -> t -> t
  val meet : t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module type BOOL_DOMAIN = ABS_DOMAIN with type concrete := bool

module type INT_DOMAIN = sig
  include ABS_DOMAIN with type concrete := int

  val interval : int -> int -> t

  module Transfer : sig
    val neg : t -> t
    val plus : t -> t -> t
    val minus : t -> t -> t
    val mult : t -> t -> t
    val le : t -> t -> bool -> t * t
    val lt : t -> t -> bool -> t * t
    val ge : t -> t -> bool -> t * t
    val gt : t -> t -> bool -> t * t
    val eq : t -> t -> bool -> t * t
    val neq : t -> t -> bool -> t * t
  end
end
