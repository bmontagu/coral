(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

module IntBar = struct
  type t =
    | Int of int
    | NInf
    | PInf
  [@@deriving ord, eq]

  let hash_fold t s =
    match t with
    | Int n -> Hash.fold 0 @@ Hash.fold n s
    | NInf -> Hash.fold 1 s
    | PInf -> Hash.fold 2 s

  let top = PInf
  let bot = NInf

  let is_top = function
    | PInf -> true
    | Int _ | NInf -> false

  let is_bot = function
    | NInf -> true
    | Int _ | PInf -> false

  let leq i1 i2 =
    match (i1, i2) with
    | NInf, _ | _, PInf -> true
    | _, NInf | PInf, _ -> false
    | Int n1, Int n2 -> n1 <= n2

  let join i1 i2 =
    match (i1, i2) with
    | NInf, i | i, NInf -> i
    | PInf, _ | _, PInf -> top
    | Int n1, Int n2 -> Int (max n1 n2)

  let meet i1 i2 =
    match (i1, i2) with
    | PInf, i | i, PInf -> i
    | NInf, _ | _, NInf -> bot
    | Int n1, Int n2 -> Int (min n1 n2)

  let pp fmt =
    let open Format in
    function
    | NInf -> pp_print_string fmt "-∞"
    | PInf -> pp_print_string fmt "+∞"
    | Int n -> pp_print_int fmt n

  (** Returns a lower bound of the multiplication of [i1] and [i2].
      Assumes that [i1] and [i2] cannot be [Int n] where [n <= 0].
  *)
  let mult_below i1 i2 =
    match (i1, i2) with
    | Int n, _ when n <= 0 -> assert false
    | _, Int n when n <= 0 -> assert false
    (* from here, integers are necessarily > 0 *)
    | PInf, Int n | Int n, PInf -> Int n
    | NInf, Int _ | Int _, NInf -> NInf
    | PInf, PInf | NInf, NInf -> Int 0
    | PInf, NInf | NInf, PInf -> NInf
    | Int n1, Int n2 ->
      (* 0 < n1, 0 < n2 *)
      if n2 <= max_int / n1
      then Int (n1 * n2)
      else (* overflow *)
        Int (max n1 n2)

  (** Returns an upper bound of the multiplication of [i1] and [i2].
      Assumes that [i1] and [i2] cannot be [Int n] where [n <= 0].
  *)
  let mult_above i1 i2 =
    match (i1, i2) with
    | Int n, _ when n <= 0 -> assert false
    | _, Int n when n <= 0 -> assert false
    (* from here, integers are necessarily > 0 *)
    | PInf, Int _ | Int _, PInf -> PInf
    | NInf, Int n | Int n, NInf -> Int (-n)
    | PInf, PInf | NInf, NInf -> PInf
    | PInf, NInf | NInf, PInf -> Int 0
    | Int n1, Int n2 ->
      (* 0 < n1, 0 < n2 *)
      if n2 <= max_int / n1 then Int (n1 * n2) else (* overflow *)
                                                 PInf

  (** Returns a lower bound of the negation of [i] *)
  let neg_below = function
    | PInf -> NInf
    | NInf -> Int 0
    | Int n -> if n = min_int then Int max_int else Int (-n)

  (** Returns am upper bound of the negation of [i] *)
  let neg_above = function
    | PInf -> Int 0
    | NInf -> PInf
    | Int n -> if n = min_int then PInf else Int (-n)
end

type t = IntBar.t * IntBar.t [@@deriving ord, eq]

let hash_fold (l, u) s = IntBar.hash_fold l @@ IntBar.hash_fold u s
let top = (IntBar.bot, IntBar.top)
let bot = (IntBar.top, IntBar.bot)
let is_bot (i1, i2) = IntBar.(is_top i1 && is_bot i2)
let is_top (i1, i2) = IntBar.(is_bot i1 && is_top i2)

let leq ((l1, u1) as int1) (l2, u2) =
  is_bot int1 || IntBar.(leq l2 l1 && leq u1 u2)

let make l u =
  if (IntBar.is_bot l && IntBar.is_bot u) || (IntBar.is_top l && IntBar.is_top u)
  then top
  else if IntBar.leq l u
  then (l, u)
  else bot

let mem n (l, u) =
  let open IntBar in
  let i = Int n in
  leq l i && leq i u

let singleton n =
  let i = IntBar.Int n in
  (i, i)

let interval i j = if i <= j then IntBar.(Int i, Int j) else bot

let join ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1
  then int2
  else if is_bot int2
  then int1
  else IntBar.(meet l1 l2, join u1 u2)

let widen ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1
  then int2
  else if is_bot int2
  then int1
  else
    let open IntBar in
    let l = if leq l1 l2 then l1 else bot
    and u = if leq u2 u1 then u1 else top in
    make l u

let meet ((l1, u1) as int1) ((l2, u2) as int2) =
  if is_bot int1 || is_bot int2
  then bot
  else make (IntBar.join l1 l2) (IntBar.meet u1 u2)

let pp fmt ((l, u) as int) =
  let open Format in
  if is_bot int
  then pp_print_string fmt "∅"
  else if is_top int
  then pp_print_string fmt "⊤"
  else if IntBar.compare l u = 0
  then fprintf fmt "{%a}" IntBar.pp l
  else fprintf fmt "[%a, %a]" IntBar.pp l IntBar.pp u

module Transfer = struct
  let plus (l1, u1) (l2, u2) =
    let open IntBar in
    let l =
      match (l1, l2) with
      | NInf, _ | _, NInf -> bot
      | PInf, _ | _, PInf ->
        top (* one of the intervals is the bottom interval *)
      | Int n1, Int n2 ->
        let n12 = n1 + n2 in
        if 0 <= n2
        then
          if n1 <= n12
          then (* no overflow *)
            Int n12
          else (* addition overflowed *)
            Int n1
        else if n12 <= n1
        then (* no underflow *)
          Int n12
        else (* addition underflowed *)
          bot
    and u =
      match (u1, u2) with
      | NInf, _ | _, NInf ->
        bot (* one of the intervals is the bottom interval *)
      | PInf, _ | _, PInf -> top
      | Int n1, Int n2 ->
        let n12 = n1 + n2 in
        if 0 <= n2
        then
          if n1 <= n12
          then (* no overflow *)
            Int n12
          else (* addition overflowed *)
            top
        else if n12 <= n1
        then (* no underflow *)
          Int n12
        else (* addition underflowed *)
          Int n1
    in
    (l, u)

  let neg ((l, u) as int) =
    if is_bot int then bot else IntBar.(neg_below u, neg_above l)

  let minus int1 int2 = plus int1 (neg int2)
  let pos_int = IntBar.(Int 1, PInf)
  let neg_int = IntBar.(NInf, Int (-1))

  (** assumes that [0 < l1] and [0 < l2] when [int1] and [int2] are not [bot] *)
  let mult_pos_pos ((l1, u1) as int1) ((l2, u2) as int2) =
    if is_bot int1 || is_bot int2
    then bot
    else make (IntBar.mult_below l1 l2) (IntBar.mult_above u1 u2)

  (** assumes that [0 < l1] and [l2 < 0] when [int1] and [int2] are not [bot] *)
  let mult_pos_neg int1 int2 =
    if is_bot int1 || is_bot int2
    then bot
    else neg @@ mult_pos_pos int1 (neg int2)

  (** assumes that [l1 < 0] and [0 < l2] when [int1] and [int2] are not [bot] *)
  let mult_neg_pos int1 int2 = mult_pos_neg int2 int1

  (** assumes that [l1 < 0] and [l2 < 0] when [int1] and [int2] are not [bot] *)
  let mult_neg_neg int1 int2 = mult_pos_pos (neg int1) (neg int2)

  (** assumes that [0 < fst int1] when [int1] is not [bot] *)
  let mult_pos_left int1 int2 =
    if is_bot int1 || is_bot int2
    then bot
    else
      join (meet int2 (singleton 0))
      @@ join
           (mult_pos_pos int1 (meet int2 pos_int))
           (mult_pos_neg int1 (meet int2 neg_int))

  (** assumes that [snd int1 < 0] when [int1] is not [bot] *)
  let mult_neg_left int1 int2 =
    if is_bot int1 || is_bot int2
    then bot
    else
      join (meet int2 (singleton 0))
      @@ join
           (mult_neg_pos int1 (meet int2 pos_int))
           (mult_neg_neg int1 (meet int2 neg_int))

  let mult int1 int2 =
    if is_bot int1 || is_bot int2
    then bot
    else
      join (meet int1 (singleton 0))
      @@ join
           (mult_pos_left (meet int1 pos_int) int2)
           (mult_neg_left (meet int1 neg_int) int2)

  let below (_l, u) = make IntBar.NInf u
  let below_strict int = below (minus int (singleton 1))
  let above (l, _u) = make l IntBar.PInf
  let above_strict int = above (plus int (singleton 1))

  let is_le int1 int2 =
    let int1' = meet (below int2) int1
    and int2' = meet (above int1) int2 in
    if is_bot int1' || is_bot int2' then (bot, bot) else (int1', int2')

  let is_lt int1 int2 =
    let int1' = meet (below_strict int2) int1
    and int2' = meet (above_strict int1) int2 in
    if is_bot int1' || is_bot int2' then (bot, bot) else (int1', int2')

  let is_eq i1 i2 =
    let i = meet i1 i2 in
    (i, i)

  let swap_pair (a, b) = (b, a)
  let combine_pair f g (a1, b1) (a2, b2) = (f a1 a2, g b1 b2)

  let le int1 int2 = function
    | true -> is_le int1 int2
    | false -> swap_pair @@ is_lt int2 int1

  let lt int1 int2 = function
    | true -> is_lt int1 int2
    | false -> swap_pair @@ is_le int2 int1

  let ge i1 i2 b = swap_pair @@ le i2 i1 b
  let gt i1 i2 b = swap_pair @@ lt i2 i1 b

  let eq i1 i2 = function
    | true -> is_eq i1 i2
    | false -> combine_pair join join (is_lt i1 i2) (swap_pair @@ is_lt i2 i1)

  let neq i1 i2 b = eq i1 i2 (not b)
end
