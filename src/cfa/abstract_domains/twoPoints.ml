(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright © Inria 2020
*)

type t =
  | Bot
  | Top
[@@deriving ord, eq]

let hash_fold t s =
  let i =
    match t with
    | Bot -> 0
    | Top -> 1
  in
  Hash.fold i s

let top = Top
let bot = Bot

let is_bot = function
  | Bot -> true
  | _ -> false

let is_top = function
  | Top -> true
  | _ -> false

let leq i1 i2 =
  match (i1, i2) with
  | Bot, _ | _, Top -> true
  | Top, Bot -> false

let mem _ = function
  | Bot -> false
  | Top -> true

let singleton _ = top
let interval i j = if i <= j then top else bot

let join i1 i2 =
  match (i1, i2) with
  | Bot, i | i, Bot | _, (Top as i) -> i

let widen = join

let meet i1 i2 =
  match (i1, i2) with
  | Top, i | i, Top | _, (Bot as i) -> i

let pp fmt i =
  let open Format in
  match i with
  | Bot -> pp_print_string fmt "∅"
  | Top -> pp_print_string fmt "⊤"

module Transfer = struct
  let plus i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> bot
    | Top, Top -> top

  let neg = function
    | (Bot | Top) as i -> i

  let minus i1 i2 = plus i1 (neg i2)

  let mult i1 i2 =
    match (i1, i2) with
    | (Bot as i), _ | _, (Bot as i) | (Top as i), _ -> i

  let is_le i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> (bot, bot)
    | _ -> (i1, i2)

  let is_lt i1 i2 =
    match (i1, i2) with
    | Bot, _ | _, Bot -> (bot, bot)
    | _ -> (i1, i2)

  let is_eq i1 i2 =
    let i = meet i1 i2 in
    (i, i)

  let swap_pair (a, b) = (b, a)
  let combine_pair f g (a1, b1) (a2, b2) = (f a1 a2, g b1 b2)

  let le i1 i2 = function
    | true -> is_le i1 i2
    | false -> swap_pair @@ is_lt i2 i1

  let lt i1 i2 = function
    | true -> is_lt i1 i2
    | false -> swap_pair @@ is_le i2 i1

  let ge i1 i2 b = swap_pair @@ le i2 i1 b
  let gt i1 i2 b = swap_pair @@ lt i2 i1 b

  let eq i1 i2 = function
    | true -> is_eq i1 i2
    | false -> combine_pair join join (is_lt i1 i2) (swap_pair @@ is_lt i2 i1)

  let neq i1 i2 b = eq i1 i2 (not b)
end
