(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

%{
open Ast.Outer

let locate pos2 x = Location.(locate (from_pos pos2) x)

let maybe_ascribe e oty =
  match oty with
  | None -> e
  | Some ty ->
    Location.locate Location.(from_pos (get_start ty.loc, get_end e.loc))
    @@ Ascribe (e, ty)

%}

%token EOF
%token VAL TYPE STAR OF
%token LET EQUAL IN MATCH WITH BEGIN END FUN REC
%token VBAR UNIT LPAR RPAR COMMA SEMI COLON LBRACE RBRACE UNDERSCORE RARROW DOT QMARK
%token BTRUE BFALSE BANDALSO BORELSE IF THEN ELSE
%token NEQ LT GT LE GE PLUS MINUS DIV
%token<string> TYPVAR ID CONS
%token<int> INT

%start<Ast.Outer.program> program

%%

%inline var:
| x = ID
{ x }

%inline typid:
| x = ID
{ x }

%inline field:
| f = ID
{ f }

%inline cons:
| x = CONS
{ x }

%inline located(x):
| x = x
{ locate $loc(x) x }

atomic_pattern:
| x = var
{ locate $loc @@ PVar x }
| UNDERSCORE
{ locate $loc @@ PWild }
| BTRUE
{ locate $loc @@ PBool true }
| BFALSE
{ locate $loc @@ PBool false }
| c = located(cons) _x = UNIT
{ locate $loc @@ PConstructor(c, [locate $loc(_x) @@ PTuple []]) }
| c = located(cons)
  args = loption(delimited(LPAR, separated_nonempty_list(COMMA, atomic_pattern), RPAR))
{ locate $loc @@ PConstructor(c, args) }
| c = located(cons)
  l = delimited(LBRACE, separated_nonempty_list(SEMI, field_pattern), RBRACE)
{ locate $loc @@ PConstructor(c, [locate $loc(l) @@ PRecord l]) }
| c = located(cons) x = var
{ locate $loc @@ PConstructor(c, [locate $loc(x) @@ PVar x]) }
| c = located(cons) _uscore = UNDERSCORE
{ locate $loc @@ PConstructor(c, [locate $loc(_uscore) @@ PWild]) }
| c = located(cons) c2 = located(cons)
{ locate $loc @@ PConstructor(c, [locate $loc(c2) @@ PConstructor (c2, [])]) }
| UNIT
{ locate $loc @@ PTuple [] }
| args = delimited(LPAR, separated_list(COMMA, atomic_pattern), RPAR)
{ match args with
  | [p] -> p
  | _ -> locate $loc @@ PTuple args
}
| l = delimited(LBRACE, separated_nonempty_list(SEMI, field_pattern), RBRACE)
{ locate $loc @@ PRecord l }
| LPAR p = pair_pattern VBAR ps = separated_nonempty_list(VBAR, pair_pattern)
  RPAR
{ locate $loc @@ POr (p :: ps) }

pair_pattern:
| ps = separated_nonempty_list(COMMA, atomic_pattern)
{ match ps with
  | [p] -> p
  | _ -> locate $loc @@ PTuple ps
}

pattern:
| ps = separated_nonempty_list(VBAR, pair_pattern)
{ match ps with
  | [p] -> p
  | _ -> locate $loc @@ POr ps
}

%inline field_pattern:
| f = located(field) EQUAL p = pattern
{ (f, p) }

%inline branch(e):
| p = pattern RARROW e = e
{ (p, e) }

%inline tuple_exprs:
| e = expr COMMA l = separated_nonempty_list(COMMA, expr)
{ e :: l }

%inline field_expr:
| f = located(field) EQUAL e = expr
{ (f, e) }

atomic_expr:
| e = atomic_expr_no_arith_unary
| e = arith_unary_expr
{ e }

arith_unary_expr:
| MINUS e = atomic_expr
{ locate $loc @@ Unop (UMINUS, e) }

atomic_expr_no_arith_unary:
| e = atomic_expr_no_tuples_no_arith_unary
{ e }
| es = delimited(LPAR, tuple_exprs, RPAR)
{ locate $loc @@ Tuple es }

atomic_expr_no_tuples_no_arith_unary:
| e = atomic_expr_no_cons_no_tuples_no_arith_unary
{ e }
| c = located(cons)
{ locate $loc @@ Constructor(c, []) }

atomic_expr_no_tuples:
| e = atomic_expr_no_tuples_no_arith_unary
| e = arith_unary_expr
{ e }

atomic_expr_no_cons_no_tuples:
| e = atomic_expr_no_cons_no_tuples_no_arith_unary
| e = arith_unary_expr
{ e }

atomic_expr_no_cons_no_tuples_no_arith_unary:
| UNIT
{ locate $loc @@ Tuple [] }
| n = INT
{ locate $loc @@ Int n }
| BTRUE
{ locate $loc @@ Bool true }
| BFALSE
{ locate $loc @@ Bool false }
| x = var
{ locate $loc @@ Var x }
| fs = delimited(LBRACE, separated_nonempty_list(SEMI, field_expr), RBRACE)
{ locate $loc @@ Record fs }
| LBRACE e = sum_expr WITH
  fs = separated_nonempty_list(SEMI, field_expr)
  RBRACE
{ locate $loc @@ RecordUpdate (e, fs) }
| e = atomic_expr_no_arith_unary DOT f = located(field)
{ locate $loc @@ RecordProj(e, f) }
| e = atomic_expr_no_arith_unary DOT LPAR i = located(INT) COMMA j = located(INT) RPAR
{ locate $loc @@ TupleProj(e, i, j) }
| QMARK
{ locate $loc @@ Unknown }
| MATCH e = tuple_expr WITH
  VBAR? ps = separated_nonempty_list(VBAR, branch(expr)) END
{ locate $loc @@ Match(e, ps) }
| e = delimited(BEGIN, expr, END)
{ locate $loc @@ Parens(BeginEnd, e) }
| e = delimited(LPAR, expr, RPAR)
{ locate $loc @@ Parens(Parentheses, e) }

app_expr:
| e = atomic_expr_no_cons_no_tuples
{ e }
| c = located(cons) args = delimited(LPAR, tuple_exprs, RPAR)
{ locate $loc @@ Constructor(c, args) }
| c = located(cons) arg = atomic_expr_no_tuples
{ locate $loc @@ Constructor(c, [arg]) }
| e1 = app_expr e2 = atomic_expr_no_arith_unary
{ locate $loc @@ App(e1, e2) }

div_expr:
| e = app_expr
{ e }
| e1 = app_expr DIV e2 = div_expr
{ locate $loc @@ Binop(DIV, e1, e2) }

factor_expr:
| e = div_expr
{ e }
| e1 = div_expr STAR e2 = factor_expr
{ locate $loc @@ Binop(TIMES, e1, e2) }

diff_expr:
| e = factor_expr
{ e }
| e1 = diff_expr MINUS e2 = factor_expr
{ locate $loc @@ Binop(MINUS, e1, e2) }

sum_expr:
| e = diff_expr
{ e }
| e1 = diff_expr PLUS e2 = sum_expr
{ locate $loc @@ Binop(PLUS, e1, e2) }

cmp:
| NEQ { NEQ }
| LT { LT }
| GT { GT }
| EQUAL { EQ }
| LE { LE }
| GE { GE }

cmp_expr:
| e = sum_expr
{ e }
| e1 = sum_expr op = cmp e2 = cmp_expr
{ locate $loc @@ Binop(op, e1, e2) }

bool_factor_expr:
| e = cmp_expr
{ e }
| e1 = cmp_expr BANDALSO e2 = bool_factor_expr
{ locate $loc @@ BAndAlso (e1, e2) }

bool_sum_expr:
| e = bool_factor_expr
{ e }
| e1 = bool_factor_expr BORELSE e2 = bool_sum_expr
{ locate $loc @@ BOrElse (e1, e2) }

tuple_expr:
| es = separated_nonempty_list(COMMA, expr)
{ match es with
  | [e] -> e
  | _ -> locate $loc @@ Tuple es
}

expr:
| e = bool_sum_expr
{ e }
| c = located(cons)
{ locate $loc @@ Constructor(c, []) }
| es = delimited(LPAR, tuple_exprs, RPAR)
{ locate $loc @@ Tuple es }
| LET x = var
  oty = option(preceded(COLON, type_expr))
  EQUAL e1 = expr
  IN e2 = expr
{ locate $loc @@ Let(x, maybe_ascribe e1 oty, e2)
}
| FUN xs = params+
  oty_res = option(preceded(COLON, type_expr0))
  RARROW e = expr
{ List.fold_right (fun x acc ->
    let location =
      Location.(get_start x.loc, snd $loc)
    in
    locate location @@ Fun(x, acc))
    xs
    (maybe_ascribe e oty_res)
}
| LET f = var xs = params+
  oty = option(preceded(COLON, type_expr))
  EQUAL e1 = expr
  IN e2 = expr
{ let e1' = List.fold_right (fun x acc ->
    let location =
      Location.(get_start x.loc, snd $loc)
    in
    locate location @@ Fun(x, acc))
    xs
    (maybe_ascribe e1 oty)
  in
  locate $loc @@ Let(f, e1', e2)
}
| LET REC f = var xs = params+
  oty_res = option(preceded(COLON, type_expr))
  EQUAL e1 = expr IN e2 = expr
{ let e1' =
    match xs with
    | [] -> assert false
    | args::argss ->
    let e =
      List.fold_right (fun x acc ->
        let location =
          Location.(get_start x.loc, snd $loc)
        in
        locate location @@ Fun(x, acc))
      argss
      (maybe_ascribe e1 oty_res)
    in
    locate $loc @@ FixFun(f, args, e)
  in
  locate $loc @@ Let(f, e1', e2)
}
| IF e1 = expr THEN e2 = expr ELSE e3 = expr
{ locate $loc @@ IfThenElse (e1, e2, e3) }

param:
| x = located(var)
{ (x, None) }
| x = located(var) COLON ty = type_expr
{ (x, Some ty) }

params:
| x = located(var)
{ locate $loc [(x, None)] }
| UNIT
{ locate $loc [] }
| xs = located(delimited(LPAR, separated_nonempty_list(COMMA, param), RPAR))
{ xs }

val_decl:
| VAL x = located(var)
  oty = option(preceded(COLON, type_expr))
  EQUAL e = expr
{ DeclVal(x, maybe_ascribe e oty) }
| VAL x = located(var) argss = located(params+)
  oty = option(preceded(COLON, type_expr))
  EQUAL e = expr
{ DeclFun(x, argss, maybe_ascribe e oty) }
| VAL REC x = located(var) argss = located(params+)
  oty = option(preceded(COLON, type_expr))
  EQUAL e = expr
{ DeclRecFun(x, argss, maybe_ascribe e oty) }

type_expr_atomic:
| x = typid
{ locate $loc @@ TyId (x, []) }
| x = TYPVAR
{ locate $loc @@ TyVar x }
| LPAR ty = type_expr RPAR
{ locate $loc @@ TyParens ty }

type_expr0:
| ty = type_expr_atomic
{ ty }
| arg = type_expr0 x = typid
{ locate $loc @@ TyId (x, [arg]) }
| LPAR arg1 = type_expr COMMA args = separated_nonempty_list(COMMA, type_expr)
  RPAR x = typid
{ locate $loc @@ TyId (x, arg1 :: args) }

type_expr1:
| ty = type_expr_atomic
{ ty }
| ty1 = type_expr_atomic RARROW ty2 = type_expr1
{ locate $loc @@ TyFun(ty1, ty2) }

type_expr:
| ty = type_expr1
{ ty }
| ty = type_expr1 STAR tys = separated_nonempty_list(STAR, type_expr1)
{ locate $loc @@ TyTuple(ty :: tys) }

field_type_expr:
| f = located(field) COLON ty = type_expr
{ (f, ty) }

record_type_expr:
| fs = delimited(LBRACE, separated_nonempty_list(SEMI, field_type_expr), RBRACE)
{ fs }

case_type_expr:
| c = located(cons)
{ (c, []) }
| c = located(cons) OF args = separated_nonempty_list(STAR, type_expr0)
{ (c, args) }

variant_type_expr:
| VBAR? cases = separated_nonempty_list(VBAR, case_type_expr)
{ cases }

%inline typ_var_args:
|
{ [] }
| arg = TYPVAR
{ [arg] }
| args = delimited(LPAR, separated_nonempty_list(COMMA, TYPVAR), RPAR)
{ args }

type_decl:
| TYPE args = typ_var_args x = located(var) EQUAL ty = type_expr
{ DeclTyp (x, TyDefAlias (args, ty)) }
| TYPE args = typ_var_args x = located(var) EQUAL fs = record_type_expr
{ DeclTyp (x, TyDefRecord (args, fs)) }
| TYPE args = typ_var_args x = located(var) EQUAL cs = variant_type_expr
{ DeclTyp (x, TyDefVariant (args, cs)) }

decl:
| d = val_decl
| d = type_decl
{ d }

program:
| ds = decl* EOF
{ ds }
