(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

module Int = struct
  type t = int

  let equal : int -> int -> bool = ( = )
  let compare : int -> int -> int = compare
end

module Tag = struct
  include String

  let pp = Format.pp_print_string
  let true_ = "true"
  let false_ = "false"
end

module Field = struct
  include String

  let pp = Format.pp_print_string
end

module Var = struct
  include String

  let pp = Format.pp_print_string
end

module TyId = struct
  include String

  let pp = Format.pp_print_string
end

module TyVar = struct
  include String

  let pp = Format.pp_print_string
end

module FieldMap = Map.Make (Field)
module TagMap = Map.Make (Tag)
module VarMap = Map.Make (Var)
module TyVarMap = Map.Make (TyVar)
module TyIdMap = Map.Make (TyId)
module IntMap = Map.Make (Int)

let bool_arity =
  TagMap.add Tag.true_ 0 @@ TagMap.add Tag.false_ 0 @@ TagMap.empty

type binop =
  | NEQ
  | LT
  | GT
  | EQ
  | LE
  | GE
  | PLUS
  | MINUS
  | TIMES
  | DIV

type unop = UMINUS

let pp_binop fmt = function
  | NEQ -> Format.pp_print_string fmt "<>"
  | LT -> Format.pp_print_string fmt "<"
  | GT -> Format.pp_print_string fmt ">"
  | EQ -> Format.pp_print_string fmt "="
  | LE -> Format.pp_print_string fmt "<="
  | GE -> Format.pp_print_string fmt ">="
  | PLUS -> Format.pp_print_string fmt "+"
  | MINUS -> Format.pp_print_string fmt "-"
  | TIMES -> Format.pp_print_string fmt "*"
  | DIV -> Format.pp_print_string fmt "/"

let pp_unop fmt = function
  | UMINUS -> Format.pp_print_string fmt "-"

module Outer = struct
  type typ = typ_ Location.located

  and typ_ =
    | TyVar of TyVar.t
    | TyId of TyId.t * typ list
    | TyTuple of typ list
    | TyFun of typ * typ
    | TyParens of typ

  type typ_def =
    | TyDefAlias of TyVar.t list * typ
    | TyDefRecord of TyVar.t list * (Field.t Location.located * typ) list
    | TyDefVariant of TyVar.t list * (Tag.t Location.located * typ list) list

  type fun_arg = Var.t Location.located * typ option
  type fun_args = fun_arg list Location.located

  type pattern = pattern_ Location.located

  and pattern_ =
    | PVar of Var.t
    | PWild
    | PConstructor of Tag.t Location.located * pattern list
    | PTuple of pattern list
    | PRecord of (Field.t Location.located * pattern) list
    | POr of pattern list
    | PBool of bool

  type parens_style =
    | Parentheses
    | BeginEnd

  type expr = expr_ Location.located

  and expr_ =
    | Var of Var.t
    | Let of Var.t * expr * expr
    | Tuple of expr list
    | TupleProj of expr * int Location.located * int Location.located
    | Record of (Field.t Location.located * expr) list
    | RecordProj of expr * Field.t Location.located
    | RecordUpdate of expr * (Field.t Location.located * expr) list
    | Constructor of Tag.t Location.located * expr list
    | Match of expr * branches
    | App of expr * expr
    | Fun of fun_args * expr
    | FixFun of Var.t * fun_args * expr
    | Parens of parens_style * expr
    | Unknown
    | Ascribe of expr * typ
    | Bool of bool
    | BOrElse of expr * expr
    | BAndAlso of expr * expr
    | IfThenElse of expr * expr * expr
    | Int of int
    | Binop of binop * expr * expr
    | Unop of unop * expr

  and branch = pattern * expr
  and branches = branch list

  type decl =
    | DeclTyp of TyId.t Location.located * typ_def
    | DeclVal of Var.t Location.located * expr
    | DeclFun of Var.t Location.located * fun_args list Location.located * expr
    | DeclRecFun of
        Var.t Location.located * fun_args list Location.located * expr

  type program = decl list

  let rec pp_typ fmt loc_typ = pp_typ_ fmt loc_typ.Location.contents

  and pp_typ_ fmt =
    let open Format in
    function
    | TyVar x -> TyVar.pp fmt x
    | TyId (x, []) -> TyId.pp fmt x
    | TyId (x, [arg]) -> (
      match arg.Location.contents with
      | TyFun _ -> fprintf fmt "@[(%a)@ %a@]" pp_typ arg TyId.pp x
      | _ -> fprintf fmt "@[%a@ %a@]" pp_typ arg TyId.pp x)
    | TyId (x, args) ->
      fprintf
        fmt
        "@[(%a)@ %a@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_typ)
        args
        TyId.pp
        x
    | TyTuple [] -> fprintf fmt "unit"
    | TyTuple tys ->
      fprintf
        fmt
        "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt " *@ ") pp_typ)
        tys
    | TyFun (ty1, ty2) -> fprintf fmt "@[<hv>%a ->@ %a@]" pp_typ ty1 pp_typ ty2
    | TyParens ty -> fprintf fmt "@[(%a)@]" pp_typ ty

  let pp_fun_arg fmt ({ Location.contents = x; _ }, oty) =
    match oty with
    | Some ty -> Format.fprintf fmt "@[%a:@ %a@]" Var.pp x pp_typ ty
    | None -> Var.pp fmt x

  let pp_fun_args fmt args =
    let open Format in
    pp_print_list
      ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
      pp_fun_arg
      fmt
      args.Location.contents

  let rec pp_pattern fmt loc_pat = pp_pattern_ fmt loc_pat.Location.contents

  and pp_pattern_ fmt =
    let open Format in
    function
    | PVar x -> Var.pp fmt x
    | PWild -> pp_print_string fmt "_"
    | PConstructor ({ Location.contents = t; _ }, ps) ->
      fprintf
        fmt
        "@[%a(@[%a@]@,)@]"
        Tag.pp
        t
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_pattern)
        ps
    | PTuple ps ->
      fprintf
        fmt
        "(@[%a@]@,)"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_pattern)
        ps
    | PRecord ps ->
      fprintf
        fmt
        "{@[%a@]@,}"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
           (fun fmt ({ Location.contents = f; _ }, p) ->
             fprintf fmt "@[<hv 2>%a =@ %a@]" Field.pp f pp_pattern p))
        ps
    | POr ps ->
      fprintf
        fmt
        "@[%a@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ | ") pp_pattern)
        ps
    | PBool b -> if b then fprintf fmt "true" else fprintf fmt "false"

  let rec pp_expr fmt loc_expr = pp_expr_ fmt loc_expr.Location.contents

  and pp_expr_ fmt =
    let open Format in
    function
    | Var x -> Var.pp fmt x
    | Let (x, e1, e2) ->
      fprintf fmt "@[let %a =@ %a@ in@ %a@]" Var.pp x pp_expr e1 pp_expr e2
    | Tuple es ->
      fprintf
        fmt
        "@[(%a@,)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_expr)
        es
    | TupleProj (e, { Location.contents = i; _ }, { Location.contents = j; _ })
      -> fprintf fmt "@[%a.(%i,%i)@]" pp_expr e i j
    | Record fs ->
      fprintf
        fmt
        "@[{%a@,}@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, e) ->
             fprintf fmt "@[%a =@ %a@]" Field.pp f pp_expr e))
        fs
    | RecordProj (e, { Location.contents = f; _ }) ->
      fprintf fmt "@[%a.%a@]" pp_expr e Field.pp f
    | RecordUpdate (e, fs) ->
      fprintf
        fmt
        "@[{%a@ with@ %a@,}@]"
        pp_expr
        e
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, e) ->
             fprintf fmt "@[%a =@ %a@]" Field.pp f pp_expr e))
        fs
    | Constructor ({ Location.contents = c; _ }, []) ->
      fprintf fmt "@[%a@]" Tag.pp c
    | Constructor ({ Location.contents = c; _ }, [e]) ->
      fprintf fmt "@[%a@ %a@]" Tag.pp c pp_expr e
    | Constructor ({ Location.contents = c; _ }, args) ->
      fprintf fmt "@[%a@ %a@]" Tag.pp c pp_expr_ (Tuple args)
    | Match (e, bs) ->
      fprintf fmt "@[match@ %a@ with@ %a@]" pp_expr e pp_branches bs
    | App (e1, e2) -> fprintf fmt "@[%a@ %a@]" pp_expr e1 pp_expr e2
    | Parens (Parentheses, e) -> fprintf fmt "@[(%a)@]" pp_expr e
    | Fun (fun_args, e) ->
      fprintf fmt "@[<hv>fun@ @[(%a)@] ->@ %a@]" pp_fun_args fun_args pp_expr e
    | FixFun (f, fun_args, e) ->
      fprintf
        fmt
        "@[<hv>fixfun@ %a @[(%a)@]@ ->@ %a@]"
        Var.pp
        f
        pp_fun_args
        fun_args
        pp_expr
        e
    | Parens (BeginEnd, e) -> fprintf fmt "@[begin@ %a@ end@]" pp_expr e
    | Unknown -> fprintf fmt "?"
    | Ascribe (e, typ) -> fprintf fmt "@[(%a:@ %a)@]" pp_expr e pp_typ typ
    | Bool b -> if b then fprintf fmt "true" else fprintf fmt "false"
    | BOrElse (e1, e2) -> fprintf fmt "@[(%a)@ ||@ (%a)@]" pp_expr e1 pp_expr e2
    | BAndAlso (e1, e2) ->
      fprintf fmt "@[(%a)@ &&@ (%a)@]" pp_expr e1 pp_expr e2
    | IfThenElse (e1, e2, e3) ->
      fprintf
        fmt
        "@[if@ %a@ then@ %a@ else@ %a@]"
        pp_expr
        e1
        pp_expr
        e2
        pp_expr
        e3
    | Int n -> pp_print_int fmt n
    | Binop (op, e1, e2) ->
      fprintf fmt "@[(%a)@ %a@ (%a)@]" pp_expr e1 pp_binop op pp_expr e2
    | Unop (op, e) -> fprintf fmt "@[%a@ (%a)@]" pp_unop op pp_expr e

  and pp_branch fmt (p, e) =
    Format.fprintf fmt "@[| %a ->@ %a@]" pp_pattern p pp_expr e

  and pp_branches fmt l =
    let open Format in
    fprintf
      fmt
      "@[%a@]"
      (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@,") pp_branch)
      l

  let pp_typ_args fmt =
    let open Format in
    function
    | [] -> ()
    | [arg] -> TyVar.pp fmt arg
    | args ->
      fprintf
        fmt
        "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") TyVar.pp)
        args

  let pp_typ_def fmt =
    let open Format in
    function
    | TyDefAlias ([], ty) -> pp_typ fmt ty
    | TyDefAlias (args, ty) ->
      fprintf fmt "@[%a.@ %a@]" pp_typ_args args pp_typ ty
    | TyDefRecord ([], fs) ->
      fprintf
        fmt
        "@[{%a@,}@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, ty) ->
             fprintf fmt "@[%a:@ %a@]" Field.pp f pp_typ ty))
        fs
    | TyDefRecord (args, fs) ->
      fprintf
        fmt
        "@[%a.@ {%a@,}@]"
        pp_typ_args
        args
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, ty) ->
             fprintf fmt "@[%a:@ %a@]" Field.pp f pp_typ ty))
        fs
    | TyDefVariant ([], cs) ->
      pp_print_list
        ~pp_sep:pp_print_space
        (fun fmt ({ Location.contents = c; _ }, args) ->
          match args with
          | [] -> Tag.pp fmt c
          | _ ->
            fprintf
              fmt
              "@[%a of@ %a@]"
              Tag.pp
              c
              (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt " *@ ") pp_typ)
              args)
        fmt
        cs
    | TyDefVariant (args, cs) ->
      fprintf
        fmt
        "@[%a.@ %a@]"
        pp_typ_args
        args
        (pp_print_list
           ~pp_sep:pp_print_space
           (fun fmt ({ Location.contents = c; _ }, args) ->
             match args with
             | [] -> Tag.pp fmt c
             | _ ->
               fprintf
                 fmt
                 "@[%a of@ %a@]"
                 Tag.pp
                 c
                 (pp_print_list
                    ~pp_sep:(fun fmt () -> fprintf fmt " *@ ")
                    pp_typ)
                 args))
        cs

  let pp_decl fmt =
    let open Format in
    function
    | DeclTyp (x, TyDefAlias ([], ty)) ->
      fprintf fmt "@[type@ %a@ =@ %a@]" TyId.pp x.Location.contents pp_typ ty
    | DeclTyp (x, TyDefAlias (args, ty)) ->
      fprintf
        fmt
        "@[type@ %a@ %a@ =@ %a@]"
        pp_typ_args
        args
        TyId.pp
        x.Location.contents
        pp_typ
        ty
    | DeclTyp (x, TyDefRecord ([], fs)) ->
      fprintf
        fmt
        "@[type@ %a@ =@ {%a@,}@]"
        TyId.pp
        x.Location.contents
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, ty) ->
             fprintf fmt "@[%a:@ %a@]" Field.pp f pp_typ ty))
        fs
    | DeclTyp (x, TyDefRecord (args, fs)) ->
      fprintf
        fmt
        "@[type@ %a@ %a@ =@ {%a@,}@]"
        pp_typ_args
        args
        TyId.pp
        x.Location.contents
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt ({ Location.contents = f; _ }, ty) ->
             fprintf fmt "@[%a:@ %a@]" Field.pp f pp_typ ty))
        fs
    | DeclTyp (x, TyDefVariant ([], cs)) ->
      fprintf
        fmt
        "@[type@ %a@ =@ %a@]"
        TyId.pp
        x.Location.contents
        (pp_print_list
           ~pp_sep:pp_print_space
           (fun fmt ({ Location.contents = c; _ }, args) ->
             match args with
             | [] -> Tag.pp fmt c
             | _ ->
               fprintf
                 fmt
                 "@[%a of@ %a@]"
                 Tag.pp
                 c
                 (pp_print_list
                    ~pp_sep:(fun fmt () -> fprintf fmt " *@ ")
                    pp_typ)
                 args))
        cs
    | DeclTyp (x, TyDefVariant (args, cs)) ->
      fprintf
        fmt
        "@[type@ %a@ %a@ =@ %a@]"
        pp_typ_args
        args
        TyId.pp
        x.Location.contents
        (pp_print_list
           ~pp_sep:pp_print_space
           (fun fmt ({ Location.contents = c; _ }, args) ->
             match args with
             | [] -> Tag.pp fmt c
             | _ ->
               fprintf
                 fmt
                 "@[%a of@ %a@]"
                 Tag.pp
                 c
                 (pp_print_list
                    ~pp_sep:(fun fmt () -> fprintf fmt " *@ ")
                    pp_typ)
                 args))
        cs
    | DeclVal (x, e) ->
      fprintf fmt "@[let@ %a@ =@ %a@]" Var.pp x.Location.contents pp_expr e
    | DeclFun (x, { Location.contents = argss; _ }, e) ->
      fprintf
        fmt
        "@[let@ %a@ @[(%a)@]@ =@ %a@]"
        Var.pp
        x.Location.contents
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_fun_args)
        argss
        pp_expr
        e
    | DeclRecFun (x, { Location.contents = argss; _ }, e) ->
      fprintf
        fmt
        "@[let rec@ %a@ @[(%a)@]@ =@ %a@]"
        Var.pp
        x.Location.contents
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ ") pp_fun_args)
        argss
        pp_expr
        e

  let pp_program fmt decls =
    let open Format in
    fprintf
      fmt
      "@[<v>%a@]"
      (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@.@.") pp_decl)
      decls
end

module Inner = struct
  type typ =
    | TyUnifVar of int
    | TyVar of TyVar.t
    | TyId of TyId.t * typ list
    | TyTuple of typ list
    | TyFun of typ * typ
    | TyBool
    | TyInt

  type typ_scheme = TyVar.t list * typ

  type typ_body =
    | TyBodyAlias of TyVar.t list * typ
    | TyBodyRecord of TyVar.t list * typ FieldMap.t
    | TyBodyVariant of TyVar.t list * typ list TagMap.t

  type pattern = pattern_ Location.located

  and pattern_ =
    | PVar of Var.t
    | PWild
    | PConstructor of Tag.t * pattern list * (Tag.t * int) list
    | PTuple of pattern list
    | PRecord of (Field.t * pattern) list * Field.t list
    | POr of pattern list
    | PBool of bool

  type expr = expr_ Location.located

  and expr_ =
    | Var of Var.t
    | Let of Var.t * expr * expr
    | Tuple of expr list
    | TupleProj of expr * int * int
        (** expression, index to project on, length of the tuple *)
    | Record of (Field.t * expr) list
    | RecordProj of expr * Field.t * Field.t list
        (** expression, field to project on, fields of the record *)
    | RecordUpdate of expr * Field.t * expr * Field.t list
        (** expression, field to update, fields of the record *)
    | Constructor of Tag.t * expr list * (Tag.t * int) list
        (** constructor, arguments *)
    | Match of expr * branches
    | Fun of Var.t Location.located * typ * expr
    | FixFun of Var.t * Var.t Location.located * typ * expr
    | App of expr * expr
    | Unknown
    | Bool of bool
    | BAndAlso of expr * expr
    | BOrElse of expr * expr
    | IfThenElse of expr * expr * expr
    | Int of int
    | Binop of binop * expr * expr
    | Unop of unop * expr

  and branch = pattern * expr
  and branches = branch list

  type decl =
    | DeclTyp of TyId.t * typ_body
    | DeclVal of Var.t * typ_scheme * expr

  type program = decl list

  let rec equal_typ ty1 ty2 =
    match (ty1, ty2) with
    | TyUnifVar x1, TyUnifVar x2 -> x1 = x2
    | TyVar x1, TyVar x2 -> TyId.equal x1 x2
    | TyId (x1, tys1), TyId (x2, tys2) -> (
      TyId.equal x1 x2
      &&
      try List.for_all2 equal_typ tys1 tys2 with Invalid_argument _ -> false)
    | TyTuple tys1, TyTuple tys2 -> (
      try List.for_all2 equal_typ tys1 tys2 with Invalid_argument _ -> false)
    | TyFun (dom1, rng1), TyFun (dom2, rng2) ->
      equal_typ dom1 dom2 && equal_typ rng1 rng2
    | TyBool, TyBool -> true
    | TyInt, TyInt -> true
    | TyUnifVar _, (TyVar _ | TyId _ | TyTuple _ | TyFun _ | TyBool | TyInt)
    | TyVar _, (TyUnifVar _ | TyId _ | TyTuple _ | TyFun _ | TyBool | TyInt)
    | TyId _, (TyUnifVar _ | TyVar _ | TyTuple _ | TyFun _ | TyBool | TyInt)
    | TyTuple _, (TyUnifVar _ | TyVar _ | TyId _ | TyFun _ | TyBool | TyInt)
    | TyFun _, (TyUnifVar _ | TyVar _ | TyId _ | TyTuple _ | TyBool | TyInt)
    | TyBool, (TyUnifVar _ | TyVar _ | TyId _ | TyTuple _ | TyFun _ | TyInt)
    | TyInt, (TyUnifVar _ | TyVar _ | TyId _ | TyTuple _ | TyFun _ | TyBool) ->
      false

  let rec typ_subst tyenv = function
    | TyUnifVar _ as ty -> ty
    | TyVar x as ty -> (
      match TyVarMap.find_opt x tyenv with
      | Some tyx -> tyx
      | None -> ty)
    | TyId (x, tys) -> TyId (x, List.map (typ_subst tyenv) tys)
    | TyTuple tys -> TyTuple (List.map (typ_subst tyenv) tys)
    | TyFun (ty1, ty2) -> TyFun (typ_subst tyenv ty1, typ_subst tyenv ty2)
    | (TyBool | TyInt) as ty -> ty

  let rec typ_subst_unif_var tyenv = function
    | TyUnifVar x as ty -> (
      match IntMap.find_opt x tyenv with
      | Some tyx -> tyx
      | None -> ty)
    | TyVar _ as ty -> ty
    | TyId (x, tys) -> TyId (x, List.map (typ_subst_unif_var tyenv) tys)
    | TyTuple tys -> TyTuple (List.map (typ_subst_unif_var tyenv) tys)
    | TyFun (ty1, ty2) ->
      TyFun (typ_subst_unif_var tyenv ty1, typ_subst_unif_var tyenv ty2)
    | (TyBool | TyInt) as ty -> ty

  let rec pp_pattern fmt pat = pp_pattern_ fmt pat.Location.contents

  and pp_pattern_ fmt =
    let open Format in
    function
    | PVar x -> Var.pp fmt x
    | PWild -> pp_print_string fmt "_"
    | PConstructor (t, ps, _cs) ->
      fprintf
        fmt
        "@[%a(@[%a@]@,)@]"
        Tag.pp
        t
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_pattern)
        ps
    | PTuple ps ->
      fprintf
        fmt
        "(@[%a@]@,)"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_pattern)
        ps
    | PRecord (ps, _) ->
      fprintf
        fmt
        "{@[%a@]@,}"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
           (fun fmt (f, p) ->
             fprintf fmt "@[<hv 2>%a =@ %a@]" Field.pp f pp_pattern p))
        ps
    | POr ps ->
      fprintf
        fmt
        "@[%a@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt "@ | ") pp_pattern)
        ps
    | PBool b -> if b then fprintf fmt "true" else fprintf fmt "false"

  let is_typ_fun = function
    | TyFun _ -> true
    | _ -> false

  let rec pp_typ fmt =
    let open Format in
    function
    | TyUnifVar x -> Format.fprintf fmt "_weak%i" x
    | TyVar x -> TyVar.pp fmt x
    | TyId (x, []) -> TyId.pp fmt x
    | TyId (x, [ty]) when not @@ is_typ_fun ty ->
      fprintf fmt "@[%a@ %a@]" pp_typ ty TyId.pp x
    | TyId (x, tys) ->
      fprintf
        fmt
        "@[(@[%a@])@ %a@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_typ)
        tys
        TyId.pp
        x
    | TyTuple [] -> fprintf fmt "unit"
    | TyTuple tys ->
      fprintf
        fmt
        "@[(%a)@]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt " *@ ")
           (fun fmt ty ->
             if is_typ_fun ty
             then fprintf fmt "@[(%a)@]" pp_typ ty
             else pp_typ fmt ty))
        tys
    | TyFun (ty1, ty2) ->
      if is_typ_fun ty1
      then fprintf fmt "@[<hv>@[(%a)@] ->@ %a@]" pp_typ ty1 pp_typ ty2
      else fprintf fmt "@[<hv>%a ->@ %a@]" pp_typ ty1 pp_typ ty2
    | TyBool -> fprintf fmt "bool"
    | TyInt -> fprintf fmt "int"

  let pp_typ_scheme fmt (vars, typ) =
    let open Format in
    match vars with
    | [] -> pp_typ fmt typ
    | _ ->
      fprintf
        fmt
        "@[∀ %a.@ %a@]"
        (pp_print_list ~pp_sep:pp_print_space TyVar.pp)
        vars
        pp_typ
        typ
end
