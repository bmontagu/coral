(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2020
*)

(** Signature for types equipped with an equality function *)
module type WITH_EQUAL = sig
  type t

  val equal : t -> t -> bool
end

(** Signature for types equipped with an pretty printer *)
module type WITH_PP = sig
  type t

  val pp : Format.formatter -> t -> unit
end

module type MAP = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val bindings : 'a t -> (key * 'a) list
  val cardinal : 'a t -> int
  val choose : 'a t -> key * 'a
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val map_fold : ('a -> 'b) -> ('b -> 'c -> 'c) -> 'c -> 'a t -> 'c
  val mapi_fold : (key -> 'a -> 'b) -> ('b -> 'c -> 'c) -> 'c -> 'a t -> 'c

  val map_fold2 :
    ('a option -> 'b option -> 'c) ->
    ('c -> 'd -> 'd) ->
    'd ->
    'a t ->
    'b t ->
    'd

  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
end

module Make
    (Lab : WITH_PP)
    (_ : MAP with type key = Lab.t)
    (Id : sig
      type t

      val fresh : unit -> t
      val of_int : int -> t

      include WITH_EQUAL with type t := t
      include WITH_PP with type t := t
    end)
    (_ : MAP with type key = Id.t)
    (IntMap : MAP with type key = int)
    (Tag : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (TagMap : MAP with type key = Tag.t)
    (Field : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (FieldMap : MAP with type key = Field.t)
    (_ : sig
      val strict_errors : bool
    end) : sig
  module V : sig
    type t

    val eq : t
    val top : t
    val bot : t
    val leq : t -> t -> bool
    val tuple : t IntMap.t -> t
    val record : t FieldMap.t -> t
    val variant : t TagMap.t -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val pp : Format.formatter -> t -> unit
  end

  type t

  val bot : t
  val top : t
  val self : Id.t -> t
  val tuple : t IntMap.t -> t
  val record : t FieldMap.t -> t
  val variant : t TagMap.t -> t
  val func : Lab.t -> Id.t -> t -> t -> t
  val leq : t -> t -> bool
  val union : t -> t -> t
  val inter : t -> t -> t
  val widen : t -> t -> t
  val compose : t -> V.t -> t
  val apply : t -> t -> t
  val let_ : Id.t -> t -> t -> t
  val pp : Format.formatter -> t -> unit
  val canonize_names : t -> t
end
