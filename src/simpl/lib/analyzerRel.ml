(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

open Ast

(** [init_int_map_aux start len f m] extends the integer-indexed map
   [m] with the bindings [start+i -> f (start+i)] for [0 <= i < len]. *)
let rec init_int_map_aux start len f acc =
  if len <= 0
  then acc
  else init_int_map_aux (start + 1) (len - 1) f (IntMap.add start (f start) acc)

(** [init_int_map start len f] creates the integer-indexed map that
   contains the bindings [start+i -> f (start+i)] for [0 <= i < len]. *)
let init_int_map start len v = init_int_map_aux start len v IntMap.empty

(** Module of identifiers *)
module Id (Log : Logs.S) : sig
  type t
  (** The type of identifiers *)

  val equal : t -> t -> bool
  (** Equality test *)

  val compare : t -> t -> int
  (** Comparison test *)

  val pp : Format.formatter -> t -> unit
  (** Pretty-printer *)

  val get : Var.t -> t
  (** Gets the most recent identifier for a given variable *)

  val register : Var.t -> t
  (** Registers a variable into a fresh identifier *)

  val unregister : t -> unit
  (** Unregisters an identifier. Only the newest identifier of a
     variable can be unregistered. It is a programming error to
     unregister identifiers without starting with the most recent one.
     *)
end = struct
  type t = Var.t * int

  let equal (x1, i1) (x2, i2) = Int.compare i1 i2 = 0 && String.equal x1 x2

  let compare (x1, i1) (x2, i2) =
    let cx = String.compare x1 x2 in
    if cx = 0 then Int.compare i1 i2 else cx

  let get_var = fst
  let get_count = snd

  let pp fmt (x, i) =
    if i = 0
    then Format.fprintf fmt "%a" Var.pp x
    else Format.fprintf fmt "%a/%i" Var.pp x i

  module VarMap = Map.Make (Var)

  let m = ref VarMap.empty

  let get x =
    match VarMap.find_opt x !m with
    | Some n ->
      assert (n >= 0);
      (* Log.eprintf "INFO: get %a@." Var.pp x; *)
      (x, n)
    | None ->
      Log.eprintf "ERROR: variable %a is not registered@." Var.pp x;
      assert false

  let register x =
    let n =
      match VarMap.find_opt x !m with
      | Some n ->
        assert (n >= 0);
        n + 1
      | None -> 0
    in
    m := VarMap.add x n !m;
    (* Log.eprintf "INFO: register %a@." pp (x,n); *)
    (x, n)

  let unregister id =
    (* Log.eprintf "INFO: unregister %a@." pp id; *)
    let x = get_var id in
    m :=
      VarMap.update
        x
        (function
          | Some n ->
            assert (n >= 0);
            assert (n = get_count id);
            if n = 0 then None else Some (n - 1)
          | None ->
            Log.eprintf "ERROR: variable %a is not registered@." Var.pp x;
            assert false)
        !m
end

module IntMap = struct
  include Map.Make (Int)

  let dfind d x m =
    match find_opt x m with
    | Some v -> v
    | None -> d
end

module FieldMap = struct
  include Map.Make (Field)

  let dfind d x m =
    match find_opt x m with
    | Some v -> v
    | None -> d
end

module TagMap = struct
  include Map.Make (Tag)

  let dfind d x m =
    match find_opt x m with
    | Some v -> v
    | None -> d
end

module type OPTIONS = sig
  val extensional_eq : bool
  val extensional_bot : bool
  val learn_from_patterns : bool
  val strict_errors : bool
  val unroll_fixpoints : int
  val delay_widening : int
end

(** Same as [List.fold_left], but adds an extra argument to the
   higher-order function: the index of the current element. Returns
   the accumulator, along with the length of the list. *)
let fold_lefti f acc l =
  List.fold_left (fun (acc, i) x -> (f acc i x, i + 1)) (acc, 0) l

module Make (Log : Logs.S) (Options : OPTIONS) = struct
  module Corr = Coral.Make (IntMap) (Tag) (TagMap) (Field) (FieldMap) (Options)

  let error fmt =
    let open Log in
    keprintf
      (fun fmt ->
        Format.fprintf fmt "@.";
        exit 2)
      fmt

  let warn loc fmt =
    let open Log in
    eprintf "%a@.Warning: " Location.pp loc;
    eprintf fmt

  module Id = Id (Log)

  module Env : sig
    type 'a t

    val empty : 'a t
    val is_empty : 'a t -> bool
    val find_opt : Id.t -> 'a t -> 'a option
    val add : Id.t -> 'a -> 'a t -> 'a t
    val adds : 'a t -> 'a t -> 'a t
    val sort : 'a t -> 'a t
    val update : Id.t -> ('a option -> 'a option) -> 'a t -> 'a t
    val bindings : 'a t -> (Id.t * 'a) list
    val fold : (Id.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val fold_left : (Id.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val remove : Id.t -> 'a t -> 'a t
    val map : ('a -> 'b) -> 'a t -> 'b t
    val mapi : (Id.t -> 'a -> 'b) -> 'a t -> 'b t
    val exists : (Id.t -> 'a -> bool) -> 'a t -> bool

    val map2 :
      Location.t -> string -> (Id.t -> 'a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

    val for_all2 : ('a -> 'b -> bool) -> 'a t -> 'b t -> bool
  end = struct
    type 'a t = (Id.t * 'a) list

    let empty = []

    let is_empty = function
      | [] -> true
      | _ -> false

    let rec find_opt x = function
      | [] -> None
      | (y, v) :: l -> if Id.equal x y then Some v else find_opt x l

    let add x v l = (x, v) :: l
    let adds l1 l2 = l1 @ l2
    let sort l = List.sort (fun (x1, _) (x2, _) -> Id.compare x1 x2) l

    let rec update0 x f = function
      | [] -> None
      | ((y, v) as p) :: l -> (
        if Id.equal x y
        then
          match f (Some v) with
          | Some v' -> Some ((y, v') :: l)
          | None -> Some l
        else
          match update0 x f l with
          | Some l' -> Some (p :: l')
          | None -> None)

    let update x f l =
      match update0 x f l with
      | Some l' -> l'
      | None -> (
        match f None with
        | Some v -> add x v l
        | None -> l)

    let bindings l = l
    let fold f l acc = List.fold_right (fun (x, v) acc -> f x v acc) l acc
    let fold_left f l acc = List.fold_left (fun acc (x, v) -> f x v acc) acc l

    let rec remove x = function
      | [] -> []
      | ((y, _) as p) :: ys -> if Id.equal x y then ys else p :: remove x ys

    let map f l = List.map (fun (x, v) -> (x, f v)) l
    let mapi f l = List.map (fun (x, v) -> (x, f x v)) l
    let exists f l = List.exists (fun (x, v) -> f x v) l

    let rec map2 loc s f l1 l2 =
      match (l1, l2) with
      | [], [] -> []
      | (x, _) :: _, [] | [], (x, _) :: _ ->
        error "%a@.%s Missing variable: %a" Location.pp loc s Id.pp x
      | (x1, v1) :: l1, (x2, v2) :: l2 ->
        if Id.equal x1 x2
        then
          let v = f x1 v1 v2 in
          (x1, v) :: map2 loc s f l1 l2
        else
          error
            "%a@.%s Variable mismatch: %a, %a"
            Location.pp
            loc
            s
            Id.pp
            x1
            Id.pp
            x2

    let rec for_all2 f l1 l2 =
      match (l1, l2) with
      | [], [] -> true
      | (x, _) :: _, [] | [], (x, _) :: _ ->
        error "%s Missing variable: %a" "for_all2:" Id.pp x
      | (x1, v1) :: l1, (x2, v2) :: l2 ->
        if Id.equal x1 x2
        then f v1 v2 && for_all2 f l1 l2
        else error "%s Variable mismatch: %a, %a" "for_all2:" Id.pp x1 Id.pp x2
  end

  module Rel = struct
    type t =
      | Bot
      | Rel of {
          env: Corr.t Env.t;
          this: Corr.t;
        }

    let top env = Rel { env = Env.map (fun _ -> Corr.top) env; this = Corr.top }
    let bot = Bot

    let is_bot = function
      | Bot -> true
      | _ -> false

    let pp ~full fmt = function
      | Bot -> Corr.pp fmt Corr.bot
      | Rel { env; this } ->
        if (not full) || Env.is_empty env
        then Format.fprintf fmt "@[%a@]" Corr.pp this
        else
          let open Format in
          fprintf
            fmt
            "@[%a@ @[{ %a }@]@]"
            Corr.pp
            this
            (pp_print_list ~pp_sep:pp_print_space (fun fmt (x, c) ->
                 fprintf fmt "@[%a:@ %a@]" Id.pp x Corr.pp c))
            (Env.bindings env)

    let rel env this =
      if Corr.leq this Corr.bot
         || Env.exists (fun _x c -> Corr.leq c Corr.bot) env
      then bot
      else Rel { env; this }

    let leq rel1 rel2 =
      match (rel1, rel2) with
      | Bot, _ -> true
      | _, Bot -> false
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        Env.for_all2 Corr.leq env1 env2 && Corr.leq this1 this2

    let map f = function
      | Bot as r -> r
      | Rel { env; this } -> rel (Env.map f env) (f this)

    (* let composeL c r = map (fun c' -> Corr.compose c c') r *)
    let composeR r c = map (fun c' -> Corr.compose c' c) r

    (* let forgetL r = composeL Corr.top r *)
    (* let forgetR r = composeR r Corr.top *)

    let inter loc s r1 r2 =
      match (r1, r2) with
      | Bot, _ | _, Bot -> bot
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        let env12 = Env.map2 loc s (fun _x c1 c2 -> Corr.inter c1 c2) env1 env2
        and this12 = Corr.inter this1 this2 in
        rel env12 this12

    let widen loc s r1 r2 =
      match (r1, r2) with
      | Bot, r | r, Bot -> r
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        let env12 = Env.map2 loc s (fun _x c1 c2 -> Corr.widen c1 c2) env1 env2
        and this12 = Corr.widen this1 this2 in
        rel env12 this12

    let union loc s r1 r2 =
      match (r1, r2) with
      | Bot, r | r, Bot -> r
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        let env12 = Env.map2 loc s (fun _x c1 c2 -> Corr.union c1 c2) env1 env2
        and this12 = Corr.union this1 this2 in
        rel env12 this12

    let apply loc s r1 r2 =
      match (r1, r2) with
      | Bot, _ | _, Bot -> bot
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        let env12 = Env.map2 loc s (fun _x c1 c2 -> Corr.apply c1 c2) env1 env2
        and this12 = Corr.apply this1 this2 in
        rel env12 this12

    let tuple env loc s ~side ~args ~len =
      IntMap.fold
        (fun i r acc ->
          r
          |> map (fun c -> Corr.tuple ~side ~args:(IntMap.singleton i c) ~len)
          |> inter loc (s ^ " Rel.tuple:") acc)
        args
        (top env)

    let record env loc s ~side ~fields ~arity =
      FieldMap.fold
        (fun f r acc ->
          r
          |> map (fun c ->
                 Corr.record ~side ~fields:(FieldMap.singleton f c) ~arity)
          |> inter loc (s ^ " Rel.record:") acc)
        fields
        (top env)

    let sum env loc s ~side ~cases ~arity =
      TagMap.fold
        (fun t args acc ->
          let r =
            match args with
            | `Args args ->
              IntMap.fold
                (fun i r acc ->
                  r
                  |> map (fun c ->
                         Corr.sum
                           ~side
                           ~cases:
                             (TagMap.singleton
                                t
                                (Corr.Args (IntMap.singleton i c)))
                           ~arity)
                  |> inter loc (s ^ " Rel.sum:") acc)
                args
                (top env)
            | `NoArgs r ->
              r
              |> map (fun c ->
                     Corr.sum
                       ~side
                       ~cases:(TagMap.singleton t (Corr.NoArgs c))
                       ~arity)
          in
          union loc (s ^ "Rel.sum:") r acc)
        cases
        bot

    let func loc s env arg rel_arg rel_res =
      match (rel_arg, rel_res) with
      | Bot, _ ->
        let bottom_func = Corr.(func ~side:R ~arg:bot ~inner:bot ~outer:bot) in
        let env = Env.map (fun _ -> bottom_func) (Env.remove arg env)
        and this = bottom_func in
        rel env this
      | Rel { env = arg_env; this = arg_this }, Bot ->
        let env =
          Env.map
            (fun c -> Corr.(func ~side:R ~arg:c ~inner:bot ~outer:bot))
            arg_env
        and this = Corr.(func ~side:R ~arg:arg_this ~inner:bot ~outer:bot) in
        rel env this
      | ( Rel { env = arg_env; this = arg_this },
          Rel { env = res_env; this = res_this } ) ->
        let input_output_rel =
          match Env.find_opt arg res_env with
          | Some c -> Corr.inter c res_this
          | None -> assert false
        in
        let env =
          Env.map2
            loc
            s
            (fun _ argc resc ->
              Corr.(
                func
                  ~side:R
                  ~arg:argc
                  ~inner:input_output_rel
                  ~outer:(inter (compose argc input_output_rel) resc)))
            arg_env
            (Env.remove arg res_env)
        and this =
          Corr.(
            func
              ~side:R
              ~arg:arg_this
              ~inner:input_output_rel
              ~outer:(inter (compose arg_this input_output_rel) res_this))
        in
        rel env this

    let bottom_func_corr = Corr.(func ~side:R ~arg:top ~inner:bot ~outer:bot)

    let bottom_func env =
      let env = Env.map (fun _ -> bottom_func_corr) env
      and this = bottom_func_corr in
      rel env this

    let let_ loc s x rel1 rel2 =
      match (rel1, rel2) with
      | Bot, _ | _, Bot -> bot
      | Rel { env = env1; this = this1 }, Rel { env = env2; this = this2 } ->
        let r =
          match Env.find_opt x env2 with
          | Some r -> r
          | None -> assert false
        in
        let env =
          Env.map2
            loc
            s
            (fun _ c1 c2 -> Corr.(inter (compose c1 r) c2))
            env1
            (Env.remove x env2)
        and this = Corr.(inter (compose this1 r) this2) in
        rel env this

    let gather0 env this =
      Env.fold
        (fun _x rx acc -> Corr.(inter (compose (flip rx) top) acc))
        env
        (Corr.flip this)

    let gather1 e x =
      Env.fold
        (fun y ry acc ->
          match ry with
          | Bot -> Corr.bot
          | Rel { env; this } -> (
            if Id.equal x y
            then Corr.(inter (gather0 env this) acc)
            else
              match Env.find_opt x env with
              | Some envx -> Corr.(inter (compose envx top) acc)
              | None -> acc))
        e
        Corr.top

    (** [gather e] gathers all the possible inputs allowed by the
       environment [e] *)
    let gather e =
      let env = Env.mapi (fun x _ -> gather1 e x) e
      and this = Corr.top in
      rel env this

    (** [get x e] is an abstraction of [Gather e `inter` Self x] *)
    let rec get x = function
      | [] -> None
      | (y, ry) :: l -> (
        match ry with
        | Bot as r -> Some r
        | Rel { env = env_y; this = this_y } -> (
          if Id.equal x y
          then Some (rel (Env.add x Corr.(inter eq this_y) env_y) this_y)
          else
            match get x l with
            | (None | Some Bot) as res -> res
            | Some (Rel { env = env_x; this = this_x }) ->
              let cyx =
                match Env.find_opt x env_y with
                | Some cxy -> Corr.(inter (flip (inter cxy this_y)) this_x)
                | None -> assert false
              in
              Some (rel (Env.add y cyx env_x) this_x)))
  end

  type env = Rel.t Env.t

  (** Creates a map to unit whose domain the elements of the given list
      of fields. *)
  let get_record_arity fs =
    List.fold_right (fun f m -> FieldMap.add f () m) fs FieldMap.empty

  (** Creates a map to integer from an association list whose keys are
      tags. *)
  let get_sum_arity cs =
    List.fold_right (fun (c, n) m -> TagMap.add c n m) cs TagMap.empty

  open Inner

  let check_rel_domain_against_env loc genv = function
    | Rel.Bot -> ()
    | Rel.Rel { env; this = _ } ->
      ignore @@ Env.map2 loc "bad domains:" (fun _ _ _ -> None) genv env

  (** Inserts a correlation into a list of correlations that denotes a
     disjunction of correlations. Returns a list of correlations that
     denotes a union of its elements. *)
  let rec add_union c = function
    | [] -> if Corr.leq c Corr.bot then [] else [c]
    | c' :: cs as l ->
      if Corr.leq c c'
      then l
      else if Corr.leq c' c
      then c :: cs
      else c' :: add_union c cs

  (** Computes the union of two lists of correlations, that each
     denote a union of correlations. Returns a list of correlations
     that denotes a union of its elements. *)
  let rec union_of_union cs1 cs2 =
    match cs2 with
    | [] -> cs1
    | c2 :: cs2 -> union_of_union (add_union c2 cs1) cs2

  (** Computes the intersection of a correlation and a list of
     correlations that denotes a union of correlations. Returns a list
     of correlations that denotes a union of its elements.*)
  let rec inter0_of_union c = function
    | [] -> []
    | c' :: cs -> add_union (Corr.inter c c') (inter0_of_union c cs)

  (** Computes the intersection of two lists of correlations that
     denote a union of correlations. Returns a list of correlations
     that denotes a union of its elements.*)
  let rec inter_of_union cs1 cs2 =
    match cs2 with
    | [] -> []
    | c2 :: cs2 ->
      union_of_union (inter0_of_union c2 cs1) (inter_of_union cs1 cs2)

  (** [postfixpoint loc s f base] computes a post-fixpoint of f that
     is above [base]. [Options.unroll_fixpoints] iterations of the
     fixpoint are unrolled first. Then, [Options.delay_widening]
     iterations are performed before using widening. It terminates
     only when there is no infinite ascending chain in the abstract
     lattice. We should use a stronger form of widening, should that
     property not be satisfied, or to accelerate convergence. *)
  let postfixpoint loc s f base =
    let rec loop n unrolled base =
      let next = f base in
      if n < Options.unroll_fixpoints
      then (loop [@tailcall]) (n + 1) (Rel.union loc s base next) next
      else if Rel.leq next base
      then begin
        Log.eprintf
          "%a@.INFO: postfixpoint reached after %i iterations@."
          Location.pp
          loc
          n;
        Rel.union loc s unrolled base
      end
      else
        let base' =
          if n < Options.(unroll_fixpoints + delay_widening)
          then Rel.union loc s base next
          else
            let base' = Rel.widen loc s base next in
            if not @@ Rel.leq base base'
            then
              error
                "%a@.@[Incorrect widening: domain@ %a@ is not below the \
                 widened domain@ %a@]@."
                Location.pp
                loc
                (Rel.pp ~full:false)
                base
                (Rel.pp ~full:false)
                base';
            base'
        in
        (loop [@tailcall]) (n + 1) unrolled base'
    in
    (loop [@tailcall]) 0 base base

  let true_sum =
    Corr.sum
      ~side:L
      ~cases:(TagMap.singleton Tag.true_ Corr.(NoArgs top))
      ~arity:bool_arity

  let is_true v = Rel.composeR v true_sum

  let false_sum =
    Corr.sum
      ~side:L
      ~cases:(TagMap.singleton Tag.false_ Corr.(NoArgs top))
      ~arity:bool_arity

  let is_false v = Rel.composeR v false_sum

  (** [interpret env e] interprets the expression [e] in the environment
      [env]. Returns the input-output correlation of [e]. *)
  let rec interpret (env : env) e =
    interpret_ e.Location.loc env e.Location.contents

  and interpret_ loc (env : env) : expr_ -> Rel.t = function
    | Var x -> (
      let id = Id.get x in
      match Rel.get id (Env.bindings env) with
      | Some r ->
        check_rel_domain_against_env loc env r;
        Rel.inter loc "interpret (Var)" r (Rel.gather env)
      | None -> error "Unknown variable %a" Id.pp id)
    | Let (x, e1, e2) ->
      let r1 = interpret env e1 in
      let id = Id.register x in
      let r2 = interpret (Env.add id r1 env) e2 in
      let res = Rel.let_ loc "interpret (Let)" id r1 r2 in
      Id.unregister id;
      res
    | Tuple [] -> Rel.gather env
    | Tuple es ->
      let args, len =
        fold_lefti
          (fun m i ei -> IntMap.add i (interpret env ei) m)
          IntMap.empty
          es
      in
      Rel.tuple env loc "interpret (Tuple):" ~side:Corr.R ~args ~len
    | TupleProj (e, i, len) ->
      Rel.composeR (interpret env e)
      @@ Corr.tuple ~side:L ~args:(IntMap.singleton i Corr.eq) ~len
    | Record [] -> Rel.gather env
    | Record es ->
      let fields, arity =
        List.fold_left
          (fun (m, arity) (f, ef) ->
            let ri = interpret env ef in
            (FieldMap.add f ri m, FieldMap.add f () arity))
          (FieldMap.empty, FieldMap.empty)
          es
      in
      Rel.record env loc "interpret (Record):" ~side:Corr.R ~fields ~arity
    | RecordProj (e, f, fs) ->
      Rel.composeR (interpret env e)
      @@ Corr.record
           ~side:L
           ~fields:(FieldMap.singleton f Corr.eq)
           ~arity:(get_record_arity fs)
    | RecordUpdate (e, f, ef, fs) ->
      let ce = interpret env e in
      let fields, arity =
        List.fold_left
          (fun (m, arity) g ->
            let cg =
              if Field.equal f g
              then
                Rel.inter
                  loc
                  "interpret (RecordUpdate)"
                  (Rel.composeR ce Corr.top)
                  (interpret env ef)
              else
                Rel.composeR ce
                @@ Corr.record
                     ~side:L
                     ~fields:(FieldMap.singleton g Corr.eq)
                     ~arity:(get_record_arity fs)
            in
            (FieldMap.add g cg m, FieldMap.add g () arity))
          (FieldMap.empty, FieldMap.empty)
          fs
      in
      Rel.record env loc "interpret (RecordUpdate)" ~side:R ~fields ~arity
    | Constructor (t, [], cs) ->
      let cases = TagMap.singleton t (`NoArgs (Rel.gather env)) in
      Rel.sum
        env
        loc
        "interpret (Constructor [])"
        ~side:Corr.R
        ~cases
        ~arity:(get_sum_arity cs)
    | Constructor (t, es, cs) ->
      let args, _len =
        fold_lefti
          (fun m i ei -> IntMap.add i (interpret env ei) m)
          IntMap.empty
          es
      in
      let cases = TagMap.singleton t (`Args args) in
      Rel.sum
        env
        loc
        "interpret (Constructor)"
        ~side:R
        ~cases
        ~arity:(get_sum_arity cs)
    | Fun ({ Location.contents = x; _ }, _ty, e) ->
      let top_val = Rel.top env in
      let id = Id.register x in
      let env' = Env.add id top_val env in
      let res =
        Rel.inter
          loc
          "interpret (Fun)"
          (Rel.gather env)
          (Rel.func loc "interpret (Fun)" env id top_val (interpret env' e))
      in
      Id.unregister id;
      res
    | FixFun (f, x, ty, e) ->
      let f_id = Id.register f in
      let functional rel =
        let res =
          interpret
            (Env.add f_id rel env)
            (Location.locate loc @@ Fun (x, ty, e))
        in
        Rel.let_ loc "interpret (LetFix)" f_id rel res
      in
      let base =
        Rel.inter
          loc
          "interpret (InterFix)"
          (Rel.gather env)
          (Rel.bottom_func env)
      in
      let res = postfixpoint loc "interpret (Fix)" functional base in
      Id.unregister f_id;
      res
    | App (e1, e2) ->
      Rel.apply loc "interpret (App):" (interpret env e1) (interpret env e2)
    | Match (e, cases) ->
      let v = interpret env e in
      let v', _matched_cases, neg_cases = interpret_branches loc env v cases in
      let actual_neg_cases =
        let open Rel in
        List.fold_left
          (fun acc neg_cases ->
            union loc "interpret_branches" acc (composeR v neg_cases))
          bot
          neg_cases
      in
      if not @@ Rel.is_bot actual_neg_cases
      then
        warn
          loc
          "Non-exhaustive pattern-matching:@.@[%a@]@.@."
          (Format.pp_print_list ~pp_sep:Format.pp_print_newline Corr.pp)
          neg_cases;
      v'
    | Unknown -> Rel.gather env
    | Bool b ->
      let t = if b then Tag.true_ else Tag.false_ in
      let cases = TagMap.singleton t (`NoArgs (Rel.gather env)) in
      Rel.sum env loc "interpret (Bool)" ~side:Corr.R ~cases ~arity:bool_arity
    | BAndAlso (e1, e2) ->
      let v1 = interpret env e1
      and v2 = interpret env e2 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      Rel.sum
        env
        loc
        "interpret (BAndAlso)"
        ~side:R
        ~cases:
          (let open TagMap in
           add
             Tag.true_
             (`NoArgs
               (Rel.inter loc "interpret (BAndAlso: true)" v1_true (is_true v2)))
           @@ add
                Tag.false_
                (`NoArgs
                  Rel.(
                    union
                      loc
                      "interpret (BAndAlso: false)"
                      v1_false
                      (inter
                         loc
                         "interpret (BAndAlso: false)"
                         v1_true
                         (is_false v2))))
           @@ empty)
        ~arity:bool_arity
    | BOrElse (e1, e2) ->
      let v1 = interpret env e1
      and v2 = interpret env e2 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      Rel.sum
        env
        loc
        "interpret (BAndAlso)"
        ~side:R
        ~cases:
          (let open TagMap in
           add
             Tag.true_
             (`NoArgs
               Rel.(
                 union
                   loc
                   "interpret (BOrElse: false)"
                   v1_true
                   (inter
                      loc
                      "interpret (BOrElse: false)"
                      v1_false
                      (is_true v2))))
           @@ add
                Tag.false_
                (`NoArgs
                  (Rel.inter
                     loc
                     "interpret (BOrElse: true)"
                     v1_false
                     (is_false v2)))
           @@ empty)
        ~arity:bool_arity
    | IfThenElse (e1, e2, e3) ->
      let v1 = interpret env e1
      and v2 = interpret env e2
      and v3 = interpret env e3 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      Rel.union
        loc
        "interpret (IfThenElse)"
        (Rel.inter loc "interpret (IfThenElse: true)" v1_true v2)
        (Rel.inter loc "interpret (IfThenElse: false)" v1_false v3)
    | Int _n ->
      (* TODO: numeric domains *)
      Rel.gather env
    | Binop ((PLUS | MINUS | TIMES | DIV), e1, e2) ->
      (* TODO: numeric domains *)
      Rel.union
        loc
        "interpret (Binop(+|-|*))"
        (Rel.composeR (interpret env e1) Corr.top)
        (Rel.composeR (interpret env e2) Corr.top)
    | Binop ((NEQ | LT | GT | EQ | LE | GE), e1, e2) ->
      (* TODO: numeric domains *)
      Rel.union
        loc
        "interpret (Binop(<|>|=|<=|>=))"
        (Rel.composeR (interpret env e1) Corr.top)
        (Rel.composeR (interpret env e2) Corr.top)
    | Unop (UMINUS, e) ->
      (* TODO: numeric domains *)
      Rel.composeR (interpret env e) Corr.top

  (** [interpret_branches env v cases] interprets the pattern matching
     cases [cases] in the environment [env] applied to a value whose
     correlation is [v]. It returns the correlation obtained after
     executing all the branches, and and the correlation that
     describes the set of values that were matched by the branches and
     the union of correlations that describes the set of values that
     were not matched by the branches. *)
  and interpret_branches loc env v cases : Rel.t * Corr.t * Corr.t list =
    List.fold_left
      (fun (cases, matched_cases, neg_cases) branch ->
        let v =
          if Options.learn_from_patterns
          then
            let open Rel in
            inter loc "interpret_branches (learning):" v
            @@ List.fold_left
                 (fun acc neg_cases ->
                   union loc "interpret_branches" acc (composeR v neg_cases))
                 bot
                 neg_cases
          else v
        in
        let case, matched_case, neg_case = interpret_branch env v branch in
        ( Rel.union loc "interpret_branches" case cases,
          Corr.(union matched_case matched_cases),
          inter_of_union neg_case neg_cases ))
      (Rel.bot, Corr.bot, [Corr.top])
      cases

  (** [interpret_branch env v (p, e)] interprets the matching of the
     pattern [p] in the environment [env] applied to a value whose
     correlation is [v], and with continuation [e]. It returns the
     correlation obtained after executing [e], and also the
     correlation that describes the set of values that are matched by
     [p], and also the union of correlation that describes the set of
     values that are not matched by [p]. *)
  and interpret_branch (env : env) v (p, e) : Rel.t * Corr.t * Corr.t list =
    let env_pattern, matched_case, neg_case =
      interpret_pattern ~intro:true Env.empty v p
    in
    let env_pattern =
      (* adds to inner environments of env_pattern the cross bindings to
         their neighbours *)
      snd
      @@ Env.fold
           (fun x r (vars, env_pattern) ->
             ( Env.add x Corr.top vars,
               let r' =
                 match r with
                 | Rel.Bot as r -> r
                 | Rel.Rel { env; this } -> Rel.rel (Env.adds vars env) this
               in
               Env.add x r' env_pattern ))
           env_pattern
           (Env.empty, Env.empty)
    in
    let p_loc = p.Location.loc in
    let v' = Rel.composeR v matched_case in
    if Rel.is_bot v' then warn p_loc "This pattern is unreachable.@.";
    let env =
      (* refine the environment with what was learnt from matching
         this pattern *)
      match Rel.composeR v Corr.(inter eq matched_case) with
      | Rel.Bot -> Env.map (fun _ -> Rel.bot) env
      | Rel { env = refined_env; this } ->
        Env.fold
          (fun x c acc ->
            let c =
              let open Corr in
              flip @@ inter (compose this top) (compose c top)
            in
            Env.update
              x
              (function
                | None -> assert false
                | Some r -> Some (Rel.map (Corr.inter c) r))
              acc)
          refined_env
          env
    in
    let env' = Env.adds env_pattern env in
    ( Rel.inter p_loc "interpret_branch:" v'
      @@ Env.fold_left
           (fun x _ acc ->
             let relx =
               match Env.find_opt x env' with
               | Some r -> r
               | None -> assert false
             in
             let res = Rel.let_ p_loc "interpret_branch:" x relx acc in
             Id.unregister x;
             res)
           env_pattern
      @@ interpret env' e,
      matched_case,
      neg_case )

  (** [interpret_pattern ~intro env v p] computes 3 values for the pattern
      matching of the value [v] along the pattern [p] in the environment
      [env]:
      - the environment below the pattern that binds the variables which
        are introduced by the pattern (necessary to interpret the continuation)
      - a correlation that describes the set of values that are matched by [p]
      - a correlation that describes the complement of the set of values
        that are matched by [p].

      If [intro = true], then the variables introduced by the pattern
      should be registered. If [intro = false], they must not be
      registered. This flag is used in the treatment of disjunctive
      patterns.
  *)
  and interpret_pattern ~intro env v p : env * Corr.t * Corr.t list =
    interpret_pattern_ ~intro p.Location.loc env v p.Location.contents

  and interpret_pattern_ ~intro loc env v p : env * Corr.t * Corr.t list =
    match p with
    | PVar x ->
      let id = if intro then Id.register x else Id.get x in
      (Env.add id v env, Corr.top, [])
    | PWild -> (env, Corr.top, [])
    | PConstructor (t, [], arity) ->
      let arity = get_sum_arity arity in
      ( env,
        Corr.(sum ~side:L ~cases:(TagMap.singleton t (NoArgs top)) ~arity),
        let cases =
          TagMap.mapi
            (fun t' len ->
              if Tag.equal t t'
              then Corr.NoArgs Corr.bot
              else if len = 0
              then NoArgs Corr.top
              else Args IntMap.empty)
            arity
        in
        add_union (Corr.sum ~side:L ~cases ~arity) [] )
    | PConstructor (t, ps, arity) ->
      let arity = get_sum_arity arity in
      let n =
        try TagMap.find t arity
        with Not_found -> error "%a@.Unknown tag %a" Location.pp loc Tag.pp t
      in
      let sum_i i =
        let open Corr in
        let cases = TagMap.singleton t @@ Args (IntMap.singleton i eq) in
        sum ~side:L ~cases ~arity
      in
      let (env', cases, neg_cases), len =
        fold_lefti
          (fun (env, cases, neg_cases) i pi ->
            let vi = Rel.composeR v @@ sum_i i in
            let env', case_i, neg_cases_i =
              interpret_pattern ~intro env vi pi
            in
            ( env',
              IntMap.add i case_i cases,
              List.fold_left
                (fun acc neg_case_i -> IntMap.singleton i neg_case_i :: acc)
                neg_cases
                neg_cases_i ))
          (env, IntMap.empty, [])
          ps
      in
      if n <> len
      then
        error
          "%a@.Mismatch of argument numbers in pattern for tag %a"
          Location.pp
          loc
          Tag.pp
          t;
      ( env',
        Corr.(sum ~side:L ~cases:(TagMap.singleton t (Args cases)) ~arity),
        let not_in_case_t =
          let cases =
            TagMap.mapi
              (fun t' len ->
                if Tag.equal t t'
                then Corr.Args (init_int_map 0 len (fun _ -> Corr.bot))
                else if len = 0
                then NoArgs Corr.top
                else Args IntMap.empty)
              arity
          in
          Corr.sum ~side:L ~cases ~arity
        in
        List.fold_left
          (fun acc neg_cases ->
            let cases =
              TagMap.mapi
                (fun t' len ->
                  if Tag.equal t t'
                  then Corr.Args neg_cases
                  else if len = 0
                  then NoArgs Corr.bot
                  else Args (init_int_map 0 len (fun _ -> Corr.bot)))
                arity
            in
            add_union (Corr.sum ~side:L ~cases ~arity) acc)
          (add_union not_in_case_t [])
          neg_cases )
    | PTuple ps ->
      let len = List.length ps in
      let tuple_i i =
        let open Corr in
        let args = IntMap.singleton i eq in
        tuple ~side:L ~args ~len
      in
      let (env', cases, neg_cases), _len =
        fold_lefti
          (fun (env, cases, neg_cases) i pi ->
            let vi = Rel.composeR v @@ tuple_i i in
            let env', case_i, neg_case_i = interpret_pattern ~intro env vi pi in
            ( env',
              Corr.inter cases
              @@ Corr.tuple ~side:L ~args:(IntMap.singleton i case_i) ~len,
              List.fold_left
                (fun acc neg_case_i ->
                  add_union
                    (Corr.tuple
                       ~side:L
                       ~args:(IntMap.singleton i neg_case_i)
                       ~len)
                    acc)
                neg_cases
                neg_case_i ))
          (env, Corr.top, [])
          ps
      in
      (env', cases, neg_cases)
    | PRecord (ps, arity) ->
      let arity = get_record_arity arity in
      let record_i i =
        let open Corr in
        let fields = FieldMap.singleton i eq in
        record ~side:L ~fields ~arity
      in
      let env', cases, neg_cases =
        List.fold_left
          (fun (env, cases, neg_cases) (i, pi) ->
            let vi = Rel.composeR v @@ record_i i in
            let env', case_i, neg_case_i = interpret_pattern ~intro env vi pi in
            ( env',
              Corr.inter cases
              @@ Corr.record
                   ~side:L
                   ~fields:(FieldMap.singleton i case_i)
                   ~arity,
              List.fold_left
                (fun acc neg_case_i ->
                  add_union
                    (Corr.record
                       ~side:L
                       ~fields:(FieldMap.singleton i neg_case_i)
                       ~arity)
                    acc)
                neg_cases
                neg_case_i ))
          (env, Corr.top, [])
          ps
      in
      (env', cases, neg_cases)
    | POr [] -> assert false
    | POr (p :: ps) ->
      let env', v_case', neg_case = interpret_pattern ~intro env v p in
      let env' = Env.sort env' in
      List.fold_left
        (fun (env_acc, v_cases_acc, neg_cases) p ->
          let env', v_case', neg_case =
            interpret_pattern ~intro:false env v p
          in
          let env' = Env.sort env' in
          ( Env.map2
              loc
              "interpret_pattern (POr):"
              (fun _x v1 v2 -> Rel.union loc "interpret_pattern (POr):" v1 v2)
              env_acc
              env',
            Corr.union v_cases_acc v_case',
            inter_of_union neg_cases neg_case ))
        (env', v_case', neg_case)
        ps
    | PBool b ->
      let t = if b then Tag.true_ else Tag.false_
      and not_t = if not b then Tag.true_ else Tag.false_
      and arity = bool_arity in
      ( env,
        Corr.(sum ~side:L ~cases:(TagMap.singleton t (NoArgs top)) ~arity),
        add_union
          Corr.(sum ~side:L ~cases:(TagMap.singleton not_t (NoArgs top)) ~arity)
          [] )

  let interpret_decl ((env, decls) as acc) = function
    | DeclTyp _ -> acc
    | DeclVal (x, ty_scheme, e) ->
      let res = interpret env e in
      let id = Id.register x in
      (Env.add id res env, (x, ty_scheme, res) :: decls)

  let interpret_program env p =
    let _env, decls = List.fold_left interpret_decl (env, []) p in
    List.rev decls

  let pp fmt decls =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt "@.@.")
      (fun fmt (x, typ_scheme, c) ->
        Format.fprintf
          fmt
          "@[val %a ::@ %a@]@."
          Var.pp
          x
          Inner.pp_typ_scheme
          typ_scheme;
        Format.fprintf fmt "@[val %a:@ %a@]" Var.pp x (Rel.pp ~full:false) c)
      fmt
      decls
end
