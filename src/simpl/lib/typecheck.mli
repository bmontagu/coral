module type OPTIONS = sig end

module Make (_ : OPTIONS) : sig
  module TEnv = Ast.TyIdMap
  module Env = Ast.VarMap

  val typecheck :
    Ast.Outer.program -> (Ast.Inner.program, Location.t * string) result
end
