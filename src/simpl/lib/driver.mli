(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

val parse :
  ((Lexing.lexbuf -> Parser.token) -> Lexing.lexbuf -> 'a) ->
  string ->
  [< `Channel of in_channel | `String of string ] ->
  ('a, Location.t * string) result
(** [parse parser name input] parses the input [input] named [name]
   using the parsing function [parser] *)

val parse_file :
  ((Lexing.lexbuf -> Parser.token) -> Lexing.lexbuf -> 'a) ->
  string ->
  ('a, Location.t * string) result
(** [parse_file parser file] parses the file [file] using the parsing
   function [parser] *)
