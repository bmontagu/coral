(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2021
*)

module AnalysisKind : sig
  type t =
    | IndependentAttribute
    | Relational

  val min : int
  val max : int
  val to_enum : t -> int
  val of_enum : int -> t option
  val to_string : t -> string
end

module Options : sig
  type t = {
    run_internal_tests: bool;
    extensional_eq: bool;
    extensional_bot: bool;
    learn_from_patterns: bool;
    strict_errors: bool;
    unroll_fixpoints: int;
    delay_widening: int;
    analysis_kind: AnalysisKind.t;
  }

  module type S = sig
    val extensional_eq : bool
    val extensional_bot : bool
    val learn_from_patterns : bool
    val strict_errors : bool
    val unroll_fixpoints : int
    val delay_widening : int
  end

  val get_module : t -> (module S)

  module type ANALYZER = sig
    module Env : sig
      type 'a t

      val empty : 'a t
    end

    module Rel : sig
      type t
    end

    type env = Rel.t Env.t

    val interpret_program :
      env ->
      Ast.Inner.program ->
      (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list

    val pp :
      Format.formatter ->
      (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list ->
      unit
  end

  val get_analysis_module : (module Logs.S) -> t -> (module ANALYZER)
end

type package = Pack : 'res * (Format.formatter -> 'res -> unit) -> package

val simpl_print : Options.t -> string -> unit
val simpl_value : Options.t -> string -> package
