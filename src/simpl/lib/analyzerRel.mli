(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

module type OPTIONS = sig
  val extensional_eq : bool
  val extensional_bot : bool
  val learn_from_patterns : bool
  val strict_errors : bool
  val unroll_fixpoints : int
  val delay_widening : int
end

module Make (_ : Logs.S) (_ : OPTIONS) : sig
  module Corr :
    Coral.S
      with type 'a int_map := 'a Map.Make(Ast.Int).t
       and type 'a tag_map := 'a Map.Make(Ast.Tag).t
       and type 'a field_map := 'a Map.Make(Ast.Field).t
       and type tag = Ast.Tag.t
       and type field = Ast.Field.t

  module Env : sig
    type 'a t

    val empty : 'a t
  end

  module Rel : sig
    type t

    val pp : full:bool -> Format.formatter -> t -> unit
  end

  type env = Rel.t Env.t

  val interpret : env -> Ast.Inner.expr -> Rel.t
  (** [interpret env e] interprets the expression [e] in the environment
        [env]. Returns the input-output correlation of [e]. *)

  val interpret_program :
    env -> Ast.Inner.program -> (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list

  val pp :
    Format.formatter -> (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list -> unit
end
