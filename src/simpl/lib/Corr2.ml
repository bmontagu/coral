(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2020
*)

(** Signature for types equipped with an equality function *)
module type WITH_EQUAL = sig
  type t

  val equal : t -> t -> bool
end

(** Signature for types equipped with an pretty printer *)
module type WITH_PP = sig
  type t

  val pp : Format.formatter -> t -> unit
end

module type MAP = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val update : key -> ('a option -> 'a option) -> 'a t -> 'a t
  val bindings : 'a t -> (key * 'a) list
  val cardinal : 'a t -> int
  val choose : 'a t -> key * 'a
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val map_fold : ('a -> 'b) -> ('b -> 'c -> 'c) -> 'c -> 'a t -> 'c
  val mapi_fold : (key -> 'a -> 'b) -> ('b -> 'c -> 'c) -> 'c -> 'a t -> 'c

  val map_fold2 :
    ('a option -> 'b option -> 'c) ->
    ('c -> 'd -> 'd) ->
    'd ->
    'a t ->
    'b t ->
    'd

  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val filter : (key -> 'a -> bool) -> 'a t -> 'a t
end

module Make
    (Lab : WITH_PP)
    (LabMap : MAP with type key = Lab.t)
    (Id : sig
      type t

      val fresh : unit -> t
      val of_int : int -> t

      include WITH_EQUAL with type t := t
      include WITH_PP with type t := t
    end)
    (IdMap : MAP with type key = Id.t)
    (IntMap : MAP with type key = int)
    (Tag : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (TagMap : MAP with type key = Tag.t)
    (Field : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (FieldMap : MAP with type key = Field.t)
    (Options : sig
      val strict_errors : bool
    end) =
struct
  let map_fold map reduce vinit l =
    List.fold_left (fun acc x -> reduce acc (map x)) vinit l

  let get_id x (_level, env) = IdMap.find x env
  let extend_env (level, env) x = (level + 1, IdMap.add x (Id.of_int level) env)

  module VCorr : sig
    type t =
      | Bot
      | Eq
      | Top
      | TupleL of t IntMap.t
      | RecordL of t FieldMap.t
      | VariantL of t TagMap.t

    val bot : t
    val eq : t
    val top : t
    val leq : t -> t -> bool
    val compose : t -> t -> t
    val union : strict:bool -> widen:bool -> t -> t -> t
    val inter : t -> t -> t
    val is_vector : t -> bool
    val pp : Format.formatter -> t -> unit
  end = struct
    type t =
      | Bot
      | Eq
      | Top
      | TupleL of t IntMap.t
      | RecordL of t FieldMap.t
      | VariantL of t TagMap.t

    let bot = Bot
    let eq = Eq
    let top = Top

    let is_top = function
      | Top -> true
      | _ -> false

    let is_bot = function
      | Bot -> true
      | _ -> false

    let rec leq c1 c2 =
      match (c1, c2) with
      | Bot, _ | _, Top | Eq, Eq -> true
      | Top, (Bot | Eq)
      | Eq, (Bot | TupleL _ | RecordL _ | VariantL _)
      | (TupleL _ | RecordL _ | VariantL _), Eq -> false
      | RecordL _, (TupleL _ | VariantL _)
      | (TupleL _ | VariantL _), RecordL _
      | TupleL _, VariantL _
      | VariantL _, TupleL _ ->
        assert (not Options.strict_errors);
        false
      | TupleL m1, TupleL m2 ->
        IntMap.for_all
          (fun i c1 ->
            let c2 =
              match IntMap.find_opt i m2 with
              | Some c2 -> c2
              | None -> top
            in
            leq c1 c2)
          m1
      | TupleL m, Bot -> IntMap.exists (fun _i c -> leq c bot) m
      | Top, TupleL m -> IntMap.for_all (fun _f c -> leq top c) m
      | RecordL m1, RecordL m2 ->
        FieldMap.for_all
          (fun f c1 ->
            let c2 =
              match FieldMap.find_opt f m2 with
              | Some c2 -> c2
              | None -> top
            in
            leq c1 c2)
          m1
      | RecordL m, Bot -> FieldMap.exists (fun _f c -> leq c bot) m
      | Top, RecordL m -> FieldMap.for_all (fun _f c -> leq top c) m
      | VariantL m1, VariantL m2 ->
        TagMap.for_all
          (fun tag c2 ->
            let c1 =
              match TagMap.find_opt tag m1 with
              | Some c1 -> c1
              | None -> bot
            in
            leq c1 c2)
          m2
      | VariantL m, Bot -> TagMap.for_all (fun _tag c -> leq c bot) m
      | Top, VariantL m -> TagMap.for_all (fun _tag c -> leq top c) m

    let rec union ~strict ~widen c1 c2 =
      match (c1, c2) with
      | Bot, c | c, Bot -> c
      | Top, _ | _, Top -> top
      | Eq, Eq -> Eq
      | Eq, _ | _, Eq -> top (* XXX loss of information *)
      | RecordL _, (TupleL _ | VariantL _)
      | VariantL _, (TupleL _ | RecordL _)
      | TupleL _, (RecordL _ | VariantL _) ->
        assert ((not strict) || not Options.strict_errors);
        top
      | TupleL m1, TupleL m2 ->
        let m =
          IntMap.merge
            (fun _i oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (union ~strict ~widen c1 c2)
              | Some _, None | None, Some _ -> Some top
              | None, None -> assert false)
            m1
            m2
        in
        if IntMap.for_all (fun _ c -> is_top c) m then top else TupleL m
      | RecordL m1, RecordL m2 ->
        let m =
          FieldMap.merge
            (fun _f oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (union ~strict ~widen c1 c2)
              | Some _, None | None, Some _ -> Some top
              | None, None -> assert false)
            m1
            m2
        in
        if FieldMap.for_all (fun _ c -> is_top c) m then top else RecordL m
      | VariantL m1, VariantL m2 ->
        let m =
          TagMap.merge
            (fun _tag oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 ->
                if widen && is_bot c1 && (not @@ is_bot c2)
                then Some top
                else Some (union ~strict ~widen c1 c2)
              | (Some _ as oc), None -> oc
              | None, (Some c as oc) ->
                if widen && (not @@ is_bot c) then Some top else oc
              | None, None -> assert false)
            m1
            m2
        in
        if TagMap.for_all (fun _ c -> is_top c) m then top else VariantL m

    let rec inter c1 c2 =
      match (c1, c2) with
      | Top, c | c, Top -> c
      | Bot, _ | _, Bot -> bot
      | Eq, Eq -> Eq
      | Eq, _ | _, Eq -> Eq (* XXX loss of information *)
      | RecordL _, (TupleL _ | VariantL _)
      | VariantL _, (TupleL _ | RecordL _)
      | TupleL _, (RecordL _ | VariantL _) ->
        assert (not Options.strict_errors);
        bot
      | TupleL m1, TupleL m2 ->
        let m =
          IntMap.merge
            (fun _i oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | (Some _ as oc), None | None, (Some _ as oc) -> oc
              | None, None -> assert false)
            m1
            m2
        in
        if IntMap.exists (fun _ c -> is_bot c) m then bot else TupleL m
      | RecordL m1, RecordL m2 ->
        let m =
          FieldMap.merge
            (fun _f oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | (Some _ as oc), None | None, (Some _ as oc) -> oc
              | None, None -> assert false)
            m1
            m2
        in
        if FieldMap.exists (fun _ c -> is_bot c) m then bot else RecordL m
      | VariantL m1, VariantL m2 ->
        let m =
          TagMap.merge
            (fun _tag oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | Some _, None | None, Some _ -> Some bot
              | None, None -> assert false)
            m1
            m2
        in
        if TagMap.for_all (fun _ c -> is_bot c) m then bot else VariantL m

    let rec compose c1 c2 =
      match (c1, c2) with
      | Bot, _ | _, Bot -> bot
      | Eq, c | c, Eq -> c
      | Top, Top -> top
      | TupleL m, c ->
        let m' = IntMap.map (fun c' -> compose c' c) m in
        if IntMap.exists (fun _ c -> is_bot c) m'
        then bot
        else if IntMap.for_all (fun _ c -> is_top c) m'
        then top
        else TupleL m'
      | Top, TupleL m -> IntMap.map_fold (fun c -> compose top c) inter top m
      | RecordL m, c ->
        let m' = FieldMap.map (fun c' -> compose c' c) m in
        if FieldMap.exists (fun _ c -> is_bot c) m'
        then bot
        else if FieldMap.for_all (fun _ c -> is_top c) m'
        then top
        else RecordL m'
      | Top, RecordL m -> FieldMap.map_fold (fun c -> compose top c) inter top m
      | VariantL m, c ->
        let m' = TagMap.map (fun c' -> compose c' c) m in
        if TagMap.for_all (fun _ c -> is_bot c) m'
        then bot
        else if TagMap.for_all (fun _ c -> is_top c) m'
        then top
        else VariantL m'
      | Top, VariantL m ->
        TagMap.map_fold
          (fun c -> compose top c)
          (union ~strict:true ~widen:false)
          bot
          m

    let is_vector c =
      let ctop = compose c top in
      leq ctop c && leq c ctop

    let rec pp fmt =
      let open Format in
      function
      | Bot -> fprintf fmt "⊥"
      | Eq -> fprintf fmt "Eq"
      | Top -> fprintf fmt "⊤"
      | TupleL m ->
        let m =
          IntMap.filter
            (fun _ -> function
              | Top -> false
              | _ -> true)
            m
        in
        if IntMap.is_empty m
        then fprintf fmt "⊤"
        else
          fprintf
            fmt
            "@[(%a)@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               (fun fmt (i, c) -> fprintf fmt "@[%i →@ %a@]" i pp c))
            (IntMap.bindings m)
      | RecordL m ->
        let m =
          FieldMap.filter
            (fun _ -> function
              | Top -> false
              | _ -> true)
            m
        in
        if FieldMap.is_empty m
        then fprintf fmt "⊤"
        else
          fprintf
            fmt
            "@[{%a}@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               (fun fmt (f, c) -> fprintf fmt "@[%a →@ %a@]" Field.pp f pp c))
            (FieldMap.bindings m)
      | VariantL m ->
        let m =
          TagMap.filter
            (fun _ -> function
              | Bot -> false
              | _ -> true)
            m
        in
        if TagMap.is_empty m
        then fprintf fmt "⊥"
        else
          fprintf
            fmt
            "@[[%a]@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt "@ | ")
               (fun fmt (tag, c) -> fprintf fmt "@[%a →@ %a@]" Tag.pp tag pp c))
            (TagMap.bindings m)
  end

  module rec ACorr : sig
    type t = Conj of basic list [@@unboxed]

    and basic =
      | Bot
      | Self of Id.t
      | Compose of basic * VCorr.t
      | App of t * LCorr.t

    val self : Id.t -> t
    val rename : Id.t -> Id.t -> t -> t
    val leq : t -> t -> bool
    val compose : t -> VCorr.t -> t
    val apply : t -> LCorr.t -> t
    val inter : t -> t -> t
    val subst : t -> Id.t -> LCorr.t -> LCorr.t
    val widen : t -> t -> t
    val is_atomic : t -> bool
    val pp : Format.formatter -> t -> unit
    val canonize_names_aux : int * Id.t IdMap.t -> t -> t
  end = struct
    type t = Conj of basic list [@@unboxed]

    and basic =
      | Bot
      | Self of Id.t
      | Compose of basic * VCorr.t
      | App of t * LCorr.t

    let bot = Conj [Bot]
    let self x = Conj [Self x]

    let rec rename i j (Conj l) = Conj (List.map (rename_basic i j) l)

    and rename_basic i j = function
      | Bot as c -> c
      | Self x as c -> if Id.equal x i then Self j else c
      | Compose (c1, c2) -> Compose (rename_basic i j c1, c2)
      | App (c1, c2) -> App (rename i j c1, LCorr.rename i j c2)

    let rec leq (Conj l1) (Conj l2) =
      List.for_all (fun c2 -> List.exists (fun c1 -> leq_basic c1 c2) l1) l2

    and leq_basic c1 c2 =
      match (c1, c2) with
      | Bot, _ -> true
      | Self _, Bot -> false
      | Self x1, Self x2 -> Id.equal x1 x2
      | Self _, App _ -> false
      | Compose (_, Bot), _ -> true
      | Compose (c1, Eq), c2 | c1, Compose (c2, Eq) -> leq_basic c1 c2
      | Compose (c1, c1'), Compose (c2, c2') ->
        leq_basic c1 Bot
        || VCorr.leq c1' VCorr.bot
        || (leq_basic c1 c2 && VCorr.leq c1' c2')
      | Self x1, Compose (Self x2, c) -> Id.equal x1 x2 && VCorr.leq VCorr.top c
      | Self _, Compose _ -> false
      | Compose (_, c), _ -> VCorr.leq c VCorr.bot
      | App (c, c'), (Bot | Self _ | Compose _) ->
        leq c bot || LCorr.leq ~strict:false c' LCorr.bot
      | App (c1, c1'), App (c2, c2') ->
        (leq c1 bot || LCorr.leq ~strict:false c1' LCorr.bot)
        || (leq c1 c2 && LCorr.leq ~strict:false c1' c2')

    let rec insert_inter c = function
      | [] -> [c]
      | c' :: l' as l -> (
        if leq_basic c c'
        then insert_inter c l'
        else if leq_basic c' c
        then l
        else
          match (c, c') with
          | Compose (Self x1, c1), Compose (Self x2, c2) when Id.equal x1 x2 ->
            let c12 = VCorr.inter c1 c2 in
            if VCorr.leq c12 VCorr.bot
            then [Bot]
            else if VCorr.leq c12 c1 && VCorr.leq c12 c2
            then
              (* we have lost no information by computing the intersection *)
              insert_inter (Compose (Self x1, c12)) l'
            else
              (* we lost information: do not merge the paths *)
              c' :: insert_inter c l'
          | _ -> c' :: insert_inter c l')

    let rec inter_of_inters l1 l2 =
      match l2 with
      | [] -> l1
      | c :: l2 ->
        if leq_basic c Bot
        then [Bot]
        else inter_of_inters (insert_inter c l1) l2

    let rec compose (Conj l) c =
      let l = List.filter_map (fun c' -> compose_basic c' c) l in
      (* simplify l by merging elements and removing unnecessary ones *)
      let l = inter_of_inters [] l in
      Conj l

    and compose_basic c' c =
      match c' with
      | Bot -> Some Bot
      | Self _ | App _ ->
        if VCorr.leq c VCorr.bot
        then Some Bot
        else if VCorr.leq VCorr.top c
        then None
        else Some (Compose (c', c))
      | Compose (c'1, c'2) ->
        let new_c = VCorr.compose c'2 c in
        if VCorr.leq new_c VCorr.bot
        then Some Bot
        else if VCorr.leq VCorr.top new_c
        then None
        else Some (Compose (c'1, new_c))

    let apply c1 c2 =
      if leq c1 bot || LCorr.leq ~strict:true c2 LCorr.bot
      then bot
      else
        let (Conj l1) = c1 in
        let all_compose_of_vector =
          List.for_all
            (function
              | Compose (_, c) -> VCorr.is_vector c
              | _ -> false)
            l1
        in
        if all_compose_of_vector then c1 else Conj [App (c1, c2)]

    let inter (Conj l1) (Conj l2) = Conj (inter_of_inters l1 l2)

    let rec subst (Conj l) x c =
      map_fold (fun c' -> subst_basic c' x c) LCorr.inter LCorr.top l

    and subst_basic c' x c =
      match c' with
      | Bot -> LCorr.bot
      | Self y -> if Id.equal x y then c else LCorr.self y
      | Compose (c'1, c'2) -> LCorr.compose (subst_basic c'1 x c) c'2
      | App (c'1, c'2) -> LCorr.apply (subst c'1 x c) (LCorr.subst c'2 x c)

    let rec widen (Conj l1) (Conj l2) =
      let conj =
        List.fold_left
          (fun acc c1 ->
            List.fold_left
              (fun acc c2 ->
                match widen_basic c1 c2 with
                | Some c12 -> inter_of_inters [c12] acc
                | None -> acc)
              acc
              l2)
          []
          l1
      in
      Conj conj

    and widen_basic c1 c2 =
      match (c1, c2) with
      | c, Bot -> Some c
      | Bot, _ -> None
      | Compose (c1, VCorr.Eq), c2 | c1, Compose (c2, VCorr.Eq) ->
        widen_basic c1 c2
      | Self x1, Self x2 -> if Id.equal x1 x2 then Some (Self x1) else None
      | Compose (c1, c1'), Compose (c2, c2') -> (
        match widen_basic c1 c2 with
        | Some c12 ->
          let c12' = VCorr.union ~strict:false ~widen:true c1' c2' in
          if VCorr.leq VCorr.top c12' then None else Some (Compose (c12, c12'))
        | None -> None)
      | App (c1, c1'), App (c2, c2') ->
        let c12 = widen c1 c2 in
        Some (App (c12, LCorr.union ~strict:false ~widen:true c1' c2'))
      | App _, Self _
      | Self _, App _
      | App _, Compose _
      | Compose _, App _
      | Self _, Compose _
      | Compose _, Self _ -> None

    let rec is_atomic = function
      | Conj [c] -> is_atomic_basic c
      | _ -> true

    and is_atomic_basic = function
      | Bot | Self _ -> true
      | Compose _ | App _ -> false

    let rec pp fmt (Conj l) =
      let open Format in
      match l with
      | [] -> fprintf fmt "⊤"
      | [c] -> pp_basic fmt c
      | _ ->
        fprintf
          fmt
          "@[⊓(%a)@]"
          (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp_basic)
          l

    and pp_basic fmt =
      let open Format in
      function
      | Bot -> fprintf fmt "⊥"
      | Self x -> Id.pp fmt x
      | Compose ((App _ as c1), c2) ->
        fprintf fmt "@[(%a);@,%a@]" pp_basic c1 VCorr.pp c2
      | Compose (c1, c2) -> fprintf fmt "@[%a;@,%a@]" pp_basic c1 VCorr.pp c2
      | App (c1, c2) ->
        if LCorr.is_atomic c2
        then fprintf fmt "@[%a @@@ %a@]" pp c1 LCorr.pp c2
        else fprintf fmt "@[%a @@@ (%a)@]" pp c1 LCorr.pp c2

    let rec canonize_names_aux env (Conj l) =
      Conj (List.map (canonize_names_basic_aux env) l)

    and canonize_names_basic_aux env = function
      | Bot -> Bot
      | Self x -> Self (get_id x env)
      | Compose (c1, c2) ->
        (* there are no names in c2 *)
        Compose (canonize_names_basic_aux env c1, c2)
      | App (c1, c2) ->
        App (canonize_names_aux env c1, LCorr.canonize_names_aux env c2)
  end

  and LCorr : sig
    type t =
      | Top
      | Disj of ACorr.t list
      | TupleR of t IntMap.t
      | RecordR of t FieldMap.t
      | VariantR of t TagMap.t
      | FunsR of (ACorr.t list * Id.t * t * t) LabMap.t

    (* disjunction of a conjunction of functions and a disjunction of
       correlations *)
    val bot : t
    val top : t
    val self : Id.t -> t
    val rename : Id.t -> Id.t -> t -> t
    val leq : strict:bool -> t -> t -> bool
    val union : strict:bool -> widen:bool -> t -> t -> t
    val inter : t -> t -> t
    val compose : t -> VCorr.t -> t
    val apply : t -> t -> t
    val subst : t -> Id.t -> t -> t
    val is_atomic : t -> bool
    val pp : Format.formatter -> t -> unit
    val canonize_names_aux : int * Id.t IdMap.t -> t -> t
    val canonize_names : t -> t
  end = struct
    type t =
      | Top
      | Disj of ACorr.t list
      | TupleR of t IntMap.t
      | RecordR of t FieldMap.t
      | VariantR of t TagMap.t
      | FunsR of (ACorr.t list * Id.t * t * t) LabMap.t

    (* disjunction of a conjunction of functions and a disjunction of
       correlations *)

    let top_disj = [ACorr.Conj []]

    let is_top_disj = function
      | [ACorr.Conj []] -> true
      | _ -> false

    let is_bot_disj = function
      | [] -> true
      | disj ->
        List.for_all
          (function
            | ACorr.Conj conj ->
              List.exists
                (function
                  | ACorr.Bot -> true
                  | _ -> false)
                conj)
          disj

    let is_top = function
      | Top -> true
      | _ -> false

    let is_bot = function
      | Disj [] -> true
      | _ -> false

    let is_atomic = function
      | Disj [c] -> ACorr.is_atomic c
      | Disj _ | Top | TupleR _ | RecordR _ | VariantR _ | FunsR _ -> true

    let rec pp fmt =
      let open Format in
      function
      | Top -> fprintf fmt "⊤"
      | Disj [] -> fprintf fmt "⊥"
      | Disj [c] -> fprintf fmt "%a" ACorr.pp c
      | Disj l ->
        fprintf
          fmt
          "@[⊔(%a)@]"
          (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") ACorr.pp)
          l
      | TupleR m ->
        let m = IntMap.filter (fun _ c -> not @@ is_top c) m in
        if IntMap.is_empty m
        then fprintf fmt "⊤"
        else
          fprintf
            fmt
            "@[(%a)@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               (fun fmt (i, c) -> fprintf fmt "@[%i ←@ %a@]" i pp c))
            (IntMap.bindings m)
      | RecordR m ->
        let m = FieldMap.filter (fun _ c -> not @@ is_top c) m in
        if FieldMap.is_empty m
        then fprintf fmt "⊤"
        else
          fprintf
            fmt
            "@[{%a}@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               (fun fmt (f, c) -> fprintf fmt "@[%a ←@ %a@]" Field.pp f pp c))
            (FieldMap.bindings m)
      | VariantR m ->
        let m = TagMap.filter (fun _ c -> not @@ is_bot c) m in
        if TagMap.is_empty m
        then fprintf fmt "⊥"
        else
          fprintf
            fmt
            "@[[%a]@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt "@ | ")
               (fun fmt (tag, c) -> fprintf fmt "@[%a ←@ %a@]" Tag.pp tag pp c))
            (TagMap.bindings m)
      | FunsR m ->
        assert (not @@ LabMap.is_empty m);
        if LabMap.cardinal m = 1
        then pp_funs_elt fmt (LabMap.choose m)
        else
          fprintf
            fmt
            "@[⊔(%a)@]"
            (pp_print_list
               ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
               pp_funs_elt)
            (LabMap.bindings m)

    and pp_funs_elt fmt (lab, (disj, x, c1, c2)) =
      let open Format in
      if is_top_disj disj
      then
        fprintf fmt "@[(λ{%a}(%a: %a) ←@ %a)@]" Lab.pp lab Id.pp x pp c1 pp c2
      else
        fprintf
          fmt
          "@[⊓(@[%a@],@ @[(λ{%a}(%a: %a) ←@ %a)@])@]"
          pp
          (Disj disj)
          Lab.pp
          lab
          Id.pp
          x
          pp
          c1
          pp
          c2

    let bot = Disj []
    let top = Top
    let self x = Disj [ACorr.self x]

    let is_bot = function
      | Disj [] -> true
      | _ -> false

    let acorr_bot = ACorr.(Conj [Bot])
    let acorr_top = ACorr.Conj []

    let rec rename i j = function
      | Top as c -> c
      | Disj l -> Disj (List.map (ACorr.rename i j) l)
      | TupleR m -> TupleR (IntMap.map (rename i j) m)
      | RecordR m -> RecordR (FieldMap.map (rename i j) m)
      | VariantR m -> VariantR (TagMap.map (rename i j) m)
      | FunsR m ->
        FunsR
          (LabMap.map
             (fun (disj, x, c1, c2) ->
               ( List.map (ACorr.rename i j) disj,
                 x,
                 rename i j c1,
                 if Id.equal x i
                 then c2
                 else begin
                   assert (not @@ Id.equal x j);
                   (* detect unintended capture *)
                   rename i j c2
                 end ))
             m)

    let rec leq ~strict c1 c2 =
      match (c1, c2) with
      | _, Top | Disj [], _ -> true
      | Top, Disj [] -> false
      | Disj l1, Disj [] -> List.for_all (fun c1 -> ACorr.leq c1 acorr_bot) l1
      | Disj l1, Disj l2 ->
        List.for_all (fun c1 -> List.exists (fun c2 -> ACorr.leq c1 c2) l2) l1
      | TupleR m1, TupleR m2 ->
        IntMap.for_all
          (fun i c1 ->
            let c2 =
              match IntMap.find_opt i m2 with
              | Some c2 -> c2
              | None -> top
            in
            leq ~strict c1 c2)
          m1
      | Top, TupleR m -> IntMap.for_all (fun _ c -> leq ~strict top c) m
      | TupleR _, (RecordR _ | VariantR _ | FunsR _) ->
        assert ((not strict) || not Options.strict_errors);
        false
      | RecordR m1, RecordR m2 ->
        FieldMap.for_all
          (fun f c1 ->
            let c2 =
              match FieldMap.find_opt f m2 with
              | Some c2 -> c2
              | None -> top
            in
            leq ~strict c1 c2)
          m1
      | Top, RecordR m -> FieldMap.for_all (fun _ c -> leq ~strict top c) m
      | RecordR _, (TupleR _ | VariantR _ | FunsR _) ->
        assert ((not strict) || not Options.strict_errors);
        false
      | VariantR m1, VariantR m2 ->
        TagMap.for_all
          (fun tag c2 ->
            let c1 =
              match TagMap.find_opt tag m1 with
              | Some c1 -> c1
              | None -> bot
            in
            leq ~strict c1 c2)
          m2
      | Top, VariantR m -> TagMap.for_all (fun _ c -> leq ~strict top c) m
      | VariantR _, (TupleR _ | RecordR _ | FunsR _) ->
        assert ((not strict) || not Options.strict_errors);
        false
      | FunsR m1, FunsR m2 ->
        assert (not @@ LabMap.is_empty m1);
        assert (not @@ LabMap.is_empty m2);
        LabMap.for_all
          (fun lab (disj1, x1, c1, c1') ->
            is_bot_disj disj1
            ||
            match LabMap.find_opt lab m2 with
            | None -> false
            | Some (disj2, x2, c2, c2') ->
              leq ~strict (Disj disj1) (Disj disj2)
              && leq ~strict c2 c1
              &&
              if Id.equal x1 x2
              then leq ~strict c1' c2'
              else
                let z = Id.fresh () in
                leq ~strict (rename x1 z c1') (rename x2 z c2'))
          m1
      | Top, FunsR m ->
        assert (not @@ LabMap.is_empty m);
        LabMap.for_all
          (fun _lab (disj, _x, c, c') ->
            is_top_disj disj && leq ~strict c bot && leq ~strict top c')
          m
      | FunsR _, (TupleR _ | RecordR _ | VariantR _) ->
        assert ((not strict) || not Options.strict_errors);
        false
      | Disj l, TupleR m ->
        IntMap.for_all
          (fun i ci ->
            let proj_i =
              let open VCorr in
              TupleL (IntMap.mapi (fun j _cj -> if i = j then Eq else Top) m)
            in
            let li = List.map (fun c -> ACorr.compose c proj_i) l in
            leq ~strict (Disj li) ci)
          m
      | Disj l, RecordR m ->
        FieldMap.for_all
          (fun f cf ->
            let proj_f =
              let open VCorr in
              RecordL
                (FieldMap.mapi
                   (fun g _cg -> if Field.equal f g then Eq else Top)
                   m)
            in
            let lf = List.map (fun c -> ACorr.compose c proj_f) l in
            leq ~strict (Disj lf) cf)
          m
      | Disj l, VariantR m ->
        TagMap.for_all
          (fun f cf ->
            let proj_f =
              let open VCorr in
              VariantL
                (TagMap.mapi (fun g _cg -> if Tag.equal f g then Eq else Bot) m)
            in
            let lf = List.map (fun c -> ACorr.compose c proj_f) l in
            leq ~strict (Disj lf) cf)
          m
      | Disj l, FunsR m ->
        List.for_all
          (fun c ->
            LabMap.exists
              (fun _lab (disj, x, _carg, cres) ->
                leq ~strict (Disj [c]) (Disj disj)
                && (* XXX weird: what about carg? *)
                leq ~strict (Disj [ACorr.apply c (self x)]) cres)
              m)
          l
      | TupleR m, Disj [] -> IntMap.exists (fun _ c -> leq ~strict c bot) m
      | RecordR m, Disj [] -> FieldMap.exists (fun _ c -> leq ~strict c bot) m
      | VariantR m, Disj [] -> TagMap.for_all (fun _ c -> leq ~strict c bot) m
      | FunsR m, Disj [] ->
        assert (not @@ LabMap.is_empty m);
        false
      | _, Disj l -> List.exists (fun c -> ACorr.leq acorr_top c) l

    (* XXX weird *)

    let rec insert_union_in_union c = function
      | [] -> [c]
      | c' :: l' as l ->
        if ACorr.leq c c'
        then l
        else if ACorr.leq c' c
        then insert_union_in_union c l'
        else c' :: insert_union_in_union c l'

    let rec union_of_unions l1 l2 =
      match l2 with
      | [] -> l1
      | c2 :: l2 ->
        if ACorr.leq c2 acorr_bot
        then union_of_unions l1 l2
        else union_of_unions (insert_union_in_union c2 l1) l2

    let rec insert_widen_in_union c = function
      | [] -> None
      | c' :: l ->
        let c'' = ACorr.widen c c' in
        if ACorr.leq acorr_top c''
        then
          match insert_widen_in_union c l with
          | Some l' -> Some (c' :: l')
          | None -> None
        else Some (c'' :: l)

    let rec list_widen_of_unions l1 l2 =
      match l2 with
      | [] -> Some l1
      | c2 :: l2 -> (
        match insert_widen_in_union c2 l1 with
        | Some l1' -> list_widen_of_unions l1' l2
        | None -> None)

    let rec union ~strict ~widen c1 c2 =
      match (c1, c2) with
      | Top, _ | _, Top -> top
      | Disj [], c | c, Disj [] -> c
      | Disj l1, Disj l2 ->
        if widen
        then
          match list_widen_of_unions l1 l2 with
          | Some l -> Disj l
          | None -> Top
        else Disj (union_of_unions l1 l2)
      | TupleR m1, TupleR m2 ->
        let m =
          IntMap.merge
            (fun _f oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (union ~strict ~widen c1 c2)
              | Some _, None | None, Some _ -> Some top
              | None, None -> assert false)
            m1
            m2
        in
        if IntMap.for_all (fun _ c -> is_top c) m then top else TupleR m
      | TupleR _, (RecordR _ | VariantR _ | FunsR _)
      | (RecordR _ | VariantR _ | FunsR _), TupleR _ ->
        assert ((not strict) || not Options.strict_errors);
        top
      | RecordR m1, RecordR m2 ->
        let m =
          FieldMap.merge
            (fun _f oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (union ~strict ~widen c1 c2)
              | Some _, None | None, Some _ -> Some top
              | None, None -> assert false)
            m1
            m2
        in
        if FieldMap.for_all (fun _ c -> is_top c) m then top else RecordR m
      | RecordR _, (VariantR _ | FunsR _) | (VariantR _ | FunsR _), RecordR _ ->
        assert ((not strict) || not Options.strict_errors);
        top
      | VariantR m1, VariantR m2 ->
        let m =
          TagMap.merge
            (fun _tag oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 ->
                if widen && is_bot c1 && (not @@ is_bot c2)
                then Some top
                else Some (union ~strict ~widen c1 c2)
              | (Some _ as oc), None -> oc
              | None, (Some c as oc) ->
                if widen && (not @@ is_bot c) then Some top else oc
              | None, None -> assert false)
            m1
            m2
        in
        if TagMap.for_all (fun _ c -> is_top c) m then top else VariantR m
      | VariantR _, FunsR _ | FunsR _, VariantR _ ->
        assert ((not strict) || not Options.strict_errors);
        top
      | FunsR m1, FunsR m2 ->
        assert (not @@ LabMap.is_empty m1);
        assert (not @@ LabMap.is_empty m2);
        FunsR
          (LabMap.union
             (fun _lab (disj1, x1, c1, c1') (disj2, x2, c2, c2') ->
               let disj12 =
                 if widen
                 then
                   let disj2' =
                     List.map (fun c2 -> ACorr.compose c2 VCorr.top) disj2
                   in
                   match list_widen_of_unions disj1 disj2' with
                   | Some conj -> conj
                   | None -> [acorr_top]
                   (* top *)
                 else union_of_unions disj1 disj2
               in
               if leq ~strict:true c1 c2
               then
                 if Id.equal x1 x2
                 then Some (disj12, x1, c1, union ~strict ~widen c1' c2')
                 else
                   let z = Id.fresh () in
                   Some
                     ( disj12,
                       z,
                       c1,
                       union ~strict ~widen (rename x1 z c1') (rename x2 z c2')
                     )
               else if leq ~strict:true c2 c1
               then
                 if Id.equal x1 x2
                 then Some (disj12, x1, c2, union ~strict ~widen c1' c2')
                 else
                   let z = Id.fresh () in
                   Some
                     ( disj12,
                       z,
                       c2,
                       union ~strict ~widen (rename x1 z c1') (rename x2 z c2')
                     )
               else Some (disj12, x1, bot, top))
             m1
             m2)
      | VariantR m1, Disj l2 ->
        let m =
          TagMap.mapi
            (fun tag c1 ->
              let ctag =
                let mtag =
                  TagMap.mapi
                    (fun tag' _ ->
                      if Tag.equal tag tag' then VCorr.eq else VCorr.bot)
                    m1
                in
                VCorr.VariantL mtag
              in
              union
                ~strict
                ~widen
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 ctag) l2)))
            m1
        in
        if TagMap.for_all (fun _ c -> is_top c) m then top else VariantR m
      | Disj l1, VariantR m2 ->
        let m =
          TagMap.mapi
            (fun tag c2 ->
              let ctag =
                let mtag =
                  TagMap.mapi
                    (fun tag' _ ->
                      if Tag.equal tag tag' then VCorr.eq else VCorr.bot)
                    m2
                in
                VCorr.VariantL mtag
              in
              union
                ~strict
                ~widen
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c1 -> ACorr.compose c1 ctag) l1))
                c2)
            m2
        in
        if TagMap.for_all (fun _ c -> is_top c) m then top else VariantR m
      | TupleR m1, Disj l2 ->
        let m =
          IntMap.mapi
            (fun i c1 ->
              let ci =
                let mi =
                  IntMap.mapi
                    (fun i' _ -> if i = i' then VCorr.eq else VCorr.top)
                    m1
                in
                VCorr.TupleL mi
              in
              union
                ~strict
                ~widen
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 ci) l2)))
            m1
        in
        if IntMap.for_all (fun _ c -> is_top c) m then top else TupleR m
      | Disj l1, TupleR m2 ->
        let m =
          IntMap.mapi
            (fun i c2 ->
              let ci =
                let mi =
                  IntMap.mapi
                    (fun i' _ -> if i = i' then VCorr.eq else VCorr.top)
                    m2
                in
                VCorr.TupleL mi
              in
              union
                ~strict
                ~widen
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c1 -> ACorr.compose c1 ci) l1))
                c2)
            m2
        in
        if IntMap.for_all (fun _ c -> is_top c) m then top else TupleR m
      | RecordR m1, Disj l2 ->
        let m =
          FieldMap.mapi
            (fun f c1 ->
              let cf =
                let mf =
                  FieldMap.mapi
                    (fun f' _ ->
                      if Field.equal f f' then VCorr.eq else VCorr.top)
                    m1
                in
                VCorr.RecordL mf
              in
              union
                ~strict
                ~widen
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 cf) l2)))
            m1
        in
        if FieldMap.for_all (fun _ c -> is_top c) m then top else RecordR m
      | Disj l1, RecordR m2 ->
        let m =
          FieldMap.mapi
            (fun f c2 ->
              let cf =
                let mf =
                  FieldMap.mapi
                    (fun f' _ ->
                      if Field.equal f f' then VCorr.eq else VCorr.top)
                    m2
                in
                VCorr.RecordL mf
              in
              union
                ~strict
                ~widen
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c1 -> ACorr.compose c1 cf) l1))
                c2)
            m2
        in
        if FieldMap.for_all (fun _ c -> is_top c) m then top else RecordR m
      | FunsR m1, (Disj _ as c2) ->
        assert (not @@ LabMap.is_empty m1);
        let disj1 =
          LabMap.map_fold
            (fun (disj, _x, _c, _c') -> Disj disj)
            (union ~strict ~widen:false)
            bot
            m1
        in
        union ~strict ~widen disj1 c2
      | (Disj _ as c1), FunsR m2 ->
        assert (not @@ LabMap.is_empty m2);
        let disj2 =
          LabMap.map_fold
            (fun (disj, _x, _c, _c') -> Disj disj)
            (union ~strict ~widen:false)
            bot
            m2
        in
        union ~strict ~widen c1 disj2

    let rec insert_inter_in_union c = function
      | [] -> []
      | c' :: l ->
        insert_union_in_union (ACorr.inter c c') @@ insert_inter_in_union c l

    let rec inter_of_unions l1 = function
      | [] -> []
      | c2 :: l2 ->
        union_of_unions (insert_inter_in_union c2 l1) (inter_of_unions l1 l2)

    let rec inter c1 c2 =
      match (c1, c2) with
      | Top, c | c, Top -> c
      | Disj [], _ | _, Disj [] -> bot
      | Disj l1, Disj l2 -> Disj (inter_of_unions l1 l2)
      | TupleR m1, TupleR m2 ->
        let m =
          IntMap.merge
            (fun _i oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | (Some _ as oc), None | None, (Some _ as oc) -> oc
              | None, None -> assert false)
            m1
            m2
        in
        if IntMap.exists (fun _ c -> is_bot c) m
        then bot
        else if IntMap.for_all (fun _ c -> is_top c) m
        then top
        else TupleR m
      | TupleR _, (RecordR _ | VariantR _ | FunsR _)
      | (RecordR _ | VariantR _ | FunsR _), TupleR _ ->
        assert (not Options.strict_errors);
        bot
      | RecordR m1, RecordR m2 ->
        let m =
          FieldMap.merge
            (fun _f oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | (Some _ as oc), None | None, (Some _ as oc) -> oc
              | None, None -> assert false)
            m1
            m2
        in
        if FieldMap.exists (fun _ c -> is_bot c) m
        then bot
        else if FieldMap.for_all (fun _ c -> is_top c) m
        then top
        else RecordR m
      | RecordR _, (VariantR _ | FunsR _) | (VariantR _ | FunsR _), RecordR _ ->
        assert (not Options.strict_errors);
        bot
      | VariantR m1, VariantR m2 ->
        let m =
          TagMap.merge
            (fun _tag oc1 oc2 ->
              match (oc1, oc2) with
              | Some c1, Some c2 -> Some (inter c1 c2)
              | Some _, None | None, Some _ -> Some bot
              | None, None -> assert false)
            m1
            m2
        in
        if TagMap.for_all (fun _ c -> is_bot c) m
        then bot
        else if TagMap.for_all (fun _ c -> is_top c) m
        then top
        else VariantR m
      | VariantR _, FunsR _ | FunsR _, VariantR _ ->
        assert (not Options.strict_errors);
        bot
      | FunsR m1, FunsR m2 ->
        assert (not @@ LabMap.is_empty m1);
        assert (not @@ LabMap.is_empty m2);
        let m =
          LabMap.merge
            (fun _lab o1 o2 ->
              match (o1, o2) with
              | Some (disj1, x1, c1, c1'), Some (disj2, x2, c2, c2') ->
                let disj12 = inter_of_unions disj1 disj2 in
                if is_bot_disj disj12
                then None
                else if leq ~strict:true c1 c2
                then
                  if Id.equal x1 x2
                  then Some (disj12, x1, c1, inter c1' c2')
                  else
                    let z = Id.fresh () in
                    Some
                      (disj12, z, c1, inter (rename x1 z c1') (rename x2 z c2'))
                else if leq ~strict:true c2 c1
                then
                  if Id.equal x1 x2
                  then Some (disj12, x1, c2, inter c1' c2')
                  else
                    let z = Id.fresh () in
                    Some
                      (disj12, z, c2, inter (rename x1 z c1') (rename x2 z c2'))
                else Some (disj12, x1, bot, top)
              | None, _ | _, None -> None)
            m1
            m2
        in
        if LabMap.is_empty m then bot else FunsR m
      | VariantR m1, Disj l2 | Disj l2, VariantR m1 ->
        let m =
          TagMap.mapi
            (fun tag c1 ->
              let ctag =
                let mtag =
                  TagMap.mapi
                    (fun tag' _ ->
                      if Tag.equal tag tag' then VCorr.eq else VCorr.bot)
                    m1
                in
                VCorr.VariantL mtag
              in
              inter
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 ctag) l2)))
            m1
        in
        if TagMap.for_all (fun _ c -> is_bot c) m
        then bot
        else if TagMap.for_all (fun _ c -> is_top c) m
        then top
        else VariantR m
      | TupleR m1, Disj l2 | Disj l2, TupleR m1 ->
        let m =
          IntMap.mapi
            (fun i c1 ->
              let ci =
                let mi =
                  IntMap.mapi
                    (fun i' _ -> if i = i' then VCorr.eq else VCorr.top)
                    m1
                in
                VCorr.TupleL mi
              in
              inter
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 ci) l2)))
            m1
        in
        if IntMap.exists (fun _ c -> is_bot c) m
        then bot
        else if IntMap.for_all (fun _ c -> is_top c) m
        then top
        else TupleR m
      | RecordR m1, Disj l2 | Disj l2, RecordR m1 ->
        let m =
          FieldMap.mapi
            (fun f c1 ->
              let cf =
                let mf =
                  FieldMap.mapi
                    (fun f' _ ->
                      if Field.equal f f' then VCorr.eq else VCorr.top)
                    m1
                in
                VCorr.RecordL mf
              in
              inter
                c1
                (Disj
                   (union_of_unions []
                   @@ List.map (fun c2 -> ACorr.compose c2 cf) l2)))
            m1
        in
        if FieldMap.exists (fun _ c -> is_bot c) m
        then bot
        else if FieldMap.for_all (fun _ c -> is_top c) m
        then top
        else RecordR m
      | FunsR m1, Disj disj2 | Disj disj2, FunsR m1 ->
        let m =
          LabMap.union
            (fun _ (disj1, x, c1', c1'') _ ->
              let disj12 = inter_of_unions disj1 disj2 in
              if is_bot_disj disj12 then None else Some (disj12, x, c1', c1''))
            m1
            m1
        in
        if LabMap.is_empty m then bot else FunsR m

    let rec compose c c' =
      match (c, c') with
      | _, VCorr.Bot -> bot
      | c, VCorr.Eq -> c
      | Top, _ -> top
      | Disj l, c' ->
        Disj (union_of_unions [] @@ List.map (fun c -> ACorr.compose c c') l)
      | TupleR m, VCorr.Top ->
        IntMap.map_fold (fun c -> compose c c') inter top m
      | TupleR m, VCorr.TupleL m' ->
        IntMap.map_fold2
          (fun oc1 oc2 ->
            match (oc1, oc2) with
            | Some c1, Some c2 -> compose c1 c2
            | Some c1, None -> compose c1 VCorr.top
            | None, Some c2 -> compose top c2
            | None, None -> assert false)
          inter
          top
          m
          m'
      | TupleR _, (VCorr.RecordL _ | VCorr.VariantL _) ->
        assert (not Options.strict_errors);
        bot
      | RecordR m, VCorr.Top ->
        FieldMap.map_fold (fun c -> compose c c') inter top m
      | RecordR m, VCorr.RecordL m' ->
        FieldMap.map_fold2
          (fun oc1 oc2 ->
            match (oc1, oc2) with
            | Some c1, Some c2 -> compose c1 c2
            | Some c1, None -> compose c1 VCorr.top
            | None, Some c2 -> compose top c2
            | None, None -> assert false)
          inter
          top
          m
          m'
      | RecordR _, (VCorr.TupleL _ | VCorr.VariantL _) ->
        assert (not Options.strict_errors);
        bot
      | VariantR m, VCorr.Top ->
        TagMap.map_fold
          (fun c -> compose c c')
          (union ~strict:true ~widen:false)
          bot
          m
      | VariantR m, VCorr.VariantL m' ->
        TagMap.map_fold2
          (fun oc1 oc2 ->
            match (oc1, oc2) with
            | Some c1, Some c2 -> compose c1 c2
            | _ -> bot)
          (union ~strict:true ~widen:false)
          bot
          m
          m'
      | VariantR _, (VCorr.TupleL _ | VCorr.RecordL _) ->
        assert (not Options.strict_errors);
        bot
      | FunsR m, VCorr.Top ->
        assert (not @@ LabMap.is_empty m);
        top
      | FunsR m, (VCorr.TupleL _ | VCorr.RecordL _ | VCorr.VariantL _) ->
        assert (not @@ LabMap.is_empty m);
        assert (not Options.strict_errors);
        bot

    let rec subst c' x c =
      match c' with
      | Top -> top
      | Disj l ->
        map_fold
          (fun c' -> ACorr.subst c' x c)
          (union ~strict:true ~widen:false)
          bot
          l
      | TupleR m ->
        let m = IntMap.map (fun c' -> subst c' x c) m in
        if IntMap.exists (fun _ c -> is_bot c) m
        then bot
        else if IntMap.for_all (fun _ c -> is_top c) m
        then top
        else TupleR m
      | RecordR m ->
        let m = FieldMap.map (fun c' -> subst c' x c) m in
        if FieldMap.exists (fun _ c -> is_bot c) m
        then bot
        else if FieldMap.for_all (fun _ c -> is_top c) m
        then top
        else RecordR m
      | VariantR m ->
        let m = TagMap.map (fun c' -> subst c' x c) m in
        if TagMap.for_all (fun _ c -> is_bot c) m
        then bot
        else if TagMap.for_all (fun _ c -> is_top c) m
        then top
        else VariantR m
      | FunsR m ->
        assert (not @@ LabMap.is_empty m);
        LabMap.mapi_fold
          (fun lab (disj, y, c', c'') ->
            let c1 = subst (Disj disj) x c
            and c2 =
              if Id.equal x y
              then FunsR (LabMap.singleton lab (top_disj, y, subst c' x c, c''))
              else
                let z = Id.fresh () in
                FunsR
                  (LabMap.singleton
                     lab
                     (top_disj, z, subst c' x c, subst (rename y z c'') x c))
            in
            inter c1 c2)
          (union ~strict:true ~widen:false)
          bot
          m

    let rec apply0 c c' =
      match c with
      | Top -> top
      | Disj l ->
        map_fold
          (fun c -> Disj [ACorr.apply c c'])
          (union ~strict:true ~widen:false)
          bot
          l
      | TupleR _ | RecordR _ | VariantR _ ->
        assert (not Options.strict_errors);
        bot
      | FunsR m ->
        assert (not @@ LabMap.is_empty m);
        LabMap.map_fold
          (fun (disj, x, c1, c2) ->
            let c0 = apply0 (Disj disj) c'
            and c0' = if leq ~strict:true c' c1 then subst c2 x c' else top in
            inter c0 c0')
          (union ~strict:true ~widen:false)
          bot
          m

    let apply c c' = inter (compose c' VCorr.top) (apply0 c c')

    let rec canonize_names_aux env = function
      | Top -> top
      | Disj l -> Disj (List.map (ACorr.canonize_names_aux env) l)
      | TupleR m -> TupleR (IntMap.map (canonize_names_aux env) m)
      | RecordR m -> RecordR (FieldMap.map (canonize_names_aux env) m)
      | VariantR m -> VariantR (TagMap.map (canonize_names_aux env) m)
      | FunsR m ->
        FunsR
          (LabMap.map
             (fun (disj, x, c1, c2) ->
               let env_x = extend_env env x in
               ( List.map (ACorr.canonize_names_aux env) disj,
                 get_id x env_x,
                 canonize_names_aux env c1,
                 canonize_names_aux env_x c2 ))
             m)

    let canonize_names c = canonize_names_aux (0, IdMap.empty) c
  end

  module V = struct
    type t = VCorr.t

    let eq = VCorr.eq
    let top = VCorr.top
    let bot = VCorr.bot
    let leq = VCorr.leq
    let tuple m = VCorr.TupleL m
    let record m = VCorr.RecordL m
    let variant m = VCorr.VariantL m
    let union c1 c2 = VCorr.union ~strict:true ~widen:false c1 c2
    let inter = VCorr.inter
    let pp = VCorr.pp
  end

  type t = LCorr.t

  let bot = LCorr.bot
  let top = LCorr.top
  let self = LCorr.self
  let tuple m = LCorr.TupleR m
  let record m = LCorr.RecordR m
  let variant m = LCorr.VariantR m
  let top_disj = [ACorr.Conj []]

  let func lab x arg body =
    LCorr.FunsR (LabMap.singleton lab (top_disj, x, arg, body))

  let leq c1 c2 = LCorr.leq ~strict:true c1 c2
  let union c1 c2 = LCorr.union ~strict:true ~widen:false c1 c2
  let inter = LCorr.inter
  let widen c1 c2 = LCorr.union ~strict:true ~widen:true c1 c2
  let compose = LCorr.compose
  let apply = LCorr.apply
  let let_ x c1 c2 = LCorr.(inter (compose c1 V.top) (subst c2 x c1))
  let pp = LCorr.pp
  let canonize_names = LCorr.canonize_names
end
