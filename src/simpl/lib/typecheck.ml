open Ast

exception Error of Location.t * string

module type OPTIONS = sig end

module Make (Options : OPTIONS) = struct
  module Env = VarMap
  module TEnv = TyIdMap
  module TyEnv = TyVarMap

  let merge_tyenvs env1 env2 = TyEnv.union (fun _ _ v -> Some v) env1 env2

  let check_ty_id_args loc x n_formals n_actuals =
    if not (n_formals = n_actuals)
    then
      let msg =
        Format.asprintf
          "Type %a expects %i %s, but %i %s provided"
          TyId.pp
          x
          n_formals
          (if n_formals >= 2 then "arguments" else "argument")
          n_actuals
          (if n_formals >= 2 then "arguments were" else "argument was")
      in
      raise @@ Error (loc, msg)

  let rec translate_typ tenv tyenv { Location.contents = typ; loc } =
    translate_typ_ loc tenv tyenv typ

  and translate_typ_ loc tenv tyenv =
    let open Outer in
    function
    | TyVar x -> (
      match TyEnv.find_opt x tyenv with
      | Some tyx -> tyx
      | None ->
        let msg = Format.asprintf "Unbound type variable %a" TyVar.pp x in
        raise @@ Error (loc, msg))
    | TyId (x, args) -> (
      match TEnv.find_opt x tenv with
      | Some (Inner.TyBodyAlias (formals, tyx)) ->
        check_ty_id_args loc x (List.length formals) (List.length args);
        let instances =
          List.fold_left2
            (fun acc xi tyi -> TyEnv.add xi (translate_typ tenv tyenv tyi) acc)
            TyEnv.empty
            formals
            args
        in
        Inner.typ_subst (merge_tyenvs tyenv instances) tyx
      | Some (Inner.TyBodyRecord _ | Inner.TyBodyVariant _) ->
        TyId (x, List.map (translate_typ tenv tyenv) args)
      | None ->
        let msg = Format.asprintf "Unbound type identifier %a" TyId.pp x in
        raise @@ Error (loc, msg))
    | TyTuple tys -> Inner.TyTuple (List.map (translate_typ tenv tyenv) tys)
    | TyFun (ty1, ty2) ->
      Inner.TyFun (translate_typ tenv tyenv ty1, translate_typ tenv tyenv ty2)
    | TyParens ty -> translate_typ tenv tyenv ty

  let make_tyenv loc args =
    List.fold_left
      (fun acc x ->
        if TyEnv.mem x acc
        then
          let msg =
            Format.asprintf
              "Type parameter %a appears more than once"
              TyVar.pp
              x
          in
          raise @@ Error (loc, msg)
        else TyEnv.add x (Inner.TyVar x) acc)
      TyEnv.empty
      args

  let translate_typ_def loc tenv = function
    | Outer.TyDefAlias (args, typ) ->
      let tyenv = make_tyenv loc args in
      Inner.TyBodyAlias (args, translate_typ tenv tyenv typ)
    | Outer.TyDefRecord (args, fs) as ty_def ->
      let tyenv = make_tyenv loc args in
      let fs =
        List.fold_left
          (fun fs ({ Location.contents = f; loc = f_loc }, ty) ->
            if FieldMap.mem f fs
            then
              let msg =
                Format.asprintf
                  "@[Duplicate field %a@ in record type definition@ %a@]"
                  Field.pp
                  f
                  Outer.pp_typ_def
                  ty_def
              in
              raise @@ Error (f_loc, msg)
            else
              let ty = translate_typ tenv tyenv ty in
              FieldMap.add f ty fs)
          FieldMap.empty
          fs
      in
      Inner.TyBodyRecord (args, fs)
    | Outer.TyDefVariant (args, cs) as ty_def ->
      let tyenv = make_tyenv loc args in
      let cs =
        List.fold_left
          (fun cs ({ Location.contents = c; loc = c_loc }, c_args) ->
            if TagMap.mem c cs
            then
              let msg =
                Format.asprintf
                  "@[Duplicate constructor %a@ in sum type definition@ %a@]"
                  Tag.pp
                  c
                  Outer.pp_typ_def
                  ty_def
              in
              raise @@ Error (c_loc, msg)
            else
              let args = List.map (translate_typ tenv tyenv) c_args in
              TagMap.add c args cs)
          TagMap.empty
          cs
      in
      Inner.TyBodyVariant (args, cs)

  let search_type f m =
    TEnv.fold (fun k a acc -> if f k a then (k, a) :: acc else acc) m []

  let search_record_type loc tenv field =
    let candidates =
      search_type
        (fun _ -> function
          | Inner.TyBodyRecord (_args, fs) -> FieldMap.mem field fs
          | _ -> false)
        tenv
    in
    match candidates with
    | [(x, Inner.TyBodyRecord (args, fs))] -> (x, args, fs)
    | [_] -> assert false
    | [] ->
      let msg = Format.asprintf "Unknown record field %a" Field.pp field in
      raise @@ Error (loc, msg)
    | (x1, _) :: (x2, _) :: _ ->
      let msg =
        Format.asprintf
          "@[Ambiguous record field. Field@ %a@ is declared in the type \
           definitions of@ %a@ and@ %a@]"
          Field.pp
          field
          TyId.pp
          x1
          TyId.pp
          x2
      in
      raise @@ Error (loc, msg)

  let search_variant_type loc tenv tag =
    let candidates =
      search_type
        (fun _ -> function
          | Inner.TyBodyVariant (_args, tags) -> TagMap.mem tag tags
          | _ -> false)
        tenv
    in
    match candidates with
    | [(x, Inner.TyBodyVariant (args, tags))] -> (x, args, tags)
    | [_] -> assert false
    | [] ->
      let msg = Format.asprintf "Unknown variant constructor %a" Tag.pp tag in
      raise @@ Error (loc, msg)
    | (x1, _) :: (x2, _) :: _ ->
      let msg =
        Format.asprintf
          "@[Ambiguous variant constructor. Constructor@ %a@ is declared in \
           the type definitions of@ %a@ and@ %a@]"
          Tag.pp
          tag
          TyId.pp
          x1
          TyId.pp
          x2
      in
      raise @@ Error (loc, msg)

  let check_fields loc es record_name record_def =
    let undefined_fields =
      List.fold_left
        (fun remaining ({ Location.contents = f; loc = loc_f }, _) ->
          if FieldMap.mem f remaining
          then FieldMap.remove f remaining
          else if FieldMap.mem f record_def
          then
            let msg =
              Format.asprintf
                "Record field %a is defined more than once"
                Field.pp
                f
            in
            raise @@ Error (loc_f, msg)
          else
            let msg =
              Format.asprintf
                "The record field %a does not belong to the definition of type \
                 %a"
                Field.pp
                f
                TyId.pp
                record_name
            in
            raise @@ Error (loc_f, msg))
        record_def
        es
    in
    if not @@ FieldMap.is_empty undefined_fields
    then
      let msg =
        Format.asprintf
          "@[The following record fields are required by the definition of \
           type %a, but are not provided:@.%a@]"
          TyId.pp
          record_name
          (Format.pp_print_list ~pp_sep:Format.pp_print_space Field.pp)
          (List.map fst @@ FieldMap.bindings undefined_fields)
      in
      raise @@ Error (loc, msg)

  module TeVar = struct
    type t = Var.t

    let equal = Var.equal
    let hash = Hashtbl.hash
    let to_string x = x
  end

  module Types = struct
    type 'a structure =
      | UnifVar of int
      | User of TyId.t * 'a list
      | Tuple of 'a list
      | Arrow of 'a * 'a
      | Bool
      | Int

    let map f = function
      | UnifVar _ as x -> x
      | User (ty, args) -> User (ty, List.map f args)
      | Tuple args -> Tuple (List.map f args)
      | Arrow (t1, t2) -> Arrow (f t1, f t2)
      | (Bool | Int) as x -> x

    let iter f = function
      | UnifVar _ -> ()
      | User (_, args) | Tuple args -> List.iter f args
      | Arrow (t1, t2) ->
        f t1;
        f t2
      | Bool | Int -> ()

    let fold f ty acc =
      match ty with
      | UnifVar _ -> acc
      | User (_, args) | Tuple args -> List.fold_right f args acc
      | Arrow (t1, t2) -> f t2 (f t1 acc)
      | Bool | Int -> acc

    exception Iter2

    let iter2 f ty1 ty2 =
      match (ty1, ty2) with
      | UnifVar x1, UnifVar x2 -> if x1 = x2 then () else raise Iter2
      | User (t1, args1), User (t2, args2) ->
        if TyId.equal t1 t2 && List.length args1 = List.length args2
        then List.iter2 f args1 args2
        else raise Iter2
      | Tuple args1, Tuple args2 ->
        if List.length args1 = List.length args2
        then List.iter2 f args1 args2
        else raise Iter2
      | Arrow (ty11, ty12), Arrow (ty21, ty22) ->
        f ty11 ty21;
        f ty12 ty22
      | Bool, Bool | Int, Int -> ()
      | UnifVar _, (User _ | Tuple _ | Arrow _ | Bool | Int)
      | User _, (UnifVar _ | Tuple _ | Arrow _ | Bool | Int)
      | Arrow _, (UnifVar _ | User _ | Tuple _ | Bool | Int)
      | Tuple _, (UnifVar _ | User _ | Arrow _ | Bool | Int)
      | Bool, (UnifVar _ | User _ | Tuple _ | Arrow _ | Int)
      | Int, (UnifVar _ | User _ | Tuple _ | Arrow _ | Bool) -> raise Iter2

    exception InconsistentConjunction = Iter2

    let conjunction f t u =
      iter2 f t u;
      t

    let pprint pprint_node =
      let open PPrint in
      function
      | UnifVar x -> char '#' ^^ OCaml.int x
      | Bool -> OCaml.string "bool"
      | Int -> OCaml.string "int"
      | User (t, []) -> OCaml.string t
      | User (t, args) ->
        group
        @@ parens (separate_map (char ',' ^^ space) pprint_node args)
        ^^ space
        ^^ OCaml.string t
      | Tuple args ->
        parens @@ separate_map (char ',' ^^ space) pprint_node args
      | Arrow (t1, t2) ->
        pprint_node t1 ^^ string " ->" ^^ space ^^ pprint_node t2
  end

  module Out = struct
    type tyvar = int

    let inject : int -> tyvar = fun x -> x

    type 'a structure = 'a Types.structure
    type ty = Inner.typ

    let variable x = Inner.TyUnifVar x

    let structure t =
      match t with
      | Types.UnifVar x -> Inner.TyUnifVar x
      | User (ty, args) -> Inner.TyId (ty, args)
      | Tuple args -> Inner.TyTuple args
      | Arrow (ty1, ty2) -> Inner.TyFun (ty1, ty2)
      | Bool -> Inner.TyBool
      | Int -> Inner.TyInt

    let mu _x ty = ty (* XXX *)
  end

  module Infer = Inferno.Solver.Make (TeVar) (Types) (Out)

  let make_symb =
    let h = Hashtbl.create 10 in
    fun base ->
      let i =
        match Hashtbl.find_opt h base with
        | Some i -> i
        | None -> 0
      in
      Hashtbl.replace h base (i + 1);
      base

  let rec n_fold_aux start i j f acc =
    if i < j then n_fold_aux (start + 1) (i + 1) j f (f start acc) else acc

  let n_fold start n f acc = n_fold_aux start 0 n f acc

  let check_equal_doms loc env1 env2 =
    ignore
    @@ Env.merge
         (fun x o1 o2 ->
           match (o1, o2) with
           | Some _, None | None, Some _ ->
             let msg =
               Format.asprintf
                 "Variable %a must appear in all cases of a disjunctive pattern"
                 Var.pp
                 x
             in
             raise @@ Error (loc, msg)
           | Some _, Some _ | None, None -> None)
         env1
         env2

  let rec env_of_pattern p acc =
    match p.Location.contents with
    | Outer.PVar x ->
      if Env.mem x acc
      then
        let msg =
          Format.asprintf
            "Variable %a appears more than once in pattern"
            Var.pp
            x
        in
        raise @@ Error (p.Location.loc, msg)
      else Env.add x () acc
    | PWild -> acc
    | PConstructor (_, ps) | PTuple ps ->
      List.fold_left (fun acc p -> env_of_pattern p acc) acc ps
    | PRecord ps ->
      List.fold_left (fun acc (_f, p) -> env_of_pattern p acc) acc ps
    | POr [] -> assert false
    | POr (p0 :: ps) ->
      let env0 = env_of_pattern p0 acc in
      let () =
        List.fold_left
          (fun () pi ->
            check_equal_doms p.Location.loc (env_of_pattern pi acc) env0)
          ()
          ps
      in
      env0
    | PBool _ -> acc

  let with_env env f =
    let open Infer in
    Env.fold
      (fun x _ acc env ->
        let@ varx = exist in
        acc (Env.add x varx env))
      env
      f
      Env.empty

  let instantiate_formals tyenv formals =
    List.map (fun x -> TyEnv.find x tyenv) formals

  let with_args formal_args g =
    let open Infer in
    List.fold_left
      (fun acc x tyenv ->
        let@ varx = exist in
        acc (TyEnv.add x varx tyenv))
      g
      formal_args
      TyEnv.empty

  let with_instances formal_args actual_args f g =
    let open Infer in
    List.fold_left2
      (fun acc x v tyenv ->
        let@ varx = exist in
        let+ _ = f v varx
        and+ env = acc (TyEnv.add x varx tyenv) in
        env)
      g
      formal_args
      actual_args
      TyEnv.empty

  let exist_list xs f g =
    let open Infer in
    let+ l =
      List.fold_left
        (fun k xi vars ->
          let@ vari = exist in
          let+ x = f xi vari
          and+ xs = k (vari :: vars) in
          x :: xs)
        (fun vars ->
          let+ _ = g vars in
          [])
        xs
        []
    in
    List.rev l

  let exist_list_ xs f g =
    let open Infer in
    List.fold_left
      (fun k xi vars ->
        let@ vari = exist in
        let+ _ = f xi vari
        and+ res = k (vari :: vars) in
        res)
      g
      xs
      []

  let exist_n start n f g =
    let open Infer in
    n_fold
      start
      n
      (fun i k vars ->
        let@ vari = exist in
        let+ _ = f i vari
        and+ vs = k (vari :: vars) in
        vs)
      g
      []

  let conj_list_gen xs h f =
    let open Infer in
    let+ l =
      List.fold_left
        (fun co x ->
          let+ y = f x
          and+ ys = co in
          h x y :: ys)
        (pure [])
        xs
    in
    List.rev l

  let conj_list xs f = conj_list_gen xs (fun _x y -> y) f

  let conj_record xs g =
    conj_list_gen
      xs
      (fun ({ Location.contents = f; _ }, _) x -> (f, x))
      (fun ({ Location.contents = f; _ }, ef) -> g f ef)

  let conj_list_gen2 xs1 xs2 h f =
    let open Infer in
    let+ l =
      List.fold_left2
        (fun co x1 x2 ->
          let+ y = f x1 x2
          and+ ys = co in
          h x1 x2 y :: ys)
        (pure [])
        xs1
        xs2
    in
    List.rev l

  let conj_list2 xs1 xs2 f = conj_list_gen2 xs1 xs2 (fun _x1 _x2 y -> y) f

  let rec equal_typ loc tenv tyenv ty var : unit Infer.co =
    let open Infer in
    match ty with
    | Inner.TyVar x -> (
      match TyEnv.find_opt x tyenv with
      | None ->
        let msg = Format.asprintf "@[Unbound type variable %a@]" TyVar.pp x in
        raise @@ Error (loc, msg)
      | Some varx -> var -- varx)
    | TyUnifVar x -> var --- Types.UnifVar x
    | TyId (x, args) -> (
      match TyIdMap.find_opt x tenv with
      | None ->
        let msg = Format.asprintf "@[Unbound type identifier %a@]" TyId.pp x in
        raise @@ Error (loc, msg)
      | Some (Inner.TyBodyAlias (formal_args, ty)) ->
        check_ty_id_args loc x (List.length formal_args) (List.length args);
        let@ tyenv' =
          with_instances formal_args args (fun argi vari ->
              equal_typ loc tenv tyenv argi vari)
        in
        equal_typ loc tenv (merge_tyenvs tyenv tyenv') ty var
      | Some (TyBodyRecord (formal_args, _) | TyBodyVariant (formal_args, _)) ->
        check_ty_id_args loc x (List.length formal_args) (List.length args);
        let@ vars =
          exist_list_ args (fun tyi vari -> equal_typ loc tenv tyenv tyi vari)
        in
        var --- Types.User (x, vars))
    | TyTuple tys ->
      let@ vars =
        exist_list_ tys (fun tyi vari -> equal_typ loc tenv tyenv tyi vari)
      in
      var --- Types.Tuple vars
    | TyFun (ty1, ty2) ->
      let@ var1 = exist in
      let@ var2 = exist in
      let+ () = equal_typ loc tenv tyenv ty1 var1
      and+ () = equal_typ loc tenv tyenv ty2 var2
      and+ () = var --- Types.Arrow (var1, var2) in
      ()
    | TyBool -> var --- Types.Bool
    | TyInt -> var --- Types.Int

  let rec infer_expr tenv e var =
    let open Infer in
    correlate (Location.get_start e.Location.loc, Location.get_end e.loc)
    @@ let+ e' = infer_expr_ e.Location.loc tenv e.Location.contents var in
       Location.locate e.Location.loc e'

  and infer_expr_ loc tenv e var =
    let open Infer in
    match e with
    | Outer.Var x ->
      let+ _ = instance x var in
      Inner.Var x
    | Let (x, e1, e2) ->
      let+ _ty_scheme, _ty_vars, e1, e2 =
        let1 x (infer_expr tenv e1) (infer_expr tenv e2 var)
      in
      Inner.Let (x, e1, e2)
    | Tuple es ->
      let+ es =
        exist_list
          es
          (fun ei vari -> infer_expr tenv ei vari)
          (fun vars -> var --- Types.Tuple vars)
      in
      Inner.Tuple es
    | TupleProj
        (e, { Location.contents = i; loc = i_loc }, { Location.contents = j; _ })
      ->
      assert (0 <= i);
      (if not (i < j)
       then
         let msg =
           Format.asprintf
             "@[Index %i is beyond the specified tuple length %i@]"
             i
             j
         in
         raise @@ Error (i_loc, msg));
      let@ var' = exist in
      let+ e = infer_expr tenv e var'
      and+ () =
        let@ vars =
          exist_n 0 j (fun i' vari' -> if i = i' then vari' -- var else pure ())
        in
        var' --- Types.Tuple vars
      in
      Inner.TupleProj (e, i, j)
    | Record es ->
      let first_field, f_loc =
        match es with
        | [] -> assert false (* empty record is forbidden *)
        | ({ Location.contents = f; loc = f_loc }, _) :: _ -> (f, f_loc)
      in
      let record_name, formals, record_def =
        search_record_type f_loc tenv first_field
      in
      let () = check_fields loc es record_name record_def in
      let@ tyenv = with_args formals in
      let+ () =
        var --- Types.User (record_name, instantiate_formals tyenv formals)
      and+ es =
        conj_record es (fun f ef ->
            let@ varf = exist in
            let+ () =
              equal_typ
                ef.Location.loc
                tenv
                tyenv
                (FieldMap.find f record_def)
                varf
            and+ res = infer_expr tenv ef varf in
            res)
      in
      Inner.Record es
    | RecordProj (e, { Location.contents = f; loc = loc_f }) ->
      let record_name, formals, record_def = search_record_type loc_f tenv f in
      let ty_f = FieldMap.find f record_def
      and fields = List.map fst @@ FieldMap.bindings record_def in
      let@ tyenv = with_args formals in
      let+ e =
        lift
          (infer_expr tenv)
          e
          (Types.User (record_name, instantiate_formals tyenv formals))
      and+ () = equal_typ loc_f tenv tyenv ty_f var in
      Inner.RecordProj (e, f, fields)
    | RecordUpdate (_, []) ->
      (* parsing ensures there is at least one field update *)
      assert false
    | RecordUpdate (e, [({ Location.contents = f; loc = loc_f }, ef)]) ->
      let record_name, formals, record_def = search_record_type loc_f tenv f in
      let () =
        (* check that the field exists in the type definition *)
        if not @@ FieldMap.mem f record_def
        then
          let msg =
            Format.asprintf
              "Record field %a does not belong to the definition of type %a"
              Field.pp
              f
              TyId.pp
              record_name
          in
          raise @@ Error (loc_f, msg)
      in
      let fields = List.map fst @@ FieldMap.bindings record_def in
      let@ tyenv = with_args formals in
      let+ e = infer_expr tenv e var
      and+ () =
        var --- Types.User (record_name, instantiate_formals tyenv formals)
      and+ ef =
        let@ varf = exist in
        let+ () =
          equal_typ ef.Location.loc tenv tyenv (FieldMap.find f record_def) varf
        and+ res = infer_expr tenv ef varf in
        res
      in
      Inner.RecordUpdate (e, f, ef, fields)
    | RecordUpdate (e, fs) ->
      let e' =
        List.fold_left
          (fun acc (f, ef) ->
            Location.locate Location.(from_pos (get_start loc, get_end ef.loc))
            @@ Outer.RecordUpdate (acc, [(f, ef)]))
          e
          fs
      in
      let+ e = infer_expr tenv e' var in
      e.Location.contents
    | Constructor ({ Location.contents = c; loc = c_loc }, args) ->
      let variant_name, formals, variant_def =
        search_variant_type c_loc tenv c
      in
      let ty_args = TagMap.find c variant_def
      and arity =
        TagMap.fold
          (fun c args acc -> (c, List.length args) :: acc)
          variant_def
          []
      in
      let () =
        let n_formals = List.length ty_args
        and n_actuals = List.length args in
        if not (n_formals = n_actuals)
        then
          let msg =
            Format.asprintf
              "@[Constructor %a expects %i arguments, but %i were provided.@]"
              Tag.pp
              c
              n_formals
              n_actuals
          in
          raise @@ Error (c_loc, msg)
      in
      let@ tyenv = with_args formals in
      let+ () =
        var --- Types.User (variant_name, instantiate_formals tyenv formals)
      and+ args =
        conj_list2 args ty_args (fun arg ty_arg ->
            let@ var' = exist in
            let+ () = equal_typ loc tenv tyenv ty_arg var'
            and+ res = infer_expr tenv arg var' in
            res)
      in
      Inner.Constructor (c, args, arity)
    | Match (e, branches) ->
      let@ var' = exist in
      let+ e = infer_expr tenv e var'
      and+ branches = infer_branches tenv branches var' var in
      Inner.Match (e, branches)
    | App (e1, e2) ->
      let@ var2 = exist in
      let+ e1 = lift (infer_expr tenv) e1 (Types.Arrow (var2, var))
      and+ e2 = infer_expr tenv e2 var2 in
      Inner.App (e1, e2)
    | Fun ({ Location.contents = [(x, oty)]; _ }, e) ->
      let@ var_arg = exist in
      begin
        let+ () =
          begin
            match oty with
            | Some ty ->
              let ty' = translate_typ tenv TyEnv.empty ty in
              equal_typ ty.Location.loc tenv TyEnv.empty ty' var_arg
            | None -> pure ()
          end
        and+ ty_arg = decode var_arg
        and+ e =
          let@ var_res = exist in
          let+ () = var --- Types.Arrow (var_arg, var_res)
          and+ e =
            def x.Location.contents var_arg @@ infer_expr tenv e var_res
          in
          e
        in
        Inner.Fun (x, ty_arg, e)
      end
    | Fun ({ Location.contents = args; loc = args_loc }, e) ->
      let arg =
        let name =
          match args with
          | [] -> "_"
          | _ ->
            let l =
              List.map (fun ({ Location.contents = x; _ }, _) -> x) args
            in
            String.concat "" l
        in
        make_symb name
      in
      let n = List.length args in
      let _, e' =
        List.fold_right
          (fun ({ Location.contents = xi; loc = xi_loc }, _xi_typ) (i, e) ->
            ( i - 1,
              Location.locate
                Location.(from_pos (get_start xi_loc, get_end e.loc))
              @@ Outer.Let
                   ( xi,
                     Location.locate xi_loc
                     @@ Outer.TupleProj
                          ( Location.locate xi_loc @@ Outer.Var arg,
                            Location.locate xi_loc i,
                            Location.locate xi_loc n ),
                     e ) ))
          args
          (n - 1, e)
      in
      let e' =
        let open Location in
        let oty =
          (* XXX give better type in the presence of non-empty tuples*)
          if n = 0 then Some (locate args_loc @@ Outer.TyTuple []) else None
        in
        Outer.Fun (locate args_loc [(locate args_loc arg, oty)], e')
      in
      infer_expr_ loc tenv e' var
    | FixFun (f, { Location.contents = [(x, oty)]; _ }, e) ->
      let@ var_arg = exist in
      begin
        let+ () =
          begin
            match oty with
            | Some ty ->
              let ty' = translate_typ tenv TyEnv.empty ty in
              equal_typ ty.Location.loc tenv TyEnv.empty ty' var_arg
            | None -> pure ()
          end
        and+ ty_arg = decode var_arg
        and+ e =
          let@ var_res = exist in
          let+ () = var --- Types.Arrow (var_arg, var_res)
          and+ e =
            def f var
            @@ def x.Location.contents var_arg
            @@ infer_expr tenv e var_res
          in
          e
        in
        Inner.FixFun (f, x, ty_arg, e)
      end
    | FixFun (f, { Location.contents = args; loc = args_loc }, e) ->
      let arg =
        let name =
          match args with
          | [] -> "_"
          | _ ->
            let l =
              List.map (fun ({ Location.contents = x; _ }, _) -> x) args
            in
            String.concat "" l
        in
        make_symb name
      in
      let n = List.length args in
      let _, e' =
        List.fold_right
          (fun ({ Location.contents = xi; loc = xi_loc }, _xi_typ) (i, e) ->
            ( i - 1,
              Location.locate
                Location.(from_pos (get_start xi_loc, get_end e.loc))
              @@ Outer.Let
                   ( xi,
                     Location.locate xi_loc
                     @@ Outer.TupleProj
                          ( Location.locate xi_loc @@ Outer.Var arg,
                            Location.locate xi_loc i,
                            Location.locate xi_loc n ),
                     e ) ))
          args
          (n - 1, e)
      in
      let e' =
        let open Location in
        let oty =
          (* XXX give better type in the presence of non-empty tuples*)
          if n = 0 then Some (locate args_loc @@ Outer.TyTuple []) else None
        in
        Outer.FixFun (f, locate args_loc [(locate args_loc arg, oty)], e')
      in
      infer_expr_ loc tenv e' var
    | Parens (_, e) ->
      let+ t = infer_expr tenv e var in
      t.Location.contents
    | Unknown -> pure Inner.Unknown
    | Ascribe (e, ty) ->
      let ty' = translate_typ tenv TyEnv.empty ty in
      let+ () = equal_typ ty.Location.loc tenv TyEnv.empty ty' var
      and+ e = infer_expr tenv e var in
      e.Location.contents
    | Bool b ->
      let+ () = var --- Types.Bool in
      Inner.Bool b
    | BAndAlso (e1, e2) ->
      let+ () = var --- Types.Bool
      and+ e1 = infer_expr tenv e1 var
      and+ e2 = infer_expr tenv e2 var in
      Inner.BAndAlso (e1, e2)
    | BOrElse (e1, e2) ->
      let+ () = var --- Types.Bool
      and+ e1 = infer_expr tenv e1 var
      and+ e2 = infer_expr tenv e2 var in
      Inner.BOrElse (e1, e2)
    | IfThenElse (e1, e2, e3) ->
      let+ e1 = lift (infer_expr tenv) e1 Types.Bool
      and+ e2 = infer_expr tenv e2 var
      and+ e3 = infer_expr tenv e3 var in
      Inner.IfThenElse (e1, e2, e3)
    | Int n ->
      let+ () = var --- Types.Int in
      Inner.Int n
    | Binop (op, e1, e2) ->
      let is_boolean_test =
        match op with
        | NEQ | LT | GT | EQ | LE | GE -> true
        | PLUS | MINUS | TIMES | DIV -> false
      in
      let+ () = var --- if is_boolean_test then Types.Bool else Types.Int
      and+ e1 = lift (infer_expr tenv) e1 Types.Int
      and+ e2 = lift (infer_expr tenv) e2 Types.Int in
      Inner.Binop (op, e1, e2)
    | Unop ((UMINUS as op), e) ->
      let+ () = var --- Types.Int
      and+ e = lift (infer_expr tenv) e Types.Int in
      Inner.Unop (op, e)

  and infer_branches tenv branches var_in var_out =
    conj_list branches (fun branch -> infer_branch tenv branch var_in var_out)

  and infer_branch tenv (p, e) var_in var_out =
    let open Infer in
    let@ env = with_env (env_of_pattern p Env.empty) in
    let+ p = infer_pattern tenv env p var_in
    and+ e = Env.fold def env @@ infer_expr tenv e var_out in
    (p, e)

  and infer_pattern tenv env p var_in =
    let open Infer in
    match p.Location.contents with
    | Outer.PVar x ->
      let+ () = var_in -- Env.find x env in
      Location.locate_with p @@ Inner.PVar x
    | PWild -> pure (Location.locate_with p @@ Inner.PWild)
    | PConstructor ({ Location.contents = c; loc = c_loc }, ps) ->
      let variant_name, formals, variant_def =
        search_variant_type c_loc tenv c
      in
      let arity =
        TagMap.fold
          (fun c args acc -> (c, List.length args) :: acc)
          variant_def
          []
      in
      let ty_args = TagMap.find c variant_def in
      let () =
        let n_formals = List.length ty_args
        and n_actuals = List.length ps in
        if not (n_formals = n_actuals)
        then
          let msg =
            Format.asprintf
              "@[Constructor %a expects %i arguments, but %i were provided.@]"
              Tag.pp
              c
              n_formals
              n_actuals
          in
          raise @@ Error (c_loc, msg)
      in
      let@ tyenv = with_args formals in
      let+ () =
        var_in --- Types.User (variant_name, instantiate_formals tyenv formals)
      and+ ps =
        conj_list2 ps ty_args (fun pi tyi ->
            let@ vari = exist in
            let+ () = equal_typ c_loc tenv tyenv tyi vari
            and+ res = infer_pattern tenv env pi vari in
            res)
      in
      Location.locate_with p @@ Inner.PConstructor (c, ps, arity)
    | PTuple ps ->
      let+ ps =
        exist_list
          ps
          (fun pi vari -> infer_pattern tenv env pi vari)
          (fun vars -> var_in --- Types.Tuple vars)
      in
      Location.locate_with p @@ Inner.PTuple ps
    | PRecord ps ->
      let first_field, f_loc =
        match ps with
        | [] -> assert false (* empty record is forbidden *)
        | ({ Location.contents = f; loc = f_loc }, _) :: _ -> (f, f_loc)
      in
      let record_name, formals, record_def =
        search_record_type f_loc tenv first_field
      in
      let () = check_fields p.Location.loc ps record_name record_def in
      let fields = List.map fst @@ FieldMap.bindings record_def in
      let@ tyenv = with_args formals in
      let+ () =
        var_in --- Types.User (record_name, instantiate_formals tyenv formals)
      and+ ps =
        conj_record ps (fun f pf ->
            let@ varf = exist in
            let loc_pf = pf.Location.loc in
            let+ () =
              equal_typ loc_pf tenv tyenv (FieldMap.find f record_def) varf
            and+ res = infer_pattern tenv env pf varf in
            res)
      in
      Location.locate_with p @@ Inner.PRecord (ps, fields)
    | POr ps ->
      let+ ps = conj_list ps (fun pi -> infer_pattern tenv env pi var_in) in
      Location.locate_with p @@ Inner.POr ps
    | PBool b ->
      let+ () = var_in --- Types.Bool in
      Location.locate_with p @@ Inner.PBool b

  let make_name i =
    let base =
      match i mod 24 with
      | 0 -> "α"
      | 1 -> "β"
      | 2 -> "γ"
      | 3 -> "δ"
      | 4 -> "ε"
      | 5 -> "ζ"
      | 6 -> "η"
      | 7 -> "θ"
      | 8 -> "ι"
      | 9 -> "κ"
      | 10 -> "λ"
      | 11 -> "μ"
      | 12 -> "ν"
      | 13 -> "ξ"
      | 14 -> "ο"
      | 15 -> "π"
      | 16 -> "ρ"
      | 17 -> "σ"
      | 18 -> "τ"
      | 19 -> "υ"
      | 20 -> "φ"
      | 21 -> "χ"
      | 22 -> "ψ"
      | 23 -> "ω"
      | _ -> assert false
    in
    let j = i / 24 in
    if j = 0 then base else base ^ string_of_int j

  let make_scheme (vars, ty) =
    let _, vars', m =
      List.fold_left
        (fun (i, l, m) xi ->
          let name = make_name i in
          (i + 1, name :: l, IntMap.add xi (Inner.TyVar name) m))
        (0, [], IntMap.empty)
        vars
    in
    (List.rev vars', Inner.typ_subst_unif_var m ty)

  let rec infer_decl tenv decl =
    let open Infer in
    match decl with
    | Outer.DeclTyp ({ Location.contents = x; loc = x_loc }, ty_def) ->
      (if TEnv.mem x tenv
       then
         let msg = Format.asprintf "Duplicate type definition %a" TyId.pp x in
         raise @@ Error (x_loc, msg));
      let ty_def = translate_typ_def x_loc tenv ty_def in
      ( TEnv.add x ty_def tenv,
        fun co ->
          let+ decls = co in
          Inner.DeclTyp (x, ty_def) :: decls )
    | Outer.DeclVal ({ Location.contents = x; _ }, e) ->
      ( tenv,
        fun co ->
          let+ _ty_vars, scheme, e, decls = let1 x (infer_expr tenv e) co in
          Inner.DeclVal (x, make_scheme scheme, e) :: decls )
    | Outer.DeclFun (x, argss, e) ->
      let e' =
        List.fold_right
          (fun args acc ->
            let open Location in
            let loc = from_pos (get_start args.loc, get_end e.loc) in
            locate loc @@ Outer.Fun (args, acc))
          argss.Location.contents
          e
      in
      infer_decl tenv (Outer.DeclVal (x, e'))
    | Outer.DeclRecFun (x, argss, e) ->
      let loc = Location.(from_pos (get_start x.loc, get_end e.loc)) in
      let e' =
        match argss.Location.contents with
        | [] -> assert false
        | args :: argss ->
          let e' =
            List.fold_right
              (fun args acc ->
                let open Location in
                let loc = from_pos (get_start args.loc, get_end e.loc) in
                locate loc @@ Outer.Fun (args, acc))
              argss
              e
          in
          Location.locate loc @@ Outer.FixFun (x.Location.contents, args, e')
      in
      infer_decl tenv (Outer.DeclVal (x, e'))

  let infer_program tenv env prog =
    let open Infer in
    let init_co co =
      let0
      @@ Env.fold
           (fun x ty co ->
             let@ varx = exist in
             let+ () = equal_typ Location.dummy tenv TyEnv.empty ty varx
             and+ res = def x varx co in
             res)
           env
           co
    in
    List.fold_left
      (fun (tenv_acc, k_acc) decl ->
        let tenv', k' = infer_decl tenv_acc decl in
        (tenv', fun co -> k_acc (k' co)))
      (tenv, fun co -> co)
      prog
    |> fun (tenv, k) ->
    ( tenv,
      let+ _tyvars, decls = init_co (k (pure [])) in
      decls )

  let check_program tenv env prog =
    let open Infer in
    try Ok (solve ~rectypes:false @@ snd @@ infer_program tenv env prog) with
    | Unbound (loc, x) ->
      let msg = Format.asprintf "Unbound variable %a" Var.pp x in
      Error (Location.from_pos loc, msg)
    | Unify (loc, ty1, ty2) ->
      let msg =
        Format.asprintf
          "@[This expression has type@ @[%a@]@ but an expression was expected \
           of type@ @[%a@]@]"
          Inner.pp_typ
          ty2
          Inner.pp_typ
          ty1
      in
      Error (Location.from_pos loc, msg)
    | Cycle (loc, ty) ->
      let msg = Format.asprintf "Cyclic type@ %a" Inner.pp_typ ty in
      Error (Location.from_pos loc, msg)
    | VariableScopeEscape loc ->
      let msg = Format.asprintf "Some rigid variable escapes its scope" in
      Error (Location.from_pos loc, msg)
    | Error (loc, msg) -> Error (loc, msg)

  let init_tenv =
    let open TEnv in
    empty
    |> add "unit" Inner.(TyBodyAlias ([], TyTuple []))
    |> add "bool" Inner.(TyBodyAlias ([], TyBool))
    |> add "int" Inner.(TyBodyAlias ([], TyInt))

  let init_env = Env.empty
  let typecheck prog = check_program init_tenv init_env prog
end
