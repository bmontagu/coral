(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

let parse_string s = Driver.parse Parser.program "" (`String s)

module Parsing = struct
  let test () =
    List.iter
      (fun s ->
        match parse_string s with
        | Ok _ -> Printf.printf "Parsing: OK\n%!"
        | Error (loc, msg) ->
          Format.eprintf "Parsing: Error:@.%a@.%s@." Location.pp loc msg;
          exit 2)
      [
        "val x = ()";
        "type t1 = unit\n\
         val f x = x\n\
         val g (x:t1, y: t2) = (y, x)\r\n\
         val h () = ()\n\
         val x = match () with A y -> match y with | () -> z end end\n\
         val y = let x = () in match x with () -> () end\n\
         val z = match () with () -> let y = () in y end\n\
         val t = match x with A | B -> C end\n\
         val u = let x = A in B\n\
         val u = C\n\
         val u = C ()\n\
         val f = fun () -> ()\n\
         val f () = ()\n\
         val f = fun (x: unit) -> x\n\
         val f (x: unit) = x\n\
         val f = fun (x: unit, y:t) -> x\n\
         val f (x: unit, y:t) = x\n";
      ]
end

module TypeChecking = struct
  module Options = struct end
  module Check = Typecheck.Make (Options)

  let test () =
    List.iter
      (fun s ->
        match parse_string s with
        | Ok p -> (
          Printf.printf "Parsing: OK\n%!";
          match Check.typecheck p with
          | Ok _ -> Printf.printf "Type checking: OK\n%!"
          | Error (loc, msg) ->
            Format.eprintf "Type checking: Error:@.%a@.%s@." Location.pp loc msg
          )
        | Error (loc, msg) ->
          Format.eprintf "Parsing: Error:@.%a@.%s@." Location.pp loc msg)
      [
        "val x = ()";
        "type t1 = unit\n\
         type t2 = A of t1 | B\n\
         val f x = x\n\
         val g (x:t1, y: t2) = (y, x)\r\n\
         val h () = ()\n\
         val x = match A () with A y -> match y with | () -> y end end\n\
         val y = let x = () in match x with () -> () end\n\
         val z = match () with () -> let y = () in y end\n\
         type t3 = C\n\
         val t = match B with A _ | B -> C end\n\
         val u = let x = A () in B\n\
         val u = C\n\
         type u = C2 of unit\n\
         val u = C2 ()\n\
         val f = fun () -> ()\n\
         val f () = ()\n\
         val f = fun (x: unit) -> x\n\
         val f (x: unit) = x\n\
         val f = fun (x: unit, y:t2) -> x\n\
         val f (x: unit, y:t1) = x\n\
         val y = (fun () -> ()) ()";
        "val f (x: unit) = x";
        "val f (x: unit) (y: unit) = x";
        "val f (x: unit) = fun (y: unit) -> x";
      ]
end

let run () =
  print_endline "### Start of internal tests ###";
  Parsing.test ();
  TypeChecking.test ();
  print_endline "### End of internal tests ###"
