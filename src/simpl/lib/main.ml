(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2021
*)

module AnalysisKind = struct
  type t =
    | IndependentAttribute
    | Relational
  [@@deriving enum]

  let to_string = function
    | IndependentAttribute -> "independent attribute"
    | Relational -> "relational"
end

module Options = struct
  type t = {
    run_internal_tests: bool;
    extensional_eq: bool;
    extensional_bot: bool;
    learn_from_patterns: bool;
    strict_errors: bool;
    unroll_fixpoints: int;
    delay_widening: int;
    analysis_kind: AnalysisKind.t;
  }

  module type S = sig
    val extensional_eq : bool
    val extensional_bot : bool
    val learn_from_patterns : bool
    val strict_errors : bool
    val unroll_fixpoints : int
    val delay_widening : int
  end

  let get_module options =
    let module Options = struct
      let extensional_eq = options.extensional_eq
      let extensional_bot = options.extensional_bot
      let learn_from_patterns = options.learn_from_patterns
      let strict_errors = options.strict_errors
      let unroll_fixpoints = options.unroll_fixpoints
      let delay_widening = options.delay_widening
    end in
    (module Options : S)

  module type ANALYZER = sig
    module Env : sig
      type 'a t

      val empty : 'a t
    end

    module Rel : sig
      type t
    end

    type env = Rel.t Env.t

    val interpret_program :
      env ->
      Ast.Inner.program ->
      (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list

    val pp :
      Format.formatter ->
      (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list ->
      unit
  end

  let get_analysis_module (logs : (module Logs.S)) options =
    let module Log = (val logs) in
    let module Options = (val get_module options) in
    (* let module I = (val IntegerDomain.get_module options.integer_domain) in
     * let module C = (val CallDomain.get_module options.call_domain) in
     * let module B = AbstractDomains.Bools in
     * let open Analyzers in *)
    match options.analysis_kind with
    | AnalysisKind.IndependentAttribute ->
      (module AnalyzerRel.Make (Log) (Options) : ANALYZER)
    | AnalysisKind.Relational ->
      (module AnalyzerRel2.Make (Log) (Options) : ANALYZER)
end

let ( >>= ) m f =
  match m with
  | Error _ as err -> err
  | Ok v -> f v

let return x = Ok x

type package = Pack : 'res * (Format.formatter -> 'res -> unit) -> package

type _ output_choice =
  | Value : package output_choice
  | Print : unit output_choice

let run (type a res) (out : res output_choice)
    (pp : Format.formatter -> a -> unit) : _ -> res = function
  | Error (loc, msg) ->
    Format.printf "%a@.%s@." Location.pp loc msg;
    exit 2
  | Ok v -> (
    match out with
    | Print -> Format.printf "@[%a@]@." pp v
    | Value -> Pack (v, pp))

let simpl_gen out options file =
  if options.Options.run_internal_tests then Tests.run ();
  let module Options = struct
    open Options

    let extensional_eq = options.extensional_eq
    let extensional_bot = options.extensional_bot
    let learn_from_patterns = options.learn_from_patterns
    let strict_errors = options.strict_errors
    let unroll_fixpoints = options.unroll_fixpoints
    let delay_widening = options.delay_widening
  end in
  let module Check = Typecheck.Make (Options) in
  let analyze pp interpret empty_env =
    run out pp
    @@ ( Driver.parse_file Parser.program file >>= fun prog ->
         Check.typecheck prog >>= fun prog' ->
         return @@ interpret empty_env prog' )
  in
  match options.analysis_kind with
  | IndependentAttribute ->
    let module Analyze = AnalyzerRel.Make (Logs.Std) (Options) in
    analyze Analyze.pp Analyze.interpret_program Analyze.Env.empty
  | Relational ->
    let module Analyze = AnalyzerRel2.Make (Logs.Std) (Options) in
    analyze Analyze.pp Analyze.interpret_program Analyze.Env.empty

let simpl_print = simpl_gen Print
let simpl_value = simpl_gen Value
