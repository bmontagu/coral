(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2020
*)

open Ast

(** Module of identifiers *)
module Id (Log : Logs.S) : sig
  type t
  (** The type of identifiers *)

  val equal : t -> t -> bool
  (** Equality test *)

  val compare : t -> t -> int
  (** Comparison test *)

  val pp : Format.formatter -> t -> unit
  (** Pretty-printer *)

  val fresh : unit -> t

  val get : Var.t -> t
  (** Gets the most recent identifier for a given variable *)

  val register : Var.t -> t
  (** Registers a variable into a fresh identifier *)

  val unregister : t -> unit
  (** Unregisters an identifier. Only the newest identifier of a
     variable can be unregistered. It is a programming error to
     unregister identifiers without starting with the most recent one.
     *)

  val of_int : int -> t
end = struct
  type t = Var.t * int

  let equal (x1, i1) (x2, i2) = Int.compare i1 i2 = 0 && String.equal x1 x2

  let compare (x1, i1) (x2, i2) =
    let cx = String.compare x1 x2 in
    if cx = 0 then Int.compare i1 i2 else cx

  let get_var = fst
  let get_count = snd

  let pp fmt (x, i) =
    if i = 0
    then Format.fprintf fmt "%a" Var.pp x
    else Format.fprintf fmt "%a/%i" Var.pp x i

  let fresh_counter = ref 0
  let fresh_name = ""

  let fresh () =
    let n = !fresh_counter in
    incr fresh_counter;
    (fresh_name, n)

  module VarMap = Map.Make (Var)

  let m = ref VarMap.empty

  let get x =
    match VarMap.find_opt x !m with
    | Some n ->
      assert (n >= 0);
      (* Log.eprintf "INFO: get %a@." Var.pp x; *)
      (x, n)
    | None ->
      Log.eprintf "ERROR: variable %a is not registered@." Var.pp x;
      assert false

  let register x =
    let n =
      match VarMap.find_opt x !m with
      | Some n ->
        assert (n >= 0);
        n + 1
      | None -> 0
    in
    m := VarMap.add x n !m;
    (* Log.eprintf "INFO: register %a@." pp (x,n); *)
    (x, n)

  let unregister id =
    (* Log.eprintf "INFO: unregister %a@." pp id; *)
    let x = get_var id in
    m :=
      VarMap.update
        x
        (function
          | Some n ->
            assert (n >= 0);
            assert (n = get_count id);
            if n = 0 then None else Some (n - 1)
          | None ->
            Log.eprintf "ERROR: variable %a is not registered@." Var.pp x;
            assert false)
        !m

  let of_int i =
    let base =
      match i mod 24 with
      | 0 -> "α"
      | 1 -> "β"
      | 2 -> "γ"
      | 3 -> "δ"
      | 4 -> "ε"
      | 5 -> "ζ"
      | 6 -> "η"
      | 7 -> "θ"
      | 8 -> "ι"
      | 9 -> "κ"
      | 10 -> "λ"
      | 11 -> "μ"
      | 12 -> "ν"
      | 13 -> "ξ"
      | 14 -> "ο"
      | 15 -> "π"
      | 16 -> "ρ"
      | 17 -> "σ"
      | 18 -> "τ"
      | 19 -> "υ"
      | 20 -> "φ"
      | 21 -> "χ"
      | 22 -> "ψ"
      | 23 -> "ω"
      | _ -> assert false
    in
    let j = i / 24 in
    (base, j)
end

module IdMap (Log : Logs.S) = struct
  include Map.Make (Id (Log))

  let map_fold map reduce init m =
    fold (fun _k v acc -> reduce (map v) acc) m init

  let mapi_fold map reduce init m =
    fold (fun k v acc -> reduce (map k v) acc) m init

  let map_fold2 map reduce init m1 m2 =
    fold
      (fun _k v acc -> reduce v acc)
      (merge (fun _k o1 o2 -> Some (map o1 o2)) m1 m2)
      init
end

module IntMap = struct
  include Map.Make (Int)

  let map_fold map reduce init m =
    fold (fun _k v acc -> reduce (map v) acc) m init

  let mapi_fold map reduce init m =
    fold (fun k v acc -> reduce (map k v) acc) m init

  let map_fold2 map reduce init m1 m2 =
    fold
      (fun _k v acc -> reduce v acc)
      (merge (fun _k o1 o2 -> Some (map o1 o2)) m1 m2)
      init
end

module FieldMap = struct
  include Map.Make (Field)

  let map_fold map reduce init m =
    fold (fun _k v acc -> reduce (map v) acc) m init

  let mapi_fold map reduce init m =
    fold (fun k v acc -> reduce (map k v) acc) m init

  let map_fold2 map reduce init m1 m2 =
    fold
      (fun _k v acc -> reduce v acc)
      (merge (fun _k o1 o2 -> Some (map o1 o2)) m1 m2)
      init
end

module TagMap = struct
  include Map.Make (Tag)

  let map_fold map reduce init m =
    fold (fun _k v acc -> reduce (map v) acc) m init

  let mapi_fold map reduce init m =
    fold (fun k v acc -> reduce (map k v) acc) m init

  let map_fold2 map reduce init m1 m2 =
    fold
      (fun _k v acc -> reduce v acc)
      (merge (fun _k o1 o2 -> Some (map o1 o2)) m1 m2)
      init
end

module type OPTIONS = sig
  val extensional_eq : bool
  val extensional_bot : bool
  val learn_from_patterns : bool
  val strict_errors : bool
  val unroll_fixpoints : int
  val delay_widening : int
end

(** Same as [List.fold_left], but adds an extra argument to the
   higher-order function: the index of the current element. Returns
   the accumulator, along with the length of the list. *)
let fold_lefti f acc l =
  List.fold_left (fun (acc, i) x -> (f acc i x, i + 1)) (acc, 0) l

module Lab = struct
  type t = Lexing.position

  let from_loc = Location.get_start
  let compare = compare

  let pp fmt { Lexing.pos_fname; pos_lnum; pos_bol; pos_cnum } =
    Format.fprintf fmt "%s:%i:%i" pos_fname pos_lnum (pos_cnum - pos_bol)
end

module LabMap = struct
  include Map.Make (Lab)

  let map_fold map reduce init m =
    fold (fun _k v acc -> reduce (map v) acc) m init

  let mapi_fold map reduce init m =
    fold (fun k v acc -> reduce (map k v) acc) m init

  let map_fold2 map reduce init m1 m2 =
    fold
      (fun _k v acc -> reduce v acc)
      (merge (fun _k o1 o2 -> Some (map o1 o2)) m1 m2)
      init
end

module Make (Log : Logs.S) (Options : OPTIONS) = struct
  module Id = Id (Log)

  module Rel =
    Corr2.Make (Lab) (LabMap) (Id) (IdMap (Log)) (IntMap) (Tag) (TagMap) (Field)
      (FieldMap)
      (Options)

  let error fmt =
    let open Log in
    keprintf
      (fun fmt ->
        Format.fprintf fmt "@.";
        exit 2)
      fmt

  let warn loc fmt =
    let open Log in
    eprintf "%a@.Warning: " Location.pp loc;
    eprintf fmt

  module Env : sig
    type 'a t

    val empty : 'a t
    val find_opt : Id.t -> 'a t -> 'a option
    val add : Id.t -> 'a -> 'a t -> 'a t
    val adds : 'a t -> 'a t -> 'a t
    val sort : 'a t -> 'a t
    val fold : (Id.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val fold_left : (Id.t -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

    val map2 :
      Location.t -> string -> (Id.t -> 'a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t
  end = struct
    type 'a t = (Id.t * 'a) list

    let empty = []

    let rec find_opt x = function
      | [] -> None
      | (y, v) :: l -> if Id.equal x y then Some v else find_opt x l

    let add x v l = (x, v) :: l
    let adds l1 l2 = l1 @ l2
    let sort l = List.sort (fun (x1, _) (x2, _) -> Id.compare x1 x2) l
    let fold f l acc = List.fold_right (fun (x, v) acc -> f x v acc) l acc
    let fold_left f l acc = List.fold_left (fun acc (x, v) -> f x v acc) acc l

    let rec map2 loc s f l1 l2 =
      match (l1, l2) with
      | [], [] -> []
      | (x, _) :: _, [] | [], (x, _) :: _ ->
        error "%a@.%s Missing variable: %a" Location.pp loc s Id.pp x
      | (x1, v1) :: l1, (x2, v2) :: l2 ->
        if Id.equal x1 x2
        then
          let v = f x1 v1 v2 in
          (x1, v) :: map2 loc s f l1 l2
        else
          error
            "%a@.%s Variable mismatch: %a, %a"
            Location.pp
            loc
            s
            Id.pp
            x1
            Id.pp
            x2
  end

  type env = Rel.t Env.t

  (** Creates a map to unit whose domain the elements of the given list
      of fields. *)
  let get_record_arity fs =
    List.fold_right (fun f m -> FieldMap.add f () m) fs FieldMap.empty

  (** Creates a map to integer from an association list whose keys are
      tags. *)
  let get_sum_arity cs =
    List.fold_right (fun (c, n) m -> TagMap.add c n m) cs TagMap.empty

  open Inner

  let rec int_fold f i j acc =
    if i < j then int_fold f (i + 1) j (f i acc) else acc

  let int_map_init i j f =
    int_fold (fun k acc -> IntMap.add k (f k) acc) i j IntMap.empty

  let gather env =
    Env.fold
      (fun x r acc ->
        let open Rel in
        inter (compose (inter r (self x)) V.top) acc)
      env
      Rel.top

  (** Inserts a correlation into a list of correlations that denotes a
     disjunction of correlations. Returns a list of correlations that
     denotes a union of its elements. *)
  let rec add_union c =
    let open Rel.V in
    function
    | [] -> if leq c bot then [] else [c]
    | c' :: cs as l ->
      if leq c c' then l else if leq c' c then c :: cs else c' :: add_union c cs

  (** Computes the union of two lists of correlations, that each
     denote a union of correlations. Returns a list of correlations
     that denotes a union of its elements. *)
  let rec union_of_union cs1 cs2 =
    match cs2 with
    | [] -> cs1
    | c2 :: cs2 -> union_of_union (add_union c2 cs1) cs2

  (** Computes the intersection of a correlation and a list of
     correlations that denotes a union of correlations. Returns a list
     of correlations that denotes a union of its elements.*)
  let rec inter0_of_union c = function
    | [] -> []
    | c' :: cs -> add_union (Rel.V.inter c c') (inter0_of_union c cs)

  (** Computes the intersection of two lists of correlations that
     denote a union of correlations. Returns a list of correlations
     that denotes a union of its elements.*)
  let rec inter_of_union cs1 cs2 =
    match cs2 with
    | [] -> []
    | c2 :: cs2 ->
      union_of_union (inter0_of_union c2 cs1) (inter_of_union cs1 cs2)

  (** [postfixpoint loc s f base] computes a post-fixpoint of f that
     is above [base]. [Options.unroll_fixpoints] iterations of the
     fixpoint are unrolled first. Then, [Options.delay_widening]
     iterations are performed before using widening. It terminates
     only when there is no infinite ascending chain in the abstract
     lattice. We should use a stronger form of widening, should that
     property not be satisfied, or to accelerate convergence. *)
  let postfixpoint loc f base =
    let rec loop n unrolled base =
      (* START debugging for fixpoint iteration *)
      (* Log.eprintf "Iteration %i:@.@[%a@]@." n Rel.pp base;
       * let max = 4 in
       * if n > max
       * then
       *   error "%a@.ERROR: No postfixpoint was reached after %i iterations"
       *     Location.pp loc
       *     n; *)
      (* END debugging *)
      let next = f base in
      if n < Options.unroll_fixpoints
      then (loop [@tailcall]) (n + 1) (Rel.union base next) next
      else if Rel.leq next base
      then begin
        Log.eprintf
          "%a@.INFO: postfixpoint reached after %i iterations@."
          Location.pp
          loc
          n;
        Rel.union unrolled base
      end
      else
        let base' =
          if n < Options.(unroll_fixpoints + delay_widening)
          then Rel.union base next
          else
            let base' = Rel.widen base next in
            if not @@ Rel.leq base base'
            then
              error
                "%a@.@[Incorrect widening: domain@ %a@ is not below the \
                 widened domain@ %a@]@."
                Location.pp
                loc
                Rel.pp
                base
                Rel.pp
                base';
            base'
        in
        (loop [@tailcall]) (n + 1) unrolled base'
    in
    (loop [@tailcall]) 0 base base

  let true_variant =
    let open TagMap in
    Rel.variant @@ add Tag.true_ Rel.top @@ add Tag.false_ Rel.bot @@ empty

  let true_vvariant =
    let open TagMap in
    Rel.V.variant
    @@ add Tag.true_ Rel.V.top
    @@ add Tag.false_ Rel.V.bot
    @@ empty

  let is_true v = Rel.compose v true_vvariant

  let false_variant =
    let open TagMap in
    Rel.variant @@ add Tag.true_ Rel.bot @@ add Tag.false_ Rel.top @@ empty

  let false_vvariant =
    let open TagMap in
    Rel.V.variant
    @@ add Tag.true_ Rel.V.bot
    @@ add Tag.false_ Rel.V.top
    @@ empty

  let is_false v = Rel.compose v false_vvariant

  (** [interpret env ctxt e] interprets the expression [e] in the
     environment [env], with initial knowledge [ctxt] about the
     environment. Returns the input-output correlation of [e]. So far,
     [ctxt] is only used to convey accurate warnings about unreachable
     pattern branches or non-exhaustive pattern matching. *)
  let rec interpret (env : env) ctxt e =
    interpret_ e.Location.loc env ctxt e.Location.contents

  and interpret_ loc (env : env) ctxt : expr_ -> Rel.t = function
    | Var x -> (
      let id = Id.get x in
      match Env.find_opt id env with
      | Some r ->
        let open Rel in
        if leq top r then inter (self id) (gather env) else inter r (gather env)
      | None -> error "Unknown variable %a" Id.pp id)
    | Let (x, e1, e2) ->
      let r1 = interpret env ctxt e1 in
      let id = Id.register x in
      let r2 = interpret (Env.add id r1 env) ctxt e2 in
      let res = Rel.let_ id r1 r2 in
      Id.unregister id;
      res
    | Tuple [] -> Rel.(inter (gather env) (tuple IntMap.empty))
    | Tuple es ->
      let args, _len =
        fold_lefti
          (fun m i ei -> IntMap.add i (interpret env ctxt ei) m)
          IntMap.empty
          es
      in
      Rel.tuple args
    | TupleProj (e, i, len) ->
      let args =
        int_map_init 0 len (fun k -> if k = i then Rel.V.eq else Rel.V.top)
      in
      Rel.compose (interpret env ctxt e) @@ Rel.V.tuple args
    | Record [] -> Rel.(inter (gather env) (record FieldMap.empty))
    | Record es ->
      let fields =
        List.fold_left
          (fun m (f, ef) ->
            let ri = interpret env ctxt ef in
            FieldMap.add f ri m)
          FieldMap.empty
          es
      in
      Rel.record fields
    | RecordProj (e, f, fs) ->
      let fields =
        List.fold_left
          (fun acc g ->
            let c = if Field.equal f g then Rel.V.eq else Rel.V.top in
            FieldMap.add g c acc)
          FieldMap.empty
          fs
      in
      Rel.compose (interpret env ctxt e) @@ Rel.V.record fields
    | RecordUpdate (e, f, ef, fs) ->
      let ce = interpret env ctxt e in
      let fields =
        List.fold_left
          (fun m g ->
            let cg =
              if Field.equal f g
              then Rel.(inter (compose ce V.top) (interpret env ctxt ef))
              else
                let eq_g =
                  List.fold_left
                    (fun acc h ->
                      let ch =
                        if Field.equal h g then Rel.V.eq else Rel.V.top
                      in
                      FieldMap.add h ch acc)
                    FieldMap.empty
                    fs
                in
                Rel.compose ce @@ Rel.V.record eq_g
            in
            FieldMap.add g cg m)
          FieldMap.empty
          fs
      in
      Rel.record fields
    | Constructor (t, [], cs) ->
      let cases =
        List.fold_left
          (fun acc (u, _) ->
            let c = if Tag.equal u t then Rel.top else Rel.bot in
            TagMap.add u c acc)
          TagMap.empty
          cs
      in
      Rel.(inter (gather env) (variant cases))
    | Constructor (t, es, cs) ->
      let args, _len =
        fold_lefti
          (fun m i ei -> IntMap.add i (interpret env ctxt ei) m)
          IntMap.empty
          es
      in
      let cases =
        List.fold_left
          (fun acc (u, _) ->
            let c = if Tag.equal u t then Rel.tuple args else Rel.bot in
            TagMap.add u c acc)
          TagMap.empty
          cs
      in
      Rel.variant cases
    | Fun ({ Location.contents = x; _ }, _ty, e) ->
      let top_val = Rel.top in
      let id = Id.register x in
      let env' = Env.add id top_val env in
      let res =
        Rel.(
          inter
            (gather env)
            (func (Lab.from_loc loc) id top_val (interpret env' ctxt e)))
      in
      Id.unregister id;
      res
    | FixFun (f, x, ty, e) ->
      let lab = Lab.from_loc loc in
      let gather_env = gather env in
      let y = Id.fresh () in
      let fun_ext rel = Rel.(func lab y top (apply rel (self y))) in
      let f_id = Id.register f in
      let functional rel =
        let res =
          interpret
            (Env.add f_id rel env)
            ctxt
            (Location.locate loc @@ Fun (x, ty, e))
        in
        Rel.(union (inter gather_env (fun_ext rel)) (let_ f_id rel res))
      in
      let base = Rel.(inter gather_env (func lab y top bot)) in
      let res = postfixpoint loc functional base in
      Id.unregister f_id;
      res
    | App (e1, e2) ->
      let r1 = interpret env ctxt e1 in
      let r2 = interpret env ctxt e2 in
      Rel.apply r1 r2
    | Match (e, cases) ->
      let v = interpret env ctxt e in
      let v', _matched_cases, neg_cases = interpret_branches env ctxt v cases in
      let actual_neg_cases =
        let open Rel in
        List.fold_left
          (fun acc neg_cases -> union acc (compose (inter ctxt v) neg_cases))
          bot
          neg_cases
      in
      if not @@ Rel.leq actual_neg_cases Rel.bot
      then
        warn
          loc
          "Non-exhaustive pattern-matching:@.@[%a@]@.@."
          (Format.pp_print_list ~pp_sep:Format.pp_print_newline Rel.V.pp)
          neg_cases;
      v'
    | Unknown -> gather env
    | Bool b ->
      Rel.(inter (gather env) (if b then true_variant else false_variant))
    | BAndAlso (e1, e2) ->
      let v1 = interpret env ctxt e1 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      let v2 = interpret env (Rel.inter ctxt v1_true) e2 in
      let cases =
        let open TagMap in
        add Tag.true_ (Rel.inter v1_true (is_true v2))
        @@ add Tag.false_ Rel.(union v1_false (inter v1_true (is_false v2)))
        @@ empty
      in
      Rel.variant cases
    | BOrElse (e1, e2) ->
      let v1 = interpret env ctxt e1 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      let v2 = interpret env (Rel.inter ctxt v1_false) e2 in
      let cases =
        let open TagMap in
        add Tag.true_ Rel.(union v1_true (inter v1_false (is_true v2)))
        @@ add Tag.false_ Rel.(inter v1_false (is_false v2))
        @@ empty
      in
      Rel.variant cases
    | IfThenElse (e1, e2, e3) ->
      let v1 = interpret env ctxt e1 in
      let v1_true = is_true v1
      and v1_false = is_false v1 in
      let v2 = interpret env (Rel.inter ctxt v1_true) e2 in
      let v3 = interpret env (Rel.inter ctxt v1_false) e3 in
      Rel.union (Rel.inter v1_true v2) (Rel.inter v1_false v3)
    | Int _n ->
      (* TODO: numeric domains *)
      gather env
    | Binop ((PLUS | MINUS | TIMES | DIV), e1, e2) ->
      (* TODO: numeric domains *)
      let open Rel in
      union
        (compose (interpret env ctxt e1) V.top)
        (compose (interpret env ctxt e2) V.top)
    | Binop ((NEQ | LT | GT | EQ | LE | GE), e1, e2) ->
      (* TODO: numeric domains *)
      let open Rel in
      union
        (compose (interpret env ctxt e1) V.top)
        (compose (interpret env ctxt e2) V.top)
    | Unop (UMINUS, e) ->
      (* TODO: numeric domains *)
      let open Rel in
      compose (interpret env ctxt e) V.top

  (** [interpret_branches env ctxt v cases] interprets the pattern matching
     cases [cases] in the environment [env] applied to a value whose
     correlation is [v], with initial knowledge [ctxt] about the
     environment. It returns the correlation obtained after
     executing all the branches, and the correlation that describes
     the set of values that were matched by the branches and the union
     of correlations that describes the set of values that were not
     matched by the branches. *)
  and interpret_branches env ctxt v cases : Rel.t * Rel.V.t * Rel.V.t list =
    List.fold_left
      (fun (cases, matched_cases, neg_cases) branch ->
        let new_ctxt, v =
          if Options.learn_from_patterns
          then
            let open Rel in
            let v' =
              List.fold_left
                (fun acc neg_cases -> union acc (compose v neg_cases))
                bot
                neg_cases
            in
            (inter ctxt v', inter v v')
          else (ctxt, v)
        in
        let case, matched_case, neg_case =
          interpret_branch env new_ctxt v branch
        in
        ( Rel.union case cases,
          Rel.V.union matched_case matched_cases,
          inter_of_union neg_case neg_cases ))
      (Rel.bot, Rel.V.bot, [Rel.V.top])
      cases

  (** [interpret_branch env v (p, e)] interprets the matching of the
     pattern [p] in the environment [env] applied to a value whose
     correlation is [v], with initial knowledge [ctxt] about the
     environment, and with continuation [e]. It returns the
     correlation obtained after executing [e], and also the
     correlation that describes the set of values that are matched by
     [p], and also the union of correlation that describes the set of
     values that are not matched by [p]. *)
  and interpret_branch (env : env) ctxt v (p, e) :
      Rel.t * Rel.V.t * Rel.V.t list =
    let env_pattern, matched_case, neg_case =
      interpret_pattern ~intro:true Env.empty v p
    in
    let v' = Rel.compose v matched_case in
    if Rel.(leq (inter ctxt v') bot)
    then warn p.Location.loc "This pattern is unreachable.@.";
    let env' = Env.adds env_pattern env in
    ( Rel.inter v'
      @@ Env.fold_left
           (fun x _ acc ->
             let relx =
               match Env.find_opt x env' with
               | Some r -> r
               | None -> assert false
             in
             let res = Rel.let_ x relx acc in
             Id.unregister x;
             res)
           env_pattern
      @@ interpret env' ctxt e,
      matched_case,
      neg_case )

  (** [interpret_pattern ~intro env v p] computes 3 values for the pattern
      matching of the value [v] along the pattern [p] in the environment
      [env]:
      - the environment below the pattern that binds the variables which
        are introduced by the pattern (necessary to interpret the continuation)
      - a correlation that describes the set of values that are matched by [p]
      - a union of correlations that describes the complement of the set
        of values that are matched by [p].

      If [intro = true], then the variables introduced by the pattern
      should be registered. If [intro = false], they must not be
      registered. This flag is used in the treatment of disjunctive
      patterns.
  *)
  and interpret_pattern ~intro env v p : env * Rel.V.t * Rel.V.t list =
    interpret_pattern_ ~intro p.Location.loc env v p.Location.contents

  and interpret_pattern_ ~intro loc env v p : env * Rel.V.t * Rel.V.t list =
    match p with
    | PVar x ->
      let id = if intro then Id.register x else Id.get x in
      (Env.add id v env, Rel.V.top, [])
    | PWild -> (env, Rel.V.top, [])
    | PConstructor (t, [], arity) ->
      let cases =
        List.fold_left
          (fun acc (u, _) ->
            let c = if Tag.equal u t then Rel.V.top else Rel.V.bot in
            TagMap.add u c acc)
          TagMap.empty
          arity
      and neg_cases =
        List.fold_left
          (fun acc (u, _) ->
            let c = if Tag.equal u t then Rel.V.bot else Rel.V.top in
            TagMap.add u c acc)
          TagMap.empty
          arity
      in
      (env, Rel.V.variant cases, add_union (Rel.V.variant neg_cases) [])
    | PConstructor (t, ps, arity) ->
      let sum_i i =
        let args_i n =
          int_map_init 0 n (fun j -> if i = j then Rel.V.eq else Rel.V.top)
        in
        let cases =
          List.fold_left
            (fun acc (u, n) ->
              let c =
                if Tag.equal t u then Rel.V.tuple (args_i n) else Rel.V.bot
              in
              TagMap.add u c acc)
            TagMap.empty
            arity
        in
        Rel.V.variant cases
      in
      let len = List.length ps in
      let (env', cases, neg_cases), _len =
        fold_lefti
          (fun (env, cases, neg_cases) i pi ->
            let vi = Rel.compose v @@ sum_i i in
            let env', case_i, neg_cases_i =
              interpret_pattern ~intro env vi pi
            in
            ( env',
              IntMap.add i case_i cases,
              List.fold_left
                (fun acc neg_case_i ->
                  int_map_init 0 len (fun j ->
                      if j = i then neg_case_i else Rel.V.top)
                  :: acc)
                neg_cases
                neg_cases_i ))
          (env, IntMap.empty, [])
          ps
      in
      let arity2 = get_sum_arity arity in
      let n =
        try TagMap.find t arity2
        with Not_found -> error "%a@.Unknown tag %a" Location.pp loc Tag.pp t
      in
      if n <> len
      then
        error
          "%a@.Mismatch of argument numbers in pattern for tag %a"
          Location.pp
          loc
          Tag.pp
          t;
      let cases =
        List.fold_left
          (fun acc (u, _) ->
            let c = if Tag.equal t u then Rel.V.tuple cases else Rel.V.bot in
            TagMap.add u c acc)
          TagMap.empty
          arity
      and neg_cases =
        let open Rel.V in
        let not_in_t =
          let m =
            List.fold_left
              (fun acc (u, _) ->
                TagMap.add u (if Tag.equal u t then bot else top) acc)
              TagMap.empty
              arity
          in
          variant m
        in
        List.fold_left
          (fun acc neg_case ->
            let m =
              List.fold_left
                (fun acc (u, _) ->
                  TagMap.add
                    u
                    (if Tag.equal u t then tuple neg_case else bot)
                    acc)
                TagMap.empty
                arity
            in
            add_union (variant m) acc)
          (add_union not_in_t [])
          neg_cases
      in
      (env', Rel.V.variant cases, neg_cases)
    | PTuple ps ->
      let len = List.length ps in
      let tuple_i i =
        let open Rel.V in
        tuple @@ int_map_init 0 len (fun j -> if j = i then eq else top)
      in
      let (env', cases, neg_cases), _len =
        fold_lefti
          (fun (env, cases, neg_cases) i pi ->
            let vi = Rel.compose v @@ tuple_i i in
            let env', case_i, neg_case_i = interpret_pattern ~intro env vi pi in
            let open Rel.V in
            ( env',
              inter cases
              @@ tuple
              @@ int_map_init 0 len (fun j -> if j = i then case_i else top),
              List.fold_left
                (fun acc neg_case_i ->
                  add_union
                    (tuple
                    @@ int_map_init 0 len (fun j ->
                           if j = i then neg_case_i else top))
                    acc)
                neg_cases
                neg_case_i ))
          (env, Rel.V.top, [])
          ps
      in
      (env', cases, neg_cases)
    | PRecord (ps, arity) ->
      let arity = get_record_arity arity in
      let record_i i =
        let open Rel.V in
        let fields =
          FieldMap.mapi (fun j () -> if Field.equal j i then eq else top) arity
        in
        record fields
      in
      let env', cases, neg_cases =
        List.fold_left
          (fun (env, cases, neg_cases) (i, pi) ->
            let vi = Rel.compose v @@ record_i i in
            let env', case_i, neg_case_i = interpret_pattern ~intro env vi pi in
            let open Rel.V in
            ( env',
              inter cases
              @@ record
              @@ FieldMap.mapi
                   (fun j () -> if Field.equal j i then case_i else top)
                   arity,
              List.fold_left
                (fun acc neg_case_i ->
                  add_union
                    (record
                    @@ FieldMap.mapi
                         (fun j () ->
                           if Field.equal j i then neg_case_i else top)
                         arity)
                    acc)
                neg_cases
                neg_case_i ))
          (env, Rel.V.top, [])
          ps
      in
      (env', cases, neg_cases)
    | POr [] -> assert false
    | POr (p :: ps) ->
      let env', v_case', neg_case = interpret_pattern ~intro env v p in
      let env' = Env.sort env' in
      List.fold_left
        (fun (env_acc, v_cases_acc, neg_cases) p ->
          let env', v_case', neg_case =
            interpret_pattern ~intro:false env v p
          in
          let env' = Env.sort env' in
          ( Env.map2
              loc
              "interpret_pattern (POr):"
              (fun _x v1 v2 -> Rel.union v1 v2)
              env_acc
              env',
            Rel.V.union v_cases_acc v_case',
            inter_of_union neg_cases neg_case ))
        (env', v_case', neg_case)
        ps
    | PBool b ->
      ( env,
        (if b then true_vvariant else false_vvariant),
        add_union (if b then false_vvariant else true_vvariant) [] )

  let interpret env e = interpret env Rel.top e

  let interpret_decl ((env, decls) as acc) = function
    | DeclTyp _ -> acc
    | DeclVal (x, typ_scheme, e) ->
      (* Log.eprintf "START analyzing %a...@." Var.pp x; *)
      let res = interpret env e in
      (* Log.eprintf ">>>>> Closure... @?"; *)
      let res = Env.fold Rel.let_ env res in
      (* Log.eprintf "DONE.@."; *)
      (* Log.eprintf ">>>>> Canonization... @?"; *)
      let res = Rel.canonize_names res in
      (* Log.eprintf "DONE.@."; *)
      let id = Id.register x in
      (* Log.eprintf "END analyzing %a.@." Var.pp x; *)
      (Env.add id res env, (x, typ_scheme, res) :: decls)

  let interpret_program env p =
    let _env, decls = List.fold_left interpret_decl (env, []) p in
    List.rev decls

  let pp fmt decls =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt "@.@.")
      (fun fmt (x, typ_scheme, c) ->
        Format.fprintf
          fmt
          "@[val %a ::@ %a@]@."
          Var.pp
          x
          Inner.pp_typ_scheme
          typ_scheme;
        Format.fprintf fmt "@[val %a:@ %a@]" Var.pp x Rel.pp c)
      fmt
      decls
end
