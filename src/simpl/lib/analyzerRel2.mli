(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

module type OPTIONS = sig
  val extensional_eq : bool
  val extensional_bot : bool
  val learn_from_patterns : bool
  val strict_errors : bool
  val unroll_fixpoints : int
  val delay_widening : int
end

module Make (_ : Logs.S) (_ : OPTIONS) : sig
  module Rel : sig
    type t
  end

  module Env : sig
    type 'a t

    val empty : 'a t
  end

  type env = Rel.t Env.t

  val interpret : env -> Ast.Inner.expr -> Rel.t
  (** [interpret env e] interprets the expression [e] in the environment
        [env]. Returns the input-output correlation of [e]. *)

  val interpret_program :
    env -> Ast.Inner.program -> (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list

  val pp :
    Format.formatter -> (Ast.Var.t * Ast.Inner.typ_scheme * Rel.t) list -> unit
end
