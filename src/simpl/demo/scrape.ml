module Config = struct
  let project = "JavaScript demo of stable relations"

  let dir =
    Filename.(concat (Sys.getcwd ()) @@ concat parent_dir_name "examples")

  let extension = ".simpl"

  let exclude_list =
    [
      "heintze_mcallester_10.simpl";
      "heintze_mcallester_20.simpl";
      "heintze_mcallester_30.simpl";
      "heintze_mcallester_40.simpl";
      "heintze_mcallester_50.simpl";
      "heintze_mcallester_100.simpl";
      "heintze_mcallester_200.simpl";
      "heintze_mcallester_300.simpl";
      "heintze_mcallester_400.simpl";
      "heintze_mcallester_500.simpl";
      "heintze_mcallester_600.simpl";
      "heintze_mcallester_700.simpl";
      "heintze_mcallester_800.simpl";
      "heintze_mcallester_900.simpl";
      "heintze_mcallester_1000.simpl";
      "internal-test.simpl";
    ]

  let value_name = "examples"
  let value_type = `Array
  let out_channel = stdout
end

let () =
  let module Scrap = Scraper.Make (Config) in
  Scrap.run ()
