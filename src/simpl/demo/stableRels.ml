open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
open JsDemo

let ( >>= ) m f =
  match m with
  | Error _ as err -> err
  | Ok v -> f v

let return x = Ok x

module Options : OPTIONS = struct
  module Examples = struct
    let collection = Examples.examples
    let default = "combine"
    let label = "Example programs:"
  end

  module Color = struct
    let normal_text = CSS.Color.(Name Black)
    let error_text = CSS.Color.(Name Red)
    let disabled_text = CSS.Color.(Name Gray)
  end

  module Button = struct
    let label = "Analyse!"
    let dom_id = "analyse-button"
    let dom_class = ["analyse-button"]
  end

  module Output = struct
    let label = "Output:"
    let dom_id = "output"
    let dom_class = ["output"]
    let font_size = 16
  end

  module Editor = struct
    let dom_id = "editor"
    let dom_class = ["editor"]
    let tab_size = 2
    let theme = "ace/theme/chrome"
    let mode = "ace/mode/ocaml"
    let font_size = 16
  end

  module OptionsDialog = struct
    type value = SimplLib.Main.Options.t

    let items = []
    let modify_onchange _ = ()

    let analysis, get_analysis, modify_onchange_analysis =
      let open SimplLib.Main.AnalysisKind in
      Utils.make_select
        ~name:"Analysis:"
        ~min:0
        ~max:1
        ~of_enum
        ~to_string
        ~default:1

    let items = analysis :: items

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_analysis x

    let ( learn_from_patterns,
          get_learn_from_patterns,
          modify_onchange_learn_from_patterns ) =
      Utils.make_checkbox ~name:"Learn from patterns" ~checked:true

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_learn_from_patterns x

    let items = learn_from_patterns :: items

    let strict_errors, get_strict_errors, modify_onchange_strict_errors =
      Utils.make_checkbox
        ~name:"Fail when a type mismatch is found"
        ~checked:true

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_strict_errors x

    let items = strict_errors :: items

    let unroll_fixpoints, get_unroll_fixpoints, modify_onchange_unroll_fixpoints
        =
      Utils.make_select
        ~name:"How many fixpoint iterations must be unrolled"
        ~min:0
        ~max:2
        ~default:0
        ~of_enum:(fun n -> Some n)
        ~to_string:string_of_int

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_unroll_fixpoints x

    let items = unroll_fixpoints :: items

    let delay_widening, get_delay_widening, modify_onchange_delay_widening =
      Utils.make_select
        ~name:"Number of iterations before widening must start"
        ~min:0
        ~max:2
        ~default:0
        ~of_enum:(fun n -> Some n)
        ~to_string:string_of_int

    let modify_onchange x =
      modify_onchange x;
      modify_onchange_delay_widening x

    let items = delay_widening :: items

    let html_elt =
      let open Html in
      fieldset
        ~legend:(legend [txt "Analyser options:"])
        ~a:[a_id "options"]
        (List.rev items)

    let get_value () =
      let open SimplLib.Main.Options in
      {
        analysis_kind = get_analysis ();
        run_internal_tests = false;
        strict_errors = get_strict_errors ();
        extensional_eq = false;
        extensional_bot = false;
        learn_from_patterns = get_learn_from_patterns ();
        unroll_fixpoints = get_unroll_fixpoints ();
        delay_widening = get_delay_widening ();
      }

    let set_onchange = modify_onchange
  end

  let loc_to_ace loc =
    let to_loc pos =
      let open Lexing in
      (pos.pos_lnum, pos.pos_cnum - pos.pos_bol)
    in
    {
      Ezjs_ace.Ace.loc_start = to_loc (Location.get_start loc);
      loc_end = to_loc (Location.get_end loc);
    }

  let analyzer options input =
    let module F = struct
      let buf = Buffer.create 1024
      let std_formatter = Format.formatter_of_buffer buf
      let err_formatter = Format.formatter_of_buffer buf
    end in
    let module Log = Logs.Make (F) in
    let module Opt = (val SimplLib.Main.Options.get_module options) in
    let module Check = SimplLib.Typecheck.Make (Opt) in
    let open SimplLib.Main in
    let module Analyzer = (val Options.get_analysis_module (module Log) options)
    in
    let run () =
      let open SimplLib in
      Driver.parse Parser.program "" (`String input) >>= fun prog ->
      Check.typecheck prog >>= fun prog ->
      return @@ Analyzer.interpret_program Analyzer.Env.empty prog
    in
    let mres, duration = Time.call run in
    Result.map_error (fun (loc, msg) ->
        let s =
          Log.eprintf "%a@.%s@." Location.pp loc msg;
          Buffer.contents F.buf
        in
        (loc_to_ace loc, s))
    @@ Result.map
         (fun res ->
           Log.printf
             "%a@.@.(* Running time: %a *)@."
             Analyzer.pp
             res
             Time.pp
             duration;
           (Buffer.contents F.buf, []))
         mres
end

let () =
  let module Demo = Make (Options) in
  Demo.setup ()
