(* configuration *)
module Config = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out"

  (* extention for the expected output files *)
  let expected_ext = ".expected"

  (* name of the executable to run for each test *)
  let exe = "coral-simpl"

  (* flags that should be given by default to the executable *)
  let default_flags = ["--strict-errors"; "--relational"]

  (* add "--relational" to perform the tests with the relational analysis *)

  (* flags that should be given to specific files *)
  let file_specific_flags =
    [(["internal-test"], ["--strict-errors"; "--run-internal-tests"])]

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (Config) in
  M.generate_rules stdout Filename.current_dir_name
