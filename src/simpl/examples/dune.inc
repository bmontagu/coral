; Test rule for input file accesses.simpl
(rule
  (deps accesses.simpl)
  (target accesses.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff accesses.expected accesses.out)))

; Test rule for input file accesses_native_bool.simpl
(rule
  (deps accesses_native_bool.simpl)
  (target accesses_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff accesses_native_bool.expected accesses_native_bool.out)))

; Test rule for input file ack.simpl
(rule
  (deps ack.simpl)
  (target ack.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff ack.expected ack.out)))

; Test rule for input file apply.simpl
(rule
  (deps apply.simpl)
  (target apply.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff apply.expected apply.out)))

; Test rule for input file binomial.simpl
(rule
  (deps binomial.simpl)
  (target binomial.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff binomial.expected binomial.out)))

; Test rule for input file cfa.simpl
(rule
  (deps cfa.simpl)
  (target cfa.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff cfa.expected cfa.out)))

; Test rule for input file combine.simpl
(rule
  (deps combine.simpl)
  (target combine.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff combine.expected combine.out)))

; Test rule for input file combine_pairs.simpl
(rule
  (deps combine_pairs.simpl)
  (target combine_pairs.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff combine_pairs.expected combine_pairs.out)))

; Test rule for input file continuation_monad.simpl
(rule
  (deps continuation_monad.simpl)
  (target continuation_monad.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff continuation_monad.expected continuation_monad.out)))

; Test rule for input file cps.simpl
(rule
  (deps cps.simpl)
  (target cps.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff cps.expected cps.out)))

; Test rule for input file cps_native_bool.simpl
(rule
  (deps cps_native_bool.simpl)
  (target cps_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff cps_native_bool.expected cps_native_bool.out)))

; Test rule for input file cps_wrapped.simpl
(rule
  (deps cps_wrapped.simpl)
  (target cps_wrapped.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff cps_wrapped.expected cps_wrapped.out)))

; Test rule for input file cps_wrapped_native_bool.simpl
(rule
  (deps cps_wrapped_native_bool.simpl)
  (target cps_wrapped_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff cps_wrapped_native_bool.expected cps_wrapped_native_bool.out)))

; Test rule for input file eta_n.simpl
(rule
  (deps eta_n.simpl)
  (target eta_n.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff eta_n.expected eta_n.out)))

; Test rule for input file eta_n_tailrec.simpl
(rule
  (deps eta_n_tailrec.simpl)
  (target eta_n_tailrec.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff eta_n_tailrec.expected eta_n_tailrec.out)))

; Test rule for input file euclid.simpl
(rule
  (deps euclid.simpl)
  (target euclid.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff euclid.expected euclid.out)))

; Test rule for input file ext_norm_sums.simpl
(rule
  (deps ext_norm_sums.simpl)
  (target ext_norm_sums.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff ext_norm_sums.expected ext_norm_sums.out)))

; Test rule for input file factorial.simpl
(rule
  (deps factorial.simpl)
  (target factorial.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff factorial.expected factorial.out)))

; Test rule for input file fib.simpl
(rule
  (deps fib.simpl)
  (target fib.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff fib.expected fib.out)))

; Test rule for input file hanoi.simpl
(rule
  (deps hanoi.simpl)
  (target hanoi.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff hanoi.expected hanoi.out)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected heintze_mcallester.out)))

; Test rule for input file heintze_mcallester_10.simpl
(rule
  (deps heintze_mcallester_10.simpl)
  (target heintze_mcallester_10.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_10.expected heintze_mcallester_10.out)))

; Test rule for input file heintze_mcallester_100.simpl
(rule
  (deps heintze_mcallester_100.simpl)
  (target heintze_mcallester_100.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_100.expected heintze_mcallester_100.out)))

; Test rule for input file heintze_mcallester_1000.simpl
(rule
  (deps heintze_mcallester_1000.simpl)
  (target heintze_mcallester_1000.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_1000.expected heintze_mcallester_1000.out)))

; Test rule for input file heintze_mcallester_20.simpl
(rule
  (deps heintze_mcallester_20.simpl)
  (target heintze_mcallester_20.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_20.expected heintze_mcallester_20.out)))

; Test rule for input file heintze_mcallester_200.simpl
(rule
  (deps heintze_mcallester_200.simpl)
  (target heintze_mcallester_200.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_200.expected heintze_mcallester_200.out)))

; Test rule for input file heintze_mcallester_30.simpl
(rule
  (deps heintze_mcallester_30.simpl)
  (target heintze_mcallester_30.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_30.expected heintze_mcallester_30.out)))

; Test rule for input file heintze_mcallester_300.simpl
(rule
  (deps heintze_mcallester_300.simpl)
  (target heintze_mcallester_300.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_300.expected heintze_mcallester_300.out)))

; Test rule for input file heintze_mcallester_40.simpl
(rule
  (deps heintze_mcallester_40.simpl)
  (target heintze_mcallester_40.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_40.expected heintze_mcallester_40.out)))

; Test rule for input file heintze_mcallester_400.simpl
(rule
  (deps heintze_mcallester_400.simpl)
  (target heintze_mcallester_400.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_400.expected heintze_mcallester_400.out)))

; Test rule for input file heintze_mcallester_50.simpl
(rule
  (deps heintze_mcallester_50.simpl)
  (target heintze_mcallester_50.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_50.expected heintze_mcallester_50.out)))

; Test rule for input file heintze_mcallester_500.simpl
(rule
  (deps heintze_mcallester_500.simpl)
  (target heintze_mcallester_500.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_500.expected heintze_mcallester_500.out)))

; Test rule for input file heintze_mcallester_600.simpl
(rule
  (deps heintze_mcallester_600.simpl)
  (target heintze_mcallester_600.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_600.expected heintze_mcallester_600.out)))

; Test rule for input file heintze_mcallester_700.simpl
(rule
  (deps heintze_mcallester_700.simpl)
  (target heintze_mcallester_700.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_700.expected heintze_mcallester_700.out)))

; Test rule for input file heintze_mcallester_800.simpl
(rule
  (deps heintze_mcallester_800.simpl)
  (target heintze_mcallester_800.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_800.expected heintze_mcallester_800.out)))

; Test rule for input file heintze_mcallester_900.simpl
(rule
  (deps heintze_mcallester_900.simpl)
  (target heintze_mcallester_900.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester_900.expected heintze_mcallester_900.out)))

; Test rule for input file hindley_milner_exponential.simpl
(rule
  (deps hindley_milner_exponential.simpl)
  (target hindley_milner_exponential.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff hindley_milner_exponential.expected hindley_milner_exponential.out)))

; Test rule for input file internal-test.simpl
(rule
  (deps internal-test.simpl)
  (target internal-test.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --run-internal-tests %{deps}))))
(rule
  (alias   runtest)
  (action (diff internal-test.expected internal-test.out)))

; Test rule for input file loops.simpl
(rule
  (deps loops.simpl)
  (target loops.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff loops.expected loops.out)))

; Test rule for input file mccarthy91.simpl
(rule
  (deps mccarthy91.simpl)
  (target mccarthy91.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff mccarthy91.expected mccarthy91.out)))

; Test rule for input file option_monad.simpl
(rule
  (deps option_monad.simpl)
  (target option_monad.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff option_monad.expected option_monad.out)))

; Test rule for input file principles_of_program_analysis_cfa.simpl
(rule
  (deps principles_of_program_analysis_cfa.simpl)
  (target principles_of_program_analysis_cfa.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff principles_of_program_analysis_cfa.expected principles_of_program_analysis_cfa.out)))

; Test rule for input file reader_monad.simpl
(rule
  (deps reader_monad.simpl)
  (target reader_monad.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff reader_monad.expected reader_monad.out)))

; Test rule for input file shivers.simpl
(rule
  (deps shivers.simpl)
  (target shivers.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff shivers.expected shivers.out)))

; Test rule for input file state_monad.simpl
(rule
  (deps state_monad.simpl)
  (target state_monad.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff state_monad.expected state_monad.out)))

; Test rule for input file tak.simpl
(rule
  (deps tak.simpl)
  (target tak.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff tak.expected tak.out)))

; Test rule for input file test00.simpl
(rule
  (deps test00.simpl)
  (target test00.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test00.expected test00.out)))

; Test rule for input file test01.simpl
(rule
  (deps test01.simpl)
  (target test01.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test01.expected test01.out)))

; Test rule for input file test02.simpl
(rule
  (deps test02.simpl)
  (target test02.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test02.expected test02.out)))

; Test rule for input file test03.simpl
(rule
  (deps test03.simpl)
  (target test03.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test03.expected test03.out)))

; Test rule for input file test04.simpl
(rule
  (deps test04.simpl)
  (target test04.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test04.expected test04.out)))

; Test rule for input file test04_native_bool.simpl
(rule
  (deps test04_native_bool.simpl)
  (target test04_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test04_native_bool.expected test04_native_bool.out)))

; Test rule for input file test05.simpl
(rule
  (deps test05.simpl)
  (target test05.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test05.expected test05.out)))

; Test rule for input file test06.simpl
(rule
  (deps test06.simpl)
  (target test06.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test06.expected test06.out)))

; Test rule for input file test07.simpl
(rule
  (deps test07.simpl)
  (target test07.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test07.expected test07.out)))

; Test rule for input file test08.simpl
(rule
  (deps test08.simpl)
  (target test08.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test08.expected test08.out)))

; Test rule for input file test09.simpl
(rule
  (deps test09.simpl)
  (target test09.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test09.expected test09.out)))

; Test rule for input file test_midtgaard_jensen_cfa.simpl
(rule
  (deps test_midtgaard_jensen_cfa.simpl)
  (target test_midtgaard_jensen_cfa.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test_midtgaard_jensen_cfa.expected test_midtgaard_jensen_cfa.out)))

; Test rule for input file test_midtgaard_jensen_cfa_cps.simpl
(rule
  (deps test_midtgaard_jensen_cfa_cps.simpl)
  (target test_midtgaard_jensen_cfa_cps.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff test_midtgaard_jensen_cfa_cps.expected test_midtgaard_jensen_cfa_cps.out)))

; Test rule for input file xor.simpl
(rule
  (deps xor.simpl)
  (target xor.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff xor.expected xor.out)))

