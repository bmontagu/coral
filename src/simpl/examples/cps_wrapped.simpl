type boolean = True | False

type ('a, 'b) cont = { apply: ('a -> 'b) -> 'b }

val run m = m.apply (fun x -> x)

val var x = { apply = fun k -> k x }

val lam m = { apply = fun k ->
    k (fun x -> (m x).apply)
}

val app m1 m2 = { apply = fun k ->
    m1.apply (fun k1 ->
        m2.apply (fun k2 ->
            k1 k2 k
          )
      )
}

val true_ = { apply = fun k -> k True }
val false_ = { apply = fun k -> k False }

val k1 = lam (fun x -> lam (fun y -> var x))
val k1TF = app (app k1 true_) false_
val run_k1TF = run k1TF

val k2 = lam (fun x -> lam (fun y -> var y))
val k2TF = app (app k2 true_) false_
val run_k2TF = run k2TF

val pair m1 m2 = { apply = fun k ->
  m1.apply (fun k1 ->
    m2.apply (fun k2 ->
      k (k1, k2)
    )
  )
}

val run_pair_TF = run (pair true_ false_)

val fst m = { apply = fun k ->
  m.apply (fun x ->
    k match x with (x1, _) -> x1 end
  )
}

val snd m = { apply = fun k ->
  m.apply (fun x ->
    k match x with (_, x2) -> x2 end
  )
}

val run_fst_pair_TF = run (fst (pair true_ false_))
val run_snd_pair_TF = run (snd (pair true_ false_))
