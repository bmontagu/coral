; Test rule for input file blur.simpl
(rule
  (deps blur.simpl)
  (target blur.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff blur.expected blur.out)))

; Test rule for input file eta.simpl
(rule
  (deps eta.simpl)
  (target eta.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff eta.expected eta.out)))

; Test rule for input file kcfa2.simpl
(rule
  (deps kcfa2.simpl)
  (target kcfa2.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff kcfa2.expected kcfa2.out)))

; Test rule for input file kcfa3.simpl
(rule
  (deps kcfa3.simpl)
  (target kcfa3.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff kcfa3.expected kcfa3.out)))

; Test rule for input file mj09.simpl
(rule
  (deps mj09.simpl)
  (target mj09.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff mj09.expected mj09.out)))

; Test rule for input file sat3.simpl
(rule
  (deps sat3.simpl)
  (target sat3.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat3.expected sat3.out)))

; Test rule for input file sat3-generic.simpl
(rule
  (deps sat3-generic.simpl)
  (target sat3-generic.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat3-generic.expected sat3-generic.out)))

; Test rule for input file sat3-generic_native_bool.simpl
(rule
  (deps sat3-generic_native_bool.simpl)
  (target sat3-generic_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat3-generic_native_bool.expected sat3-generic_native_bool.out)))

; Test rule for input file sat3_native_bool.simpl
(rule
  (deps sat3_native_bool.simpl)
  (target sat3_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat3_native_bool.expected sat3_native_bool.out)))

; Test rule for input file sat4.simpl
(rule
  (deps sat4.simpl)
  (target sat4.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat4.expected sat4.out)))

; Test rule for input file sat4_native_bool.simpl
(rule
  (deps sat4_native_bool.simpl)
  (target sat4_native_bool.out)
  (action
    (with-outputs-to %{target} (run coral-simpl --strict-errors --relational %{deps}))))
(rule
  (alias   runtest)
  (action (diff sat4_native_bool.expected sat4_native_bool.out)))

