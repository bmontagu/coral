(* Example from https://github.com/ilyasergey/reachability/blob/master/benchmarks/gcfa2/kcfa3.scm *)

type boolean = True | False

(* result evaluates to False *)
val result =
  (fun f1 -> let a = f1 True in f1 False)
  (fun x1 ->
    (fun f2 -> let b = f2 True in f2 False)
    (fun x2 ->
      (fun f3 -> let c = f3 True in f3 False)
      (fun x3 -> (fun z -> z x1 x2 x3) (fun y1 y2 y3 -> y1))
    )
  )

(* same as result, with a sub-expression factored *)
val result_factored =
  let g f = let a = f True in f False in
  g
  (fun x1 ->
    g
    (fun x2 ->
      g
      (fun x3 -> (fun z -> z x1 x2 x3) (fun y1 y2 y3 -> y1))
    )
  )
