(* This OCaml program generates the Heintze-McAllester example if size n.
   This example exhibits the cubic behaviour of CFA. *)

open Printf

let prologue i =
  printf
    "(* Example taken from the article with n = %i :\n\
    \   Linear-time subtransitive control flow analysis\n\
    \   PLDI'97\n\
    \   Nevin Heintze and David McAllester\n\
     *)"
    i;
  printf "(* Start of prologue *)\n";
  printf "val fs x = x\n";
  printf "val bs x = x\n";
  printf "(* End of prologue *)\n"

let iteration i =
  printf "(* Start of iteration %i *)\n" i;
  printf "val f%i x = x\n" i;
  printf "val b%i x = x\n" i;
  printf "val x%i = b%i (fs f%i)\n" i i i;
  printf "val y%i = (bs b%i) f%i\n" i i i;
  printf "(* End of iteration %i *)\n" i

let rec iter i =
  if i > 0
  then begin
    iter (i - 1);
    printf "\n";
    iteration i
  end

let program n =
  prologue n;
  iter n

let run () =
  let nr_args = Array.length Sys.argv - 1 in
  if nr_args <> 1
  then begin
    eprintf
      "Error: %i arguments were provided, but 1 argument was expected.\n"
      nr_args;
    exit 1
  end
  else
    let arg = Sys.argv.(1) in
    match int_of_string_opt arg with
    | Some n when n >= 0 ->
      program n;
      exit 0
    | _ ->
      eprintf
        "Error: a non-negative integer argument was expected, but %s was \
         provided.\n"
        arg;
      exit 1

let () = run ()
