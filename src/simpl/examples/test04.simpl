type t = T1 | T2
type u = U
type v = V

val id (x: t) = x

val k (x: t) (y: u) = x

val k2 (x: t) (y: u) = y

val apply (f: t -> u) (x: t) = f x

val fst (x: t, y: t) = x

val snd (x: t, y: t) = y

val pair (x: t) (y: t) = (x, y)

val swap_pair (x: t, y: t) = (y, x)

val swap_pair_twice (p: t * t) = swap_pair (swap_pair p)

val swap (x: t) (y: t) = (y, x)

val swap_twice (x: t) (y: t) =
    swap (fst (swap x y)) (snd (swap x y))

val swap_twice2 (x: t) (y: t) =
    let p = swap x y in
    swap (fst (swap x y)) (snd (swap x y))

val swap_twice3 (x: t) (y: t) =
    let p = swap x y in
    swap (fst p) (snd p)

val cycle (x: t) (y: t) (z: t) =
    (z, x, y)

val cycle_twice (x: t) (y: t) (z: t) =
    match cycle x y z with
    | (x', y', z') -> cycle x' y' z'
    end

val cycle_thrice (x: t) (y: t) (z: t) =
    match cycle_twice x y z with
    | (x', y', z') -> cycle x' y' z'
    end

val cycle_thrice' (x: t) (y: t) (z: t) =
    match cycle x y z with
    | (x', y', z') -> cycle_twice x' y' z'
    end

val cycle_pair (x: t, y: t, z: t) =
    (z, x, y)

val cycle_pair_twice (p: t * t * t) =
    cycle_pair (cycle_pair p)

val cycle_pair_thrice (p: t * t * t) =
    cycle_pair_twice (cycle_pair p)

val cycle_pair_thrice' (p: t * t * t) =
    cycle_pair (cycle_pair_twice p)

type boolean = True | False

val negb (x: boolean) =
  match x with
  | True -> False
  | False -> True
  end

val andb (x: boolean) (y: boolean) =
  match x with
  | True -> y
  | False -> x
  end

val orb (x: boolean) (y: boolean) =
  match x with
  | True -> x
  | False -> y
  end

val negb_negb (x: boolean) =
  negb (negb x)

val andb_negb (x: boolean) =
  andb (negb x) x

val andb_self (x: boolean) =
  andb x x

val orb_negb (x: boolean) =
  orb (negb x) x

val orb_self (x: boolean) =
  orb x x

val equivb (x: boolean) (y: boolean) =
  match x with
  | True -> y
  | False -> negb y
  end

val equivb2 (x : boolean, y : boolean) =
  match x with
  | True -> y
  | False -> negb y
  end

val equivb3 (x : boolean) (y : boolean) =
  match (x, y) with
  | (True,  True) | (False, False) -> True
  | (True, False) | (False,  True) -> False
  end

val equivb4 (x : boolean, y : boolean) =
  match (x, y) with
  | (True,  True) | (False, False) -> True
  | (True, False) | (False,  True) -> False
  end

val choose (c: boolean) (x: t) (y: t) =
  match c with
  | True -> x
  | False -> y
  end

val choose2 (c: boolean) (p: t * t) =
  match c with
  | True -> fst p
  | False -> snd p
  end

val choose3 (p: boolean * t * t) =
  match p with
  | (True,  x, _) -> x
  | (False, _, y) -> y
  end

val choose3bis (p: boolean * t * t) =
  let b = match p with (b, _, _) -> b end in
  match p with
  | (_,  x, y) -> match b with
    | True -> x
    | False -> y
    end
  end

val might_swap (c: boolean) (x: t) (y: t) =
  match c with
  | True -> swap x y
  | False -> pair x y
  end

val might_swap2 (c: boolean) (x: t) (y: t) =
  (match c with | True -> swap | False -> pair end) x y

val might_swap_fun (c: boolean) =
  match c with | True -> swap | False -> pair end

val might_swap_fun_ext1 (c: boolean) (x: t) =
  might_swap_fun c x

val might_swap_fun_ext2 (c: boolean) (x: t) (y: t) =
  might_swap_fun c x y

val might_swap_fun_apply (c: boolean) =
  might_swap_fun c T1 T2

val might_swap_fun_apply_True =
  might_swap_fun_apply True

val might_swap_fun_apply_False =
  might_swap_fun_apply False

val might_swap_pair (c: boolean) (p: t * t) =
  match c with
  | True -> swap_pair p
  | False -> p
  end
