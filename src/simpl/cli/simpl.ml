(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

open SimplLib.Main
open Cmdliner

let analysis_kind_t =
  let open Arg in
  let open AnalysisKind in
  value
  & vflag
      IndependentAttribute
      [
        ( IndependentAttribute,
          info
            ["independent-attribute"]
            ~doc:
              "Performs an analysis that infers a relation between the current \
               environment and the output value. The independent attribute \
               abstraction is used during the analysis." );
        ( Relational,
          info
            ["relational"]
            ~doc:
              "Performs an analysis that infers a relation between the current \
               environment and the output value. This analysis is fully \
               relational." );
      ]

let run_internal_tests_t =
  let open Arg in
  value & flag & info ["run-internal-tests"] ~doc:"Runs internal tests."

let extensional_eq_t =
  let open Arg in
  value & flag & info ["ext-eq"] ~doc:"Handles equality extensionnally."

let extensional_bot_t =
  let open Arg in
  value
  & flag
  & info ["ext-bot"] ~doc:"Handles the bottom element extensionnally."

let dont_learn_from_patterns_t =
  let open Arg in
  value
  & flag
  & info
      ["dont-learn-from-patterns"]
      ~doc:
        "Disables the learning of cases for values being pattern matched \
         against."

let strict_errors_t =
  let open Arg in
  value & flag & info ["strict-errors"] ~doc:"Makes errors fatal."

let unroll_fixpoints_t =
  let open Arg in
  value
  & opt int 0
  & info
      ["unroll-fixpoints"]
      ~doc:"How many unrollings of fixpoints must be performed by default."

let delay_widening_t =
  let open Arg in
  value
  & opt int 0
  & info
      ["delay-widening"]
      ~doc:
        "How many fixpoints iterations must be performed by default before \
         widening is used."

let ( let+ ) v f =
  let open Term in
  const f $ v

let ( and+ ) v1 v2 =
  let open Term in
  const (fun x y -> (x, y)) $ v1 $ v2

let options_t : Options.t Term.t =
  let+ run_internal_tests = run_internal_tests_t
  and+ extensional_eq = extensional_eq_t
  and+ extensional_bot = extensional_bot_t
  and+ dont_learn_from_patterns = dont_learn_from_patterns_t
  and+ strict_errors = strict_errors_t
  and+ unroll_fixpoints = unroll_fixpoints_t
  and+ delay_widening = delay_widening_t
  and+ analysis_kind = analysis_kind_t in
  {
    Options.run_internal_tests;
    extensional_eq;
    extensional_bot;
    learn_from_patterns = not dont_learn_from_patterns;
    strict_errors;
    unroll_fixpoints = max 0 unroll_fixpoints;
    delay_widening = max 0 delay_widening;
    analysis_kind;
  }

let file_t =
  let open Arg in
  required
  & pos 0 (some string) None
  & info [] ~docv:"FILE" ~doc:"File to analyse"

let simpl_t =
  let open Term in
  const simpl_print $ options_t $ file_t

let simpl_info =
  let open Cmd in
  info "simpl" ~doc:"Correlation analyser for a simple functional language."

let () = exit @@ Cmd.(eval @@ v simpl_info simpl_t)
