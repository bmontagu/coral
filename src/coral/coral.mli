(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

(** Signature for types equipped with an equality function *)
module type WITH_EQUAL = sig
  type t

  val equal : t -> t -> bool
end

(** Signature for types equipped with a pretty printer *)
module type WITH_PP = sig
  type t

  val pp : Format.formatter -> t -> unit
end

(** Basic signature of maps *)
module type MAP = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val dfind : 'a -> key -> 'a t -> 'a
  val mem : key -> 'a t -> bool
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val bindings : 'a t -> (key * 'a) list
  val cardinal : 'a t -> int
end

(** Signature of options for the correlation domain *)
module type OPTIONS = sig
  val extensional_bot : bool
  val extensional_eq : bool
  val strict_errors : bool
end

module type S = sig
  type tag
  (** Type of "tags", i.e. names for variant cases *)

  type field
  (** Type of "fields", i.e. names for record components *)

  type 'a int_map
  (** Type of maps indexed by integers *)

  type 'a tag_map
  (** Type of maps indexed by tags *)

  type 'a field_map
  (** Type of maps indexed by fields *)

  (** The "side" of a correlation *)
  type side =
    | L
    | R

  (** The type of correlations *)
  type t = private
    | Bot  (** Empty relation *)
    | Eq  (** Idendity relation *)
    | Top  (** Full relation *)
    | Tuple of {
        side: side;
        args: t int_map;  (** missing bindings are implicitly set to [Top] *)
        len: int;
      }  (** Combinator for tuples on one side *)
    | Sum of {
        side: side;
        cases: args tag_map;
            (** missing bindings in the arguments are implicitly set to
           [Top]; missing bindings in the map of tags are implicitly
           set to [Bot] *)
        arity: int tag_map;
      }
        (** Combinator for sums on one side. [cases(tag) = None] iff the
       case is impossible, i.e. at least one of its arguments is set
       to [Bot]. *)
    | Record of {
        side: side;
        fields: t field_map;
            (** missing bindings are implicitly set to [Top] *)
        arity: unit field_map;
      }  (** Combinator for records on one side *)
    | Func of {
        side: side;
        arg: t;
            (** how the argument is related to the other value in the relation *)
        inner: t;  (** how the result is related to the argument *)
        outer: t;
            (** how the result is related to the other value in the relation *)
      }  (** Combinator for functions on one side *)

  and args =
    | Args of t int_map  (** When there is at least one argument *)
    | NoArgs of t
        (** When there is no argument, the provided correlation keeps
       information about the other side of the relation *)

  val bot : t
  (** Smart constructor for [Bot] *)

  val eq : t
  (** Smart constructor for [Eq] *)

  val top : t
  (** Smart constructor for [Top] *)

  val tuple : side:side -> args:t int_map -> len:int -> t
  (** Smart constructor for [Tuple] *)

  val sum : side:side -> cases:args tag_map -> arity:int tag_map -> t
  (** Smart constructor for [Sum] *)

  val record : side:side -> fields:t field_map -> arity:unit field_map -> t
  (** Smart constructor for [Record] *)

  val func : side:side -> arg:t -> inner:t -> outer:t -> t
  (** Smart constructor for [Func] *)

  val leq : t -> t -> bool
  (** Inclusion on correlations. *)

  val widen : t -> t -> t
  (** Widening of correlations. *)

  val union : t -> t -> t
  (** Union of correlations. *)

  val inter : t -> t -> t
  (** Intersection of correlations. *)

  val compose : t -> t -> t
  (** Composition of correlations. *)

  val apply : t -> t -> t
  (** Application of correlations. *)

  val flip : t -> t
  (** Computes the converse of the relation *)

  val pp : Format.formatter -> t -> unit
  (** Pretty-printer *)
end

(** Generic functor that builds an implementation of the domain of
   correlations *)
module Make
    (IntMap : MAP with type key = int)
    (Tag : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (TagMap : MAP with type key = Tag.t)
    (Field : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (FieldMap : MAP with type key = Field.t)
    (_ : OPTIONS) :
  S
    with type 'a int_map := 'a IntMap.t
     and type 'a tag_map := 'a TagMap.t
     and type 'a field_map := 'a FieldMap.t
     and type tag = Tag.t
     and type field = Field.t
