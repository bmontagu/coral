(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

(** Signature for types equipped with an equality function *)
module type WITH_EQUAL = sig
  type t

  val equal : t -> t -> bool
end

(** Signature for types equipped with a pretty printer *)
module type WITH_PP = sig
  type t

  val pp : Format.formatter -> t -> unit
end

(** Basic signature of maps *)
module type MAP = sig
  type key
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val find : key -> 'a t -> 'a
  val find_opt : key -> 'a t -> 'a option
  val dfind : 'a -> key -> 'a t -> 'a
  val mem : key -> 'a t -> bool
  val map : ('a -> 'b) -> 'a t -> 'b t
  val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val bindings : 'a t -> (key * 'a) list
  val cardinal : 'a t -> int
end

(** Signature of options for the correlation domain *)
module type OPTIONS = sig
  val extensional_bot : bool
  val extensional_eq : bool
  val strict_errors : bool
end

module type S = sig
  type tag
  (** Type of "tags", i.e. names for variant cases *)

  type field
  (** Type of "fields", i.e. names for record components *)

  type 'a int_map
  (** Type of maps indexed by integers *)

  type 'a tag_map
  (** Type of maps indexed by tags *)

  type 'a field_map
  (** Type of maps indexed by fields *)

  (** The "side" of a correlation *)
  type side =
    | L
    | R

  (** The type of correlations *)
  type t = private
    | Bot  (** Empty relation *)
    | Eq  (** Idendity relation *)
    | Top  (** Full relation *)
    | Tuple of {
        side: side;
        args: t int_map;  (** missing bindings are implicitly set to [Top] *)
        len: int;
      }  (** Combinator for tuples on one side *)
    | Sum of {
        side: side;
        cases: args tag_map;
            (** missing bindings in the arguments are implicitly set to
           [Top]; missing bindings in the map of tags are implicitly
           set to [Bot] *)
        arity: int tag_map;
      }
        (** Combinator for sums on one side. [cases(tag) = None] iff the
       case is impossible, i.e. at least one of its arguments is set
       to [Bot]. *)
    | Record of {
        side: side;
        fields: t field_map;
            (** missing bindings are implicitly set to [Top] *)
        arity: unit field_map;
      }  (** Combinator for records on one side *)
    | Func of {
        side: side;
        arg: t;
            (** how the argument is related to the other value in the relation *)
        inner: t;  (** how the result is related to the argument *)
        outer: t;
            (** how the result is related to the other value in the relation *)
      }  (** Combinator for functions on one side *)

  and args =
    | Args of t int_map  (** When there is at least one argument *)
    | NoArgs of t
        (** When there is no argument, the provided correlation keeps
       information about the other side of the relation *)

  val bot : t
  (** Smart constructor for [Bot] *)

  val eq : t
  (** Smart constructor for [Eq] *)

  val top : t
  (** Smart constructor for [Top] *)

  val tuple : side:side -> args:t int_map -> len:int -> t
  (** Smart constructor for [Tuple] *)

  val sum : side:side -> cases:args tag_map -> arity:int tag_map -> t
  (** Smart constructor for [Sum] *)

  val record : side:side -> fields:t field_map -> arity:unit field_map -> t
  (** Smart constructor for [Record] *)

  val func : side:side -> arg:t -> inner:t -> outer:t -> t
  (** Smart constructor for [Func] *)

  val leq : t -> t -> bool
  (** Inclusion on correlations. *)

  val widen : t -> t -> t
  (** Widening of correlations. *)

  val union : t -> t -> t
  (** Union of correlations. *)

  val inter : t -> t -> t
  (** Intersection of correlations. *)

  val compose : t -> t -> t
  (** Composition of correlations. *)

  val apply : t -> t -> t
  (** Application of correlations. *)

  val flip : t -> t
  (** Computes the converse of the relation *)

  val pp : Format.formatter -> t -> unit
  (** Pretty-printer *)
end

(** Fold on integers in [[start, start+len)] in increasing order *)
let rec int_fold f start len acc =
  if len <= 0 then acc else int_fold f (start + 1) (len - 1) (f start acc)

(** [for_all] on integers in [[start, start+len)] in increasing order *)
let rec int_for_all f start len =
  if len <= 0 then true else f start && int_for_all f (start + 1) (len - 1)

(** Generic functor that builds an implementation of the domain of
   correlations *)
module Make
    (IntMap : MAP with type key = int)
    (Tag : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (TagMap : MAP with type key = Tag.t)
    (Field : sig
      include WITH_EQUAL
      include WITH_PP with type t := t
    end)
    (FieldMap : MAP with type key = Field.t)
    (Options : OPTIONS) =
struct
  type tag = TagMap.key
  (** Type of "tags", i.e. names for variant cases *)

  type field = FieldMap.key
  (** Type of "fields", i.e. names for record components *)

  (** The "side" of a correlation *)
  type side =
    | L
    | R

  (** Returns the reverse of a side *)
  let rev_side = function
    | L -> R
    | R -> L

  let pp_side fmt side =
    Format.fprintf
      fmt
      (match side with
      | L -> "→"
      | R -> "←")

  (* TODO: add bounds for recursive types to limit unfolding *)

  (** The type of correlations. Invariant: the maps in the [Tuple],
     [Sum] and [Record] cases cannot be fully set to [Bot], or to
     [Top]. *)
  type t =
    | Bot  (** Empty relation *)
    | Eq  (** Idendity relation *)
    | Top  (** Full relation *)
    | Tuple of {
        side: side;
        args: t IntMap.t;  (** missing bindings are implicitly set to [Top] *)
        len: int;
      }  (** Combinator for tuples on one side *)
    | Sum of {
        side: side;
        cases: args TagMap.t;
            (** missing bindings in the arguments are implicitly set to
           [Top]; missing bindings in the map of tags are implicitly
           set to [Bot] *)
        arity: int TagMap.t;
      }
        (** Combinator for sums on one side. [cases(tag) = None] iff the
       case is impossible, i.e. at least one of its arguments is set
       to [Bot]. *)
    | Record of {
        side: side;
        fields: t FieldMap.t;
            (** missing bindings are implicitly set to [Top] *)
        arity: unit FieldMap.t;
      }  (** Combinator for records on one side *)
    | Func of {
        side: side;
        arg: t;
            (** how the argument is related to the other value in the relation *)
        inner: t;  (** how the result is related to the argument *)
        outer: t;
            (** how the result is related to the other value in the relation *)
      }  (** Combinator for functions on one side *)

  and args =
    | Args of t IntMap.t  (** When there is at least one argument *)
    | NoArgs of t
        (** When there is no argument, the provided correlation keeps
       information about the other side of the relation *)

  (** Smart constructor for [Bot] *)
  let bot = Bot

  (** Smart constructor for [Eq] *)
  let eq = Eq

  (** Smart constructor for [Top] *)
  let top = Top

  (** Whether the argument is [Bot] *)
  let is_bot = function
    | Bot -> true
    | _ -> false

  (** Whether the argument is [Top] *)
  let is_top = function
    | Top -> true
    | _ -> false

  type map_analysis =
    | MapInit
    | MapUnknown
    | MapAllBot
    | MapAllTop

  let analyze_map fold args acc =
    fold
      (fun _index arg acc ->
        match (arg, acc) with
        | Bot, (MapInit | MapAllBot) -> MapAllBot
        | Top, (MapInit | MapAllTop) -> MapAllTop
        | _, _ -> MapUnknown)
      args
      acc

  (** Smart constructor for [Tuple] *)
  let tuple ~collapse ~side ~args ~len =
    match analyze_map IntMap.fold args MapInit with
    | MapUnknown ->
      if collapse && IntMap.exists (fun _ x -> is_bot x) args
      then Bot
      else Tuple { side; args; len }
    | MapAllBot -> Bot
    | MapAllTop | MapInit -> Top

  let tuple_no_collapse ~side ~args ~len =
    tuple ~collapse:false ~side ~args ~len

  let tuple ~side ~args ~len = tuple ~collapse:true ~side ~args ~len

  (** Smart constructor for [Record] *)
  let record ~collapse ~side ~fields ~arity =
    match analyze_map FieldMap.fold fields MapInit with
    | MapUnknown ->
      if collapse && FieldMap.exists (fun _ x -> is_bot x) fields
      then Bot
      else Record { side; fields; arity }
    | MapAllBot -> Bot
    | MapAllTop | MapInit -> Top

  let record_no_collapse ~side ~fields ~arity =
    record ~collapse:false ~side ~fields ~arity

  let record ~side ~fields ~arity = record ~collapse:true ~side ~fields ~arity

  let analyze_sum_map cases acc =
    TagMap.fold
      (fun tag args (s, m, card) ->
        match s with
        | MapInit | MapAllBot | MapAllTop -> (
          match args with
          | Args args' -> (
            match analyze_map IntMap.fold args' MapInit with
            | MapAllBot ->
              let s =
                match s with
                | MapInit | MapAllBot -> MapAllBot
                | MapAllTop -> MapUnknown
                | MapUnknown -> assert false
              in
              (s, m, card + 1)
            | MapInit | MapAllTop ->
              let s =
                match s with
                | MapInit -> MapAllTop
                | MapAllBot -> MapUnknown
                | MapAllTop -> MapAllTop
                | MapUnknown -> assert false
              in
              (s, TagMap.add tag (Args IntMap.empty) m, card + 1)
            | MapUnknown -> (MapUnknown, TagMap.add tag (Args args') m, card + 1)
            )
          | NoArgs Bot ->
            let s =
              match s with
              | MapInit | MapAllBot -> MapAllBot
              | MapAllTop -> MapUnknown
              | MapUnknown -> assert false
            in
            (s, m, card + 1)
          | NoArgs Top ->
            let s =
              match s with
              | MapInit -> MapAllTop
              | MapAllBot -> MapUnknown
              | MapAllTop -> MapAllTop
              | MapUnknown -> assert false
            in
            (s, TagMap.add tag args m, card + 1)
          | NoArgs _ -> (MapUnknown, TagMap.add tag args m, card + 1))
        | MapUnknown -> (
          match args with
          | Args args' -> (
            match analyze_map IntMap.fold args' MapInit with
            | MapAllBot -> (MapUnknown, m, card + 1)
            | _ -> (MapUnknown, TagMap.add tag args m, card + 1))
          | NoArgs Bot -> (MapUnknown, m, card + 1)
          | NoArgs _ -> (MapUnknown, TagMap.add tag args m, card + 1)))
      cases
      acc

  (** Smart constructor for [Sum] *)
  let sum ~collapse ~side ~cases ~arity =
    let cases =
      if collapse
      then
        TagMap.map
          (function
            | Args args as arg ->
              if IntMap.exists (fun _ c -> is_bot c) args
              then
                (* if there is some Bot, then put all of them to Bot *)
                Args (IntMap.map (fun _ -> bot) args)
              else arg
            | NoArgs _ as no_arg -> no_arg)
          cases
      else cases
    in
    let s, cases, card = analyze_sum_map cases (MapInit, TagMap.empty, 0) in
    match s with
    | MapInit | MapUnknown -> Sum { side; cases; arity }
    | MapAllBot -> Bot
    | MapAllTop ->
      if card = TagMap.cardinal arity then Top else Sum { side; cases; arity }

  let sum_no_collapse ~side ~cases ~arity =
    sum ~collapse:false ~side ~cases ~arity

  let sum ~side ~cases ~arity = sum ~collapse:true ~side ~cases ~arity

  (** Smart constructor for [Func] *)
  let func ~side ~arg ~inner ~outer =
    if is_bot arg || (is_top inner && is_top outer)
    then top
    else Func { side; arg; inner; outer }

  (** Projection of depth 1 *)
  type path =
    | ProjTuple of {
        index: int;
        len: int;
      }
        (** Projection on the component [index] of a tuple of size [len].
       Invariant: [0 <= i < len] *)
    | ProjSum of {
        tag: tag;
        arity: int TagMap.t;
        index: int;
      }
        (** Projection on the argument [index] of the case [tag] of a case
       whose arity is described by [arity]. Invariants:
       [tag \in dom arity], and [0 <= index < arity(tag)] *)
    | ProjTag of {
        tag: tag;
        arity: int TagMap.t;
      }
        (** Projection on the case [tag] of a case whose arity is
       described by [arity]. Invariant: [tag \in dom arity] *)
    | ProjRecord of {
        field: field;
        arity: unit FieldMap.t;
      }
        (** Projection on the field [field] of a record whose arity is
       described by [arith]. Invariant: [field \in dom arity] *)

  let int_map_init f start len =
    int_fold (fun i acc -> IntMap.add i (f i) acc) start len IntMap.empty

  (** Checks that [0 <= index < len] and [len = len'] *)
  let check_index_len index len len' =
    if not (0 <= index && index < len && len = len')
    then assert (not Options.strict_errors)

  (** Checks that [tag] and the optional index [indexo] belong to
     [arity], and that [arity = arity'] *)
  let check_tags tag indexo arity arity' =
    begin
      match indexo with
      | Some index -> (
        match TagMap.find tag arity with
        | len -> check_index_len index len len
        | exception Not_found -> assert (not Options.strict_errors))
      | None ->
        if not @@ TagMap.mem tag arity then assert (not Options.strict_errors)
    end;
    if not @@ TagMap.equal (( = ) : int -> int -> bool) arity arity'
    then assert (not Options.strict_errors)

  (** Checks that [field] belongs to [arity] and that [arity = arity'] *)
  let check_fields field arity arity' =
    if not @@ FieldMap.mem field arity then assert (not Options.strict_errors);
    if not @@ FieldMap.equal (fun () () -> true) arity arity'
    then assert (not Options.strict_errors)

  (** Projects the correlation [c] along the path [p] on the side
     [side]. *)
  let rec proj ~strong side p c =
    match (side, p, c) with
    | _, _, ((Bot | Top) as c) -> c
    | _, ProjTuple { index; len }, Eq ->
      Tuple { side = rev_side side; args = IntMap.singleton index Eq; len }
    | _, ProjSum { tag; arity; index }, Eq ->
      Sum
        {
          side = rev_side side;
          cases = TagMap.singleton tag @@ Args (IntMap.singleton index Eq);
          arity;
        }
    | _, ProjTag { tag; arity }, Eq ->
      Sum
        {
          side = rev_side side;
          cases = TagMap.singleton tag @@ NoArgs eq;
          arity;
        }
    | _, ProjRecord { field; arity }, Eq ->
      Record
        { side = rev_side side; fields = FieldMap.singleton field Eq; arity }
    | L, ProjTuple { index; len }, Tuple { side = L; args; len = len' }
    | R, ProjTuple { index; len }, Tuple { side = R; args; len = len' } ->
      check_index_len index len len';
      IntMap.dfind Top index args
    | L, _, Tuple { side = R as side'; args; len }
    | R, _, Tuple { side = L as side'; args; len } ->
      Tuple { side = side'; args = IntMap.map (proj ~strong side p) args; len }
    | L, (ProjSum _ | ProjTag _ | ProjRecord _), Tuple { side = L; _ }
    | R, (ProjSum _ | ProjTag _ | ProjRecord _), Tuple { side = R; _ } ->
      assert (not Options.strict_errors);
      if strong then bot else top
    | L, ProjSum { tag; arity; index }, Sum { side = L; cases; arity = arity' }
    | R, ProjSum { tag; arity; index }, Sum { side = R; cases; arity = arity' }
      -> (
      check_tags tag (Some index) arity arity';
      match TagMap.find_opt tag cases with
      | Some (Args m) -> IntMap.dfind Top index m
      | Some (NoArgs _) -> assert false
      (* should not happen since there is no argument on which to project *)
      | None -> Bot)
    | L, _, Sum { side = R as side'; cases; arity }
    | R, _, Sum { side = L as side'; cases; arity } ->
      Sum
        {
          side = side';
          cases =
            TagMap.map
              (function
                | Args args -> Args (IntMap.map (proj ~strong side p) args)
                | NoArgs c -> NoArgs (proj ~strong side p c))
              cases;
          arity;
        }
    | L, ProjTag { tag; arity }, Sum { side = L; cases; arity = arity' }
    | R, ProjTag { tag; arity }, Sum { side = R; cases; arity = arity' } -> (
      check_tags tag None arity arity';
      match TagMap.find_opt tag cases with
      | Some (Args _) -> Top
      | Some (NoArgs c) -> c
      | None -> Bot)
    | L, (ProjTuple _ | ProjRecord _), Sum { side = L; _ }
    | R, (ProjTuple _ | ProjRecord _), Sum { side = R; _ } ->
      assert (not Options.strict_errors);
      if strong then bot else top
    | ( L,
        ProjRecord { field; arity },
        Record { side = L; fields; arity = arity' } )
    | ( R,
        ProjRecord { field; arity },
        Record { side = R; fields; arity = arity' } ) ->
      check_fields field arity arity';
      FieldMap.dfind Top field fields
    | L, _, Record { side = R as side'; fields; arity }
    | R, _, Record { side = L as side'; fields; arity } ->
      Record
        {
          side = side';
          fields = FieldMap.map (proj ~strong side p) fields;
          arity;
        }
    | L, (ProjTuple _ | ProjSum _ | ProjTag _), Record { side = L; _ }
    | R, (ProjTuple _ | ProjSum _ | ProjTag _), Record { side = R; _ } ->
      assert (not Options.strict_errors);
      if strong then bot else top
    | ( L,
        ((ProjTuple _ | ProjRecord _) as p),
        Func { side = R as side'; arg; inner; outer } )
    | ( R,
        ((ProjTuple _ | ProjRecord _) as p),
        Func { side = L as side'; arg; inner; outer } ) ->
      (* XXX: /!\ can we get anything more precise? *)
      if strong && is_top arg
      then
        func
          ~side:side'
          ~arg:(proj ~strong side p arg)
          ~inner
          ~outer:(proj ~strong side p outer)
      else top
    | ( L,
        ((ProjSum _ | ProjTag _) as p),
        Func { side = R as side'; arg; inner; outer } )
    | ( R,
        ((ProjSum _ | ProjTag _) as p),
        Func { side = L as side'; arg; inner; outer } ) ->
      (* XXX: /!\ can we get anything more precise? *)
      if strong && is_top arg
      then
        func
          ~side:side'
          ~arg:(proj ~strong side p arg)
          ~inner
          ~outer:(proj ~strong side p outer)
      else top
    | L, _, Func { side = L; _ } | R, _, Func { side = R; _ } ->
      assert (not Options.strict_errors);
      if strong then bot else top

  let weak_proj = proj ~strong:false

  (** Inclusion on correlations. *)
  let rec leq c1 c2 =
    match (c1, c2) with
    | Bot, _ | _, Top | Eq, Eq -> true
    | (Top | Eq | Func _), Bot | Top, Eq -> false
    | c, Tuple { side; args; len } ->
      IntMap.for_all
        (fun index c_index ->
          leq (weak_proj side (ProjTuple { index; len }) c) c_index)
        args
    | Tuple { side = _; args; len }, Bot ->
      Options.extensional_bot
      && (* we need a [for_all] here, so that projection is monotonic (an
            [exists] is sufficient, semantically, though) *)
      int_for_all (fun index -> leq (IntMap.dfind Top index args) Bot) 0 len
    | Tuple { side; args; len }, Eq ->
      Options.extensional_eq
      && int_for_all
           (fun index ->
             let c_index = IntMap.dfind Top index args in
             (* there is no need to test the other "coordinates", because
                on the eta-expansion of [Eq] those coordinates are [Top] *)
             leq
               (weak_proj (rev_side side) (ProjTuple { index; len }) c_index)
               Eq)
           0
           len
    | c, Sum { side; cases; arity } ->
      TagMap.for_all
        (fun tag _len ->
          match TagMap.find_opt tag cases with
          | Some (Args args) ->
            IntMap.for_all
              (fun index c_index ->
                leq (weak_proj side (ProjSum { tag; arity; index }) c) c_index)
              args
          | Some (NoArgs c_tag) ->
            leq (weak_proj side (ProjTag { tag; arity }) c) c_tag
          | None -> leq (weak_proj side (ProjTag { tag; arity }) c) Bot)
        arity
    | Sum { side = _; cases; arity = _ }, Bot ->
      Options.extensional_bot
      && TagMap.for_all
           (fun _tag args ->
             match args with
             | Args args ->
               (not @@ IntMap.is_empty args)
               && IntMap.for_all (fun _index c_index -> leq c_index Bot) args
             | NoArgs c_tag -> leq c_tag Bot)
           cases
    | Sum { side; cases; arity }, Eq ->
      Options.extensional_eq
      && TagMap.for_all
           (fun tag len ->
             match TagMap.find_opt tag cases with
             | None -> true
             | Some (Args args) ->
               TagMap.for_all
                 (fun tag' _len ->
                   if Tag.equal tag tag'
                   then
                     (* we are on the diagonal of the matrix *)
                     int_for_all
                       (fun index ->
                         let c_index = IntMap.dfind Top index args in
                         (* there is no need to test the other
                            "coordinates", because on the eta-expansion
                            of [Eq] those coordinates are [Top] *)
                         let c_index2 =
                           weak_proj
                             (rev_side side)
                             (ProjSum { tag; arity; index })
                             c_index
                         in
                         leq c_index2 Eq)
                       0
                       len
                   else
                     (* we are outside of the diagonal: the coordinates
                        of the eta-expansion of [Eq] are [Bot] *)
                     not @@ TagMap.mem tag' cases)
                 arity
             | Some (NoArgs c_tag) ->
               TagMap.for_all
                 (fun tag' _len ->
                   if Tag.equal tag tag'
                   then
                     (* we are on the diagonal ofthe matrix *)
                     leq
                       (weak_proj
                          (rev_side side)
                          (ProjTag { tag; arity })
                          c_tag)
                       Eq
                   else
                     (* we are outside of the diagonal: the coordinates
                        of the eta-expansion of [Eq] are [Bot] *)
                     leq
                       (weak_proj
                          (rev_side side)
                          (ProjTag { tag; arity })
                          c_tag)
                       Bot)
                 arity)
           arity
    | c, Record { side; fields; arity } ->
      FieldMap.for_all
        (fun field c_field ->
          leq (weak_proj side (ProjRecord { field; arity }) c) c_field)
        fields
    | Record { side; fields; arity }, Bot ->
      Options.extensional_bot
      && (* we need a [for_all] here, so that projection is monotonic (an
            [exists] is sufficient, semantically, though) *)
      FieldMap.for_all
        (fun field () ->
          let c_field = FieldMap.dfind Top field fields in
          leq (weak_proj side (ProjRecord { field; arity }) c_field) Bot)
        arity
    | Record { side; fields; arity }, Eq ->
      Options.extensional_eq
      && FieldMap.for_all
           (fun field () ->
             let c_field = FieldMap.dfind Top field fields in
             (* there is no need to test the other "coordinates", because
                on the eta-expansion of [Eq] those coordinates are [Top] *)
             leq
               (weak_proj (rev_side side) (ProjRecord { field; arity }) c_field)
               Eq)
           arity
    (* XXX /!\ check all this for soundness *)
    | Top, Func _ -> false
    | Eq, Func _ -> false (* ??? *)
    | Func _, Eq -> false (* extensionality on functions? *)
    | ( Func { side = L; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = L; arg = arg2; inner = inner2; outer = outer2 } )
    | ( Func { side = R; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = R; arg = arg2; inner = inner2; outer = outer2 } ) ->
      leq arg2 arg1 && leq inner1 inner2 && leq outer1 outer2
    | Func { side = L; _ }, Func { side = R; _ }
    | Func { side = R; _ }, Func { side = L; _ } ->
      false (* how can we compare this? *)
    | Tuple { side = L; _ }, Func { side = L; _ }
    | Tuple { side = R; _ }, Func { side = R; _ }
    | Record { side = L; _ }, Func { side = L; _ }
    | Record { side = R; _ }, Func { side = R; _ }
    | Sum { side = L; _ }, Func { side = L; _ }
    | Sum { side = R; _ }, Func { side = R; _ } -> false (* ill-typed cases *)
    | Tuple { side = L; _ }, Func { side = R; _ }
    | Tuple { side = R; _ }, Func { side = L; _ } ->
      false (* how can we compare this? *)
    | Record { side = L; _ }, Func { side = R; _ }
    | Record { side = R; _ }, Func { side = L; _ } ->
      false (* how can we compare this? *)
    | Sum { side = L; _ }, Func { side = R; _ }
    | Sum { side = R; _ }, Func { side = L; _ } -> false

  (* how can we compare this? *)

  (** Widening of correlations: computes an upper bound for [leq]. In
     the absence of recursive types, for fixed types, the domain is
     finite, therefore there is no infinite ascending chain. When we
     add recursive types or numeric abstract domains, this needs to be
     modified! *)
  let rec widen c1 c2 =
    match (c1, c2) with
    | c, Bot | Bot, c | _, (Top as c) | (Top as c), _ | (Eq as c), Eq -> c
    | Sum { side; cases; arity }, c | c, Sum { side; cases; arity } ->
      let cases =
        TagMap.mapi
          (fun tag len ->
            match TagMap.find_opt tag cases with
            | Some (Args args) ->
              (* XXX: issue when len = 0 *)
              let m =
                IntMap.mapi
                  (fun index arg ->
                    widen arg
                    @@ weak_proj side (ProjSum { tag; index; arity }) c)
                  args
              in
              Args m
            | Some (NoArgs c_tag) ->
              NoArgs (widen c_tag @@ weak_proj side (ProjTag { tag; arity }) c)
            | None ->
              if len = 0
              then NoArgs (weak_proj side (ProjTag { tag; arity }) c)
              else
                let m =
                  int_map_init
                    (fun index ->
                      weak_proj side (ProjSum { tag; index; arity }) c)
                    0
                    len
                in
                Args m)
          arity
      in
      sum_no_collapse ~side ~cases ~arity
    | Tuple { side; args; len }, c | c, Tuple { side; args; len } ->
      let args =
        (* len = 0 should not happen *)
        IntMap.mapi
          (fun index arg ->
            widen arg @@ weak_proj side (ProjTuple { index; len }) c)
          args
      in
      tuple_no_collapse ~side ~args ~len
    | Record { side; fields; arity }, c | c, Record { side; fields; arity } ->
      let fields =
        (* empty arity empty should not happen *)
        FieldMap.mapi
          (fun field c_field ->
            widen c_field @@ weak_proj side (ProjRecord { field; arity }) c)
          fields
      in
      record_no_collapse ~side ~fields ~arity
    (* XXX /!\ check all this for soundness *)
    | Func _, Eq | Eq, Func _ ->
      (* XXX what else could we do? *)
      top
    | ( Func { side = L as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = L; arg = arg2; inner = inner2; outer = outer2 } )
    | ( Func { side = R as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = R; arg = arg2; inner = inner2; outer = outer2 } ) ->
      (* XXX: for arguments, one should use an under-approximation of
         the intersection *)
      if leq arg1 arg2
      then
        func
          ~side
          ~arg:arg1
          ~inner:(widen inner1 inner2)
          ~outer:(widen outer1 outer2)
      else if leq arg2 arg1
      then
        func
          ~side
          ~arg:arg2
          ~inner:(widen inner1 inner2)
          ~outer:(widen outer1 outer2)
      else top
    | Func { side = L; _ }, Func { side = R; _ }
    | Func { side = R; _ }, Func { side = L; _ } ->
      (* XXX: what should we do here? *)
      top

  let proj = proj ~strong:true

  (** Union of correlations. *)
  let rec union c1 c2 =
    match (c1, c2) with
    | c, Bot | Bot, c | _, (Top as c) | (Top as c), _ | (Eq as c), Eq -> c
    | Sum { side; cases; arity }, c | c, Sum { side; cases; arity } ->
      let cases =
        TagMap.mapi
          (fun tag len ->
            match TagMap.find_opt tag cases with
            | Some (Args args) ->
              (* XXX: issue when len = 0 *)
              let m =
                IntMap.mapi
                  (fun index arg ->
                    union arg @@ proj side (ProjSum { tag; index; arity }) c)
                  args
              in
              Args m
            | Some (NoArgs c_tag) ->
              NoArgs (union c_tag @@ proj side (ProjTag { tag; arity }) c)
            | None ->
              if len = 0
              then NoArgs (proj side (ProjTag { tag; arity }) c)
              else
                let m =
                  int_map_init
                    (fun index -> proj side (ProjSum { tag; index; arity }) c)
                    0
                    len
                in
                Args m)
          arity
      in
      sum ~side ~cases ~arity
    | Tuple { side; args; len }, c | c, Tuple { side; args; len } ->
      let args =
        (* len = 0 should not happen *)
        IntMap.mapi
          (fun index arg -> union arg @@ proj side (ProjTuple { index; len }) c)
          args
      in
      tuple ~side ~args ~len
    | Record { side; fields; arity }, c | c, Record { side; fields; arity } ->
      let fields =
        (* empty arity empty should not happen *)
        FieldMap.mapi
          (fun field c_field ->
            union c_field @@ proj side (ProjRecord { field; arity }) c)
          fields
      in
      record ~side ~fields ~arity
    (* XXX /!\ check all this for soundness *)
    | Func _, Eq | Eq, Func _ ->
      (* XXX what else could we do? *)
      top
    | ( Func { side = L as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = L; arg = arg2; inner = inner2; outer = outer2 } )
    | ( Func { side = R as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = R; arg = arg2; inner = inner2; outer = outer2 } ) ->
      (* XXX: for arguments, one should use an under-approximation of
         the intersection *)
      if leq arg1 arg2
      then
        func
          ~side
          ~arg:arg1
          ~inner:(union inner1 inner2)
          ~outer:(union outer1 outer2)
      else if leq arg2 arg1
      then
        func
          ~side
          ~arg:arg2
          ~inner:(union inner1 inner2)
          ~outer:(union outer1 outer2)
      else top
    | Func { side = L; _ }, Func { side = R; _ }
    | Func { side = R; _ }, Func { side = L; _ } ->
      (* XXX: what should we do here? *)
      top

  (** Intersection of correlations. *)
  let rec inter c1 c2 =
    match (c1, c2) with
    | c, Top | Top, c | _, (Bot as c) | (Bot as c), _ | (Eq as c), Eq -> c
    | Sum { side; cases; arity }, c | c, Sum { side; cases; arity } ->
      let cases =
        TagMap.mapi
          (fun tag args ->
            match args with
            | Args args ->
              let len = TagMap.find tag arity in
              (* len = 0 should not happen *)
              let m =
                int_map_init
                  (fun index ->
                    let arg = IntMap.dfind Top index args in
                    inter arg @@ proj side (ProjSum { tag; index; arity }) c)
                  0
                  len
              in
              Args m
            | NoArgs c_tag ->
              NoArgs (inter c_tag @@ proj side (ProjTag { tag; arity }) c))
          cases
      in
      sum ~side ~cases ~arity
    | Tuple { side; args; len }, c | c, Tuple { side; args; len } ->
      let args =
        (* len = 0 should not happen *)
        int_map_init
          (fun index ->
            let arg = IntMap.dfind Top index args in
            inter arg @@ proj side (ProjTuple { index; len }) c)
          0
          len
      in
      tuple ~side ~args ~len
    | Record { side; fields; arity }, c | c, Record { side; fields; arity } ->
      let fields =
        (* empty arity should not happen *)
        FieldMap.mapi
          (fun field () ->
            let c_field = FieldMap.dfind Top field fields in
            inter c_field @@ proj side (ProjRecord { field; arity }) c)
          arity
      in
      record ~side ~fields ~arity
    (* XXX /!\ check all this for soundness *)
    | Eq, (Func _ as c) | (Func _ as c), Eq ->
      (* what else could we do? *)
      c
    | ( Func { side = L as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = L; arg = arg2; inner = inner2; outer = outer2 } )
    | ( Func { side = R as side; arg = arg1; inner = inner1; outer = outer1 },
        Func { side = R; arg = arg2; inner = inner2; outer = outer2 } ) ->
      (* XXX: for arguments, one should use an under-approximation of
         the intersection *)
      if leq arg1 arg2
      then
        func
          ~side
          ~arg:arg1
          ~inner:(inter inner1 inner2)
          ~outer:(inter outer1 outer2)
      else if leq arg2 arg1
      then
        func
          ~side
          ~arg:arg2
          ~inner:(inter inner1 inner2)
          ~outer:(inter outer1 outer2)
      else top
    | Func { side = L; _ }, Func { side = R; _ }
    | Func { side = R; _ }, Func { side = L; _ } ->
      (* XXX: what should we do here? *)
      top

  (** Computes the converse of the relation *)
  let rec flip = function
    | (Top | Eq | Bot) as c -> c
    | Tuple { side; args; len } ->
      Tuple { side = rev_side side; args = IntMap.map flip args; len }
    | Sum { side; cases; arity } ->
      Sum
        {
          side = rev_side side;
          cases =
            TagMap.map
              (function
                | Args m -> Args (IntMap.map flip m)
                | NoArgs c -> NoArgs (flip c))
              cases;
          arity;
        }
    | Record { side; fields; arity } ->
      Record { side = rev_side side; fields = FieldMap.map flip fields; arity }
    | Func { side; arg; inner; outer } ->
      Func
        {
          side = rev_side side;
          arg = flip arg;
          inner;
          (* do _not_ flip! *)
          outer = flip outer;
        }

  let map_inters fold m = fold (fun _ c1 c2 -> inter c1 c2) m Top
  let map_inters_with fold f m = fold (fun _ c acc -> inter (f c) acc) m Top
  let map_unions fold m = fold (fun _ c1 c2 -> union c1 c2) m Bot
  let map_unions_with fold f m = fold (fun _ c acc -> union (f c) acc) m Bot

  (** Composition of correlations. *)
  let rec compose c1 c2 =
    match (c1, c2) with
    | (Top as c), Top | (Bot as c), _ | _, (Bot as c) | Eq, c | c, Eq -> c
    | c, Sum { side = R as side; cases; arity } ->
      let cases =
        (* XXX: issue when arity of some constructor is 0 *)
        TagMap.map
          (function
            | Args args -> Args (IntMap.map (fun c' -> compose c c') args)
            | NoArgs c_tag -> NoArgs (compose c c_tag))
          cases
      in
      sum ~side ~cases ~arity
    | Sum { side = L as side; cases; arity }, c ->
      let cases =
        (* XXX: issue when arity of some constructor is 0 *)
        TagMap.map
          (function
            | Args args -> Args (IntMap.map (fun c' -> compose c' c) args)
            | NoArgs c_tag -> NoArgs (compose c_tag c))
          cases
      in
      sum ~side ~cases ~arity
    | ( Sum { side = R; cases = cases1; arity = arity1 },
        Sum { side = L; cases = cases2; arity = arity2 } ) ->
      if not (TagMap.equal (( = ) : int -> int -> bool) arity1 arity2)
      then begin
        assert (not Options.strict_errors);
        Bot
      end
      else
        let m =
          TagMap.merge
            (fun _ o1 o2 ->
              match (o1, o2) with
              | Some (Args args1), Some (Args args2) ->
                let m =
                  IntMap.merge
                    (fun _ c1o c2o ->
                      match (c1o, c2o) with
                      | Some c1, Some c2 -> Some (compose c1 c2)
                      | Some c1, None -> Some (compose c1 Top)
                      | None, Some c2 -> Some (compose Top c2)
                      | None, None -> assert false)
                    args1
                    args2
                in
                Some (map_inters IntMap.fold m)
              | Some (NoArgs c1), Some (NoArgs c2) -> Some (compose c1 c2)
              | Some (NoArgs _), Some (Args _) | Some (Args _), Some (NoArgs _)
                -> assert false
              | None, _ | _, None -> None)
            cases1
            cases2
        in
        map_unions TagMap.fold m
    | Sum { side = R; cases; _ }, Top ->
      (* XXX: issue when arity of some constructor is 0 *)
      map_unions_with
        TagMap.fold
        (function
          | Args args ->
            map_inters_with IntMap.fold (fun c -> compose c Top) args
          | NoArgs c -> compose c Top)
        cases
    | Top, Sum { side = L; cases; _ } ->
      (* XXX: issue when arity of some constructor is 0 *)
      map_unions_with
        TagMap.fold
        (function
          | Args args ->
            map_inters_with IntMap.fold (fun c -> compose Top c) args
          | NoArgs c -> compose Top c)
        cases
    | c, Tuple { side = R as side; args; len } ->
      let args =
        (* len = 0 should not happen *)
        int_map_init
          (fun index -> compose c (IntMap.dfind Top index args))
          0
          len
      in
      tuple ~side ~args ~len
    | Tuple { side = L as side; args; len }, c ->
      let args =
        (* len = 0 should not happen *)
        int_map_init
          (fun index -> compose (IntMap.dfind Top index args) c)
          0
          len
      in
      tuple ~side ~args ~len
    | ( Tuple { side = R; args = args1; len = len1 },
        Tuple { side = L; args = args2; len = len2 } ) ->
      if not (len1 = len2)
      then begin
        assert (not Options.strict_errors);
        Bot
      end
      else
        let m =
          (* len1 = 0 should not happen *)
          IntMap.merge
            (fun _ c1o c2o ->
              match (c1o, c2o) with
              | Some c1, Some c2 -> Some (compose c1 c2)
              | Some c1, None -> Some (compose c1 Top)
              | None, Some c2 -> Some (compose Top c2)
              | None, None -> assert false)
            args1
            args2
        in
        map_inters IntMap.fold m
    | Tuple { side = R; args; _ }, Top ->
      (* len = 0 should not happen *)
      map_inters_with IntMap.fold (fun c -> compose c Top) args
    | Top, Tuple { side = L; args; _ } ->
      (* len = 0 should not happen *)
      map_inters_with IntMap.fold (fun c -> compose Top c) args
    | c, Record { side = R as side; fields; arity } ->
      let fields =
        (* empty arity should not happen *)
        FieldMap.mapi
          (fun field () -> compose c (FieldMap.dfind Top field fields))
          arity
      in
      record ~side ~fields ~arity
    | Record { side = L as side; fields; arity }, c ->
      let fields =
        (* empty arity should not happen *)
        FieldMap.mapi
          (fun field () -> compose (FieldMap.dfind Top field fields) c)
          arity
      in
      record ~side ~fields ~arity
    | ( Record { side = R; fields = fields1; arity = arity1 },
        Record { side = L; fields = fields2; arity = arity2 } ) ->
      if not (FieldMap.equal (fun () () -> true) arity1 arity2)
      then begin
        assert (not Options.strict_errors);
        Bot
      end
      else
        let m =
          (* empty arity1 should not happen *)
          FieldMap.merge
            (fun _ c1o c2o ->
              match (c1o, c2o) with
              | Some c1, Some c2 -> Some (compose c1 c2)
              | Some c1, None -> Some (compose c1 Top)
              | None, Some c2 -> Some (compose Top c2)
              | None, None -> assert false)
            fields1
            fields2
        in
        map_inters FieldMap.fold m
    | Record { side = R; fields; _ }, Top ->
      (* empty arity should not happen *)
      map_inters_with FieldMap.fold (fun c -> compose c Top) fields
    | Top, Record { side = L; fields; _ } ->
      (* empty arity should not happen *)
      map_inters_with FieldMap.fold (fun c -> compose Top c) fields
    | Tuple { side = R; _ }, (Sum { side = L; _ } | Record { side = L; _ })
    | (Sum { side = R; _ } | Record { side = R; _ }), Tuple { side = L; _ }
    | Sum { side = R; _ }, Record { side = L; _ }
    | Record { side = R; _ }, Sum { side = L; _ } ->
      assert (not Options.strict_errors);
      Bot
    (* XXX /!\ check all this for soundness *)
    | Func { side = L as side; arg; inner; outer }, c ->
      let arg' = compose arg c in
      if leq (compose arg' (flip c)) arg
      then func ~side ~arg:arg' ~inner ~outer:(compose outer c)
      else top
    | c, Func { side = R as side; arg; inner; outer } ->
      let arg' = compose c arg in
      if leq (compose (flip c) arg') arg
      then
        func
          ~side
          ~arg:arg'
          ~inner (* XXX is that ok? *)
          ~outer:(compose c outer)
      else top
    | ( Func { side = R; arg = _; inner = _; outer = _outer1 },
        Func { side = L; arg = _; inner = _; outer = _outer2 } ) -> top
    | Func { side = R; _ }, Top ->
      (* XXX what else could we do here? *)
      top
    | Top, Func { side = L; _ } ->
      (* XXX what else could we do here? *)
      top
    | ( (Tuple { side = R; _ } | Record { side = R; _ } | Sum { side = R; _ }),
        Func { side = L; _ } )
    | ( Func { side = R; _ },
        (Tuple { side = L; _ } | Record { side = L; _ } | Sum { side = L; _ }) )
      ->
      assert (not Options.strict_errors);
      Bot

  (* XXX /!\ check everything for soundness *)

  (** Application of correlations. *)
  let rec apply c1 c2 =
    match (c1, c2) with
    | Bot, _ | _, Bot -> bot
    | Func { side = R; arg; inner; outer }, c ->
      if leq c arg then inter (compose c inner) outer else top
    | Tuple { side = L as side; args; len }, c ->
      let args =
        (* len = 0 should not happen *)
        int_map_init
          (fun index ->
            apply
              (IntMap.dfind Top index args)
              (proj L (ProjTuple { index; len }) c))
          0
          len
      in
      tuple ~side ~args ~len
    | Record { side = L as side; fields; arity }, c ->
      let fields =
        (* empty arity should not happen *)
        FieldMap.mapi
          (fun field () ->
            apply
              (FieldMap.dfind Top field fields)
              (proj L (ProjRecord { field; arity }) c))
          arity
      in
      record ~side ~fields ~arity
    | Sum { side = L as side; cases; arity }, c ->
      let cases =
        (* XXX: issue when arity of some constructor is 0 *)
        TagMap.mapi
          (fun tag -> function
            | Args args ->
              Args
                (IntMap.mapi
                   (fun index c' ->
                     apply c' (proj L (ProjSum { tag; arity; index }) c))
                   args)
            | NoArgs c_tag ->
              NoArgs (apply c_tag (proj L (ProjTag { tag; arity }) c)))
          cases
      in
      sum ~side ~cases ~arity
    | (Tuple { side = R; _ } | Record { side = R; _ } | Sum { side = R; _ }), _
      ->
      assert (not Options.strict_errors);
      Bot
    | c, Tuple { side = L as side; args; len } ->
      let args =
        (* len = 0 should not happen *)
        int_map_init
          (fun index ->
            apply
              (proj L (ProjTuple { index; len }) c)
              (IntMap.dfind Top index args))
          0
          len
      in
      tuple ~side ~args ~len
    | c, Record { side = L as side; fields; arity } ->
      let fields =
        (* empty arity should not happen *)
        FieldMap.mapi
          (fun field () ->
            apply
              (proj L (ProjRecord { field; arity }) c)
              (FieldMap.dfind Top field fields))
          arity
      in
      record ~side ~fields ~arity
    | c, Sum { side = L as side; cases; arity } ->
      let cases =
        (* XXX: issue when arity of some constructor is 0 *)
        TagMap.mapi
          (fun tag -> function
            | Args args ->
              Args
                (IntMap.mapi
                   (fun index c' ->
                     apply (proj L (ProjSum { tag; arity; index }) c) c')
                   args)
            | NoArgs c_tag ->
              NoArgs (apply (proj L (ProjTag { tag; arity }) c) c_tag))
          cases
      in
      sum ~side ~cases ~arity
    | (Eq | Top), _ -> Top
    | Func { side = L; _ }, _ ->
      (* XXX: can we do better here? *)
      top

  (** Pretty-printer *)
  let rec pp fmt =
    let open Format in
    function
    | Bot -> fprintf fmt "⊥"
    | Top -> fprintf fmt "⊤"
    | Eq -> fprintf fmt "Eq"
    | Tuple { side; args; len = _ } ->
      fprintf
        fmt
        "(@[%a@]@,)"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (i, arg) ->
             fprintf fmt "@[<hv 2>%i %a@ %a@]" i pp_side side pp arg))
        (IntMap.bindings args)
    | Sum { side; cases; arity = _ } ->
      fprintf
        fmt
        "@[[%a@]@,]"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt "@,|")
           (fun fmt (tag, args) ->
             match args with
             | Args args ->
               fprintf
                 fmt
                 "@[<hv 1>%a %a@ (@[%a@]@,)@]"
                 Tag.pp
                 tag
                 pp_side
                 side
                 (pp_print_list
                    ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
                    (fun fmt (i, arg) ->
                      fprintf fmt "@[<hv 2>%i:@ %a@]" i pp arg))
                 (IntMap.bindings args)
             | NoArgs c ->
               fprintf
                 fmt
                 "@[<hv 1>%a %a@ @[%a@]@]"
                 Tag.pp
                 tag
                 pp_side
                 side
                 pp
                 c))
        (TagMap.bindings cases)
    | Record { side; fields; arity = _ } ->
      fprintf
        fmt
        "{@[%a@]@,}"
        (pp_print_list
           ~pp_sep:(fun fmt () -> fprintf fmt ";@ ")
           (fun fmt (field, arg) ->
             fprintf
               fmt
               "@[<hv 2>%a %a@ %a@]"
               Field.pp
               field
               pp_side
               side
               pp
               arg))
        (FieldMap.bindings fields)
    | Func { side; arg; inner; outer } ->
      fprintf
        fmt
        "Fun%a(@[<hv 0>@[<hv 2>arg:@ %a,@]@ @[<hv 2>inner:@ %a,@]@ @[<hv \
         2>outer:@ %a@]@])"
        pp_side
        side
        pp
        arg
        pp
        inner
        pp
        outer
end
